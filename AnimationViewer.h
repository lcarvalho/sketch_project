#ifndef ANIMATION_VIEWER_H
#define ANIMATION_VIEWER_H

#include "Animation.h"
#include <string>
#include <stack>

/// Object to view an Animation one frame at a time.
class AnimationViewer{
	public:
		/// Constructor.
		AnimationViewer(Animation& animation);

		/// Get the animation bounded to this viewer.
		Animation& get_animation();

		/// Get the center point.
		const Point& get_center_point() const;
		
		/// Update the center point from current drawing.
		void update_center_point();

		/// Check if current frame is a keyframe.
		bool is_current_keyframe() const;

		/// Changes the current frame to the next frame.
		bool next_frame();

		/// Get the index of the current frame.
		size_t current_frame_index() const;

		/// Set the current frame.
		void set_current_frame(size_t i);

		/// Number of frames.
		size_t n_frames() const;

		/// Number of layers
		size_t n_layers() const;

		/// Get base layer.
		Node<Layer>& get_base_layer() const;

		/// Get layer l.
		Node<Layer>& get_layer(size_t l) const;

		/// Get the current layer.
		Node<Layer>& current_layer();

		/// Set the current layer as the base layer.
		void set_base_layer();

		/// Set current layer as the animation's root layer.
		void goto_root_layer();

		/// Set the base layer as the parent of the current base layer.
		void goto_parent_base_layer();

		/// Get the index of the current layer.
		size_t current_layer_index() const;

		/// Set the current frame.
		void set_current_layer(size_t i);

		/// Set the current layer as the next layer.
		void next_layer();

		/// Set the current layer as the previous layer.
		void prev_layer();

		/// Get the Drawing from frame f, and layer l.
		Drawing* get_drawing(size_t f, size_t l) const;

		/// Get the Drawing from frame f, and layer l.
		Drawing* get_drawing(size_t f, size_t l, bool create);

		/// Get the Drawing from current frame and current layer.
		Drawing* get_drawing(bool create);

		/// Get a list with all visible drawable objects at frame f.
		std::vector<const Drawing*> get_visible_drawings(
			const Node<Layer>& layer, 
			size_t f,
			const Node<Layer>* except=nullptr
		);

		/// Add a new layer over current layer.
		void add_layer(const std::string& name);

		/// Remove current layer (if it is not the last one).
		void remove_layer();

		/// Move the current layer up.
		void layer_up();

		/// Move the current layer down.
		void layer_down();
		
		/// Move the current drawing up.
		void up_depth();

		/// Move the current drawing down.
		void down_depth();

		/// Update auxiliary curves around current keyframe.
		void update_auxiliary_curves();

		/// Update auxiliary curves from the last keyframe to the current keyframe.
		/** 
		* Or from the current to keyframe to the next.
		*/
		void update_auxiliary_curves(bool from_prev);

		/// Remove empty curves.
		void clear_empty_curves();

		/// Read drawings from svg file.
		void read_svg(const std::string& filename);

		/// Save the current drawing into svg file.
		void save_svg(const std::string& filename);

	private:
		/// Animation bound to this viewer.
		Animation& animation;

		/// Base layer.
		Node<Layer>* base_layer = nullptr;

		/// Index of the current (active) frame.
		size_t _current_frame_index = 0;

		/// Index of the current (active) layer.
		size_t _current_layer_index = 0;

		/// Current center point.
		Point center_point;

		/// Stack of base layers.
		std::stack<Node<Layer>*> layer_stack;
};

/// Constructor.
inline AnimationViewer::AnimationViewer(Animation& _animation):
	animation(_animation),
	base_layer(&animation.get_root_layer())
{}

/// Get the animation bounded to this viewer.
inline Animation& AnimationViewer::get_animation(){
	return animation;
}
		
/// Get the center point.
inline const Point& AnimationViewer::get_center_point() const{
	return center_point;
}

/// Check if current frame is a keyframe.
inline bool AnimationViewer::is_current_keyframe() const{
	return animation.is_keyframe(_current_frame_index);
}

/// Changes the current frame to the next frame.
inline bool AnimationViewer::next_frame(){
	++_current_frame_index;
	if(_current_frame_index == animation.n_frames()){
		--_current_frame_index;
		return false;
	}

	//update_center_point();

	return true;
}

/// Get the index of the current frame.
inline size_t AnimationViewer::current_frame_index() const{
	return _current_frame_index;
}

/// Set the current frame.
inline void AnimationViewer::set_current_frame(size_t i){
	_current_frame_index = i;
	//update_center_point();
}

/// Number of frames.
inline size_t AnimationViewer::n_frames() const{
	return animation.n_frames();
}

/// Number of layers
inline size_t AnimationViewer::n_layers() const{
	return base_layer->n_children()+1;
}

/// Get base layer.
inline Node<Layer>& AnimationViewer::get_base_layer() const{
	return *base_layer;
}

/// Get layer l.
inline Node<Layer>& AnimationViewer::get_layer(size_t l) const{
	return (l == 0)? *base_layer: base_layer->child(l-1);
}

/// Get the current layer.
inline Node<Layer>& AnimationViewer::current_layer(){
	return get_layer(_current_layer_index);
}

/// Get the index of the current layer.
inline size_t AnimationViewer::current_layer_index() const{
	return _current_layer_index;
}

/// Set the current frame.
inline void AnimationViewer::set_current_layer(size_t i){
	_current_layer_index = i;
	//update_center_point();
}
		
/// Make base layer as the current layer.
inline void AnimationViewer::goto_root_layer(){
	_current_layer_index = 0;
	base_layer = &animation.get_root_layer();
	//update_center_point();
}

/// Set the current layer as the next layer.
inline void AnimationViewer::next_layer(){
	size_t nc = n_layers();
	if(nc == 1)
		return;

	//animation.select_none();
	++_current_layer_index;
	if(_current_layer_index == nc)
		_current_layer_index = 0;
	//update_center_point();
}

/// Set the current layer as the previous layer.
inline void AnimationViewer::prev_layer(){
	size_t nc = n_layers();
	if(nc == 1)
		return;

	//animation.select_none();
	if(_current_layer_index == 0)
		_current_layer_index = nc-1;
	else
		--_current_layer_index;
	//update_center_point();
}


/// Get the Drawing from frame f, and layer l.
inline Drawing* AnimationViewer::get_drawing(size_t f, size_t l) const{
	if(animation.is_cyclic() and f == animation.last_keyframe_index())
		f = animation.first_keyframe_index();

	return get_layer(l).get_drawing(f);
}

/// Get the Drawing from frame f, and layer l.
inline Drawing* AnimationViewer::get_drawing(size_t f, size_t l, bool create){
	if(animation.is_cyclic() and f == animation.last_keyframe_index())
		f = animation.first_keyframe_index();

	return get_layer(l).get_drawing(f);
}

/// Get the Drawing from current frame and current layer.
inline Drawing* AnimationViewer::get_drawing(bool create){
	return get_drawing(_current_frame_index, _current_layer_index, create);
}

/// Update auxiliary curves around current keyframe.
inline void AnimationViewer::update_auxiliary_curves(){
	update_auxiliary_curves(false);
	update_auxiliary_curves(true);
}
#endif
