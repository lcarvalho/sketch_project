#ifndef GUIDELINE_OPERATIONS_H
#define GUIDELINE_OPERATIONS_H

#include "bipartite_matching.h"
#include "BezierPath.h"

BezierPathList normalized_points(const BezierPathList& paths);

double get_scale(const BezierPathList& paths);

void normalize_scale(BezierPathList& paths0, const BezierPathList& paths1);

void rotate_45degrees(BezierPathList& paths);

bipartite_matching::Edges best_match(
	const BezierPathList& paths0,
	const BezierPathList& paths1
);

using CostInfo = std::tuple<double, std::vector<int>, std::vector<bool>>;

std::tuple<double, std::vector<int>, std::vector<bool>> get_cost(
	const bipartite_matching::Edges& bimatch,
	const BezierPathList& paths0,
	const BezierPathList& paths1
);

#endif
