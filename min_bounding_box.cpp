#include "loc.h"
#include "Point.h"
#include <limits>

using std::vector;

/// Vector that can be rotated, and is associated with the i-th vertex of a polygon.
struct Caliper{
	/// Direction.
	Point d;

	/// Index of associated point in convex hull.
	int i;

	/// Rotate the caliper.
	void rotate(double angle);
};

/// Rotate the caliper.
inline void Caliper::rotate(double angle){
	double cosa = cos(angle);
	double sina = sin(angle);

	double x = d[0];
	double y = d[1];

	d[0] = cosa*x + sina*y;
	d[1] = -sina*x + cosa*y;
}

/// Intersection point of two calipers.
Point intersection(const Caliper& cal0, const Caliper& cal1, const PointList& pts){
	Point dif = pts[cal1.i] - pts[cal0.i];

	double a = cal0.d[0];
	double b = -cal1.d[0];
	double c = cal0.d[1];
	double d = -cal1.d[1];
	double e = dif[0];
	double f = dif[1];

	double t0 = (e*d - b*f)/(a*d - b*c);
	return pts[cal0.i] + t0*cal0.d;
}

constexpr double inf = loc::infinity<double>();

/// Rotate calipers by the minimum angle.
double rotate_calipers(vector<Caliper>& cals, const PointList& pts){
	const int s = pts.size();

	auto get_angle = [&](const Caliper& cal){
		int i0 = cal.i;
		int i1 = (i0+1)%s;
		Point v = pts[i1] - pts[i0];
		return acos((v*cal.d)/v.norm());
	};

	auto min_angle_element = loc::min_val_element(cals, get_angle);

	double min_angle = get_angle(*min_angle_element);

	for(Caliper& cal: cals)
		cal.rotate(min_angle);

	min_angle_element->i = (min_angle_element->i+1)%s;

	return min_angle;
}

/// Minimum bounding box of a set of points.
Point4 min_bounding_box(const PointList& pts){
	Point4 min_bb;

	if(pts.empty())
		return min_bb;

	PointList hull = convex_hull(pts);

	auto hb = hull.begin();

	auto x_value = [](const Point& p) { return p[0]; };
	auto y_value = [](const Point& p) { return p[1]; };

	auto minmax_x = loc::minmax_val_element(hull, x_value);
	auto minmax_y = loc::minmax_val_element(hull, y_value);

	vector<Caliper> c = {
		{Point{0,  1}, int(minmax_x.first - hb)},
		{Point{1,  0}, int(minmax_y.second - hb)},
		{Point{0, -1}, int(minmax_x.second - hb)},
		{Point{-1, 0}, int(minmax_y.first - hb)}
	};

	constexpr double full_circ = 2*3.14159265359;

	double min_area = inf;
	double total_rotation = 0.0;

	while(total_rotation < full_circ){
		total_rotation += rotate_calipers(c, hull);

		Point4 bb;
		bb[0] = intersection(c[0], c[1], hull);
		bb[1] = intersection(c[1], c[2], hull);
		bb[2] = intersection(c[2], c[3], hull);
		bb[3] = intersection(c[3], c[0], hull);

		double area_bb = dist(bb[1], bb[0])*dist(bb[1], bb[2]);

		if(area_bb < min_area){
			min_bb = move(bb);
			min_area = area_bb;
		}
	}

	return min_bb;
}
