#ifndef ANIMATION_EDITOR_H
#define ANIMATION_EDITOR_H

#include <QtWidgets>
#include "AnimationViewer.h"
#include "FrameEditor.h"
#include "TimelineWidget.h"
#include "LayerWidget.h"
#include "ColorWidget.h"
#include "Selection.h"
#include "SpeedWidget.h"

/// Qt window to control an animation.
class AnimationEditor : public QMainWindow{
	Q_OBJECT

	public:
		/// Constructor.
		AnimationEditor();

		/// Animation controlled by this editor.
		Animation& get_animation();

		/// Animation viewer used by this editor.
		AnimationViewer& get_viewer();

		/// Selection control used by this editor.
		Selection& get_selection();

		/// Repaint the window.
		void update_window();

		/// Set the current layer as the base layer.
		void set_base_layer();

		/// Set the base layer as the parent of the current base layer.
		void goto_parent_base_layer();

		/// Insert new frame at current position.
		void insert_frame();
		
		/// Remove frame at current position.
		void remove_frame();

		/// Check if speed control is opened.
		bool speed_control_open() const;

		/// Show/hide speed control.
		void toggle_speed_control();

		/// Action done when tablet pen enters.
		void tablet_enter(QTabletEvent* e);
	private:
		/// Create the application menu.
		void create_menu();

		/// Create the left side part of the window.
		QWidget* make_left();

		/// Create the right side part of the window.
		QWidget* make_right();

		/// Create the toolbar.
		QWidget* make_toolbar();

		/// Create the layer editor.
		QWidget* make_layer_editor();

		/// Create the animation controls.
		QWidget* make_animation_controls();

		/// Create the spinbox to control frames.
		QWidget* make_frame_spinbox();

		/// Create the slider that controls tolerance.
		QWidget* make_slider_tolerance();

		/// Create the checkbox to control cyclic animation.
		QWidget* make_cyclic_control();

		/// Create the button to generate animation.
		QWidget* make_button_auto_guides();

		/// Create the button to play/stop the animation.
		QWidget* make_button_play_animation();

		/// Create the color tool.
		QWidget* make_color_tool();

		/// Create the slider that controls smoothness.
		QWidget* make_slider_smoothness();

		/// Create the frame editor.
		QWidget* make_editor();

		/// Create the timeline.
		QWidget* make_timeline();
		
		/// Create a tool to the toolbar.
		QToolButton* make_tool(
			const char* icon,
			const char* tooltip,
			std::function<void()> f
		)const;

		/// Create the pencil tool.
		QWidget* make_pencil_tool() const;

		/// Create the eraser tool.
		QWidget* make_eraser_tool();

		/// Create the sketch pencil tool.
		QWidget* make_sketch_pencil_tool() const;

		/// Create the sketch eraser tool.
		QWidget* make_sketch_eraser_tool() const;

		/// Create the curve selector tool.
		QWidget* make_curve_selector_tool() const;

		/// Create the region selector tool.
		QWidget* make_region_selector_tool() const;

		/// Create the center point edition tool.
		QWidget* make_center_tool() const;

		/// Create the control point edition tool.
		QWidget* make_cp_editor_tool() const;

		/// Read frames from a file.
		void read_frames();

		/// Update the current file.
		void save_current_file();

		/// Write frames to a file.
		void save_frames();

		/// Read drawing from svg file.
		void read_svg();

		/// Save current drawing to svg file.
		void save_svg();

		/// Play the animation.
		void play_animation();

		/// Read background image.
		void read_bg_image();

		////////////////////////////////////////////////////////////////
		// Methods that change editor state

		/// Create a new layer.
		void new_layer();

		/// Remove current layer.
		void delete_layer();

		////////////////////////////////////////////////////////////////
		// Methods that change editor flags

		/// Create a region from the selected curves.
		void make_region_from_selection();

		/// Add forbidden pairs of curves.
		void add_forbidden_pairs_from_selection();

	protected:
		////////////////////////////////////////////////////////////////
		// Events

		/// Keyboard event.
		void keyPressEvent(QKeyEvent* k);

		/// Close window event.
		void closeEvent(QCloseEvent* event);

		////////////////////////////////////////////////////////////////
		// Parameters.

		/// Animation controled by this editor.
		Animation animation;

		/// Animation viewer.
		AnimationViewer viewer;

		/// Selection/clipboard controller.
		Selection selection;

		/// Frame editor.
		FrameEditor* editor;

		/// Timeline editor.
		TimelineWidget* timeline;

		/// Speed control widget.
		SpeedWidget speed_widget;

		/// Layers editor.
		LayerWidget* layer_editor;

		/// Color editor.
		ColorWidget* color_tool;

		/// Spinbox to keep current frame index.
		QSpinBox* frame_spinbox;

		/// Slider to control curve distance tolerance.
		QSlider* slider_tolerance;

		/// Checkbox to control cyclic animation.
		QCheckBox* cyclic_checkbox;

		/// Flag to specify when the animation is playing.
		bool playing;

		/// Timer to control frame duration.
		QTimer timer;

		/// Path with models.
		QString models_path;

		/// Current file.
		QString current_filename;
};

////////////////////////////////////////////////////////////////////////////
// Implementation of inline methods.

/// Animation controlled by this editor.
inline Animation& AnimationEditor::get_animation(){
	return animation;
}

/// Animation viewer used by this editor.
inline AnimationViewer& AnimationEditor::get_viewer(){
	return viewer;
}

/// Selection control used by this editor.
inline Selection& AnimationEditor::get_selection(){
	return selection;
}

/// Repaint the window.
inline void AnimationEditor::update_window(){
	editor->repaint();
	timeline->repaint();
	layer_editor->repaint();
	frame_spinbox->setValue(viewer.current_frame_index()+1);
	speed_widget.update_curve();
}

/// Check if speed control is opened.
inline bool AnimationEditor::speed_control_open() const{
	return speed_widget.isVisible();
}

/// Show/hide speed control.
inline void AnimationEditor::toggle_speed_control(){
	speed_widget.setVisible(!speed_widget.isVisible());
}


/////////////////////////////////////////////////////
// Methods that change editor state

inline void AnimationEditor::set_base_layer(){
	viewer.set_base_layer();
	layer_editor->update_data();
	update_window();
}

/// Set the base layer as the parent of the current base layer.
inline void AnimationEditor::goto_parent_base_layer(){
	viewer.goto_parent_base_layer();
	layer_editor->update_data();
	update_window();
}
		
#endif
