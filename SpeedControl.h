#ifndef SPEED_CONTROL_H
#define SPEED_CONTROL_H

#include <exception>
#include "loc.h" 
#include "CubicBezier.h"
#include "json11.hpp"
#include "polynomial_roots.h"

/// Speed controller between keyframes.
class SpeedControl{
	public:

	/// Constructor.
	SpeedControl();

	json11::Json to_json() const{
		return json11::Json::object{
			{"p1", bezier[1]},
			{"p2", bezier[2]}
		};
	}

	/// Value associated with parameter t.
	std::function<double(double)> function() const{
		return [&](double x){
			if(x < 0.0 or x > 1.0)
				throw std::runtime_error{"Invalid value"};
			auto p = bezier.point_at(parameter_at_x(x));
			return p[1];
		};
	}

	/// Get the control point number 1.
	Point& get_p1(){
		return bezier[1];
	}
	
	/// Get the control point number 2.
	Point& get_p2(){
		return bezier[2];
	}

	private:

	/// Find the parameter t of the bezier curve with specified x coordinate.
	double parameter_at_x(double x) const;

	/// Control curve.
	CubicBezier bezier;
};
	
// Implementation of inline methods.

/// Constructor
inline SpeedControl::SpeedControl() 
	:bezier{{0.0, 0.0}, {.33, .33}, {.66, .66}, {1.0, 1.0}}
{}
	

/// Find the parameter t of the bezier curve with specified x coordinate.
inline double SpeedControl::parameter_at_x(double x) const{
	const double& x0 = bezier[0][0];
	const double& x1 = bezier[1][0];
	const double& x2 = bezier[2][0];
	const double& x3 = bezier[3][0];
	const double a = -x0 + 3*x1 - 3*x2 + x3;
	const double b = 3*x0 - 6*x1 + 3*x2;
	const double c = -3*x0 + 3*x1;
	const double d = x0 - x;
	auto r = real_roots(a, b, c, d);
	if(r.empty()){
		std::cout << "empty?\n" << std::endl;
		return 0.0;
	}
	loc::partition(r, [](double v){ return v>=0.0 and v<=1.0; });
	return r.front();
}

#endif
