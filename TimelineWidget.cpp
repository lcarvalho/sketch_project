#include "TimelineWidget.h"
#include <iostream>
#include "AnimationEditor.h"

using std::function;

/////////////////////////////////////////////////////////////
/// Constructor.
TimelineWidget::TimelineWidget(AnimationEditor& ed):
	QWidget{&ed}, editor(ed)
{
	update_size();
	menu = new QMenu(this);
	setup_menu();
}

/// Update the size.
void TimelineWidget::update_size(){
	auto& viewer = editor.get_viewer();
	n_frames = viewer.n_frames();
	int w = n_frames*(frame_width + frame_sep) + 1;
	int h = (frame_height+1) + 1;
	setMinimumSize(w, h);
}
		
/// Create the menu items.
void TimelineWidget::setup_menu(){
	auto add_item = [&](
			const char* str,
			function<void()> method
		){

			auto action = menu->addAction(tr(str));
			connect(action, &QAction::triggered, method);

			this->addAction(action);

			return action;
		};

	set_keyframe_action = add_item(
		"Make keyframe", 
		[&]{ 
			auto& viewer = editor.get_viewer();
			size_t i = viewer.current_frame_index();
			editor.get_animation().set_keyframe(i);
			viewer.update_auxiliary_curves();
			editor.update_window();
			repaint();
		}
	);
	
	add_item(
		"Insert frame before", 
		[&]{ 
			editor.insert_frame();
			update_size();
			repaint();
		}
	);
	
	add_item(
		"Remove frame", 
		[&]{ 
			editor.remove_frame();
			update_size();
			repaint();
		}
	);
	
	toggle_acc_action = add_item(
		"Activate acceleration control", 
		[&]{ 
			editor.toggle_speed_control();
		}
	);
}

////////////////////////////////////////////////////////////////////////////////
// Events.

/// Paint event.
void TimelineWidget::paintEvent(QPaintEvent* event){
	QPainter painter;
	painter.begin(this);

	painter.setRenderHint(QPainter::Antialiasing, true);

	int w = n_frames*(frame_width + frame_sep) + 1;
	int h = (frame_height+1) + 1;
	painter.fillRect(0, 0, w, h, QColor{50, 50, 50});

	auto& animation = editor.get_animation();
	auto& viewer = editor.get_viewer();

	// current keyframe.
	auto cf = viewer.current_frame_index();

	// Highlight and dark colors.
	static QColor dark = QPalette().dark().color();
	static QColor highlight = QPalette().highlight().color();

	////////////////////////////////////////////////////////////////////////
	// Draw frames.
	QRect fr{1, 1, frame_width, frame_height};
	int x = fr.left();

	/// Keyframe marker parameters.
	int xc = x+frame_width/2;
	int yc = frame_height/4;
	int r = frame_width/3;

	for(size_t i = 0; i < n_frames; ++i){
		painter.fillRect(
			fr,
			(i == cf)? 
				highlight:
			animation.inside_range(i)?
				Qt::white:
				dark
		);

		if(animation.is_keyframe(i)){
			// Draws a circle centered at current frame.
			QPen pen{Qt::blue, 1};
			painter.setPen(pen);
			painter.drawEllipse(QPoint{xc, yc}, r, r);
		}

		x += frame_width + 1;
		fr.moveLeft(x);
		xc += frame_width + 1;
	}

	painter.end();

}

/// Mouse press event.
void TimelineWidget::mousePressEvent(QMouseEvent* event){
	auto& animation = editor.get_animation();
	auto& viewer = editor.get_viewer();
	auto& selection = editor.get_selection();

	QPointF ep = event->pos();

	if(ep.x() < frame_sep)
		return;

	size_t i = floor((ep.x() - frame_sep)/(frame_sep+frame_width));

	if(i >= viewer.n_frames())
		return;
			
	auto select_frame = [&]{
		if(i == viewer.current_frame_index())
			return;
		viewer.set_current_frame(i);
		editor.update_window();
		selection.select_none();
		repaint();
	};

	switch(event->button()){
		case Qt::LeftButton:
			select_frame();
			break;

		case Qt::MiddleButton:
			select_frame();
			animation.set_keyframe(i);
			viewer.update_auxiliary_curves();
			editor.update_window();
			repaint();
			break;
		
		case Qt::RightButton:
			select_frame();
			set_keyframe_action->setText(
				viewer.is_current_keyframe()? 
				"Unset keyframe":
				"Set keyframe"
			);
			toggle_acc_action->setText(
				editor.speed_control_open()? 
				"Close speed control":
				"Open speed control"
			);
			menu->exec(QCursor::pos());
			break;

		default:
			break;
	}
}

/// Mouse move event.
void TimelineWidget::mouseMoveEvent(QMouseEvent* event){
	QPointF ep = event->pos();

	auto& viewer = editor.get_viewer();

	auto i = floor((ep.x() - frame_sep)/(frame_sep+frame_width));
	if(i < 0)
		i = 0;
	else if(i >= viewer.n_frames())
		return;

	if(!(event->buttons() & Qt::LeftButton))
		return;

	if(i == viewer.current_frame_index())
		return;

	viewer.set_current_frame(i);
	editor.update_window();
	repaint();
}
