#ifndef POSTSCRIPTWRITER_H
#define POSTSCRIPTWRITER_H

#include <fstream>
#include <vector>
#include "BezierPath.h"
#include "CubicBezier.h"
#include "Region.h"

/// Object to write postscript files.
class PostScriptWriter{
	public:
		/// Constructor.
		PostScriptWriter(
			const std::string& filename,
			double x0,
			double y0,
			double w,
			double h
		);

		/// Constructor.
		PostScriptWriter(
			const std::string& filename,
			const Point& p0,
			const Point& d
		);

		/// Define the color.
		void set_color(double r, double g, double b);

		/// Apply translation.
		void translate(double dx, double dy);

		/// Draw a box.
		void box(const Point2& b);

		/// Write a command from a string.
		PostScriptWriter& operator<<(const std::string& s);

		/// Draw a point.
		PostScriptWriter& operator<<(const Point& p);

		/// Draw a cubic Bezier curve..
		PostScriptWriter& operator<<(const CubicBezier& c);

		/// Draw a path.
		PostScriptWriter& operator<<(const BezierPath& c);
		
		/// Draw a BezierBool.
		PostScriptWriter& operator<<(const BezierBool& bb);

		/// Draw a 2d region.
		PostScriptWriter& operator<<(const Region& c);

		/// Draw a drawable.
		PostScriptWriter& operator<<(const Drawable& c);

	private:
		/// Output stream.
		std::ofstream out;

		/// File closer.
		std::shared_ptr<void> closer;

		/// Corner x coordinate.
		double x0;

		/// Corner y coordinate.
		double y0;

		/// Width.
		double w;

		/// Height.
		double h;

		/// Current position.
		Point current;
};

template<class... PointContainer>
inline PostScriptWriter postscript(const std::string& filename, const PointContainer&... c){
	auto bb = axis_aligned_bounding_box(c...);
	double xmin = bb[0][0];
	double ymin = bb[0][1];
	double xmax = bb[1][0];
	double ymax = bb[1][1];
	double w = xmax - xmin;
	double h = ymax - ymin;
	double dx = 0.05*w;
	double dy = 0.05*h;
	xmin -= dx;
	ymin -= dy;
	w += 2*dx;
	h += 2*dy;

	return {filename, xmin, ymin, w, h};
}
		
#endif
