#include "FittingCurve.h"
#include "curven.hpp"

#include <chrono>
#include <cmath>

using std::swap;

/// Constructor.
FittingCurve::FittingCurve() : w_dist(600), dist(w_dist*w_dist){
	double c = w_dist/2;
	center = {c, c};

	imin = 0;
	imax = w_dist-1;

	jmin = 0;
	jmax = w_dist-1;

	max_error = 50;
}

/// Insert a new input point.
void FittingCurve::insert_point(const Point& p){
	using namespace std::chrono;
	static system_clock::time_point last_time;
	auto this_time = system_clock::now();
	if(
		(this_time - last_time) < milliseconds(20) or
		(
		 !input.empty() and dist2(p, input.back()) <= 5
		)
	)
		return;
	last_time = this_time;

	input.push_back(p);

	if(
		input.size() > 1 and 
		(
		 p[0] < imin or  p[0] > imax or
		 p[1] < jmin or  p[1] > jmax
		)
	){
		Point p0 = input[input.size() - 2];

		init_distance_field(p0);
		if(p[0] >= imin and p[0] <= imax and p[1] >= jmin and p[1] <= jmax){
			stroke.add(p0);
			stroke.add(p0);
			stroke.add(p0);

			input.push_back(p);
			update_distance_field();

			reqG1 = true;
			adjust_control_points();
		}else{
			// Point p is too far from the last point.
			stroke.add(.6666*p0 + .3333*p);
			stroke.add(.3333*p0 + .6666*p);
			stroke.add(p);
			init_distance_field(p, false);
			stroke.add(p);
			stroke.add(p);
			stroke.add(p);
		}

	}else if(stroke.empty()){
		init_cubic_curve();
		reqG1 = false;

	}else{
		// Test for a corner.
		test_corner();

		// Update vector distance field.
		update_distance_field();

		// Adjust control points to reduce
		//   distance between curve and polyline.
		// Create new curve segment if necessary.
		adjust_control_points();
	}
}

/// Init the data of the cubic bézier.
void FittingCurve::init_cubic_curve(){
	const Point& p = input.back();
	stroke.add(p);
	stroke.add(p);
	stroke.add(p);
	stroke.add(p);

	init_distance_field(p);
}

/// Test if a corner is formed with the new point.
void FittingCurve::test_corner(){
	const Point& p = input[input.size()-1];
	const auto& points = stroke.control_points();
	const Point& p0 = points[points.size()-1];
	const Point& p1 = points[points.size()-2];

	if(dist2(p0, p1) < 0.01)
		return;

	Point v0 = p0 - p1;
	Point v1 = p  - p0;

	v0.unitize();
	v1.unitize();

	double cangle = v0*v1;
	if(cangle < 0.2){
		reqG1 = false;

		stroke.add(0.75*p0 + 0.25*p);
		stroke.add(0.25*p0 + 0.75*p);
		stroke.add(p);

		init_distance_field(p0);
		input.push_back(p);
		update_distance_field();
	}
}

/// Initialize the vector distance field centered in p.
void FittingCurve::init_distance_field(const Point& p, bool update_matrix){
	input.clear();
	input.push_back(p);

	imin = p[0] - center[0];
	imax = imin + w_dist - 1;

	jmin = p[1] - center[1];
	jmax = jmin + w_dist - 1;

	#pragma omp parallel
	for(int i = 0; i < w_dist; ++i)
		for(int j = 0; j < w_dist; ++j)
			distance(i,j) = center - Point{double(i), double(j)};
}

Point min_direction(const Point& p0, const Point& p1, const Point& p){
	double s = project(p0, p1, p);
	return (s > 0 and s < 1)?
		p0 + s*(p1 - p0) - p:
		(s <= 0)?
		p0 - p:
		p1 - p;
}

/// Update the vector distance field to include the new point.
void FittingCurve::update_distance_field(){
	int is = input.size();
	if(is < 2)
		return;

	const Point& p1 = input[is-1];
	const Point& p0 = input[is-2];

	#pragma omp parallel
	for(int i = 0; i < w_dist; ++i){
		for(int j = 0; j < w_dist; ++j){
			// Real position.
			Point real_pos = point_at(i, j);

			Point& min_d = distance(i, j);

			Point new_d = min_direction(p0, p1, real_pos);

			if(new_d.norm2() < min_d.norm2())
				min_d = new_d;
		}
	}
}

/// Adjust point c1.
Point FittingCurve::adjust_c1(const Point& c1, Point f1){
	if(!reqG1)
		return adjusted_point(c1, 0.2*f1);

	auto&& cx = stroke.get_cubic(-2);
	const Point& pa = cx[3];
	const Point& pb = cx[2];
	Point v = pa - pb;

	f1 = (f1*v)/(v*v)*v;

	Point nc1 = adjusted_point(c1, 0.2*f1);

	return (dist2(nc1, pb) > dist2(pa, pb))? nc1: pa;
}

/// Calculate the value of f1 and f2.
Point2 FittingCurve::calculate_fs(double& e){
	Point f1, f2;
	int N = 20;
	e = 0.0;
	for(int i = 0; i < N; ++i){
		double ti = i/(N-1.0);
		Point d = distance(stroke.last_cubic_at(ti));
		double l = d.norm();
		e += l*l;
		f1 += ti*(1-ti)*(1-ti)*l*d;
		f2 += ti*ti*(1-ti)*l*d;
	}

	return {6.0/N*f1, 6.0/N*f2};
}

Point2 FittingCurve::eval_input(double t0, double t1) const{
	PolygonalCurve<double, 2> input_polygonal;
	for(Point p: input)
		input_polygonal.add({p[0], p[1]});
	auto _c1 = input_polygonal.eval(t0);
	auto _c2 = input_polygonal.eval(t1);

	return {Point{_c1[0], _c1[1]}, Point{_c2[0], _c2[1]}};
}

/// Adjust control points to fit the polygonal input curve.
void FittingCurve::adjust_control_points(){
	auto c = stroke.last_cubic();

	auto old_c = c.copy();
	try{

		auto eval = eval_input(0.4, 0.6);
		c[1] = eval[0];
		c[2] = eval[1];
		c[3] = input.back();


		if(reqG1){
			auto&& cx = stroke.get_cubic(-2);
			Point vx = cx[3] - cx[2];
			Point v1 = c[1] - c[0];
			double a = v1*vx;
			double b = vx*vx;
			if(fabs(a) > 1e-8 and fabs(b) > 1e-8)
				c[1] = adjusted_point(c[0], (a/b)*vx);
		}

		// Number of iterations.
		int K = 400;

		double error;

		for(int k = 0; k < K; ++k){
			auto fs = calculate_fs(error);
			c[1] = adjust_c1(c[1], fs[0]);
			c[2] = adjusted_point(c[2], 0.2*fs[1]);
		}


		double old_l = old_c.polygon_length();
		double l = c.polygon_length();

		if((l < old_l or error > max_error) and dist2(old_c[0], old_c[3]) > 0.0){
			c[0] = old_c[0];
			c[1] = old_c[1];
			c[2] = old_c[2];
			c[3] = old_c[3];

			init_distance_field(c[3]);
			update_distance_field();
			stroke.add(c[3]);
			stroke.add(c[3]);
			stroke.add(c[3]);

			reqG1 = true;
			adjust_control_points();
		}
	}catch(...){
		c[0] = old_c[0];
		c[1] = old_c[1];
		c[2] = old_c[2];
		c[3] = old_c[3];
		input.pop_back();
	}
}

/// Get a point in the line p+v0 inside the distance field.
Point FittingCurve::adjusted_point(const Point& p, const Point& v0){
	Point v = v0;
	Point c = p + v0;

	if(c[0] < imin){
		v = (imin - p[0])/v[0]*v;
		c = p + v;
	}

	if(c[0] > imax){
		v = (imax - p[0])/v[0]*v;
		c = p + v;
	}

	if(c[1] < jmin){
		v = (jmin - p[1])/v[1]*v;
		c = p + v;
	}

	if(c[1] > jmax){
		v = (jmax - p[1])/v[1]*v;
		c = p + v;
	}

	if(std::isnan(c[0]))
		throw std::runtime_error{"Not a Number in adjusted_point"};

	return c;
}

inline double interp_angle(double s, double a0, double a1){
	constexpr double pi = 3.14159265;

	if(a1 < a0){
		swap(a0, a1);
		s = 1-s;
	}

	if(a1 - a0 > pi)
		a0 += 2*pi;

	return (1-s)*a0 + s*a1;
}

