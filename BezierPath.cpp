#include "BezierPath.h"
#include <iostream>
#include <algorithm>
#include <valarray>
#include "PostScriptWriter.h"
#include "SimilarityMorph.h"

using std::swap;
using std::move;
using std::min;
using std::max;
using std::sqrt;
using std::floor;
using std::tie;
using std::cout;
using std::cerr;
using std::vector;
using std::pair;
using std::logic_error;
using std::ostream;
using std::istream;

BezierPath::BezierPath(const json11::Json& json) :
	Drawable{1}
{
	points.reserve(json.array_items().size());
	for(auto& p: json.array_items())
		points.emplace_back(p);

	update_proportions();
}

/// Get all the control points of a list of BezierPaths.
PointList all_control_points(const BezierPathList& paths){
	PointList pts;

	for(const auto& c: paths)
		loc::insert_back(pts, c->control_points());
	
	return pts;
}

/// Check if joint j is G1 continuous
/** where j is joint between cubics j and j+1. */
bool BezierPath::is_g1(size_t j) const{
	// Avoid extreme cases.
	if(j >= n_cubics()-1)
		return false;

	// Vertex index.
	size_t i = 3*j + 3;

	// Directions around vertex i.
	Point u = point(i-1) - point(i);
	double nu = u.norm();
	if(nu < 1e-5)
		return true;

	Point v = point(i+1) - point(i);
	double nv = v.norm();
	if(nv < 1e-5)
		return true;

	// Cosine of angle between u and v.
	double cosa = ((u*v)/(u.norm()*v.norm()));

	// Return true if angle is close to 180 degrees.
	return fabs(cosa + 1) < 1e-5;
}

/// Sample the curve with exactly N points. 
PointList BezierPath::uniform_sample(size_t N){
	sample_points.resize(N);
	if(N == 0)
		return sample_points;

	/// Make sure proportions are updated.
	update_proportions();

	double dt = 1.0/(N-1);
	for(auto i = 0u; i < N; ++i)
		sample_points[i] = point_at(i*dt);

	return sample_points;
}

/// Sample the curve with exactly N points (const version).
PointList BezierPath::uniform_sample(size_t N) const{
	PointList result(N);
	if(N == 0)
		return result;

	double dt = 1.0/(N-1);
	for(auto i = 0u; i < N; ++i)
		result[i] = point_at(i*dt);

	return result;
}

/// Sample with npc points per cubic.
PointList BezierPath::sample(size_t npc){
	return uniform_sample(npc*n_cubics());
}

/// Sample with npc points per cubic (const version).
PointList BezierPath::sample(size_t npc) const{
	return uniform_sample(npc*n_cubics());
}

/// Calculates the root of the equation ax + b = 0,
/// returns false if there isn't root.
inline bool root(double a, double b, double& x){
	return (a != 0.0)? (x=-b/a, true): false;
}

/// Calculates the root of the equation ax² + bx + c = 0,
/// returns false if there isn't root.
inline bool root(double a, double b, double c, double& x1, double& x2){
	if(a == 0)
		return root(b, c, x1)? (x2=x1, true): false;

	double delta = b*b - 4*a*c;
	if(delta < 0)
		return false;

	double sd = sqrt(delta);
	x1 = (-b - sd)/(2*a);
	x2 = (-b + sd)/(2*a);
	return true;
}

/// Checks if the point C is close to the segment P-Q.)
/// with distance less than r.
bool is_point_close_to_segment(
	const Point& C,
	double r,
	const Point& P,
	const Point& Q
){
	double xc = C[0];
	double yc = C[1];
	double xp = P[0];
	double yp = P[1];
	double xq = Q[0];
	double yq = Q[1];

	double dx = xp - xc;
	double dy = yp - yc;

	if(dx*dx + dy*dy <= r*r)
		return true;

	dx = xq - xc;
	dy = yq - yc;

	if(dx*dx + dy*dy <= r*r)
		return true;

	double a = xp*xc + yp*yc;
	double b = xq*xc + yq*yc;
	double c = xp*xq + yp*yq;
	double d = xp*xp + yp*yp;
	double e = xq*xq + yq*yq;
	double f = xc*xc + yc*yc - r*r;

	double t1, t2;
	if(root(d - 2*c + e, 2*(a - b + c - d), d - 2*a + f, t1, t2))
		return (t1 >= 0.0 and t1 <= 1.0) or (t2 >= 0.0 and t2 <= 1.0);

	return false;
}

/// Check if the curve crosses segment (xp,yp)-(xq, yq).
bool BezierPath::cross_segment(const Point& p, const Point& q) const{
	return ::cross_segment(sample_points, p, q);
}

/// Calculate the distance between control point of this curve with curve c.
double BezierPath::distance2(const BezierPath& c) const{
	const auto& pc1 = sample_points;
	const auto& pc2 = c.sample_points;

	size_t N = min(pc1.size(), pc2.size());

	double d = 0;
	for(size_t i = 0; i < N; ++i)
		d += dist2(pc1[i], pc2[i]);

	return d;
}

/// Reverse distance between curves.
/** 
* Calculate the distance between control point of
* this curve (in reverse order) with curve c.
*/
double BezierPath::reverse_distance2(const BezierPath& c) const{
	const auto& pc1 = sample_points;
	const auto& pc2 = c.sample_points;

	size_t N = min(pc1.size(), pc2.size());

	double d = 0;
	for(size_t i = 0; i < N; ++i)
		d += dist2(pc1[i], pc2[N-1-i]);

	return d;
}

/// Calculate curve lenght.
double BezierPath::lenght() const{
	double d = 0;
	for(size_t i = 1; i < size(); ++i)
		d += dist2(point(i), point(i-1));

	return d;
}

/// Check if the curve is close to the point (x,y).
bool BezierPath::is_close_to(double x, double y, double r)const{
	const auto& c = sample_points;
	Point p = {x, y};
	for(auto i = 1u; i < sample_points.size(); ++i)
		if(is_point_close_to_segment(p, r, c[i-1], c[i]))
			return true;

	return false;
}

/// Get the Hausdorff distance between two curves.
double hausdorff_distance(const BezierPath& c1, const BezierPath& c2){
	return hausdorff_distance(c1.sample(10), c2.sample(10));
}

/// Count the number of crossing lines.
int n_crossings(const PointList& pts1, const PointList& pts2, bool fw){
	size_t n = min(pts1.size(), pts2.size());
	vector<Point2> lines;
	for(size_t i = 0; i < n; ++i)
		lines.push_back({pts1[i], pts2[fw?i: n-1-i]});

	int count = 0;
	for(size_t i = 0; i < n; ++i)
		for(size_t j = i+1; j < n; ++j)
			if(crossing_segments(lines[i], lines[j]))
				++count;
	return count;
}

/// Decide if two sets best fit in forward direction.
bool is_forward(const PointList& pts1, const PointList& _pts2){
	PointList pts2 = _pts2;

	Point center1 = loc::average(pts1);
	Point center2 = loc::average(pts2);

	Point dif = center2 - center1;

	for(Point& p: pts2)
		p = p - dif;

	auto n = pts1.size();

	double dF = 0.0;
	for(auto i = 0u; i < n; ++i)
		dF += dist(pts1[i], pts2[i]);

	double dB = 0.0;
	for(auto i = 0u; i < n; ++i)
		dB += dist(pts1[i], pts2[n-i-1]);

	return dF < dB;
}
		
/// Get a uniform sample of two curves.
loc::pair_of<PointList> sample_curves(const BezierPath& c1, const BezierPath& c2, int nk=2){
	size_t n = max(c1.n_cubics(), c2.n_cubics())*nk;
	
	return {c1.uniform_sample(n), c2.uniform_sample(n)};
}

/// Calculate the distance between two sequences of points in order.
double sequence_distance(const PointList& pts1, const PointList& pts2, bool is_fw){
	double d = 0.0;
	
	size_t n = std::min(pts1.size(), pts2.size());

	if(is_fw)
		for(size_t i = 0; i < n; ++i)
			d += dist(pts1[i], pts2[i]);

	else
		for(size_t i = 0; i < n; ++i)
			d += dist(pts1[i], pts2[n-i-1]);

	return d/n;
}

/// Calculate a distance between two curves.
DistanceInfo distance_info(const BezierPath& c1, const BezierPath& c2){
	PointList pts1, pts2;
	tie(pts1, pts2) = sample_curves(c1, c2);

	bool is_fw = is_forward(pts1, pts2);

#if 1
	double d = hausdorff_distance(pts1, pts2);
#else
	double d = sequence_distance(pts1, pts2, is_fw);
#endif

	return {&c1, &c2, d, is_fw};
}

/// Check if the curve is next to point p.
bool BezierPath::is_next_to(const Point& p, double scale){
	sample(10);
	return is_close_to(p[0], p[1], scale*5);
}

/// Calculate a list with values related to proportions of each cubic.
/**
  Let li be the lenght of i-th curve in bbs,
  and L = sum(li),
  the return vector contains values:
  [0.0, l0/L, (l0+l1)/L, (l0+l1+l2)/L, ..., 1.0]
*/
vector<double> BezierPath::calculate_proportions() const{
	size_t nc = n_cubics();

	if(nc == 0)
		return {0.0, 1.0};

	// Generate list of curve lengths.
	vector<double> ls(nc);
	for(size_t i = 0; i < nc; ++i)
		ls[i] = get_cubic(i).arc_length();

	// Calculate partial sums.
	vector<double> proportions = {0.0};
	proportions.reserve(nc + 1);
	partial_sum(ls.begin(), ls.end(), back_inserter(proportions));
	auto s = proportions.back();
	
	if(s < 1e-5)
		return {0.0, 1.0};

	// Normalize result.
	for(double& w: proportions)
		w /= s;
	proportions.back() = 1.0;

	return proportions;
}

/// Find the cubic path that contains the parameter t.
pair<size_t, double> BezierPath::find_cubic(double t) const{
	if(n_cubics() == 0)
		throw logic_error{"find_cubic: there are no cubic segments."};

	if(t <= 0.0)
		return {0, 0.0};
	else if(t >= 1.0)
		return {n_cubics()-1, 1.0};

	if(parametrization == Parametrization::arc_length_aware){
		const auto& proportions = get_proportions();

		auto it = loc::upper_bound(proportions, t);
		size_t b = it - proportions.begin() - 1;
	
		double wb = *prev(it);
		double s = (t - wb)/(*it - wb);

		return {b, s};
	}else{
		auto tn = t*n_cubics();
		size_t b = floor(tn);
		return {b, tn - b};
	}
}

/// Force G1 continuity at the beginning of cubic c.
void BezierPath::force_g1(size_t c){
	// Avoid extreme points.
	if(c == 0 or c >= n_cubics())
		return;

	// Index of center vertex.
	size_t i = 3*c;

	// Previous point.
	Point& p = point(i-1);

	// Center point (won't be modified).
	const Point& q = point(i);

	// Next point.
	Point& r = point(i+1);

	// Directions.
	Point u = p - q;
	Point v = r - q;

	double lu = u.norm();
	double lv = v.norm();

	// Ignore if angle is already 180 degrees.
	if(fabs((u*v)/(lu*lv) + 1.0) < 1e-5)
		return;

	if(lu > 0.0 and lv > 0.0){
		Point dir = v - u;
		dir.unitize();
		p = q - lu*dir;
		r = q + lv*dir;
	}
}

/// Force G1 continuity at the beginning of cubic c.
void BezierPath::remove_control_point(size_t c){
	if(points.empty())
		return;

	if(size() <= 4){
		points.clear();
		return;
	}

	auto it = points.begin() + 3*c;

	if(next(it) == points.end())
		it -= 2;		
	else if(it != points.begin())
		--it;
		
	points.erase(it, it+3);
}

/// Remove small components.
void BezierPath::prune(double rmin){
	PointList pruned;
	size_t nc = n_cubics();
	
	if(nc == 0)
		points.clear();

	pruned.push_back(points.front());

	for(size_t c = 0; c < nc; ++c){
		auto seg = get_cubic(c);
		if(seg.polygon_length() > rmin){
			auto p = &seg[0];
			pruned.insert(pruned.end(), p+1, p+4);
		}
	}

	if(pruned.size() > 1)
		points = move(pruned);
	else
		points.clear();
}


/// Merge two curves.
BezierPath merge_curves(
	const BezierPath& c0,
	const BezierPath& c1,
	double alpha,
	bool fw1,
	bool fw2
){
	BezierPath res;

	auto linerp = [&](double u, double v){ 
		return loc::linerp(alpha, u, v); 
	};

	auto linerp_col = [&](int i) -> int{ 
		return linerp(c0.get_color()[i], c1.get_color()[i]);
	};
	
	res.color = {linerp_col(0), linerp_col(1), linerp_col(2)};

	res.index = c0.index;

	res.visible = (alpha < 0.5)? c0.is_visible(): c1.is_visible();

	const auto& cp0 = c0.points;
	const auto& cp1 = c1.points;

	size_t N = min(cp0.size(), cp1.size());

	int dj = fw2? 1: -1;
	size_t j = fw2? 0: N-1;

	int di = fw1? 1: -1;
	size_t i = fw1? 0: N-1;

	res.points.reserve(N);

	for(size_t k = 0; k < N; ++k, i+=di, j+=dj)
		res.points.emplace_back(
			linerp(cp0[i][0], cp1[j][0]),
			linerp(cp0[i][1], cp1[j][1])
		);

	return res;
}

/// Output stream operator <<.
ostream& operator<<(ostream& out, const BezierPath& path){
	const auto& points = path.points;
	out << points.size() << '\n';

	for(const auto& p: points)
		out << p << '\n';

	return out;
}

/// Input stream operator >>.
istream& operator>>(istream& in, BezierPath& path){
	auto& points = path.points;
	size_t n_points;
	in >> n_points;

	size_t s = points.size();

	points.resize(s + n_points);

	for(size_t i = 0; i < n_points; ++i)
		in >> points[i+s];

	return in;
}

/// Split the curve in two parts, one from parameters 0.0 to t,
/// and the other from t to 1.0.
loc::pair_of<BezierPath> BezierPath::split(double t, bool fw) const{
	//update_proportions();

	size_t c;
	double s;
	tie(c, s) = find_cubic(t, 1e-5);
	
	auto pb = points.begin(), pe = points.end();
	
	PointList ptsa, ptsb;

	if(s == 0.0){
		if(c > 0){
			ptsa = {pb, pb + 3*c + 1};
			ptsb = {pb + 3*c, pe};
		}else
			ptsb = points;
	}else if(s == 1.0){
		++c;
		if(c < n_cubics()){
			ptsa = {pb, pb + 3*c + 1};
			ptsb = {pb + 3*c, pe};
		}else
			ptsa = points;
		
	}else{
		ptsa = {pb, pb + 3*c};

		// Split segment c.
		auto segs = get_cubic(c).split2(s);

		loc::insert_back(ptsa, segs.first); 
		loc::insert_back(ptsb, segs.second); 

		// Insert control points remaining.
		if(++c < n_cubics())
			ptsb.insert(ptsb.end(),	pb + 3*c + 1, pe);
	}
	
	if(!fw){
		swap(ptsa, ptsb);
		loc::reverse(ptsa);
		loc::reverse(ptsb);
	}

	return {{move(ptsa)}, {move(ptsb)}};
}

/// Get the result of the curve between parameters ta and tb.
BezierPath BezierPath::cut(double ta, double tb) const{
	if(ta > tb)
		swap(ta, tb);

	size_t ca;
	double sa;
	tie(ca, sa) = find_cubic(ta);
	
	size_t cb;
	double sb;
	tie(cb, sb) = find_cubic(tb);

	if(ca == cb){
		auto seg = get_cubic(ca).split(sa, sb);
		return {PointList{begin(seg), end(seg)}};
	}

	auto beg = points.begin();

	PointList pts;
	
	// Add points from cubic ca.
	if(sa < 1.0)
		loc::insert_back(pts, get_cubic(ca).rsplit(sa));

	// Add intermediate control points (if any).
	auto b = beg+(ca+1)*3+1;
	auto e = beg+cb*3;
	if(b < e){
		if(*b == pts.back())
			++b;
		pts.insert(pts.end(), b, e);
	}
	
	// Add points from cubic cb.
	if(sb > 0.0){
		auto pts_split = get_cubic(cb).split(sb);
		if(pts_split[0] == pts.back())
			pts.pop_back();
		loc::insert_back(pts, pts_split);
	}

	return {move(pts)};
}

BezierPath fit(const CubicBezier& c, const Morph& H, int level = 0){
	auto target = H.morph_list(c.sample(10));

	CubicBezier c1{
		target.front(),
		Point{},
		Point{},
		target.back()
	};

	double err = c1.fit(target);

	BezierPath curve;
	PointList& res = curve.control_points();

	if(level == 10){
		cout << "epaa\n";
		cout << "error = " << err << '\n';
		auto pts = c.sample(20);

		auto bb = axis_aligned_bounding_box(pts);
		
		double xmin = bb[0][0];
		double ymin = bb[0][1];
		double xmax = bb[1][0];
		double ymax = bb[1][1];
		
		double w = xmax - xmin;
		double h = ymax - ymin;
		
		double xc = 0.5*(xmax + xmin);
		double yc = 0.5*(ymax + ymin);

		double scale = 2;
		xmin = xc - scale*w;
		ymin = yc - scale*h;

		w = 2*scale*w;
		h = 2*scale*h;

		PointList grid;
		int N = 50;
		for(int j = 0; j < N; ++j)
			for(int i = 0; i < N; ++i){
				double x = xmin + w*i/(N-1);
				double y = ymin + h*j/(N-1);
				grid.emplace_back(x, y);
			}

		H.transform(grid);

		auto&& out = postscript("out.eps", grid);
		out << "/dot {gsave newpath";
		out << ".002 0 360 arc gsave fill grestore";
		out << "closepath grestore";
		out << "} bind def";

		out << "0 0 0 setrgbcolor";
		for(Point p: grid)
			out << p << "dot";

		//		out << "0 0 0 setrgbcolor";
		//		for(Point p: target)
		//			out << p << "dot";

		//		out << "0 0 0 setrgbcolor";
		//		out << "0.001 setlinewidth";
		//		out << c1[0] << "moveto" << c1 << "stroke";
	}

	if(level == 10 or err < 1)
		loc::insert_back(res, c1);
	else{
		auto p = c.split2(0.5);
		loc::insert_back(res, fit(p.first, H, level+1).control_points());
		res.pop_back();
		loc::insert_back(res, fit(p.second, H, level+1).control_points());
	}

	for(size_t i = 1; i < curve.n_cubics(); ++i)
		curve.force_g1(i);

	return res;
}

/// Create a morphed curve.
BezierPath BezierPath::morph(const Morph& H) const{
	if(n_cubics() == 0)
		return {};

	BezierPath curve;
	PointList& res = curve.control_points();
	res = fit(get_cubic(0), H).control_points();

	for(size_t i = 1; i < n_cubics(); ++i){
		auto nc = curve.n_cubics();
		res.pop_back();
		loc::insert_back(res, fit(get_cubic(i), H).control_points());
		if(is_g1(i-1))
			curve.force_g1(nc);
	}

	curve.update_proportions();
	
	return curve;
}
/// Update proportions.
void BezierPath::update_proportions(){
	auto& d = proportion_data;

	if(d.control_points != control_points()){
		d.proportions = calculate_proportions();
		d.control_points = control_points();
	}

	if(d.proportions.empty())
		d.proportions = {0.0, 1.0};

}
		
const vector<double>& BezierPath::get_proportions() const{
	auto& d = proportion_data;

	if(d.control_points != control_points()){
		cerr << "Warning! " << this << " needs update! Fix this!\n";
		cerr << "d.control_points\n";
		for(Point p: d.control_points)
			cerr << p << '\n';
		cerr << "control_points\n";
		for(Point p: control_points())
			cerr << p << '\n';
		throw std::runtime_error{"Update required!"};
	}

	return d.proportions;
}
