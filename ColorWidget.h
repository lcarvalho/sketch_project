#ifndef _COLOR_WIDGET_H
#define _COLOR_WIDGET_H

#include <QtWidgets>
#include "Animation.h"

class FrameEditor;

/// Qt Widget to edit frames from an animation.
class ColorWidget : public QWidget{
	Q_OBJECT
	public:
		/// Constructor.
		ColorWidget(FrameEditor& editor, QWidget* parent);

		/// Set the main color.
		void set_color(const QColor& c);

		/// Set the background color.
		void set_bg_color(const QColor& c);

		/// Get the main color.
		QColor get_color() const;

		/// Get the background color.
		QColor get_bg_color() const;

	protected:
		/// Paint event
		void paintEvent(QPaintEvent* event);

		/// Mouse press event.
		void mousePressEvent(QMouseEvent* event);

		/// Animation editor.
		FrameEditor& editor;

		/// Main color.
		QColor main_color;

		/// Background color.
		QColor bg_color;
};

/// Convert QColor to Color.
inline Color toColor(const QColor& c){
	return {c.red(), c.green(), c.blue()};
}

/// Set the main color.
inline void ColorWidget::set_color(const QColor& c){
	main_color = c;
	repaint();
}

/// Set the background color.
inline void ColorWidget::set_bg_color(const QColor& c){
	bg_color = c;
	repaint();
}

/// Get the main color.
inline QColor ColorWidget::get_color() const{
	return main_color;
}

/// Get the main color.
inline QColor ColorWidget::get_bg_color() const{
	return bg_color;
}

#endif
