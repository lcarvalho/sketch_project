#include "Layer.h"
#include <sstream>

using std::pair;
using std::vector;
using std::tie;
using std::istream;
using std::string;
using std::set;

/// Constructor from Json object.
Layer::Layer(const json11::Json& json, set<size_t>& keyframes){
	using namespace json11;
	name = json["name"].string_value();
	for(auto& f: json["frames"].array_items()){
		int index = f["frame_index"].int_value();
		keyframes.insert(index);
		*get_drawing(index) = {f["drawings"]};
	}
}

/// Reset the drawing list.
void Layer::reset_drawings(size_t n_frames){
	drawings.clear();
	drawings.resize(n_frames);
}

/// Return layer status.
pair<size_t, size_t> Layer::status() const{
	size_t ng = 0;
	size_t nd = 0;

	auto d0 = get_drawing_const(0);

	if(d0 != nullptr){
		ng += d0->get_guidelines().size();
		nd += d0->n_drawables();
	}

	return {ng, nd};
}

/// Set the curve distance tolerance.
void Layer::set_tolerance(double tol){
	tolerance = tol;
}

/// Get a list with all drawable objects at frame f.
DrawableList Layer::get_all_drawables(size_t f) const{
	auto d = get_drawing_const(f);

	if(visible == false or d == nullptr)
		return {};

	return d->get_drawables();
}

/// Get a list with all guidelines at frame f.
BezierPathList Layer::get_all_guidelines(size_t f) const{
	BezierPathList res;

	if(visible == false)
		return res;

	auto add_guidelines = [&](const BezierPathList& guidelines){
		loc::insert_back(res, guidelines);
	};

	auto d = get_drawing_const(f);
	if(d != nullptr)
		add_guidelines(d->get_guidelines());

	return res;
}

/// Get a list with all auxiliary curves at frame f.
BezierPathList Layer::get_all_auxiliary_curves(size_t f) const{
	BezierPathList res;

	if(visible == false)
		return res;

	auto add_curves = [&](const BezierPathList& curves){
		loc::insert_back(res, curves);
	};

	auto d = get_drawing_const(f);
	if(d != nullptr)
		add_curves(d->get_auxiliary_curves());

	return res;
}

/// Get a list with all auxiliary curves at frame f.
vector<BezierPathList> Layer::get_all_clusters(size_t f) const{
	vector<BezierPathList> res;

	if(visible == false)
		return res;

	auto add_clusters = [&](const vector<BezierPathList>& clusters){
		loc::insert_back(res, clusters);
	};

	auto d = get_drawing_const(f);
	if(d != nullptr)
		add_clusters(d->get_clusters());

	return res;
}

/// Remove empty curves.
void Layer::clear_empty_curves(){
	for(auto& d: drawings)
		d.clear_empty_curves();
}

/// Calculate morph from frame f0 to frame f1.
SimilarityMorph_sp Layer::calculate_morph(
		size_t f0,
		size_t f1,
		const SimilarityMorph_sp& base_morph
	){

	auto interp = get_interpolator(f0, f1);

	if(interp != nullptr)
		interp->match_guidelines();

	return (interp == nullptr)?
		nullptr:
		interp->calculate_morph(base_morph);
}

/// Get the interpolator object associated with a pair of drawings.
Interpolator* Layer::get_interpolator(size_t f0, size_t f1){
	auto d0 = get_drawing(f0);
	auto d1 = get_drawing(f1);

	if(d0 == nullptr or d1 == nullptr)
		return nullptr;

	auto& interp = interp_map[{d0, d1}];

	/*
	size_t n0 = d0->get_guidelines().size();
	size_t n1 = d1->get_guidelines().size();
	if(n1 < 0.5*n0){
		if(interp != nullptr){
			auto is_interp = [&](const Interpolator& x){ 
				return &x == interp; 
			};
			interpolators.erase(loc::find_if(interpolators, is_interp));
			interp = nullptr;
		}
		return nullptr;
	}*/

	if(interp == nullptr){
		interpolators.emplace_back(*d0, *d1);
		interp = &interpolators.back();
	}

	return interp;
}

/// Mix object from frame f_start to f_end at frame f.
loc::pair_of<SimilarityMorph> Layer::mix_objects(
	size_t f_start, size_t f_end, size_t f, size_t f_end_actual,
	std::function<double(double)> speed,
	const SimilarityMorph& Sp,
	const SimilarityMorph& Spia
){
	double alpha = speed(double(f - f_start)/(f_end - f_start));
	auto interpolator = get_interpolator(f_start, f_end_actual);

	SimilarityMorph SSp, SpiaSia;

	if(interpolator != nullptr)
		tie(SSp, SpiaSia) =
			interpolator->mix_objects(
				*get_drawing(f),
				alpha,
				Sp,
				Spia
			);

	return {SSp, SpiaSia};
}


/// Get auxiliary curves transforming curves from keyframe f0 to the keyframe f1.
void Layer::calculate_auxiliary_curves(size_t f0, size_t f1){
	auto interp = get_interpolator(f0, f1);

	if(interp != nullptr)
		interp->calculate_auxiliary_curves();
}

/// Clear inbetween info.
void Layer::clear_inbetween_info(){
	interpolators.clear();
	interp_map.clear();
}

/// Update inbetween info.
void Layer::update_inbetween_info(){
	for(auto& interpolator: interpolators)
		interpolator.get_inbetween_info(tolerance);
}


/// Get a list with all the control points of this layer and sublayers.
PointList Layer::all_control_points() const{
	PointList pts;

	auto insert_pts = [&](const PointList& cpts){
		loc::insert_back(pts, cpts);
	};

	for(auto& d: drawings)
		insert_pts(d.all_control_points());

	return pts;
}
		
/// Update curve proportions.
void Layer::update_proportions(){
	for(auto& d: drawings)
		d.update_proportions();
}
		
/// Istream operator.
istream& operator>>(istream& in, Layer& layer){
	loc::ignore_spaces(in);
	getline(in, layer.name);

	// Read number of drawings.
	size_t nd = 0;
	in >> nd;

	for(size_t i = 0; i < nd; ++i){
		string frame;
		in >> frame;

		size_t f;
		in >> f;

		in >> *layer.get_drawing(f);
	}

	return in;
}

/// Insert frame at position i.
void Layer::insert_frame(size_t i){
	auto it = drawings.begin();
	advance(it, i);
	drawings.emplace(it);
}

// Remove all interpolators using drawing d.
void Layer::clear_interpolators_using(const Drawing& d){
	for(auto p: interp_map){
		DrawingPair dd = p.first;
		if(dd.first == &d or dd.second == &d)
			interpolators.remove_if(
				[&](const Interpolator& interp){
					return &interp == p.second;
				}
			);
	}
}

/// Remove frame at position i.
void Layer::remove_frame(size_t i){
	auto it = drawings.begin();
	advance(it, i);

	clear_interpolators_using(*it);

	drawings.erase(it);
}
