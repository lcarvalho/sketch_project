#include "DrawableSet.h"
#include <functional>

using std::function;
using std::mem_fn;

/// Add a region.
void DrawableSet::add(const Region_sp& r){
	auto& r_curves = r->get_curves();
	auto it = loc::find_first_of(
		drawables, r_curves,
		[&](const Drawable_sp& d, const BezierBool& c){
			return d == c.curve;
		}
	);
	drawables.insert(it, r);
}

/// Remove all curves that satisfy operation op.
bool DrawableSet::remove_curves_if(function<bool(const BezierPath&)> op){
	return remove_drawables_if(
		[&](const Drawable& d){ 
			return d.type() == 1 and op(toBezierPath(d));
		} 
	);
}

/// Remove all regionscurves that satisfy operation op.
bool DrawableSet::remove_regions_if(function<bool(const Region&)> op){
	return remove_drawables_if(
		[&](const Drawable& d){ 
			return d.type() == 2 and op(toRegion(d));
		} 
	);
}

/// Remove all drawables that satisfy operation op.
bool DrawableSet::remove_drawables_if(function<bool(const Drawable&)> op){
	return loc::remove_if_and_erase(
		drawables,
		[&](const Drawable_sp& d){ return op(*d); }
	);
}

/// Remove a curve anywhere it appears.
void DrawableSet::remove_all(const BezierPath_sp& curve){
	// Remove curve from list of curves.
	remove(curve);

	// Remove curve from all regions that contains it.
	for(auto& r: get_regions())
		r->remove_curve(curve);

	// Remove regions that became empty.
	remove_regions_if([](const Region& r){ return r.empty(); });

}

/// Sort the drawable list by the index.
void DrawableSet::sort(){
	using D = const Drawable_sp&;
	loc::stable_sort(drawables, [](D d0, D d1){ return d0->get_index() < d1->get_index(); });
}
