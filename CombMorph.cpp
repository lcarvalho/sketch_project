#include "CombMorph.h"

/// Apply morph to a single point.
Point CombMorph::morph(const Point& c) const{
	Point r = c;

	for(auto& m: morphs)
		r = m->morph(r);

	return r;
}
