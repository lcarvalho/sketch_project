#ifndef REGION_H
#define REGION_H

#include "BezierPath.h"
#include "loc.h"
#include <vector>
#include <tuple>
#include <map>

class Region;

/// Alias for a shared pointer of Region.
using Region_sp = std::shared_ptr<Region>;

/// Alias of a vector of shared pointers of Region.
using RegionList = std::vector<Region_sp>;

/// Alias for a vector of BezierBool.
using BBList = std::vector<BezierBool>;

/// A two dimensional region defined between curves.
class Region : public Drawable{
	public:
		/// Constructor.
		Region();

		/// Get the list of curves (const version).
		const BBList& get_curves() const;

		/// Get the list of curves.
		BBList& get_curves();

		/// Add a curve to the list.
		void add_path(const BezierPath_sp& path, bool fw = true);

		/// Check if region is empty.
		bool empty() const;

		/// Adjust the order of the curves.
		void adjust_order();

		/// Check if point p is inside the region.
		bool contains(const Point& p);

		/// Check if the region is next to point p.
		bool is_next_to(const Point& p, double scale = 1.0) override;

		/// Increment the index.
		void inc_index() override;

		/// Decrement the index.
		void dec_index() override;

		/// Select region.
		void select() override;

		/// Unselect region.
		void deselect() override;

		/// Replace curves according to map m.
		void replace_curves(
			const std::map<BezierPath_sp, BezierPath_sp> m
		);

		/// Remove curve.
		void remove_curve(const BezierPath_sp& c);

	private:

		/// List of curves.
		BBList curves;
};

inline Region_sp toRegion_s(const Drawable_sp& ptr){
	return std::static_pointer_cast<Region>(ptr);
}

inline Region* toRegion(const Drawable_sp& ptr){
	return static_cast<Region*>(ptr.get());
}

inline Region* toRegion(Drawable* ptr){
	return static_cast<Region*>(ptr);
}

inline Region& toRegion(Drawable& d){
	return static_cast<Region&>(d);
}

inline const Region& toRegion(const Drawable& d){
	return static_cast<const Region&>(d);
}
////////////////////////////////////////////////////////////////////////////////
// Implementation of inline methods.

/// Get the list of curves (const version).
inline const BBList& Region::get_curves() const{
	return curves;
}

/// Get the list of curves.
inline BBList& Region::get_curves(){
	return curves;
}

/// Add a curve to the list.
inline void Region::add_path(const BezierPath_sp& path, bool fw){
	path->set_parent(this);
	curves.push_back({path, fw});
}

/// Check if region is empty.
inline bool Region::empty() const{
	return curves.empty();
}

/// Increment the index.
inline void Region::inc_index(){
	++index;
	for(auto& c: curves)
		c.curve->inc_index();
}

/// Decrement the index.
inline void Region::dec_index(){
	--index;
	for(auto& c: curves)
		c.curve->dec_index();
}


#endif
