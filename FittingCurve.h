#ifndef NEW_FITTING_CURVE_H
#define NEW_FITTING_CURVE_H

#include "BezierPath.h"
#include "Point.h"
#include <Eigen/SVD>

/// \brief Bezier curve fitted from a polygonal curve.
///
/// Implementation of Sarah Frisken's work.
class FittingCurve{
	public:

	/// Constructor.
	FittingCurve();

	/// Get the width of the distance field box.
	int distance_field_width() const;

	/// Get the maximum error allowed.
	double maximum_error() const;

	/// Set the maximum error allowed.
	void maximum_error(double e);

	/// Get the input polygonal curve.
	const PointList& input_curve() const;

	/// Get the fitted curve.
	const BezierPath& fitted_curve() const;

	/// Get the geometry of the distance field.
	std::array<int, 4> field_geometry() const;

	/// Clear curve.
	void clear();

	/// Check if curve is empty.
	bool empty() const;

	/// Insert a new input point.
	void insert_point(const Point& p);

	/// Get the vector distance at position (i,j) (const version).
	const Point& distance(int i, int j) const;

	/// Get the vector distance at position (i,j).
	Point& distance(int i, int j);

	/// Get the real coordinates of point at position (i,j).
	Point point_at(int i, int j);

	/// Adjust point c1.
	Point adjust_c1(const Point& c1, Point f1);

	private:
	
	////////////////////////////////////////////////////////////////////////
	// Private methods.

	/// Init the data of the cubic bézier.
	void init_cubic_curve();

	/// Test if a corner is formed with the new point.
	void test_corner();

	/// Update the vector distance field to include the new point.
	void update_distance_field();

	/// Adjust control points to fit the polygonal input curve.
	void adjust_control_points();

	/// Initialize the vector distance field centered in p.
	void init_distance_field(const Point& p, bool update_matrix = true);

	/// Get a point in the line p+v0 inside the distance field.
	Point adjusted_point(const Point& p, const Point& v0);

	/// Get the vector distance at arbitrary point Q.
	Point distance(const Point& Q);

	/// Calculate the value of f1 and f2.
	Point2 calculate_fs(double& e);

	/// Evaluate polygonal input at parameters ts.
	Point2 eval_input(double t0, double t1) const;

	////////////////////////////////////////////////////////////////////////
	// Parameters

	/// Input points.
	PointList input;

	/// Bezier path.
	BezierPath stroke;

	/// side size of distance field.
	int w_dist;

	/// Center of the distance field.
	Point center;

	/// Vector distance field.
	std::vector<Point> dist;

	/// geometry of distance field
	int imin, imax, jmin, jmax;

	/// Maximum error allowed.
	double max_error;

	/// True if G1 continuity is required for last curve segment.
	bool reqG1;
};

////////////////////////////////////////////////////////////////////////////////
// Implementation of inline methods.

/// Get the width of the distance field box.
inline int FittingCurve::distance_field_width() const{
	return w_dist;
}

/// Get the maximum error allowed.
inline double FittingCurve::maximum_error() const{
	return max_error;
}

/// Set the maximum error allowed.
inline void FittingCurve::maximum_error(double e){
	max_error = e;
}

/// Get the input polygonal curve.
inline const PointList& FittingCurve::input_curve() const{
	return input;
}

/// Get the fitted curve.
inline const BezierPath& FittingCurve::fitted_curve() const{
	return stroke;
}

/// Get the geometry of the distance field.
inline std::array<int, 4> FittingCurve::field_geometry() const{
	return {imin, jmin, imax, jmax};
}

/// Clear curve.
inline void FittingCurve::clear(){
	stroke.control_points().clear();
	input.clear();
}

/// Check if curve is empty.
inline bool FittingCurve::empty() const{
	return dist.size() == 0;
}

/// Get the vector distance at position (i,j) (const version).
inline const Point& FittingCurve::distance(int i, int j) const{
	if(!(i >= 0 and i < w_dist and j >= 0 and j < w_dist))
		throw std::logic_error{
			"invalid point: " 
			+ loc::to_string(i) + ' '
			+ loc::to_string(j) + ' '
			+ loc::to_string(w_dist)
		};
	return dist[i + j*w_dist];
}

/// Get the vector distance at position (i,j).
inline Point& FittingCurve::distance(int i, int j){
	if(!(i >= 0 and i < w_dist and j >= 0 and j < w_dist))
		throw std::logic_error{
			"invalid point: " 
			+ loc::to_string(i) + ' '
			+ loc::to_string(j) + ' '
			+ loc::to_string(w_dist)
		};
	
	return dist[i + j*w_dist];
}

inline Point FittingCurve::point_at(int i, int j){
	double x = imin + i;
	double y = jmin + j;
	return {x, y};
}

/// Get the vector distance at arbitrary point Q.
inline Point FittingCurve::distance(const Point& Q){
	return distance(round(Q[0] - imin), round(Q[1] - jmin));
}

#endif

