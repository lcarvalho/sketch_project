#include "SimilarityMorph.h"
#include <Eigen/Dense>
#include <Eigen/SVD>
#include <cstring>
#include <iostream>

using std::min;
using std::ostream;

////////////////////////////////////////////////////////////////////////////////
/// Constructor.
SimilarityMorph::SimilarityMorph() :
	a{
		1, 0,
		0, 0
	}{}

/// Calculate the best affine transformation that transforms
/// the n points in ps into points in qs.
void SimilarityMorph::get_morph(const PointList& ps, const PointList& qs){
	int n = min(ps.size(), qs.size());
	if(n == 0){
		a[0] = 1.0;
		a[1] = 0.0;
		a[2] = 0.0;
		a[3] = 0.0;
		return;
	}

	Eigen::MatrixXd A{2*n, 4};
	Eigen::VectorXd b{2*n};
	for(int i = 0; i < n; ++i){
		const Point& p = ps[i];

		const double& x = p[0];
		const double& y = p[1];

		A.row(2*i)   << x, -y, 1, 0;
		A.row(2*i+1) << y,  x, 0, 1;

		const Point& q = qs[i];
		b(2*i) = q[0];
		b(2*i+1) = q[1];
	}
	auto svd = A.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV);
	Eigen::Map<Eigen::VectorXd> ax{a, 4};
	ax = svd.solve(b);
}

/// Apply morph to a single point.
Point SimilarityMorph::morph(const Point& c) const{
	const double& x = c[0];
	const double& y = c[1];

	return {
		a[0]*x - a[1]*y + a[2],
		a[1]*x + a[0]*y + a[3]
	};
}

/// Get the inverse transformation.
SimilarityMorph SimilarityMorph::inverse() const{
	const double& u = a[0];
	const double& v = a[1];

	const double& tx = a[2];
	const double& ty = a[3];

	double s = u*u + v*v;

	double ui = u/s;
	double vi = -v/s;

	SimilarityMorph S;

	S.a[0] = ui;
	S.a[1] = vi;

	S.a[2] = -ui*tx + vi*ty;
	S.a[3] = -vi*tx - ui*ty;

	return S;
}

/// Composition operator.
SimilarityMorph SimilarityMorph::operator*(const SimilarityMorph& S) const{
	const double& c0 = S.a[0];
	const double& s0 = S.a[1];
	const double& x0 = S.a[2];
	const double& y0 = S.a[3];

	const double& c1 = a[0];
	const double& s1 = a[1];
	const double& x1 = a[2];
	const double& y1 = a[3];

	SimilarityMorph R;

	R.a[0] = c0*c1 - s0*s1;
	R.a[1] = c0*s1 + s0*c1;
	R.a[2] = c1*x0 - s1*y0 + x1;
	R.a[3] = s1*x0 + c1*y0 + y1;

	return R;
}

/// Get the intermediate transformation at parameter t,
/// where t == 0 means Identity, t == 1 means *this.
SimilarityMorph SimilarityMorph::intermediate(double t, const Point& c0) const{
	SimilarityMorph St;

	if(t <= 0.0)
		return St;

	if(t >= 1.0)
		return *this;

	const double& u = a[0];
	const double& v = a[1];

	const double& tx = a[2];
	const double& ty = a[3];

	double s = hypot(u, v);
	double theta = atan2(v, u);

	// Linear interpolation of scale and rotation angle.
	double st = 1.0 + t*(s - 1.0);
	double thetat = t*theta;

	double ut = st*cos(thetat);
	double vt = st*sin(thetat);

	St.a[0] = ut;
	St.a[1] = vt;

	double s2 = s*s;

	double ui = u/s2;
	double vi = -v/s2;

	double txi = ui*tx - vi*ty;
	double tyi = vi*tx + ui*ty;

	double txt = t*txi;
	double tyt = t*tyi;

	St.a[2] = ut*txt - vt*tyt;
	St.a[3] = vt*txt + ut*tyt;

	Point ct = (1-t)*c0 + t*morph(c0);

	Point dif = ct - St.morph(c0);

	St.a[2] += dif[0];
	St.a[3] += dif[1];

	return St;
}

ostream& operator<<(ostream& out, const SimilarityMorph& S){
	out << S.a[0] << ' ' << -S.a[1] << ' ' << S.a[2] << '\n';
	out << S.a[1] << ' ' << S.a[0] << ' ' << S.a[3];

	return out;
}

