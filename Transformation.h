#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

#include "Point.h"
#include <array>

/// Transformation that can be applied to Points.
class Transformation{
	public:
		/// Constructor from parameters.
		Transformation(
			const std::array<double, 4>& A,
			const Point& a = {0.0, 0.0}
		);

		/// Apply transformation to point p.
		Point operator()(const Point& p) const;

		/// Apply transformation to list of points.
		PointList& operator()( PointList& p) const;

		/// Combine two transformations.
		friend Transformation operator*(
			const Transformation& T1,
			const Transformation& T2
		);

		/// Get an identity transformation.
		static Transformation identity();

		/// Get a scale transformation.
		static Transformation scale(double sx, double sy);

		/// Get a translate transformation.
		static Transformation translate(double tx, double ty);

		/// Get a rotation transformation.
		static Transformation rotate(double angle, const Point& c);

		/// Get a skew in x transformation.
		static Transformation skewX(double angle);

		/// Get a skew in y transformation.
		static Transformation skewY(double angle);

	private:
		/// 2x2 Matrix in the order {a11, a21, a12, a22}.
		std::array<double, 4> A;

		/// Translation.
		Point a;
};

#endif
