#include "PostScriptWriter.h"
#include "bipartite_matching.h"
#include "Animation.h"
#include "CubicBezier.h"
#include "polynomial_roots.h"
#include "FittingCurve.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <chrono>
#include <random>
#include <functional>
#include <iomanip>
#include <cfloat>
#include <Eigen/Dense>
#include <Eigen/SVD>

using std::string;
using std::ofstream;
using std::ifstream;
using std::get;
using std::ostringstream;
using std::cout;
using std::pair;
using std::bind;
using std::swap;
using std::make_shared;
using std::function;
using std::move;
using std::make_tuple;
using std::vector;
using std::cerr;
using std::cin;
	
PostScriptWriter postscript(const PointList& c, const string& filename){
	auto bb = axis_aligned_bounding_box(c);
	double xmin = bb[0][0];
	double ymin = bb[0][1];
	double xmax = bb[1][0];
	double ymax = bb[1][1];
	double w = xmax - xmin;
	double h = ymax - ymin;
	double dx = 0.05*w;
	double dy = 0.05*h;
	xmin -= dx;
	ymin -= dy;
	w += 2*dx;
	h += 2*dy;

	return {filename, xmin, ymin, w, h};
}

void curve_cut(bool simple, bool random_pts){
	PointList c;

	if(random_pts){
		auto now = std::chrono::system_clock::now();
		auto seed = now.time_since_epoch().count();
		std::default_random_engine generator(seed);
		std::uniform_real_distribution<double> distribution{0.0, 50.0};
		auto dice = bind(distribution, generator);

		c = {
			{dice(), dice()},
			{dice(), dice()},
			{dice(), dice()},
			{dice(), dice()}
		};
	}else
		c = {
			{5.04912, 28.547},
			{24.8198, 5.55973},
			{32.2212, 45.8195},
			{42.5924, 13.6449}
		};


	for(auto p: c)
		cout << p << '\n';

	CubicBezier curve{c.data()};

	double w = simple? 100: 50;

	PostScriptWriter out{"curve_cut.eps", 0, 4, w, 43};

	out << "/bigdot {gsave newpath";
	out << "1 0 360 arc gsave 1 1 1 setrgbcolor fill grestore ";
	out << "gsave 0.1 setlinewidth 0 0 0 setrgbcolor stroke grestore";
	out << "closepath grestore";
	out << "} bind def";

	out << "/the_curve {";
	out << "gsave [1 1] 0 setdash";
	out << "newpath";
	out << "0.6 0.6 0.6 setrgbcolor";
	out << c[0] << "moveto";
	for(auto& p: c)
		out << p << "lineto";
	out << "stroke grestore";
	for(auto& p: c)
		out << p << "bigdot";
	out << "0.5 0.5 0.5 setrgbcolor";
	out << curve << "stroke} bind def";

	out << "/dot {gsave newpath";
	out << ".6 0 360 arc gsave fill grestore ";
	out << "gsave 0.1 setlinewidth 0 0 0 setrgbcolor stroke grestore";
	out << "closepath grestore";
	out << "} bind def";

	out << "0.1 setlinewidth";
	out << "the_curve";

	auto draw_curve = [&](const CubicBezier& curve){
		out << curve << "stroke";
		out << curve[0] << "dot";
		out << curve[1] << "dot";
		out << curve[2] << "dot";
		out << curve[3] << "dot";		
	};

	if(simple){
		auto curves = curve.split2(0.7);

		out << "1 0 0 setrgbcolor";
		draw_curve(curves.first);

		out.translate(50, 0);

		out << "the_curve";

		out << "0.5 0.5 1 setrgbcolor";
		draw_curve(curves.second);
	}else{
		out << "1 0 0 setrgbcolor";
		draw_curve(curve.split(0.2, 0.8));
	}
}

void bezier_path_split(bool simple){
	/*
	PointList c = {
		{45, 241},
		{44.6349, 156.576},
		{173.795, 141.492},
		{146, 238},
		{120.085, 327.98},
		{293.309, 350.844},
		{244, 193},
		{271, 156},
		{362.258, 111.987},
		{382, 222},
		{394.074, 289.285},
		{554, 312},
		{548, 236}
	};
	*/
	PointList c{
		{489.157, 624.019 },
		{483.954, 623.31 },
		{484.127, 619.634 },
		{479.257, 618.519 },
		{470.907, 616.606 },
		{460.836, 590.158 },
		{456.157, 572.319 }
	};
	BezierPath curve{c};

	auto bb = axis_aligned_bounding_box(c);
	double xmin = bb[0][0];
	double ymin = bb[0][1];
	double xmax = bb[1][0];
	double ymax = bb[1][1];
	double w = xmax - xmin;
	double h = ymax - ymin;
	double dx = 0.05*w;
	double dy = 0.05*h;
	xmin -= dx;
	ymin -= dy;
	w += 2*dx;
	h += 2*dy;

	if(simple)
		w = 2*w;

	PostScriptWriter out{"BezierPathSplit.eps", xmin, ymin, w, h};

	out << "/bigdot {gsave newpath";
	out << "1 0 360 arc gsave 1 1 1 setrgbcolor fill grestore ";
	out << "gsave 0.1 setlinewidth 0 0 0 setrgbcolor stroke grestore";
	out << "closepath grestore";
	out << "} bind def";

	out << "/the_curve {";
	out << "gsave [1 1] 0 setdash";
	out << "newpath";
	out << "0.6 0.6 0.6 setrgbcolor";
	out << c[0] << "moveto";
	for(auto& p: c)
		out << p << "lineto";
	out << "stroke grestore";
	for(auto& p: c)
		out << p << "bigdot";
	out << "0.5 0.5 0.5 setrgbcolor";
	out << curve.front() << "moveto";
	out << curve << "stroke} bind def";

	out << "/dot {gsave newpath";
	out << ".6 0 360 arc gsave fill grestore ";
	out << "gsave 0.1 setlinewidth 0 0 0 setrgbcolor stroke grestore";
	out << "closepath grestore";
	out << "} bind def";

	out << "0.1 setlinewidth";
	out << "the_curve";

	auto draw_curve = [&](const BezierPath& curve){
		out << curve.front() << "moveto";
		out << curve << "stroke";
		for(auto p: curve.control_points())
			out << p << "dot";
	};

	if(simple){
		auto curves = curve.split(0.4);

		out << "1 0 0 setrgbcolor";
		draw_curve(curves.first);

		out.translate(w/2, 0);

		out << "the_curve";

		out << "0.5 0.5 1 setrgbcolor";
		draw_curve(curves.second);
	}else{
		out << "1 0 0 setrgbcolor";
		draw_curve(curve.cut(0.1, 0.9));
	}
}

void sample_test(){
	BezierPath curve{
		{
		{163.836090, 228.943695},
		{262.240692, 199.121155},
		{151.184567, 367.001993},
		{340.936096, 310.343689},
		{373.323541, 300.673055},
		{370.742972, 312.673955},
		{358.770172, 317.695740},
		{329.649958, 329.909712},
		{417.445129, 380.398926},
		{402.536102, 289.443695},
		{395.965240, 253.122055},
		{405.307129, 236.981003},
		{443.236084, 228.943695}
		}
	};

	PostScriptWriter out{"sample_test.eps", 160, 220, 600, 130};
	out << "/dot {2 0 360 arc fill} bind def";
	out << "/the_curve {";
	out << "0.2 setlinewidth";
	out << "0.6 0.6 0.6 setrgbcolor";
	out << curve << "stroke} bind def";

	int N = 50;
	
	curve.set_parametrization(Parametrization::simple);

	out << "the_curve";
	out << "1 0 0 setrgbcolor";
	for(auto& p: curve.uniform_sample(N))
		out << p << "dot";

	curve.set_parametrization(Parametrization::arc_length_aware);

	out.translate(300, 0);
	out << "the_curve";
	out << "0 0 1 setrgbcolor";
	for(auto& p: curve.uniform_sample(N))
		out << p << "dot";
}

void bipartite_matching_test(){
	// Testing bipartite matching algorithm.
	using namespace std::chrono;

	std::default_random_engine generator;
	std::uniform_real_distribution<double> distribution(0.001, 10.0);
	auto dice = bind(distribution, generator);

	std::uniform_int_distribution<size_t> distribution_int(50, 300);
	//auto dice_int = bind(distribution_int, generator);

	for(int i = 1; i <= 1; ++i){
		auto nu = 500u;//dice_int();
		auto nv = 480u;//dice_int();

		bipartite_matching::BipartiteGraph G{nu, nv};

		auto ne = 0u;
		for(auto i = 0u; i < nu; ++i){
			auto neu = 0u;
			for(auto j = 0u; j < nv; ++j){
				double w = dice();
				if(w < 1.0 and neu < 10){
					G.add_edge(i, j, w);
					++ne;
					++neu;
				}
			}
		}
		cout << nu << ' ' << nv << ' ' << ne << ' ';

		auto start = high_resolution_clock::now();

		//auto&& M = G.maximum_weight_matching();

		auto end = high_resolution_clock::now();
		auto elapsed = duration_cast<milliseconds>(end - start);

		cout << nu*nv*ne << " " << elapsed.count() << '\n';

//		for(auto& e: M)
//			cout << e.first << ' ' << e.second << '\n';
	}
}

void adjust_order(vector<Point2>& pts){
	int n = pts.size();

	bool flipped;

	int ni = 0;

	do{
		flipped = false;
		int i = 0;
		for(auto& c: pts){
			Point& qa = c[0];
			Point& qb = c[1];
			Point& p = pts[(i+n-1)%n][1];
			Point& r = pts[(i+1)%n][0];

			if(
				dist(qa, p) + dist(qb, r) >
				dist(qa, r) + dist(qb, p)
			 ){
				swap(qa, qb);
				flipped = true;
			}
			++i;
		}

		++ni;
	}while(flipped and ni < 100);

	if(ni == 100)
		cerr << "Possible infinite loop!\n";
}

void test_order(int N){
	using namespace std::chrono;
	auto now = std::chrono::system_clock::now();
	auto seed = now.time_since_epoch().count();
	std::default_random_engine generator(seed);
	std::uniform_real_distribution<double> distribution(0.0, 10.0);
	auto dice = bind(distribution, generator);
	double alpha = 0.0;
	double dalpha = 2*loc::pi/(2*N-1);
	auto random_point = [&](){ 
		Point p{5*cos(alpha)+5, 5*sin(alpha)+5}; 
		alpha += dalpha;
		return p;
	};

	vector<Point2> pts(N);
	for(auto& p: pts){
		p[0] = random_point();
		p[1] = random_point();
		if(dice() > 3.0)
			swap(p[0], p[1]);
	}

	PostScriptWriter out{"order_test.eps", 0, 0, 20, 10};
	out << "/dot {2 0 360 arc fill} bind def";

	auto show_pts = [&](){

		out << pts[0][0] << "moveto";
		for(auto& p2: pts)
			out << p2[0] << "lineto" << p2[1] << "lineto";
		out << "closepath fill";
	};

	out << "0.1 setlinewidth";

	out.set_color(0.0, 0.0, 0.0);
	for(auto& p2: pts)
		out << p2[0] << "moveto" << p2[1] << "lineto stroke";
	out.set_color(1.0, 0.6, 0.6);
	show_pts();

	out.translate(10.0, 0.0);
	adjust_order(pts);
	
	out.set_color(0.0, 0.0, 0.0);
	for(auto& p2: pts)
		out << p2[0] << "moveto" << p2[1] << "lineto stroke";
	out.set_color(0.6, 0.6, 1.0);
	show_pts();


}

PointList random_points(int n){
	using namespace std::chrono;
	auto now = std::chrono::system_clock::now();
	auto seed = now.time_since_epoch().count();
	std::default_random_engine generator(seed);
	std::uniform_real_distribution<double> distribution(0.0, 80.0);
	auto dice = bind(distribution, generator);
	auto random_point = [&]{ return Point{dice(), dice()}; };

	PointList pts(n);
	loc::generate(pts, random_point);
	return pts;
}

Point distance(const Point& p, const PointList& pts){
	size_t n = pts.size();

	PointList ps;
	for(size_t i = 1; i < n; ++i){
		double t = project(pts[i-1], pts[i], p);
		if(t >= 0.0 and t <= 1.0)
			ps.push_back((1-t)*pts[i-1] + t*pts[i]);
	}

	const PointList& x = ps.empty()? pts: ps;

	return *loc::min_val_element(
		x, [&](const Point& q){
			return dist2(p, q);
		}
	) - p;
	
}
void transform_test(){

	double a = -0.7, b = 1.3, c = 1.3, d = -0.2, e = 100.0, f=5.0;
	auto T = [&](const Point p){ 
		return Point{
			1.e-2*p[0]*p[0] + 
			a*p[0] + b*p[1] + e, 
			1.e-3*p[1]*p[0]+ 
			1.e-2*p[1]*p[1]+ 
			c*p[0] + d*p[1] + f
		};
	};
	
	int N = 23;
	
	PointList pts = {{0, 0}, {0, 50}, {60, 0}, {60, 50}};
//	PointList pts = random_points(4);//{{0, 0}, {0, 50}, {60, 0}, {60, 50}};
	/*
		{
			{87.8663, 250.207},
			{239.723, 241.494},
			{-47.6325, 457.069},
			{239.319, 431.478}
		};
	*/
	BezierPath C{pts};
	PointList sampleC = C.uniform_sample(N);
	
	PointList Tpts(pts.size());
	loc::transform(pts, Tpts, T);
	BezierPath TC{Tpts};
	PointList dst(N);
	loc::transform(sampleC, dst, T);

	BezierPath newTC{TC};
	newTC.get_cubic(0).fit(dst);

	int M = 15;
	int P = 13;
	PointList grid;
	grid.reserve(M*P);
	for(int i = 0; i < M; ++i)
		for(int j = 0; j < P; ++j)
			grid.emplace_back(5*(i-1), 5*(j-1));

	PointList all_pts = grid;
	for(Point p: grid)
		all_pts.push_back(T(p));
	auto&& out = postscript(all_pts, "transform.eps");
	
	out << "/dot {gsave newpath";
	out << ".5 0 360 arc gsave fill grestore ";
	out << "gsave 0.1 setlinewidth 0 0 0 setrgbcolor stroke grestore";
	out << "closepath grestore";
	out << "} bind def";
	
	out << "/bigdot {gsave newpath";
	out << "1.2 0 360 arc gsave 1 1 1 setrgbcolor fill grestore ";
	out << "gsave 0.1 setlinewidth 0 0 0 setrgbcolor stroke grestore";
	out << "closepath grestore";
	out << "} bind def";

	out << "0.4 setlinewidth";

	auto draw_curve = [&](const BezierPath& c){
		out << "gsave [1 1] 0 setdash";
		out << "newpath";
		out << "0.6 0.6 0.6 setrgbcolor";
		out << c.front() << "moveto";
		for(auto& p: c.control_points())
			out << p << "lineto";
		out << "stroke grestore";
		
		for(auto& p: c.control_points())
			out << p << "bigdot";
		
		out << c.front() << "moveto" << c << "stroke";
	};

	out << "0.5 0.8 0.8 setrgbcolor";
	for(Point p: grid)
		out << p << ".3 0 360 arc fill";

	out << "1 0 0 setrgbcolor";
	draw_curve(C);
	
	out << "0.5 0.8 0.8 setrgbcolor";
	for(Point p: grid)
		out << T(p) << ".3 0 360 arc fill";
//	out << "0 0 1 setrgbcolor";
//	draw_curve(TC);
	
	out << "0 0 1 setrgbcolor";
	draw_curve(newTC);

	out << "0 0 0 setrgbcolor";
	for(Point& p: sampleC)
		out << p << "dot" << T(p) << "dot";
}

class TestMorph : public Morph{
	public:
		Point morph(const Point& p) const{
			double a = -0.7, b = 1.3, c = 1.3, d = -0.2, e = 100.0, f=5.0;
			double x = 1.e-2*p[0]*p[0] + a*p[0] + b*p[1] + e;
			double y = 1.e-3*p[1]*p[0] + 1.e-2*p[1]*p[1] + c*p[0] + d*p[1] + f;
			return {x, y};
		}
};


void transform_test1(){
	TestMorph T;
	
	int N = 40;
	
	PointList pts = {{0, 0}, {0, 50}, {60, 0}, {60, 50}};
//	PointList pts = random_points(4);//{{0, 0}, {0, 50}, {60, 0}, {60, 50}};
	
	BezierPath C{pts};
	PointList sampleC = C.uniform_sample(N);
	
	PointList Tpts(pts.size());
	loc::transform(pts, Tpts, T);

	BezierPath nC = C.morph(T);

	int M = 15;
	int P = 13;
	PointList grid;
	grid.reserve(M*P);
	for(int i = 0; i < M; ++i)
		for(int j = 0; j < P; ++j)
			grid.emplace_back(5*(i-1), 5*(j-1));
	PointList Tgrid(grid.size());
	loc::transform(grid, Tgrid, T);

	auto&& out = postscript("transform.eps", pts, Tpts, grid, Tgrid);
	
	out << "/dot {gsave newpath";
	out << ".5 0 360 arc gsave fill grestore ";
	out << "gsave 0.1 setlinewidth 0 0 0 setrgbcolor stroke grestore";
	out << "closepath grestore";
	out << "} bind def";
	
	out << "/bigdot {gsave newpath";
	out << "1.2 0 360 arc gsave 1 1 1 setrgbcolor fill grestore ";
	out << "gsave 0.1 setlinewidth 0 0 0 setrgbcolor stroke grestore";
	out << "closepath grestore";
	out << "} bind def";

	out << "0.4 setlinewidth";

	auto draw_curve = [&](const BezierPath& c){
		out << "gsave [1 1] 0 setdash";
		out << "newpath";
		out << "0.6 0.6 0.6 setrgbcolor";
		out << c.front() << "moveto";
		for(auto& p: c.control_points())
			out << p << "lineto";
		out << "stroke grestore";
		
		for(auto& p: c.control_points())
			out << p << "bigdot";
		
		out << c.front() << "moveto" << c << "stroke";
	};

	out << "0.5 0.8 0.8 setrgbcolor";
	for(Point p: grid)
		out << p << ".3 0 360 arc fill";

	out << "1 0 0 setrgbcolor";
	draw_curve(C);
	
	out << "0 0 1 setrgbcolor";
	draw_curve(nC);
	
	out << "0.5 0.8 0.8 setrgbcolor";
	for(Point p: grid)
		out << T(p) << ".3 0 360 arc fill";
	
	out << "0 0 0 setrgbcolor";
	for(Point& p: sampleC)
		out << p << "dot" << T(p) << "dot";
}

inline Point dot_cross(const Point& F, const Point& B){
	return {dot(F, B), cross(F, B)};
}

Point3 get_Q(const Point3& Pi, const Point3& Pj);
	
template<class FW, class Fw, class Fn>
void calculate_weight(
	const PointList& Ci, 
	const PointList& Cj, 
	size_t i, size_t j, 
	FW W, 
	Fw west, 
	Fn north
);

template<class Fw, class Fn>
vector<int2> backtrack_path(size_t ci, size_t cj, Fw west, Fn north);

vector<int2> find_path(const PointList& Ci, const PointList& Cj);

void output_svg(const vector<int2>& path, const PointList& Pi, const PointList& Pj){
	ofstream out{"out.svg"};	
	double x0 = 0.0, y0 = 0.0, w = 800.0, h = 600.0;

	out << "<svg\n"
		<< "xmlns=\"http://www.w3.org/2000/svg\"\n"
		<< "version=\"1.1\"\n"
		<< "width=\"" << w << "\" height=\"" << h << "\"\n"
		<< "viewBox=\"0 0 " << w << ' ' << h << "\"\n"
		<< "baseProfile=\"tiny\"\n"
		<< "xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n"
		<< "<g transform=\"translate(" << -x0 << ", " << -y0
		<< ")\">\n";

	ostringstream out_i, out_j;

	out_i << "M ";
	out_j << "M ";

	for(int2 p: path){
		out_i << Pi[p.first];
		out_j << Pj[p.second];
	}

	out << "<path\n";
	out << R"(style="fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1")" << '\n';
	out << "d=\"" << out_i.str() << "\"\n";
	out << ">\n";
	out << "<animate\n attributeName=\"d\"\n";
	out << "from=\"" << out_i.str() << "\"\n";
	out << "to=\"" << out_j.str() << "\"\n";
	out << R"(dur="5s" begin="0s" repeatCount="indefinite"/>)";

	out << "</path>\n";

	out << "</g>\n";
	out << "</svg>\n";
}

void output_eps(const vector<int2>& path, const PointList& Pi, const PointList& Pj){
	auto&& out = postscript("out.eps", Pi, Pj);

	out << "0.1 setlinewidth";

	auto show_pts = [&](const PointList& pts){
		out << pts[0] << "moveto";
		for(auto& p: pts)
			out <<  p << "lineto";
		out << "stroke";
	};

	out.set_color(1, 0, 0);
	show_pts(Pi);
	
	out.set_color(0, 0, 1);
	show_pts(Pj);

	out.set_color(0, 0, 0);
	for(auto p: path)
		out << Pi[p.first] << "moveto" << Pj[p.second] << "lineto";
	out << "stroke";
}

void fig_mix_curves(){
	PointList pts0 = {
		{132.000000, 148.000000}, 
		{140.000000, 173.000000},  
		{139.000000, 177.000000},  
		{165.000000, 186.000000},
		{149.000000, 199.000000},
		{131.000000, 212.000000},
		{163.000000, 253.000000},
		{172.000000, 265.000000},
		{140.000000, 310.000000},
		{132.000000, 351.000000}
	};

	PointList pts1 = {
		{140.000000, 148.000000}, 
		{139.000000, 170.000000}, 
		{141.000000, 182.000000}, 
		{166.000000, 191.000000}, 
		{115.000000, 217.000000}, 
		{177.000000, 243.000000}, 
		{163.000000, 253.000000}, 
		{123.000000, 279.000000}, 
		{150.000000, 309.000000}, 
		{142.000000, 349.000000}
	};

	for(Point& p: pts1)
		p[0] += 100;

	PointList pts(pts0.size());
	for(size_t i = 0; i < pts.size(); ++i)
		pts[i] = loc::linerp(0.5, pts0[i], pts1[i]);
	
	auto&& out = postscript("mix_curves.eps", pts0, pts1);

	out << "/dot {gsave newpath";
	out << "1.5 0 360 arc gsave 0 0 0 setrgbcolor fill grestore ";
	out << "gsave 0.1 setlinewidth 0 0 0 setrgbcolor stroke grestore";
	out << "closepath grestore";
	out << "} bind def";
	
	out << "/lightdot {gsave newpath";
	out << "2 0 360 arc gsave 1 1 1 setrgbcolor fill grestore ";
	out << "gsave 0.1 setlinewidth 0 0 0 setrgbcolor stroke grestore";
	out << "closepath grestore";
	out << "} bind def";


	for(PointList ps: {pts0, pts1, pts}){
		BezierPath c{ps};
//		c.force_g1(2);
		ps = c.control_points();
		out << "1 setlinewidth";
		out.set_color(0., 0., 0.);
		out << c.front() << "moveto" << c << "stroke";
		out << ".8 setlinewidth";

		out.set_color(80./255., 190./255., 230./255.);
		out << ps[5] << "moveto" << ps[6] << "lineto" << ps[7] << "lineto stroke";
		out << ps[5] << "dot";
		out << ps[6] << "lightdot";
		out << ps[7] << "dot";
	}
	
}

#include "FeatureMorph.h"
void test_feature_morph(){
	PointList pts0 = {
	 	{129.29953,306.8696 },
		{0.0, 90},
		{0.0, 90},
		{0.0, 90},
		{0.0, 90},
		{0.0, 90}
	};
	
	for(size_t i = 1; i < pts0.size(); ++i)
		pts0[i] += pts0[i-1];

	PointList pts1 = {
		{826.28437,296.76808},
		{75.76145,103.03556},
		{62.62945,87.88327}, 
		{-3.03045,144.45181}, 
		{-41.41626,118.18785}, 
		{-85.86297,97.9848}
	};
	for(size_t i = 1; i < pts1.size(); ++i)
		pts1[i] += pts1[i-1];

	PointList grid;

	for(int i = 0; i < 10; ++i)
		for(int j = 0; j < 10; ++j)
			grid.emplace_back(-150+i*60, 300+j*60);

	loc::vector_pair<PointList> data{{pts0, pts1}};

	FeatureMorph m{data};
	PointList mgrid = m.morph_list(grid);

	auto&& out = postscript("feature_morph.eps", pts0, pts1, grid, mgrid);

	out << "/dot {gsave newpath";
	out << "1.5 0 360 arc gsave 0 0 0 setrgbcolor fill grestore ";
	out << "gsave 0.1 setlinewidth 0 0 0 setrgbcolor stroke grestore";
	out << "closepath grestore";
	out << "} bind def";
	
	out.set_color(1.0, 0.0, 0.0);
	out << pts0.front() << "moveto";
	for(Point p: pts0)
		out << p << "lineto";
	out << "stroke";
	
	out.set_color(0.0, 0.0, 1.0);
	out << pts1.front() << "moveto";
	for(Point p: pts1)
		out << p << "lineto";
	out << "stroke";

	for(Point p: pts0)
		out << p << "dot";

	for(Point p: pts1)
		out << p << "dot";

	for(Point p: grid)
		out << p << "dot";
	
	for(Point p: mgrid)
		out << p << "dot";
}

void test_similarity(){
	PointList pts0 = {
		{200, 300},
		{400, 300},
		{400, 500},
		{200, 500}
	};
	
	PointList pts1 = {
		{484, 660},
		{705, 771},
		{592, 995},
		{371, 884}
	};
	
	PointList pts = {
		{250, 350},
		{350, 350},
		{350, 450},
		{250, 450}
	};
	
	PointList ptsX = {
		{520, 778},
		{643, 800},
		{532, 920},
		{443, 791}
	};

	SimilarityMorph S;
	
	S.get_morph(pts0, pts1);

	PointList Spts = S.morph_list(pts);

	auto&& out = postscript("similarity.eps", pts0, pts1, pts, Spts);

	out << "/dot {gsave newpath";
	out << "1.5 0 360 arc gsave 0 0 0 setrgbcolor fill grestore ";
	out << "gsave 0.1 setlinewidth 0 0 0 setrgbcolor stroke grestore";
	out << "closepath grestore";
	out << "} bind def";
	
	out.set_color(1.0, 0.0, 0.0);
	out << pts0.front() << "moveto";
	for(Point p: pts0)
		out << p << "lineto";
	out << "closepath stroke";
	out << pts.front() << "moveto";
	for(Point p: pts)
		out << p << "lineto";
	out << "closepath stroke";
	
	out.set_color(0.0, 0.0, 1.0);
	out << pts1.front() << "moveto";
	for(Point p: pts1)
		out << p << "lineto";
	out << "closepath stroke";
	out << ptsX.front() << "moveto";
	for(Point p: ptsX)
		out << p << "lineto";
	out << "closepath stroke";

	for(Point p: pts0)
		out << p << "dot";

	for(Point p: pts1)
		out << p << "dot";

	
	Point c0 = pts[0];
	Point c1 = ptsX[0];

	out << c0 << "moveto";
	out << c1 << "lineto stroke";
	int n = 30;
	for(int i = 0; i < n; ++i){
		double alpha = i/(n-1.0);
		auto Sa = S.intermediate(alpha, c0);
		PointList Sapts = Sa.morph_list(pts);
	
		auto Sia = S.inverse().intermediate(1.0-alpha, c1);
		PointList Siapts = Sia.morph_list(ptsX);

		for(int k = 0; k < 4; ++k){
			out << Sapts[k] << "moveto";
			out << Siapts[k] << "lineto stroke";
			out << loc::linerp(alpha, Sapts[k], Siapts[k]) << "dot";
		}
	}
}

int main(int argc, const char* argv[]){
	BezierPath c1a{
		{
		{117, 148},
		{194, 165 },
		{228, 182 },
		{309, 189 }
		}
	};
	BezierPath c2a{
		{
		{119, 158},
		{171, 168}, 
		{197, 164}, 
		{250, 155}
		}
	};
	BezierPath c2b{
		{
		{250, 155},
		{202, 150},
		{178, 142}, 
		{132, 126}
		}
	};

	auto d1 = distance_info(c1a, c2a);
	auto d2 = distance_info(c1a, c2b);

	std::cout << "d1 = " << d1.dist << '\n';
	std::cout << "d2 = " << d2.dist << '\n';
}
