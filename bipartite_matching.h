#ifndef DSR_BIPARTITE_MATCHING_H
#define DSR_BIPARTITE_MATCHING_H

#include <vector>
#include "loc.h"

/// Namespace for bipartite matching definitions..
namespace bipartite_matching{
	using Edge = loc::pair_of<int>;
	using Edges = std::vector<Edge>;

	/// A bipartite graph.
	class BipartiteGraph{
		public:
			/// Constructor.
			BipartiteGraph(size_t nu, size_t nv);

			/// Add an edge.
			void add_edge(int i, int j, double weight);

			/// Get the maximum_weigth_matching.
			Edges maximum_weight_matching();

		private:
			struct Vertex{
				/// index.
				int index;

				/// Flag.
				bool is_free;

				/// Last vertex in augmenting path.
				Vertex* last_vertex;

				/// Cost of augmenting path.
				double aug_cost;
			};
			
			struct WeightedEdge{
				int index;
				double weight;
			};


			/// A vertex from set U.
			struct UVertex : Vertex{
				/// Adjacent edges.
				std::vector<WeightedEdge> adj;
			};

			/// A vertex from set V.
			struct VVertex : Vertex{
				/// Adjacent edge.
				WeightedEdge adj;
			};
	
			/// Init vertices.
			void init_vertices();

			/// Find augmenting path and flip edges.
			bool augment();
	
			/// Extract matching.
			Edges extract_matching();

			/// Flip an edge connecting U vertex and V vertex.
			void flip_edge(UVertex& u, VVertex& v);

			/// Flip an edge connecting V vertex and U vertex.
			void flip_edge(VVertex& v, UVertex& u);

			/// Relax U vertices.
			bool relax_us();

			/// Relax V vertices.
			bool relax_vs();

			/// Init distance info.
			void init_distances();

			/// Relax vertices.
			void relax_vertices();
			
			/// Find VVertex with maximum augmenting path cost.
			VVertex* find_maximum_dist();

			/// Find augmenting path.
			Edges find_augmenting_path();

			/////////////////////////////////////////////////////
			// Parameters.

			/// Number of vertices of the first set.
			size_t nu;

			/// Number of vertices of the second set.
			size_t nv;

			/// List of U vertices.
			std::vector<UVertex> us;

			/// List of V vertices.
			std::vector<VVertex> vs;

	};

}
#endif // DSR_BIPARTITE_MATCHING_H

