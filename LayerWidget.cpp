#include "LayerWidget.h"

#include "AnimationEditor.h"
#include <iostream>

using loc::to_string;
using std::function;

QImage curved_arrow;
QImage plus_sign;
QImage open_eye;

/// Constructor.
LayerWidget::LayerWidget(AnimationEditor* ed):
	QWidget{ed}, editor{ed}
{
	menu = new QMenu(this);
	setup_menu();

	const double s = 0.5;
	/// Width of the label blocks.
	w_label = width();

	/// Width of the shift of the sub layers blocks.
	w_shift = 20*s;

	/// Width reserved for the plus sign.
	w_plus = 30*s;

	// width reserved for the arrow.
	w_arrow = 50*s;

	/// Vertical size of label blocks.
	h_label = 40*s;

	/// Vertical separation between label blocks.
	h_sep = 4*s;

	/// Separation between text and blocks.
	text_sep = 10*s;

	/// Background color of base layer.
	color_bg_base.setHsl(70, 150, 100);

	/// Background color of sub layers.
	color_bg_sublayer.setHsl(70, 150, 150);

	/// Background color of 'add layer' button.
	color_bg_add_layer.setHsl(70, 150, 200);

	/// Color of selected layer.
	color_selected.setHsl(25, 255, 150);

	// Read images
	curved_arrow = QImage{":/images/curved_arrow.svg"};
	plus_sign = QImage{":/images/plus_sign.svg"};
	open_eye = QImage{":/images/eye.svg"};

	text_editor = new QLineEdit{"", this};
	text_editor->setVisible(false);
	connect(
		text_editor,
		&QLineEdit::returnPressed,
		this,
		&LayerWidget::set_layer_name
	);

	update_data();
}

/// Resize event.
void LayerWidget::resizeEvent(QResizeEvent* event){
	w_label = width();
	update_data();
}

/// Create the menu items.
void LayerWidget::setup_menu(){
	auto add_item = [&](
			const char* str,
			function<void()> method
		){

			auto action = menu->addAction(tr(str));
			connect(action, &QAction::triggered, method);

			this->addAction(action);

			return action;
		};

	add_item(
		"Move selection to this layer", 
		[&]{ 
			auto& selection = editor->get_selection();
			selection.selection_to_clipboard();
			selection.paste_clipboard();
			editor->repaint();
		}
	);
	
	add_item(
		"Move layer up", 
		[&]{ 
		}
	);
}

/// Paint event.
void LayerWidget::paintEvent(QPaintEvent* event){
	QPainter painter;
	painter.begin(this);

	// Keep the current layer index.
	auto sel_index = editor->get_viewer().current_layer_index();


	painter.setRenderHint(QPainter::Antialiasing, true);

	QPen pen{Qt::black};
	painter.setPen(pen);

	QColor color = color_bg_base;

	auto i = 0u;
	for(const auto& item: layer_items){
		auto layer = item.layer;
		auto rect = item.rect;

		QColor paint_color = (layer == nullptr)? color_bg_add_layer:
				(layer->is_visible())? color: Qt::lightGray;

		if(i == sel_index)
			paint_color = color_selected;

		painter.fillRect(rect, paint_color);

		auto text = QString::fromUtf8(
			layer != nullptr? 
				layer->get_name().c_str(): 
				"Add Layer"
		);

		double x = rect.x();
		double y = rect.y();

		if(layer == nullptr){
			double dx = 0.5*(h_label - w_plus);

			painter.drawImage(
				QRectF{x+dx, y+dx, w_plus, w_plus},
				plus_sign
			);

			painter.drawText(
				x + text_sep+h_label,
				y + h_label - text_sep,
				text
			);
		}else{
			painter.drawText(
				x + text_sep,
				y + h_label - text_sep,
				text
			);
	
			auto& viewer = editor->get_viewer();
			size_t f = viewer.current_frame_index();
			Drawing* d = viewer.get_drawing(f, i);

			auto& rect = circle_rects[i];

			if(d != nullptr)
				painter.drawText(
					rect.x() - rect.width(),
					y + h_label - text_sep,
					to_string(d->depth()).c_str()
				);


			if(layer->is_visible())
				painter.drawImage(circle_rects[i], open_eye);
			else
				painter.drawArc(circle_rects[i], 0, 360*16);
		}

		if(i == 0)
			color = color_bg_sublayer;
		++i;
	}

	painter.end();
}

/// Mouse press event.
void LayerWidget::mousePressEvent(QMouseEvent* event){
	auto& viewer = editor->get_viewer();
	auto n_layers = viewer.n_layers();

	auto func = [&](const QRect& r){
		return r.contains(event->pos());
	};

	auto funcF = [&](const QRectF& r){
		return r.contains(event->pos());
	};

	if(text_editor->isVisible() == false){
		auto b = circle_rects.begin();
		auto e = circle_rects.end();
		auto it = find_if(b, e, funcF);

		if(it != e){
			auto i = it - b;

			auto& layer = viewer.get_layer(i);
			layer.set_visibility(!layer.is_visible());

			editor->repaint();

			return;
		}
	}

	if(text_editor->isVisible())
		set_layer_name();

	auto bl = layer_items.begin();
	auto el = layer_items.end();

	auto itl = find_if(
		bl, el,
		[&](const LayerItem& item) {
			return func(item.rect);
		}
	);

	if(itl != el){
		size_t i = itl - bl;

		if(i < n_layers){
			viewer.set_current_layer(i);
			editor->repaint();

			if(event->button() == Qt::MiddleButton){
				editor->get_selection().select_none();
				if(i == 0)
					editor->goto_parent_base_layer();
				else
					editor->set_base_layer();
			}else if(event->button() == Qt::RightButton)
				menu->exec(QCursor::pos());

		}else if(event->button() != Qt::MiddleButton){
			viewer.add_layer("layer " + to_string(i));
			update_data();
		}

		editor->repaint();
	}
}

/// Define current layer name from text_editor.
void LayerWidget::set_layer_name(){
	if(text_editor->isVisible() == false)
		return;
	text_editor->setVisible(false);
	QString name = text_editor->text();

	editor->get_viewer().current_layer().set_name(name.toStdString());

	editor->setFocus();
}

/// Mouse move event.
void LayerWidget::mouseDoubleClickEvent(QMouseEvent* event){
	if(event->button() != Qt::LeftButton)
		return;

	auto& viewer = editor->get_viewer();
	auto n_layers = viewer.n_layers();

	auto func = [&](const QRect& r){
		return r.contains(event->pos());
	};

	auto funcF = [&](const QRectF& r){
		return r.contains(event->pos());
	};

	{
		auto b = circle_rects.begin();
		auto e = circle_rects.end();
		auto it = find_if(b, e, funcF);

		if(it != e){
			size_t i = it - b;

			auto& layer = viewer.get_layer(i);
			layer.set_visibility(!layer.is_visible());

			editor->repaint();

			return;
		}
	}

	auto b = layer_items.begin();
	auto e = layer_items.end();

	auto it = find_if(
		b, e,
		[&](const LayerItem& item) {
			return func(item.rect);
		}
	);

	if(it == e)
		return;

	size_t i = it - b;

	if(i >= n_layers)
		return;

	auto& layer = viewer.get_layer(i);

	text_editor->setVisible(true);
	text_editor->setGeometry(it->rect);
	text_editor->setText(layer.get_name().c_str());
}

/// Prepare data.
void LayerWidget::update_data(){
	text_editor->setVisible(false);
	layer_items.clear();
	circle_rects.clear();

	auto& viewer = editor->get_viewer();
	auto n_layers = viewer.n_layers();

	int w = w_label;
	int h = h_label;

	int x = 0;
	int y = this->height()-h;

	double dy = -h_label-h_sep;

	double radius = 0.8*h_label;
	double dx = .5*(h_label-radius);
	QRectF rect_circle{w_label-h_label+dx, y+dx, radius, radius};

	for(auto i = 0u; i < n_layers; ++i){
		LayerItem item;
		item.layer = &viewer.get_layer(i);
		item.rect = {x, y, w, h};
		layer_items.push_back(item);

		circle_rects.push_back(rect_circle);

		rect_circle.translate(0, dy);
		y += dy;

		if(i == 0){
			x = w_shift;
			w -= x;
		}
	}
	layer_items.push_back({nullptr, {x, y, w, h}});
}

