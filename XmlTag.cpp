#include "XmlTag.h"
#include "loc.h"

using namespace loc::literals;
using std::string;
using std::runtime_error;
using std::vector;
using std::ostream;
using std::istream;

/// Get a property with given name.
auto XmlTag::find_property(const string& name) const -> const Property*{
	auto it = loc::find_if(
		properties,
		[&](const Property& p){
			return p.first == name;
		}
	);

	return (it == properties.end())? nullptr: &*it;
}

/// Get data from input stream.
istream& operator>>(istream& is, XmlTag& xmltag){

	auto read_char = [&](char ec, bool ig_spaces){
		if(ig_spaces)
			loc::ignore_spaces(is);
		char c = is.get();
		if(c != ec)
			throw runtime_error{
				"unexpected: "_s + c +
				"expected: "_s + ec
			};
	};
	auto read_str = [&](const string& estr){
		const auto n = estr.size()+1;
		vector<char> ss(n);
		char* s = ss.data();
		is.get(s, n);
		if(estr != s)
			throw runtime_error{
				"unexpected: \""_s + s +
				"\" expected: \""_s  +
				estr + "\""_s
			};
	};

	read_char('<', true);

	// Handle comment.
	if(is.peek() == '!'){
		is.ignore();

		bool is_comment = (is.peek() == '-');

		if(is_comment)
			read_str("--");

		auto& comment = xmltag.tag;

		if(is_comment){
			comment = "!--";

			bool finished = false;
			while(!finished){
				string next;
				getline(is, next, '-');

				//cout << "reading " << next << '\n';
				comment += next;
				if(is.peek() != '-')
					comment += "-";
				else
					finished = true;
			};

			//cout << "comment = " << comment << '\n';
			read_str("->");

			comment += "--";
		}else{
			string next;
			getline(is, next, '>');
		}

		return is;
	}

	auto& tag = xmltag.tag;
	loc::ignore_spaces(is);
	char c = is.get();
	char end_c = (c == '?')? '?': '/';
	do{
		tag.push_back(c);
		c = is.get();
	}while(c != '>' and c != end_c and !isspace(c));

	if(!isspace(c))
		is.putback(c);
	else{
		while(isspace(is.peek()))
			is.ignore();
		c = is.peek();
	}

	//cout << "reading tag " << xmltag.tag << '\n';

	while(c != end_c and c != '>'){
		string name, value;
		loc::ignore_spaces(is);
		getline(is, name, '=');
		while(isspace(name.back()) and name.empty() == false)
			name.pop_back();

		read_char('\"', true);

		getline(is, value, '\"');

		xmltag.properties.emplace_back(name, value);
		//cout << "read property " << name << " = " << value << '\n';

		loc::ignore_spaces(is);
		c = is.peek();
	}

	if(c == end_c){
		is.ignore();
		read_char('>', false);
		return is;
	}else{
		if(is.peek() == '>')
			is.ignore();

		string end_block = "</" + tag + ">";
		int n = end_block.size();
		vector<char> chars(n+1);
		char* next = &chars[0];

		loc::ignore_spaces(is);
		is.get(next, n+1, '\0');

		//cout << "next = " << next << '\n';

		while(end_block != next and is.good()){
			for(int i = n-1; i >= 0 ; --i)
				is.putback(next[i]);

			loc::ignore_spaces(is);
			if(is.peek() == '<'){
				xmltag.subtags.emplace_back();
				is >> xmltag.subtags.back();
			}else{
				getline(is, xmltag.text, '<');
				//cout << "text = " << xmltag.text << '\n';
				is.putback('<');
			}

			loc::ignore_spaces(is);
			is.get(next, n+1, '\0');

			//cout << "next = " << next << '\n';
		}
	}

	return is;
}

/// Write data to output stream.
ostream& operator<<(ostream& os, const XmlTag& xmltag){
	const auto& t = xmltag.tag;
	os << "<" << t;

	char c = !t.empty()? t[0]: '/';

	if(c != '!' and c != '?' and c != '/')
		c = '/';

	if(c != '!')
		os << '\n';

	for(auto& p: xmltag.properties)
		os << p.first << "=\"" << p.second << "\"\n";

	if(!xmltag.text.empty()){
		os << ">\n";
		os << xmltag.text << '\n';
		os << "</" << t << ">\n";
	}else if(xmltag.subtags.empty()){
		if(c != '!'){
			os << c;
		}
		os << ">\n";
	}else{
		os << ">\n";
		for(const auto& subtag: xmltag.subtags)
			os << subtag;
		os << "</" << t << ">\n";
	}
	return os;
}
