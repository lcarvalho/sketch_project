#ifndef INTERPOLATOR_H
#define INTERPOLATOR_H

#include <vector>
#include <set>

#include "Drawing.h"
#include "Morph.h"
#include "mffd.h"
#include "SimilarityMorph.h"
#include "RegionMixer.h"
#include "CurveMixer.h"

class Interpolator;

struct InbetweenInfo;
using InbetweenInfo_up = std::unique_ptr<InbetweenInfo>;

using Mffd = MFFD<4, 10, 10>;
using Mffd_sp = std::shared_ptr<Mffd>;

struct CorrespInfo;

struct BezierBool;

struct RegionInfo;

using CurvePair = loc::pair_of<BezierPath_sp>;
using CurvePairList = std::vector<CurvePair>;

using BezierBoolVector = std::pair<BezierPathList, std::vector<bool>>;

/// Interpolate two drawings..
class Interpolator{
	public:
		/// Constructor.
		Interpolator(Drawing& d0, Drawing& d1);

		/// Calculate the morph from d0 to d1.
		SimilarityMorph_sp calculate_morph(
			const SimilarityMorph_sp& base_morph = nullptr
		);

		/// Calculate the morphed curves from drawing d0;
		void calculate_auxiliary_curves();

		/// Create a new curve applying morph
		/// to original control points.
		BezierPath morphed_curve(const BezierPath& curve);

		/// Set up inbetween information.
		bool get_inbetween_info(double tolerance);

		/// Calculate correspondences between regions.
		void get_region_correspondences(
			const CurvePairList& curve_corresp
		);

		/// Mix objects with parameter alpha.
		loc::pair_of<SimilarityMorph> mix_objects(
			Drawing& drawing, double alpha,
			const SimilarityMorph& Sp,
			const SimilarityMorph& Spia
		);

		/// Mix two regions
		RegionMixer_up region_mixer(
			const Region& region1,
			const Region& region2,
			const CurvePairList& curve_corresp
		);


		/// Rearrange guidelines to make the best matching.
		void match_guidelines();

		/// Add forbidden pair of curves, where second curve is b.
		void add_forbiden(const BezierPath& b);

		struct SampleInfo{
			PointList pts0;
			PointList pts1;
			int index0;
			int index1;
		};
	private:
		/// Recreate vector of forbidden curves removing curves that don't exist anymore.
		void update_forbidden_curves();

		/// Get the set of forbidden pairs of curves.
		std::set<loc::pair_of<int>> get_forbidden_indices();

		/// Get sample points from the guidelines according
		/// to the matching.
		std::vector<SampleInfo> get_sampled_guidelines();

		void update_sampled_guidelines(
			double w,
			std::vector<SampleInfo>& samples
		);

		/// Get the correspondence between curves.
		void correspondences(
			const BezierPathList& curves1,
			const BezierPathList& curves2,
			double tolerance
		);

		/// Sort curve mixers according to the index of their curves.
		void sort_curve_mixers();

		/// First drawing.
		Drawing& d0;

		/// Second drawing.
		Drawing& d1;

		/// MFFD deformation.
		Mffd_sp mffd;

		/// Morph between drawings.
		Morph_sp morph;

		/// Similarity morph between drawings.
		SimilarityMorph S;

		/// Inbetween information.
		InbetweenInfo_up inbetween;

		/// List of matching guidelines.
		loc::vector_pair<int> matching_guidelines;

		/// Orientations used when matching guidelines.
		std::vector<bool> guidelines_orientations;

		/// Set of forbidden pairs of curves.
		loc::vector_pair<BezierPath_wp> forbidden_curves;
	
		// Morphed curves from frame 0.
		BezierPathList Hsc0;
};

/// Data about a curve correspondence.
struct CorrespInfo{
	/// Index from first list of curves.
	int d0;

	/// Index from second list of curves.
	int d1;

	/// Flag indicating if the two curves should be used in forward orientation.
	bool forward;
};

/// Data used in inbetweening.
struct InbetweenInfo{
	/// Subcurves from curves1.
	BezierPathList sc0;

	/// Subcurves from curves2.
	BezierPathList sc1;

	/// List of curve correspondences.
	std::vector<CorrespInfo> corresp;
	std::vector<CurveMixer_sp> curve_mixers;
	
	std::vector<CurveMixer_sp> guideline_mixers;

	/// List of region correspondencens.
	std::vector<RegionMixer_up> region_mixers;
};

#endif
