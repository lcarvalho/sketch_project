#ifndef NODE_LAYER_H
#define NODE_LAYER_H

#include "Node.h"
#include "Layer.h"
#include <set> 

/// Ostream operator.
template<class Op>
std::ostream& output(std::ostream& out, const Node<Layer>& node, Op op){
	const Layer& layer = node;
	output(out, layer, op);

	const auto& children = node.get_children();

	// Write children data.
	out << children.size() << '\n';
	for(const auto& c: children)
		output(out, c, op);

	return out;
}

/// Read data from json.
inline Node<Layer> input_json(const json11::Json& json, std::set<size_t>& keyframes){
	using namespace json11;

	Node<Layer> node;
	Layer& layer = node;

	layer = {json["node"], keyframes};

	if(!json["children"].is_null())
		for(auto& c: json["children"].array_items())
			node.add_child() = input_json(c, keyframes);

	return node;
}

/// Ostream operator.
template<class Op>
json11::Json output_json(const Node<Layer>& node, Op op){
	using namespace json11;

	const Layer& layer = node;

	Json::array children;
	for(const auto& c: node.get_children())
		children.push_back(output_json(c, op));

	return Json::object{
		{"node", output_json(layer, op)},
		{"children", children}
	};
}



/// Return layer status.
inline std::pair<size_t, size_t> status(const Node<Layer>& node){
	auto p = node.status();

	for(auto& child: node.get_children()){
		auto pc = status(child);
		p.first += pc.first;
		p.second += pc.second;
	}

	return p;
}

/// Get a list with all guidelines at frame f.
inline BezierPathList get_all_guidelines(const Node<Layer>& node, size_t f){
	if(!node.is_visible())
		return {};

	auto res = node.get_all_guidelines(f);

	for(const auto& child: node.get_children())
		loc::insert_back(res, get_all_guidelines(child, f));

	return res;
}

/// Get a list with all auxiliary curves at frame f.
inline BezierPathList get_all_auxiliary_curves(const Node<Layer>& node, size_t f){
	if(!node.is_visible())
		return {};
	
	auto res = node.get_all_auxiliary_curves(f);

	for(const auto& child: node.get_children())
		loc::insert_back(res, get_all_auxiliary_curves(child, f));

	return res;
}

/// Get a list with all auxiliary curves at frame f.
inline std::vector<BezierPathList> get_all_clusters(const Node<Layer>& node, size_t f){
	if(!node.is_visible())
		return {};
	
	auto res = node.get_all_clusters(f);

	for(const auto& child: node.get_children())
		loc::insert_back(res, get_all_clusters(child, f));

	return res;
}

/// Calculate morph from frame f0 to frame f1.
inline void calculate_morph(
		Node<Layer>& node,
		size_t f0, size_t f1,
		const SimilarityMorph_sp& base_morph = nullptr
	){

	auto morph = node.calculate_morph(f0, f1, base_morph);

	for(auto& child: node.get_children())
		calculate_morph(child, f0, f1, morph);
}

/// Mix object from frame f_start to f_end at frame f.
inline void mix_objects(
	Node<Layer>& node,
	size_t f_start, size_t f_end, size_t f, size_t f_end_actual,
	std::function<double(double)> speed,
	const SimilarityMorph& Sp = {},
	const SimilarityMorph& Spia = {}
){
	SimilarityMorph SSp, SpiaSia;
	std::tie(SSp, SpiaSia) = node.mix_objects(f_start, f_end, f, f_end_actual, speed, Sp, Spia);

	for(auto& child: node.get_children())
		mix_objects(child, f_start, f_end, f, f_end_actual, speed, SSp, SpiaSia);
}

/// Mix object from all frames between f_start and f_end.
inline void mix_objects(Node<Layer>& node, size_t f_start, size_t f_end, size_t f_end_actual, std::function<double(double)> speed){
	for(auto f = f_start+1; f < f_end; ++f)
		mix_objects(node, f_start, f_end, f, f_end_actual, speed);
}

/// Get auxiliary curves transforming curves from keyframe f0 to the keyframe f1.
inline void calculate_auxiliary_curves(Node<Layer>& node, size_t f0, size_t f1){
	node.calculate_auxiliary_curves(f0, f1);

	for(auto& child: node.get_children())
		calculate_auxiliary_curves(child, f0, f1);
}

/// Update inbetween info.
inline void update_inbetween_info(Node<Layer>& node){
	node.update_inbetween_info();

	for(auto& child: node.get_children())
		update_inbetween_info(child);
}

/// Get a list with all the control points of this layer and sublayers.
inline PointList all_control_points(const Node<Layer>& node){
	auto pts = node.all_control_points();

	for(const auto& child: node.get_children())
		loc::insert_back(pts, all_control_points(child));

	return pts;
}

/// Get the minimum axis aligned bounding box of all frames.
inline Point2 axis_aligned_bb(const Node<Layer>& node){
	return axis_aligned_bounding_box(all_control_points(node));
}

/// Istream operator.
inline std::istream& operator>>(std::istream& in, Node<Layer>& node){
	Layer& layer = node;
	in >> layer;

	// Read number of children.
	size_t nc = 0;
	in >> nc;
	for(size_t i = 0; i < nc; ++i)
		in >> node.add_child();

	return in;
}
#endif
