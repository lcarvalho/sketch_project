#include "Animation.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include "SVGReader.h"
#include "SVGWriter.h"
#include "PostScriptWriter.h"
#include "AnimationViewer.h"
#include "NodeLayer.h"

using std::string;
using std::function;
using std::ostream;
using std::ostringstream;

////////////////////////////////////////////////////////////////////////////////
/// Constructor.
/**
* Constructs an Animation object with the specified number of frames.
*/
Animation::Animation(size_t _n_frames) : total_frames(_n_frames){
	set_keyframe(0);
}

/// Constructor from file.
Animation::Animation(const string& filename) :
	Animation{loc::read_file(filename)}
{}

/// Update all auxiliary drawings.
void Animation::update_all_auxiliary(){
	auto viewer = get_viewer();

	for(auto i: keyframe_set){
		viewer.set_current_frame(i);
		viewer.update_auxiliary_curves(true);
		viewer.update_auxiliary_curves(false);
	}
}

/// Clear interpolated frames.
void Animation::clear_interpolated_frames(){
	size_t n = n_frames();
	for(size_t f = 0; f < n; ++f){
		if(is_keyframe(f))
			continue;

		root_layer.apply(
			[&](Layer& l){
				*l.get_drawing(f) = {};
			}
		);
	}
}
	
/// Clear current inbetween info.
void Animation::clear_inbetween_info(){
	root_layer.apply(
		[&](Layer& l){
			l.clear_inbetween_info();
		}
	);
}

/// Set or unset frame i as a keyframe.
void Animation::set_keyframe(size_t f){
	bool kf = !is_keyframe(f);
	set_keyframe(f, kf);

	size_t next_f = f;

	// If it is marked as a keyframe.
	if(!kf){
		// Remove auxiliary from current frame (not a keyframe anymore).
		root_layer.apply(
			[&](Layer& l){ 
				l.clear_auxiliary_curves(f); 
			}
		);

		// Update auxiliary curves from next keyframe (if any).
		next_f++;
		if(!next_keyframe(next_f))
			return;
	}
	
	size_t prev_f = f;
	if(!prev_keyframe(prev_f))
		return;

	root_layer.apply(
		[&](Layer& l){
			l.calculate_auxiliary_curves(prev_f, next_f);
		}
	);
}

/// Preprocessing frames.
void Animation::preprocessing(){
	clear_empty_curves();

	clear_interpolated_frames();

	clear_inbetween_info();

	update_proportions();

	calculate_morphs();

	calculate_auxiliary_curves();
}
	
/// Update all curve proportions.
void Animation::update_proportions(){
	root_layer.apply(
		[](Layer& l){
			l.update_proportions();
		}		
	);
}

string Animation::status() const{
	ostringstream out;

	// Layers.
	out << " & " << root_layer.total_children()+1;

	// Guidelines.
	auto p = ::status(root_layer);
	out << " & " << p.first;

	// Drawables.
	out << " & " << p.second;

	// Keyframes.
	size_t nf = keyframe_set.size();
	out << " & " << nf;

	// Intermediate frames.
	out << " & " << last_keyframe_index()+1-nf;

	// Threshold.
	out << " & " << root_layer.get_tolerance()*100 << "\\%";

	return out.str();
}

/// Calculate the whole animation by inbetweening keyframes.
void Animation::generate_animation(){
	if(keyframe_set.size() < 2)
		return;

	preprocessing();
	update_inbetween_info(root_layer);

	apply_to_keyframes(
		[&](size_t f0, size_t f1){
			size_t f1a = (cyclic and f1 == last_keyframe_index())? 
				first_keyframe_index():
				f1; 

			mix_objects(root_layer, f0, f1, f1a, get_speed(f0));
		}
	);
}
	
/// Get the speed control function for keyframe f.
std::function<double(double)> Animation::get_speed(size_t f){
	auto it = speed_controllers.find(f);

	if(it == speed_controllers.end())
		return [](double t){ return t; };

	return it->second.function();
}

/// Get the speed controller for keyframe f, if any
SpeedControl* Animation::get_speed_controller(size_t f){
	if(
		(keyframe_set.size() == 1) or
		(f < first_keyframe_index()) or
		(f > last_keyframe_index())
	)
		return nullptr;

	// go to previous keyframe.
	if(f != first_keyframe_index())
		prev_keyframe(f);

	auto it = speed_controllers.find(f);

	if(it == speed_controllers.end())
		return nullptr;

	return &it->second;
}

/// Get auxiliary curves from the last keyframe to the current keyframe.
void Animation::calculate_auxiliary_curves(){
	apply_to_keyframes(
		[&](size_t f0, size_t f1){
			if(cyclic and f1 == last_keyframe_index())
				f1 = first_keyframe_index();
			::calculate_auxiliary_curves(root_layer, f0, f1);
		}
	);
}

/// Calculate all morphs.
void Animation::calculate_morphs(){
	apply_to_keyframes(
		[&](size_t f0, size_t f1){
			if(cyclic and f1 == last_keyframe_index())
				f1 = first_keyframe_index();
			calculate_morph(root_layer, f0, f1);
		}
	);
}

/// Save the animation into svg file.
void Animation::save_animation_svg(const string& filename){
	auto bb = axis_aligned_bb();
	double x0 = bb[0][0];
	double y0 = bb[0][1];
	double x1 = bb[1][0];
	double y1 = bb[1][1];
	SVGWriter out{filename, x0, y0, x1-x0, y1-y0};

	double fps = 30;

	double dt = 1.0/fps;

	// group id.
	int gid = 0;

	size_t fn = last_keyframe_index();
	for(auto f = 0u; f <= fn; ++f){
		if(is_empty_frame(f))
			continue;

		out.create_group(gid, false);

		for(auto& d: get_all_drawables(f))
			out.create(*d);

		out.end_group();
		++gid;
	}

	out.set_visibility(gid-1, dt);
}

/// Save the animation into eps file.
void Animation::save_eps(const string& filename){
	const auto bb  = axis_aligned_bb();
	const Point& p0 = bb[0];
	const Point d = bb[1] - p0;
	auto n = last_keyframe_index()+1;

	PostScriptWriter out{filename, p0[0], p0[1], n*d[0], d[1]};

	for(auto f = 0u; f < n; ++f){
		out.box(bb);

		for(auto& d: get_all_drawables(f))
			out << *d;

		out.translate(d[0], 0.0);
	}
}

/// Check if frame i is empty.
bool Animation::is_empty_frame(size_t f) const{
	bool empty = true;
	
	root_layer.apply(
		[&](const Layer& l){
			auto d = l.get_drawing_const(f);
			if(d != nullptr and d->empty() == false)
				empty = false;
		}

	);

	return empty;
}

/// Get the minimum axis aligned bounding box of all frames.
Point2 Animation::axis_aligned_bb() const{
	return ::axis_aligned_bb(root_layer);
}

/// Get an animation viewer bound to this animation.
AnimationViewer Animation::get_viewer(){
	return {*this};
}
	
/// Apply operation to every pair of keyframes.
void Animation::apply_to_keyframes(function<void(size_t, size_t)> f){
	auto b = keyframe_set.begin();
	auto e = keyframe_set.end();
	for(auto it = b; it != e; ++it){
		auto n = next(it);
		if(n != e)
			f(*it, *n);
	}
}

/// Output stream operator.
ostream& operator<<(ostream& out, const Animation& a){
	return output(out, a.root_layer, [&](size_t f){ return a.is_keyframe(f); });
}

/// Constructor form json object.
Animation::Animation(const json11::Json& json){
	cyclic = json["cyclic"].bool_value();

	std::set<size_t> keyframes;

	root_layer = input_json(json, keyframes);
	
	for(auto k: keyframes)
		set_keyframe(k);

	for(auto x: json["speed_control"].object_items()){
		size_t i = std::atol(x.first.c_str());
		SpeedControl& sp = speed_controllers[i];
		sp.get_p1() = x.second["p1"];
		sp.get_p2() = x.second["p2"];
	}


	auto json_tol = json["tolerance"];
	if(json_tol.is_number())
		set_tolerance(json_tol.number_value());
	
	update_all_auxiliary();
}

/// Convert to Json object.
json11::Json Animation::to_json() const{
	auto out = output_json(root_layer, [&](size_t f){ return is_keyframe(f); });

	return json11::Json::object{
			{"cyclic", is_cyclic()},
			{"node", out["node"]},
			{"children", out["children"]},
			{"speed_control", get_speed_json()},
			{"tolerance", root_layer.get_tolerance()}
		};
}
	
/// Convert speed_controllers map to json.
json11::Json Animation::get_speed_json() const{
	json11::Json::object out;

	for(const auto& p: speed_controllers)
		out.insert({loc::to_string(p.first), p.second});

	return out;
}

/// Get all drawables of frame f.
DrawableList Animation::get_all_drawables(size_t f){
	auto viewer = get_viewer();
	
	DrawableList res;
	for(const auto& d: viewer.get_visible_drawings(root_layer, f))
		loc::insert_back(res, d->get_drawables());
	
	return res;
}
	
/// Insert frame at position i.
void Animation::insert_frame(size_t i){
	root_layer.apply(
		[&](Layer& l){
			l.insert_frame(i);
		}
	);
	
	std::set<size_t> new_keyframe_set;
	std::map<size_t, SpeedControl> new_speed_controllers;

	for(size_t k: keyframe_set){
		size_t nk = k>=i? k+1: k;
		new_keyframe_set.insert(nk);
		new_speed_controllers[nk] = std::move(speed_controllers[k]);
	}

	keyframe_set = move(new_keyframe_set);
	speed_controllers = move(new_speed_controllers);

	++total_frames;
}

/// Remove frame at position i.
void Animation::remove_frame(size_t i){
	root_layer.apply(
		[&](Layer& l){
			l.remove_frame(i);
		}
	);

	set_keyframe(i, false);

	std::set<size_t> new_keyframe_set;
	std::map<size_t, SpeedControl> new_speed_controllers;

	for(size_t k: keyframe_set){
		size_t nk = k>i? k-1: k;
		new_keyframe_set.insert(nk);
		new_speed_controllers[nk] = std::move(speed_controllers[k]);
	}

	keyframe_set = move(new_keyframe_set);
	speed_controllers = move(new_speed_controllers);
	

	--total_frames;
}
