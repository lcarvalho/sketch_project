#ifndef XML_H
#define XML_H

#include "XmlTag.h"

/// Data from xml.
class Xml{
	/// List of tags in the xml.
	std::vector<XmlTag> tags;

	public:

	/// Return the xml tags.
	const std::vector<XmlTag>& get_tags() const;

	/// Read data from input stream.
	friend std::istream& operator>>(std::istream& is, Xml& xmltag);

	/// Write data to output stream.
	friend std::ostream& operator<<(std::ostream& os, const Xml& xmltag);
};

/// Return the xml tags.
inline const std::vector<XmlTag>& Xml::get_tags() const{
	return tags;
}

#endif
