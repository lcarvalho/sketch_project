\documentclass[11pt, a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage[brazil]{babel}
\usepackage[a4paper]{geometry}

%\usepackage{subimages}
%\usepackage{subfigure}
%\setfigdir{imgs}
%\usepackage{amsmath, amssymb}

%\usepackage[lined,boxed,commentsnumbered]{algorithm2e}

\usepackage{hyperref}

\usepackage{graphicx}
%\usepackage[numbers]{natbib}

%\usepackage{palatino}

%\newcommand{\R}{\mathbb{R}}

%opening
\title{Inbetween semi-automático baseado em linhas-guias para animação 2D}
\author{Leonardo de Oliveira Carvalho}
                
%(a) introdução ao problema
%(b) motivação
%(c) avanço conseguidos
%(d) principais dificuldades até agora
%(e) estratégias para superar as dificuldades
%(f) primeiros resultados
\begin{document}
\maketitle

\section{Introdução}
Neste trabalho propomos um método semi-automático para geração de quadros intermediários
para animação 2D utilizando linhas guias para obter informação sobre a mudança de posição
do que está sendo animado.

Durante o processo de desenho, é comum os artistas usarem traços auxiliares que servem
para guiar o desenho, esses traços dão informações sobre proporções e alinhamento de
características do desenho, vamos chamar esses traços de \emph{linhas-guias}. 
A figura~\ref{goku} ilustra um exemplo de desenho em que 
foram utilizadas linhas-guias.

\begin{figure}[hbtp]
\includegraphics[width=.22\columnwidth]{imgs/Goku2a}
\includegraphics[width=.25\columnwidth]{imgs/Goku2b}
\includegraphics[width=.25\columnwidth]{imgs/Goku2c}
\includegraphics[width=.25\columnwidth]{imgs/Goku2d}
\caption{Exemplo de figura criada usando linhas-guias.}
\label{goku}
\end{figure}

Em animação 2D, é comum serem utilizados quadros-chaves, 
   que contêm objetos ou seres em posições distintas.
   Os outros quadros da animação são criados a partir destes quadros-chaves,
   este processo é conhecido como \emph{inbetween}, 
   e geralmente é feito manualmente, o que se torna uma tarefa bastante laboriosa.
   A figura~\ref{inbetween} mostra um exemplo de como é feito o inbetween manualmente numa animação 2D,
   na parte de cima podemos ver os quadros-chaves, e embaixo vemos os quadros intermediários desenhados
   para gerar a animação.
\begin{figure}[t]
\center
\includegraphics[width=.8\columnwidth]{imgs/a01}
\includegraphics[width=.8\columnwidth]{imgs/a03}
\caption{Exemplo de animação criada utilizando quadros-chaves.}
\label{inbetween}
\end{figure}

\section{Motivação}
Os métodos conhecidos de inbetween automático como \cite{Sugisaki:2009:SIC:1667146.1667156, Kort02computeraided, Fiore01automaticin-betweening} 
   geralmente tentam extrair in\-for\-ma\-ções da animação
a partir dos próprios desenhos, porém vemos que as linhas-guias também nos dão informações sobre 
a animação e possuem a vantagem de serem mais simples do que os desenhos finais. 
Esta observação nos leva a propor um método de inbetween que trabalha sobre as linhas-guias
para gerar dados sobre o movimento do que está sendo animado, 
e aplicar estes dados para criar os quadros intermediários.

\section{Método}
Para este trabalho, utilizamos um caso específico: 
animação de faces de personagens de desenho-animado,
utilizando um conjunto específico de linhas-guias.
Casos mais genéricos podem ser estudados em trabalhos futuros.

Em resumo o sistema funciona através dos passos:
\begin{enumerate}
\item O usuário cria as linhas guias do quadro inicial;
\item Com o auxílio das linhas guias, o usuário desenha a figura correspondente ao quadro inicial;
\item Ao traçar as linhas guias do quadro final, o sistema gera o esboço de desenho para o quadro final,
	de acordo com a mudança no formato das linhas guias;
\item Com o auxílio das linhas guias no quadro-final e do esboço gerado pelo sistema, o usuário 
desenha a figura do quadro final;
\item Uma vez que os dois quadros-chaves estejam prontos, 
	o sistema pode gerar os quadros intermediários.
\end{enumerate}

Todos os traços criados pelo usuário são formados por curvas de bézier cúbicas, 
      utilizando o método \emph{Efficient Curve Fitting} ~\cite{frisken2008}.

As linhas-guias utilizadas seguem o formato ilustrado na figura~\ref{guidelines},
   com um traço dividindo a face em duas partes na horizontal e outro na vertical.
   Porém o usuário é livre para criar outros traços para auxiliar durante o desenho,
   estes traços extras não são (até o momento) utilizados pelo algoritmo.
\begin{figure}[htbp]
\center
\includegraphics[width=.8\columnwidth]{imgs/guidelines}
\caption{Exemplo do formato de linhas-guias utilizadas.}
\label{guidelines}
\end{figure}

Com os traços das linhas-guias de um quadro, 
    são gerados quadriláteros utilizando os vértices (pontos iniciais e finais) 
	das duas linhas, o ponto de interseção delas, e mais quatro pontos gerados a partir dos demais,
	conforme ilustra a figura~\ref{grids}.
\begin{figure}[htbp]
\center
\includegraphics[width=.8\columnwidth]{imgs/grids}
\caption{Formação de quadriláteros a partir das linhas-guias.}
\label{grids}
\end{figure}

Com dois quadros-chaves são feitas correspondências entre estes quadriláteros
utilizando apenas a ordem com que foram feitos os traços.
Calcula-se então cada trans\-for\-ma\-ção que leva de um quadrilátero do primeiro quadro-chave
para seu correspondente no último quadro-chave. 
Como os quadriláteros são paralelogramos é suficiente calcular transformações afins,
     porém como estamos interessados em utilizar futuramente casos mais genéricos estamos 
     calculando as transformações projetivas que levam os quatro vértices de um quadrilátero para outro.

Aplicamos para cada ponto de controle dos desenhos no primeiro quadro uma das quatro transformações,
de acordo com o quadrante (definido pelos dois traços das linhas-guias) que contém este ponto.
A figura~\ref{transf} mostra uma transformação de um ponto de controle de uma curva do  primeiro para o segundo
quadro-chave.
\begin{figure}[htbp]
\center
\includegraphics[width=.8\columnwidth]{imgs/transf}
\caption{Transformação do primeiro para o segundo quadro-chave.}
\label{transf}
\end{figure}

Fazendo isto, teremos uma figura que serve como esboço para o desenho no segundo quadro-chave.
Além de guiar o usuário, estes traços transformados são úteis para o cálculo de correspondências
entre as figuras dos dois quadros-chaves, para isto primeiramente separamos cada bézier cúbica das curvas
desenhadas, isto é, tratamos separadamente cada traço definido por quatro pontos de controle.
Calculamos então as distâncias entre cada curva do primeiro e segundo quadro-chave.
Para calcular a distância entre duas curvas, fazemos uma amostragem uniforme de cada curva e somamos o
quadrado da distância entre cada ponto correspondente.
Tendo as distâncias fazemos correspondências entre as curvas mais próximas.
Estas correspondências são definidas apenas por índices, logo podemos usá-las entre
as curvas originais (sem transformação) do primeiro quadro-chave e as curvas do segundo quadro-chave.

Fazendo interpolações lineares entre os vértices dos quadriláteros gerados pelas curvas-guias
geramos quadriláteros de quadros intermediários. 
E para cada correspondência $(i,j)$ entre as curvas $c_i$ e $c_j$ dos quadros-chaves, 
  calculamos a curva $c = (1-\alpha)H_0c_i + \alpha H_1c_j$, 
  onde $H_0$ é a transformação entre o primeiro quadro-chave e o quadro atual
  (aplicando a transformação projetiva correspondente a cada ponto de controle da curva),
  $H_1$ é a transformação entre o segundo quadro-chave e o quadro atual,
  e $\alpha = \frac{k - k_0}{k_1 - k_0}$, onde $k_0$ é o índice do primeiro quadro-chave, 
  $k_1$ o índice do segundo quadro-chave, e $k$ o índice do quadro intermediário.
A figura~\ref{auto-frame} ilustra este processo.
\begin{figure}[htbp]
\center
\includegraphics[width=.8\columnwidth]{imgs/auto-frame}
\caption{Criação de um quadro intermediário.}
\label{auto-frame}
\end{figure}

Fazendo assim para todos os quadros intermediários, teremos uma transição entre o desenho
no primeiro e o segundo quadro-chave.


\section{Resultados}
As figuras abaixo mostram exemplos de resultados obtidos pelo método desenvolvido.
Vemos nas figuras~\ref{ed_kf}, \ref{a_kf} os quadros-chaves criados pelo usuário, 
      na parte esquerda vemos o primeiro quadro-chave, com as linhas guias em azul e o desenho em preto,
      na esquerda vemos o segundo quadro-chave, com o desenho, as linhas-guias e em cinza o esboço gerado pelo sistema
      a partir do primeiro quadro-chave.

Nas figuras~\ref{ed_out}, \ref{a_out} vemos os quadros intermediários gerados pelo sistema (o primeiro e último correspondem aos quadros-chaves).

\begin{figure}[htbp]
\center
\includegraphics[width=.3\columnwidth]{imgs/ed/ed_kf01}
\includegraphics[width=.3\columnwidth]{imgs/ed/ed_kf10}
\caption{Quadros-chaves criados pelo usuário.}
\label{ed_kf}
\end{figure}

\begin{figure}[htbp]
\includegraphics[width=.19\columnwidth]{imgs/ed/out0}
\includegraphics[width=.19\columnwidth]{imgs/ed/out1}
\includegraphics[width=.19\columnwidth]{imgs/ed/out2}
\includegraphics[width=.19\columnwidth]{imgs/ed/out3}
\includegraphics[width=.19\columnwidth]{imgs/ed/out4}

\includegraphics[width=.19\columnwidth]{imgs/ed/out5}
\includegraphics[width=.19\columnwidth]{imgs/ed/out6}
\includegraphics[width=.19\columnwidth]{imgs/ed/out7}
\includegraphics[width=.19\columnwidth]{imgs/ed/out8}
\includegraphics[width=.19\columnwidth]{imgs/ed/out9}
\caption{Resultado.}
\label{ed_out}
\end{figure}


\begin{figure}[htbp]
\center
\includegraphics[width=.3\columnwidth]{imgs/tenzin/kf1}
\includegraphics[width=.3\columnwidth]{imgs/tenzin/kf2}
\caption{Quadros-chaves criados pelo usuário.}
\label{a_kf}
\end{figure}

\begin{figure}[htbp]
\includegraphics[width=.19\columnwidth]{imgs/tenzin/out0}
\includegraphics[width=.19\columnwidth]{imgs/tenzin/out1}
\includegraphics[width=.19\columnwidth]{imgs/tenzin/out2}
\includegraphics[width=.19\columnwidth]{imgs/tenzin/out3}
\includegraphics[width=.19\columnwidth]{imgs/tenzin/out4}

\includegraphics[width=.19\columnwidth]{imgs/tenzin/out5}
\includegraphics[width=.19\columnwidth]{imgs/tenzin/out6}
\includegraphics[width=.19\columnwidth]{imgs/tenzin/out7}
\includegraphics[width=.19\columnwidth]{imgs/tenzin/out8}
\includegraphics[width=.19\columnwidth]{imgs/tenzin/out9}
\caption{Resultado.}
\label{a_out}
\end{figure}

Pode-se observar que os resultados são razoavelmente bons, 
	os traços são transformados de maneira suave,
	e a correspondência funciona bem na maior parte dos traços.

Um problema para o método é como tratar os traços em que não foram encontradas correspondências,
   vemos nas figuras que estes traços somem da animação nos quadros intermediários, 
   o que não é um efeito desejável.
   Outro problema a ser investigado é em relação à oclusão, 
   quando uma parte da figura é visível num quadro-chave mas não no outro, 
   pois foi escondida pelo próprio objeto,
   neste caso em geral os traços destas partes não encontram correspondentes, 
   e eles somem da animação.
Até o momento estes problemas podem ser resolvidos apenas através de edição manual, 
como eles normalmente aparecem em poucas partes da animação, o processo é simples
e mais fácil do que criar todos os quadros intermediários.
Esta etapa de ajustes manuais é essencial em qualquer método automático, 
     pois nem sempre o resultado corresponde ao desejado pelo animador.

\bibliographystyle{plain}
\bibliography{references}
\end{document}
