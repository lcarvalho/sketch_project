#ifndef FFD_H
#define FFD_H
#include <vector>
#include <unordered_map>
#include "Morph.h"
#include "PostScriptWriter.h"

/// Free Form Deformation class.
template<int M, int N>
class FFD : public Morph{
	public:
	////////////////////////////////////////////////////////////////////////
	/// Calculate the morph of the n points ps to qs.
	void get_morph(const PointList& ps, const PointList& qs);

	/// Apply morph to a single point.
	Point morph(const Point& c) const override;

	/// Adjust domain such that every point in pts is inside it.
	bool adjust_domain(const PointList& pts, bool recalculate_grid = true);

	/// Copy the domain from another FFD object.
	void set_domain(const FFD& ffd, bool recalculate_grid = true);

	/// Get the point at position i,j in the grid (const version).
	const Point& grid_at(int i, int j) const;

	static int get_width(){ return M; }
	static int get_height(){ return N; }

	/// Unormalize points.
	PointList unnormalize(const PointList& in) const;

	/// Convert a point from the grid coordinates to real coordinates.
	Point to_real_coordinates(const Point& p) const;
	
	private:
	////////////////////////////////////////////////////////////////////////
	// Private methods.

	// Init grid
	void init_grid();

	/// Get the point at position i,j in the grid.
	Point& grid_at(int i, int j);

	/// Calculate grid.
	void get_grid();

	/// Normalize points.
	PointList normalize(const PointList& in) const;

	/// Check if point is inside the valid domain.
	bool inside_domain(const Point& p) const;

	/// Check if point is inside the valid domain (in grid coordinates).
	bool inside_grid_domain(const Point& p) const;

	/// Convert a point from the real coordinates to the grid coordinates.
	Point to_grid_coordinates(const Point& p) const;

	/// Weight data structure.
	using dphi_data = std::pair<Point, double>;
	
	using dphi_map = std::unordered_map<int, std::vector<dphi_data>>;

	/// Calculate dphi data.	
	dphi_map calculate_dphis();
	
	////////////////////////////////////////////////////////////////////////
	// Parameters.

	/// Extreme points of domain rectangle.
	Point2 domain;

	/// Grid of control points.
	std::array<Point, M*N> grid;

	/// List of input points.
	PointList input_ps;

	/// List of input points.
	PointList input_qs;

};

/// Get the point at position i,j in the grid.
template<int M, int N>
inline Point& FFD<M, N>::grid_at(int i, int j){
	if(i < 0 or i >= M or j < 0 or j >= N)
		throw std::out_of_range{
			"Invalid grid point (" + loc::to_string(i) + " " + loc::to_string(j) + ")"
		};

	return grid[i + j*M];
}

/// Get the point at position i,j in the grid.
template<int M, int N>
inline const Point& FFD<M, N>::grid_at(int i, int j) const{
	if(i < 0 or i >= M or j < 0 or j >= N)
		throw std::out_of_range{
			"Invalid grid point (" + loc::to_string(i) + " " + loc::to_string(j) + ")"
		};

	return grid[i + j*M];
}

/// Check if point is inside the valid domain.
template<int M, int N>
inline bool FFD<M, N>::inside_domain(const Point& p) const{
	return p[0] > domain[0][0] and 
		p[0] < domain[1][0] and 
		p[1] > domain[0][1] and 
		p[1] < domain[1][1];
}

/// Check if point is inside the valid domain (in grid coordinates).
template<int M, int N>
inline bool FFD<M, N>::inside_grid_domain(const Point& p) const{
	return p[0] >= 0 and p[0] < M and p[1] >= 0 and p[1] < N;
}

/// Convert a point from the real coordinates to the grid coordinates.
template<int M, int N>
inline Point FFD<M, N>::to_grid_coordinates(const Point& p) const{
	double x0 = domain[0][0];
	double y0 = domain[0][1];
	double w = domain[1][0] - domain[0][0];
	double h = domain[1][1] - domain[0][1];
	return {
		(p[0] - x0)*(M-3)/w + 1,
		(p[1] - y0)*(N-3)/h + 1
	};
}

/// Convert a point from the grid coordinates to real coordinates.
template<int M, int N>
inline Point FFD<M, N>::to_real_coordinates(const Point& p) const{
	double x0 = domain[0][0];
	double y0 = domain[0][1];
	double w = domain[1][0] - domain[0][0];
	double h = domain[1][1] - domain[0][1];
	return {
		(p[0] - 1)*w/(M-3) + x0,
		(p[1] - 1)*h/(N-3) + y0
	};
}

template<int M, int N>
void FFD<M, N>::get_morph(const PointList& ps, const PointList& qs){
	if(ps.empty() or qs.empty())
		return;

	// Adjust domain to make sure all input points are inside it.
	domain = increase(1.0, axis_aligned_bounding_box(ps, qs));

	// Copy input points.
	input_ps = ps;
	input_qs = qs;

	// Calculate grid.
	get_grid();
}

// Cubic B-spline basis functions.
inline std::array<double, 4> basis_functions(const double t){
	const double t2 = t*t;
	const double t3 = t2*t;

	constexpr double one_sixth = 1.0/6.0;

	return {
		one_sixth*(1.0 - 3.0*t + 3.0*t2 -     t3),
		one_sixth*(4.0         - 6.0*t2 + 3.0*t3),
		one_sixth*(1.0 + 3.0*t + 3.0*t2 - 3.0*t3),
		one_sixth*t3
	};
}

inline std::pair<int, std::array<double, 4>> get_basis_functions(double u){
	const int fu = floor(u);
	const int i = fu - 1;
	const double s = u - fu;
	return {i, basis_functions(s)};
}

/// Apply morph to a single point.
template<int M, int N>
Point FFD<M, N>::morph(const Point& _p) const{
	// Ignore point if it is out of the morph domain.
	if(!inside_domain(_p))
		return _p;

	// Convert point to grid coordinates.
	const Point p = to_grid_coordinates(_p);

	// Calculate parameters for x coordinate.
	auto pu = get_basis_functions(p[0]);
	const int i = pu.first;
	const auto& bs = pu.second;

	// Calculate parameters for y coordinate.
	auto pv = get_basis_functions(p[1]);
	const int j = pv.first;
	const auto& bt = pv.second;

	// Calculate position of morphed point.
	Point q;
	for(int k = 0; k < 4; ++k)
		for(int l = 0; l < 4; ++l)
			q += (bs[k]*bt[l])*grid_at(i+k, j+l);

	return to_real_coordinates(q);
}

/// Adjust domain such that every point in pts is inside it.
template<int M, int N>
bool FFD<M, N>::adjust_domain(const PointList& pts, bool recalculate_grid){
	if(pts.empty())
		return false;

	Point2 new_domain = axis_aligned_bounding_box(domain, pts);

	bool changed = new_domain[0] != domain[0] or new_domain[1] != domain[1];
		
	if(changed){
		domain = increase(1.0, new_domain);
		if(recalculate_grid)
			get_grid();
	}

	return changed;
}

/// Copy the domain from another FFD object.
template<int M, int N>
void FFD<M, N>::set_domain(const FFD& ffd, bool recalculate_grid){
	domain = ffd.domain;

	if(recalculate_grid)
		get_grid();
}

////////////////////////////////////////////////////////////////////////////////
// Private methods.

/// Sum weights.
inline double sum_weights(const std::array<double, 4>& bs, const std::array<double, 4>& bt){
	double sum = 0.0;
	for(int k = 0; k < 4; ++k)
		for(int l = 0; l < 4; ++l)
			sum += std::pow(bs[k]*bt[l], 2);
	return sum;
}

// Init grid
template<int M, int N>
void FFD<M, N>::init_grid(){
	for(int j = 0; j < N; ++j)
		for(int i = 0; i < M; ++i)
			grid_at(i, j) = Point(i, j);
}
	
/// Calculate dphi data.	
template<int M, int N>
typename FFD<M, N>::dphi_map FFD<M, N>::calculate_dphis(){
	// Normalize input points.
	PointList ps = normalize(input_ps);
	PointList qs = normalize(input_qs);

	// Number of points.
	int n = std::min(ps.size(), qs.size());

	// List of weight data for each point of the grid.
	dphi_map dphis;

	for(int ii = 0; ii < n; ++ii){
		const Point& p = ps[ii];
		const Point& q = qs[ii];
		
		const Point d = q - p;

		// Calculate parameters for x coordinate.
		auto pu = get_basis_functions(p[0]);
		const int i = pu.first;
		const auto& bs = pu.second;

		// Calculate parameters for y coordinate.
		auto pv = get_basis_functions(p[1]);
		const int j = pv.first;
		const auto& bt = pv.second;

		const Point dw = (1.0/sum_weights(bs, bt))*d;

		for(int k = 0; k < 4; ++k)
			for(int l = 0; l < 4; ++l){
				double wc = bs[k]*bt[l];
				dphis[(i+k) + M*(j+l)].emplace_back(wc*dw, wc);
			}
	}

	return dphis;
}

/// Calculate grid.
template<int M, int N>
void FFD<M, N>::get_grid(){
	init_grid();

	auto truncate_point = [&](const Point& p){
		return p.transform(
			[](double v){
				//return v;
				return std::max(-0.48, std::min(0.48, v));
			}
		);
	};

	for(const auto& p: calculate_dphis()){
		Point dphi;
		double wc = 0.0;

		for(const auto& d: p.second){
			double wc2 = std::pow(d.second, 2);
			dphi += wc2*d.first;
			wc += wc2;
		}

		if(wc > 0.0)
			grid[p.first] += truncate_point((1.0/wc)*dphi);
	}
}

/// Normalize points.
template<int M, int N>
PointList FFD<M, N>::normalize(const PointList& in) const{
	PointList out;
	out.reserve(in.size());
	for(const Point& p: in)
		out.push_back(to_grid_coordinates(p));
	return out;
}

/// Unnormalize points.
template<int M, int N>
PointList FFD<M, N>::unnormalize(const PointList& in) const{
	PointList out;
	out.reserve(in.size());
	for(const Point& p: in)
		out.push_back(to_real_coordinates(p));
	return out;
}

#endif
