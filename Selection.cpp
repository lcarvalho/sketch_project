#include "Selection.h"
#include "loc.h"

using loc::copy_shared;
using std::make_shared;
using std::cout;
using std::map;
using std::swap;
using std::move;

void show(const DrawableList& selected){
	cout << "selected " << selected.size() << " curves/regions: \n";
	for(auto& d: selected)
		cout << "(" << d << ' ' << d->type() << ' ' << d->get_index() << ") ";
	cout << '\n';
}

/// Select curves that are close to point p.
void Selection::select_curves_at(const Point p, double scale){
	update_drawing();
	if(selection_drawing == nullptr)
		return;

	for(auto& d: selection_drawing->get_curves())
		if(!d->is_selected() and d->is_next_to(p, scale)){
			d->select();
			selected.push_back(d);
		}

	show(selected);
}

/// Select curves that are close to point p.
void Selection::select_regions_at(const Point p){
	update_drawing();
	if(selection_drawing == nullptr)
		return;

	for(auto& d: selection_drawing->get_regions())
		if(!d->is_selected() and d->is_next_to(p)){
			auto r = toRegion(d);
			for(auto& c: r->get_curves()){
				auto& curve = c.curve;
				if(!curve->is_selected() and curve->is_visible())
					selected.push_back(curve);
			}
			selected.push_back(d);
			d->select();
		}

	show(selected);
}

/// Deselect curves that are close to point p.
void Selection::deselect_curves_at(const Point p, double scale){
	if(selection_drawing == nullptr)
		return;

	for(auto& d: selected)
		if(d->type() == 1 and d->is_next_to(p, scale))
			d->deselect();

	auto old_selected = selected;
	selected.clear();
	for(auto& d: old_selected)
		if(d->is_selected())
			selected.push_back(d);

	show(selected);
}

/// Deselect curves that are close to point p.
void Selection::deselect_regions_at(const Point p){
	if(selection_drawing == nullptr)
		return;

	for(auto& d: selected)
		if(d->type() == 2 and d->is_next_to(p))
			d->deselect();

	auto old_selected = selected;
	selected.clear();
	for(auto& d: old_selected)
		if(d->is_selected())
			selected.push_back(d);

	show(selected);
}

/// Select all objects.
void Selection::select_all(){
	update_drawing();

	auto curves = selection_drawing->get_curves();
	auto regions = selection_drawing->get_regions();

	selected.clear();
	selected.reserve(curves.size() + regions.size());

	for(auto& curve: curves)
		selected.push_back(curve);

	for(auto& region: regions)
		selected.push_back(region);

	for(auto& d: selected)
		d->select();

	show(selected);
}

/// Deselect all objects.
void Selection::select_none(){
	for(auto& d: selected)
		d->deselect();
	selected.clear();
	selection_drawing = nullptr;
}

/// copy selection.
void Selection::copy_selection_to_clipboard(){
	clipboard.clear();
	clipboard.reserve(selected.size());

	map<BezierPath_sp, BezierPath_sp> curves_map;
	RegionList new_regions;

	for(auto& d: selected){
		if(d->type() == 1){
			auto c = toBezierPath_s(d);
			auto new_curve = copy_shared(c);
			curves_map[c] = new_curve;
			clipboard.push_back(new_curve);
		}else if(d->type() == 2)
			new_regions.push_back(copy_shared(toRegion_s(d)));
		d->deselect();
	}

	for(auto& r: new_regions){
		auto ce = curves_map.end();
		for(auto& c: r->get_curves()){
			auto& curve = c.curve;

			if(curves_map.find(curve) != ce)
				continue;

			curves_map[curve] = copy_shared(curve);

		}
		r->replace_curves(curves_map);
		clipboard.push_back(r);
	}

	selected.clear();
}

/// Cut selection.
void Selection::selection_to_clipboard(){
	if(selection_drawing == nullptr)
		return;

	auto sb = selected.begin();
	auto se = selected.end();

	// swap map.
	map<Drawable_sp, Drawable_sp> sw;

	// Unselect curves in unselected regions.
	for(auto& region: selection_drawing->get_regions()){
		if(region->is_selected())
			continue;

		for(auto& c: region->get_curves()){
			auto& curve = c.curve;

			if(curve->is_selected() == false)
				continue;

			// Create a new curve.
			auto nc = copy_shared(curve);

			// Deselect old curve.
			curve->deselect();

			// Replace old curve with new curve in selected list.
			replace(sb, se, Drawable_sp{curve}, Drawable_sp{nc});

			// Mark in the map.
			sw[curve] = nc;
		}
	}

	for(auto& region: selection_drawing->get_regions()){
		if(!region->is_selected())
			continue;

		for(auto& c: region->get_curves()){
			auto& curve = c.curve;

			auto& nc = sw[curve];

			if(curve->is_selected() or nc == nullptr)
				continue;

			curve = toBezierPath_s(nc);
		}
	}

	clipboard = selected;

	selection_drawing->remove_selected();

	selected.clear();
}


/// Bring selected objects up.
void Selection::uplevel_selected(){
	if(selection_drawing == nullptr)
		return;

	auto& d = selection_drawing->get_drawables();
	int s = d.size();
	for(int i = s-2; i >= 0; --i)
		if(d[i]->is_selected() and !d[i+1]->is_selected())
			swap(d[i], d[i+1]);
	selection_drawing->update_indices();
}

/// Bring selected objects down.
void Selection::downlevel_selected(){
	if(selection_drawing == nullptr)
		return;

	auto& d = selection_drawing->get_drawables();
	int s = d.size();
	for(int i = 1; i < s; ++i)
		if(d[i]->is_selected() and !d[i-1]->is_selected())
			swap(d[i], d[i-1]);

	selection_drawing->update_indices();
}
/// Create a region from the selected curves.
void Selection::make_region_from_selection(const Color& color, const Color& curves_color){
	if(selection_drawing == nullptr)
		return;

	if(selected.empty())
		return;

	auto region = make_shared<Region>();

	for(auto& d: selected){
		if(d->type() != 1)
			continue;
		d->deselect();
		d->set_color(curves_color);
		region->add_path(toBezierPath_s(d));
	}

	selected.clear();

	region->adjust_order();
	region->set_color(color);

	selection_drawing->hist_add(region);
}


// Remove everything that is selected.
void Selection::remove_selected(){
	if(selection_drawing == nullptr)
		return;

	selection_drawing->remove_selected();

	selected.clear();
}

/// Copy guidelines from current frame and layer to clipboard.
void Selection::copy_guidelines(){
	Drawing* d = viewer.get_drawing(false);
	if(d == nullptr)
		return;

	const auto& guides = d->get_guidelines();

	guidelines_clipboard.clear();
	guidelines_clipboard.reserve(guides.size());

	for(const auto& c: guides)
		guidelines_clipboard.emplace_back(*c);
}

/// Paste guidelines from clipboard to current frame and layer.
void Selection::paste_guidelines(){
	Drawing* d = viewer.get_drawing(false);
	if(d == nullptr)
		return;

	for(auto& p: guidelines_clipboard)
		d->add_guideline(make_shared<BezierPath>(move(p)));

	guidelines_clipboard.clear();
}


/// Paste clipboard into current frame/layer.
void Selection::paste_clipboard(){
	update_drawing();
	if(selection_drawing == nullptr)
		return;

	RegionList regions;
	regions.reserve(clipboard.size());

	// Paste curves.
	for(auto& d: clipboard)
		if(d->type() == 1)
			selection_drawing->hist_add(toBezierPath_s(d));
		else
			regions.push_back(toRegion_s(d));

	// Paste regions.
	for(auto& d: regions)
		selection_drawing->hist_add(d);

	selected = move(clipboard);

	clipboard.clear();
}
		

/// Update the current drawing (deselect everything if drawing changed).
void Selection::update_drawing(){
	auto drawing = viewer.get_drawing(false);
	if(drawing == nullptr)
		return;

	if(drawing != selection_drawing){
		select_none();
		selection_drawing = drawing;
	}
}
