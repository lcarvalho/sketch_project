#include "Region.h"
#include <limits>
#include <iostream>

using std::cerr;
using std::minmax;
using std::map;

Region::Region():
	Drawable(2)
{}

/// Adjust the order of the curves.
void Region::adjust_order(){
	int n = curves.size();
	// There's not to do with only one curve.
	if(n <= 1)
		return;

	bool flipped;

	int ni = 0;

	do{
		flipped = false;
		int i = 0;
		for(auto& c: curves){
			Point qa = c.front();
			Point qb = c.back();
			Point p = curves[(i+n-1)%n].back();
			Point r = curves[(i+1)%n].front();

			if(
				dist(qa, p) + dist(qb, r) >
				dist(qa, r) + dist(qb, p)
			 ){
				c.fw = !c.fw;
				flipped = true;
			}
			++i;
		}

		++ni;
	}while(flipped and ni < 100);

	std::cout << "n = " << n << "; ni = " << ni << '\n';

	if(ni == 100)
		cerr << "Possible infinite loop!\n";
}

/// Check if point p is inside the region.
bool Region::contains(const Point& p){
	PointList reg_sample;
	for(auto& bb: curves){
		const auto& sample = bb.curve->sample(10);
		if(bb.fw)
			reg_sample.insert(reg_sample.end(), sample.begin(), sample.end());
		else
			reg_sample.insert(reg_sample.end(), sample.rbegin(), sample.rend());
	}

	double x = p[0];
	double y = p[1];

	int count = 0;

	auto check_pair = [&](const Point& p0, const Point& p1){
		auto mm = minmax(p0[0], p1[0]);

		if(mm.first < x and x <= mm.second){
			double t = (x - p0[0])/(p1[0] - p0[0]);
			if((1.0-t)*p0[1] + t*p1[1] < y)
				++count;
		}
	};

	for(auto i = 1u; i < reg_sample.size(); ++i)
		check_pair(reg_sample[i-1], reg_sample[i]);
	check_pair(reg_sample.back(), reg_sample.front());

	return count%2 != 0;
}

/// Check if the region is next to point p.
bool Region::is_next_to(const Point& p, double scale){
	return contains(p);
}

/// Select region.
void Region::select(){
	Drawable::select();

	for(auto& c: curves)
		c.curve->select();
}

/// Unselect region.
void Region::deselect(){
	Drawable::deselect();

	for(auto& c: curves)
		c.curve->deselect();
}

/// Replace curves according to map m.
void Region::replace_curves(const map<BezierPath_sp, BezierPath_sp> m){
	for(auto& p: curves)
		p.curve = m.at(p.curve);
}

/// Remove curve.
void Region::remove_curve(const BezierPath_sp& c){
	deselect();
	loc::remove_if_and_erase(
		curves,
		[&](BezierBool& bb){
			return bb.curve == c;
		}
	);
}
