#include "SVGReader.h"
#include <fstream>
#include <sstream>
#include "Transformation.h"

using namespace loc::literals;
using std::string;
using std::ifstream;
using std::istream;
using std::exception;
using std::cerr;
using std::vector;
using std::runtime_error;
using std::make_shared;
using std::cout;
using std::cerr;
using std::array;
using std::istringstream;

/// Constructor.
SVGReader::SVGReader(const string& filename){
	try{

		ifstream in(filename);

		in >> xml;

		in.close();

	}catch(exception& e){
		cerr << "Error during reading svg file.\n";
		cerr << e.what() << '\n';
	}
}

/// A data set of paths.
struct PathsData{
	/// Transformation object.
	const Transformation& T;

	/// Input stream.
	istream& in;

	/// Vector of curves.
	vector<BezierPath_sp> curves;

	/// List of points.
	PointList pts;

	/// A character.
	char c;

	/// Last character read.
	char lc;

	/// Point used during reading.
	Point p0;

	/// Point used during reading.
	Point p1;

	/// Constructor.
	PathsData(const Transformation& _T, istream& _in)
		: T(_T), in(_in)
	{
		process_data();
	}

	/// Peek a character from istream.
	char peek(){
		loc::ignore_spaces(in);
		return in.peek();
	}

	/// Check if character c is lower case (when using relative coordinates).
	bool is_relative(){
		return islower(c);
	};

	/// Ignore one comma, throw an exception if there is more than one comma.
	void ignore_comma(){
		if(peek() == ',')
			in.ignore();

		if(peek() == ',')
			throw runtime_error{"Unexpected comma!"};
	};

	/// Read a point.
	Point read_point(){
		Point p;

		ignore_comma();
		in >> p[0];
		ignore_comma();
		in >> p[1];

		if(is_relative())
			p = p0 + p;

		return p;
	}

	/// Check if there is a number next.
	bool is_number_next(){
		char nc = peek();
		return isdigit(nc) or nc == '-' or nc == '.';
	};

	/// Create curve from transformed list of points.
	void make_curve(){
		if(pts.empty())
			return;

		// Create curve.
		curves.push_back(make_shared<BezierPath>(T(pts)));
		pts.clear();
	};

	/// Add a line from last point to point p.
	void add_line(const Point& p){
		loc::insert_values(pts, .75*p0 + .25*p, .25*p0 + .75*p, p);
		p0 = p;
	};

	/// Create a new path.
	void new_path(){
		// Make curve with current pts.
		make_curve();

		p0 = read_point();
		pts = { p0 };

		while(is_number_next())
			add_line(read_point());
	};

	/// Read a sequence of lines.
	void read_lines(){
		while(is_number_next())
			add_line(read_point());
	};

	/// Read horizontal lines.
	void read_h_lines(){
		while(is_number_next()){
			p1 = p0;
			in >> p1[0];
			if(is_relative())
				p1[0] = p0[0] + p1[0];

			add_line(p1);
		}
	};

	/// Read vertical lines.
	void read_v_lines(){
		while(is_number_next()){
			p1 = p0;
			in >> p1[1];
			if(is_relative())
				p1[1] = p0[1] + p1[1];

			add_line(p1);
		}
	};

	/// Read cubic bezier paths.
	void read_cubics(){
		while(is_number_next()){
			p1 = read_point();
			Point p2 = read_point();
			Point p3 = read_point();

			loc::insert_values(pts, p1, p2, p3);

			p0 = pts.back();
			p1 = pts[pts.size() - 2];
		}
	};

	/// Read cubic bezier paths (shorthand version).
	void read_cubics_shorthand(){
		while(is_number_next()){
			p1 = p0;
			if(lc == 'S' or lc == 'C')
				p1 = p0 - (p1 - p0);

			Point p2 = read_point();
			Point p3 = read_point();

			loc::insert_values(pts, p1, p2, p3);

			p0 = pts.back();
			p1 = pts[pts.size() - 2];
		}
	};

	/// Close the path with a line to the front.
	void close_path(){
		add_line(pts.front());
	};

	/// Read quadrics.
	void read_quadrics(){
		while(is_number_next()){
			Point pa = read_point();
			Point pb = read_point();

			p1 = p0 + 2./3.*(pa-p0);
			Point p2 = pb + 2./3.*(pa-pb);

			loc::insert_values(pts, p1, p2, pb);

			p0 = pb;
			p1 = pa;
		}
	};

	/// Read quadrics (shorthand version).
	void read_quadrics_shorthand(){
		while(is_number_next()){
			Point pa = p0;
			if(lc == 'Q' or lc == 'T')
				pa = pa - (p1 - p0);

			Point pb = read_point();

			p1 = p0 + 2.0/3.0*(pa-p0);
			Point p2 = pb + 2.0/3.0*(pa-pb);

			loc::insert_values(pts, p1, p2, pb);

			p0 = pb;
			p1 = pa;
		}
	};

	/// Read arcs.
	void read_arcs(){
		cout << "Atention! Arcs not properly implemented yet!\n";
		while(is_number_next()){
			read_point();//Point pa = read_point();

			double params[3];
			for(int i = 0; i < 3; ++i){
				ignore_comma();
				in >> params[i];
			}

			add_line(read_point());
		}
	};

	/// Process data.
	void process_data(){
		while(in >> c){
			switch(toupper(c)){
				case 'M':
					new_path();
					break;
				case 'L':
					read_lines();
					break;
				case 'H':
					read_h_lines();
					break;
				case 'V':
					read_v_lines();
					break;
				case 'C':
					read_cubics();
					break;
				case 'S':
					read_cubics_shorthand();
					break;
				case 'Z':
					close_path();
					break;

				case 'Q':
					read_quadrics();
					break;
				case 'T':
					read_quadrics_shorthand();
					break;

				case 'A':
					read_arcs();
					break;

				default:
					cerr << "unknown parameter "
					     << c << '\n';
			}
			// Keep last c.
			lc = toupper(c);
		}

		make_curve();
	};
};

/// Read a path from string.
vector<BezierPath_sp> read_path(const string& path, const Transformation& T){
	istringstream in{path};

	return PathsData{T, in}.curves;
}

/// Transformation data from input stream.
struct TransformData{
	/// Input stream.
	istream& in;

	/// Transformation.
	Transformation T;

	/// Commmand.
	string command;

	/// Constructor.
	TransformData(istream& _in)
		: in(_in), T(Transformation::identity())
	{
		process_data();
	}

	/// Check if command is the one specified by str.
	bool is_command(const string& str){
		return command.compare(0, str.size(), str) == 0;
	}

	/// Process data.
	void process_data(){
		loc::ignore_spaces(in);

		while(in.good()){
			getline(in, command, '(');

			if(is_command("matrix"))
				read_matrix();
			else if(is_command("translate"))
				read_translate();
			else if(is_command("scale"))
				read_scale();
			else if(is_command("rotate"))
				read_rotate();
			else if(is_command("skewX"))
				read_skewx();
			else if(is_command("skewY"))
				read_skewy();
			else
				throw runtime_error{
					"Unknown transformation: " + command
				};

			loc::ignore_spaces(in);
		}
	}

	/// Check next non-empty character.
	char peek(){
		loc::ignore_spaces(in);
		return in.peek();
	}

	/// Check if there is a number next.
	bool is_number_next(){
		char nc = peek();
		return isdigit(nc) or nc == '-' or nc == '.';
	};

	/// Ignore comma.
	void ignore_comma(){
		if(peek() == ',')
			in.ignore();

		if(peek() == ',')
			throw runtime_error{"Unexpected comma!"};
	};

	/// Read a character.
	void read_char(char c){
		loc::ignore_spaces(in);
		char ic = in.get();
		if(ic != c)
			throw runtime_error{
				"Invalid char "_s + ic +
				", expected "_s + c
			};
	};

	/// Read a 2x2 matrix and a point.
	void read_matrix(){
		array<double, 4> A;
		Point p;

		in >> A[0];
		ignore_comma();
		in >> A[1];
		ignore_comma();
		in >> A[2];
		ignore_comma();
		in >> A[3];
		ignore_comma();
		in >> p[0];
		ignore_comma();
		in >> p[1];
		read_char(')');

		T = T*Transformation{A, p};
	}

	/// Read translation data.
	void read_translate(){
		double tx;
		in >> tx;

		ignore_comma();

		double ty = 0.0;
		if(is_number_next())
			in >> ty;
		read_char(')');

		T = T*Transformation::translate(tx, ty);
	}

	/// Read scale data.
	void read_scale(){
		double sx;
		in >> sx;

		ignore_comma();

		double sy = sx;
		if(is_number_next())
			in >> sy;
		read_char(')');

		T = T*Transformation::scale(sx, sy);
	}

	/// Read rotate data.
	void read_rotate(){
		double angle;
		in >> angle;

		ignore_comma();
		Point c;
		if(is_number_next())
			in >> c[0];

		ignore_comma();
		if(is_number_next())
			in >> c[1];
		read_char(')');

		T = T*Transformation::rotate(angle, c);
	}

	/// Read skew in x direction data.
	void read_skewx(){
		double angle;
		in >> angle;
		read_char(')');
		T = T*Transformation::skewX(angle);

	}

	/// Read skew in y direction data.
	void read_skewy(){
		double angle;
		in >> angle;
		read_char(')');
		T = T*Transformation::skewY(angle);
	}
};

/// Get a transformation from string.
Transformation get_transform(const string& s){
	try{
		istringstream in{s};
		return TransformData{in}.T;
	}catch(exception& e){
		cerr << "Error! " << e.what() << '\n';
	}

	return Transformation::identity();
}

/// Get the drawables from a XmlTag.
void get_drawables(
	const XmlTag& tag,
	vector<Drawable_sp>& drawables,
	const Transformation& T
){
	auto T1 = T;
	auto trans = tag.find_property("transform");
	if(trans)
		T1 = T1*get_transform(trans->second);

	if(tag.name() == "path"){
		auto p = tag.find_property("d");
		if(p != nullptr){
			auto d = read_path(p->second, T1);
			drawables.insert(drawables.end(), d.begin(), d.end());
		}
	}

	for(const auto& subtag: tag.get_subtags())
		get_drawables(subtag, drawables, T1);
}

/// Get drawable objects from svg file.
DrawableList SVGReader::get_drawables() const{
	vector<Drawable_sp> drawables;

	auto I = Transformation::identity();
	for(const auto& tag: xml.get_tags())
		::get_drawables(tag, drawables, I);

	return drawables;
}

