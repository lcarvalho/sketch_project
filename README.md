# Dilight

## Introduction
Dilight is a program to create and edit keyframe animations.
The system automatically generates the intermediate frames using keyframe data.

## Compiling
To compile the project you need:
* [Cmake 2.8.9+](http://www.cmake.org/)
* [GCC 4.8+](https://gcc.gnu.org/) OR [Clang 3.5+](http://clang.llvm.org/)

The following libraries are required for compiling the project:
* [Qt 5](http://qt-project.org/)
* [Eigen 3](http://eigen.tuxfamily.org/)

The recommended way of compiling the project is by entering the main directory and
typing the following commands:
`$ mkdir build`
`$ cd build`
`$ cmake ..`
`$ make -j`

You can choose which compiler will be used by defining it to CMAKE_CXX_COMPILER, 
for example to use Clang type:
`$ cmake .. -DCMAKE_CXX_COMPILER=clang++`
`$ make -j`

After compiling, the executable file named **dilight** will be generated in directory
bin.

The project creates two libraries: 
* Library **animation** is the library with all data and algorithms of an animation.
* Library **editor** has the user interface.

The project also generates an executable called \b test, which is used to test some
features, generate images, etc.

## Running
The program may be executed with or without command options.
If no command option is passed, the program runs with the user interface.
Otherwise the user interface won't be executed, the program runs the inbetweening algorithm
to all animations defined by filenames passed as arguments.
For example, the following code:
`$ ./dilight f1.txt f2.txt`
will process the animations defined in files f1.txt and f2.txt and it will output 
some statistics for each animation, like the number of keyframes, generated frames, 
and computing times in milliseconds.