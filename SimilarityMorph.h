#ifndef SIMILARITY_MORPH_H
#define SIMILARITY_MORPH_H

#include "Morph.h"

/// Multiscale Free Form Deformation
class SimilarityMorph final : public Morph{
	public:
	////////////////////////////////////////////////////////////////////////

	/// Constructor.
	SimilarityMorph();

	/// Calculate the morph that transforms the n points
	/// in ps to points into qs.
	void get_morph(const PointList& ps, const PointList& qs);

	/// Apply morph to a single point.
	Point morph(const Point& c) const override;

	/// Get the inverse transformation.
	SimilarityMorph inverse() const;

	/// Composition operator.
	SimilarityMorph operator*(const SimilarityMorph& S) const;

	/// Get the intermediate transformation at parameter t.
	SimilarityMorph intermediate(double t, const Point& c0) const;

	/// Output stream operator.
	friend std::ostream& operator<<(
		std::ostream& out,
		const SimilarityMorph& S
	);

	private:
	////////////////////////////////////////////////////////////////////////
	// Parameters

	/// Similarity parameters.
	double a[4];
};

using SimilarityMorph_sp = std::shared_ptr<SimilarityMorph>;


#endif
