#ifndef MFFD_H
#define MFFD_H
#include "ffd.h"
#include "Morph.h"
#include <memory>
#include <array>

/// Multiscale Free Form Deformation
template<int L, int M, int N>
class MFFD : public Morph{
	public:
		
	using RefinedMFFD = MFFD<L-1, 2*M-1, 2*N-1>;

	////////////////////////////////////////////////////////////////////////
	/// Calculte morph transforming ps to qs.
	/** Calculate the morph that transforms the n points
	* in ps to points into qs.
	*/
	void get_morph(const PointList& ps, const PointList& qs);

	/// Apply morph to a single point.
	Point morph(const Point& c) const override;

	/// Adjust domain such that every point in pts is inside it.
	void adjust_domain(const PointList& pts, bool recalculate_grid = true);
	
	/// Get the refined morph.
	const RefinedMFFD& get_refined() const;

	/// Get the width of the most refined grid..
	double max_width() const;
	
	const FFD<M, N>& get_ffd() const{ return ffd; }

	private:
	////////////////////////////////////////////////////////////////////////
	// Parameters

	/// FFD at this level of the hierarchy.
	FFD<M, N> ffd;

	/// Refined mffd.
	RefinedMFFD refined;
};

/// Multiscale Free Form Deformation
template<int M, int N>
class MFFD<0, M, N>: public Morph{
	public:
	////////////////////////////////////////////////////////////////////////
	/// Calculte morph transforming ps to qs.
	/** Calculate the morph that transforms the n points
	* in ps to points into qs.
	*/
	void get_morph(const PointList& ps, const PointList& qs){}

	/// Apply morph to a single point.
	Point morph(const Point& c) const override{ return c; }
	
	/// Get the width of the most refined grid..
	double max_width() const{ return 0; }

	/// Adjust domain such that every point in pts is inside it.
	void adjust_domain(const PointList& pts, bool recalculate_grid = true){}
};

////////////////////////////////////////////////////////////////////////////////
/// Calculte morph transforming ps to qs.
/** Calculate the morph that transforms the n points
* in ps to points into qs.
*/
template<int L, int M, int N>
void MFFD<L, M, N>::get_morph(const PointList& ps, const PointList& qs){
	// Morphed points.
	ffd.get_morph(ps, qs);
	refined.get_morph(ffd.morph_list(ps), qs);
}

/// Apply the morph to the n points ps
template<int L, int M, int N>
inline Point MFFD<L, M, N>::morph(const Point& p) const{
	return refined.morph(ffd.morph(p));
}

/// Adjust domain such that every point in pts is inside it.
template<int L, int M, int N>
void MFFD<L, M, N>::adjust_domain(const PointList& pts, bool recalculate_grid){
	bool r = ffd.adjust_domain(pts, recalculate_grid);

	if(r)
		refined.adjust_domain(pts, recalculate_grid);
}

/// Get the refined morph.
template<int L, int M, int N>
auto MFFD<L, M, N>::get_refined() const -> const RefinedMFFD&{
	return refined;
}
	
/// Get the width of the most refined grid..
template<int L, int M, int N>
inline double MFFD<L, M, N>::max_width() const{
	return L==1? ffd.get_width(): refined.max_width();
}

#endif

