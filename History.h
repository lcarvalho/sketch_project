#ifndef HISTORY_H
#define HISTORY_H

/// Base class for operations on a drawing.
class Operation{
	public:
		/// Undo operation.
		virtual void undo() = 0;

		/// Redo operation.
		virtual void redo() = 0;
};

using Operation_sp = std::shared_ptr<Operation>;

/// History of operations.
class History{
	public:
	/// Add an operation.
	void add_op(Operation_sp op);

	/// Undo the last operation.
	void undo();

	/// Redo operation (if there was an undo before).
	void redo();

	protected:
	/// Operation history.
	std::vector<Operation_sp> op_history;

	/// List of operations undoed.
	std::vector<Operation_sp> undo_history;

};

/// Add an operation.
inline void History::add_op(Operation_sp op){
	op_history.push_back(move(op));
	undo_history.clear();
}

/// Undo the last operation.
inline void History::undo(){
	if(op_history.empty())
		return;

	auto& op = op_history.back();
	op->undo();

	undo_history.push_back(move(op));
	op_history.pop_back();
}

/// Redo operation (if there was an undo before).
inline void History::redo(){
	if(undo_history.empty())
		return;

	auto& op = undo_history.back();
	op->redo();

	op_history.push_back(move(op));
	undo_history.pop_back();
}
#endif
