#ifndef TIMELINE_WIDGET_H
#define TIMELINE_WIDGET_H

#include <QtWidgets>
#include "Animation.h"

class AnimationEditor;

/// Qt Widget to edit frames from an animation.
class TimelineWidget : public QWidget{
	Q_OBJECT

	public:
		////////////////////////////////////////////////////////////////

		/// Constructor.
		TimelineWidget(AnimationEditor& editor);

		/// Update the size.
		void update_size();

	protected:
		/// Create the menu items.
		void setup_menu();

		////////////////////////////////////////////////////////////////
		// Parameters

		/// Menu to control frames.
		QMenu* menu;

		/// Action related to setting/unsetting keyframes.
		QAction* set_keyframe_action;
		
		/// Action related to toggling acceleration control.
		QAction* toggle_acc_action;

		/// Number of frames.
		size_t n_frames;

		/// Frame separator size.
		size_t frame_sep = 1;

		/// Frame width.
		int frame_width = 10;

		/// Frame height.
		int frame_height = 28;

		/// Editor of the animation.
		AnimationEditor& editor;

		////////////////////////////////////////////////////////////////
		// Events.

		/// Paint event.
		void paintEvent(QPaintEvent* event);

		/// Mouse press event.
		void mousePressEvent(QMouseEvent* event);

		/// Mouse move event.
		void mouseMoveEvent(QMouseEvent* event);
};

#endif
