#ifndef SPEED_WIDGET_H
#define SPEED_WIDGET_H

#include <QtWidgets>
#include "SpeedControl.h"

class AnimationEditor;

/// Qt Widget to edit frames from an animation.
class SpeedWidget : public QDialog{
	Q_OBJECT

	public:
		////////////////////////////////////////////////////////////////

		/// Constructor.
		SpeedWidget(AnimationEditor& editor);

		/// Update curve.
		void update_curve();

	protected:
		/// Editor of the animation.
		AnimationEditor& editor;

		/// Speed controller currently used.
		SpeedControl* speed_control = nullptr;
	
		/// Transform used when drawing.
		QTransform T;

		/// Radius of handle circles.
		double r = 0.03;

		Point* selection = nullptr;

		Point last_pos;

		////////////////////////////////////////////////////////////////
		// Events.

		/// Paint event.
		void paintEvent(QPaintEvent* event);

		/// Mouse press event.
		void mousePressEvent(QMouseEvent* event);

		/// Mouse move event.
		void mouseMoveEvent(QMouseEvent* event);
		
		/// Mouse release event.
		void mouseReleaseEvent(QMouseEvent* event);
};

#endif
