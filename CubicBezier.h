#ifndef CUBIC_BEZIER
#define CUBIC_BEZIER

#include "Point.h"
#include "loc.h"

/// A cubic Bezier curve.
class CubicBezier{
	public:
		/// Constructor.
		CubicBezier(
			const Point& p0, 
			const Point& p1, 
			const Point& p2, 
			const Point& p3
		);
		
		/// Constructor from existing data.
		CubicBezier(Point* p);

		/// Constructor copying data.
		CubicBezier(const Point* p);

		/// Returns a copy of this cubic segment.
		CubicBezier copy() const;

		/// Get control point.
		Point& operator[](size_t i);

		/// Get control point (const version).
		const Point& operator[](size_t i) const;

		/// Point at parameter t.
		Point point_at(double t) const;

		/// Tangent at parameter t.
		Point tangent_at(double t) const;

		/// Get the reversed curve.
		CubicBezier reversed() const;

		/// Get the curve from 0 to t.
		CubicBezier split(double t) const;
		
		/// Get the curve from t to 1.
		CubicBezier rsplit(double t) const;
		
		/// Get the curve from ta to tb.
		CubicBezier split(double ta, double tb) const;
		
		/// Get the curves from 0 to t and from t to 1.
		loc::pair_of<CubicBezier> split2(double t) const;

		/// Calculate curve length.
		double polygon_length() const;
		
		/// Calculate chord length.
		double chord_length() const;

		/// Get the arc length of curve.
		double arc_length(double eps = 0.1, int level = 0) const;

		/// Get a uniform sample of the cubic.
		PointList sample(size_t N) const;

		/// Fit curve to a set of points.
		double fit(const PointList& pts);

	private:
		using Deleter = std::function<void(Point*)>;

		/// Points reference.
		std::unique_ptr<Point[], Deleter> c;
};

inline auto begin(const CubicBezier& c){
	return &c[0];
}

inline auto end(const CubicBezier& c){
	return begin(c) + 4;
}
		
/// Constructor.
inline CubicBezier::CubicBezier(
	const Point& p0, 
	const Point& p1, 
	const Point& p2, 
	const Point& p3
):
	c{
		new Point[4]{p0, p1, p2, p3},
		std::default_delete<Point[]>{}
	}
{}
		
/// Constructor from existing data.
inline CubicBezier::CubicBezier(Point* p):
	c{
		p,
		[](Point* p){}
	}
{}

/// Constructor from existing data.
inline CubicBezier::CubicBezier(const Point* p):
	CubicBezier{p[0], p[1], p[2], p[3]}
{}

/// Returns a copy of this cubic segment.
inline CubicBezier CubicBezier::copy() const{
	return {c[0], c[1], c[2], c[3]};
}

/// Get control point.
inline Point& CubicBezier::operator[](size_t i){
	return c[i];
}

/// Get control point.
inline const Point& CubicBezier::operator[](size_t i) const{
	return c[i];
}

/// Point at parameter t.
inline Point CubicBezier::point_at(double t) const{
	if(t <= 0.0)
		return c[0];

	if(t >= 1.0)
		return c[3];

	double t2 = t*t;
	double t3 = t*t2;
	double ti = 1-t;
	double ti2 = ti*ti;
	double ti3 = ti*ti2;

	return ti3*c[0] +
		(3*ti2*t)*c[1] +
		(3*ti*t2)*c[2] +
		t3*c[3];
}

/// Tangent at parameter t.
inline Point CubicBezier::tangent_at(double t) const{
	if(t <= 0.0)
		return 3*(c[1] - c[0]);

	if(t >= 1.0)
		return 3*(c[3] - c[2]);

	double t2 = t*t;
	double ti = 1-t;
	double ti2 = ti*ti;

	return (-3*ti2)*c[0] +
		3*(3*t2 - 4*t + 1)*c[1] +
		3*(2*t - 3*t2)*c[2] +
		(3*t2)*c[3];
}

/// Get the reversed curve.
inline CubicBezier CubicBezier::reversed() const{
	return {c[3], c[2], c[1], c[0]};
}

/// Get the curve from 0 to ta.
inline CubicBezier CubicBezier::split(double ta) const{
	if(ta == 0.0)
		return {c[0], c[0], c[0], c[0]};
	if(ta == 1.0)
		return {c[0], c[1], c[2], c[3]};

	auto cta = point_at(ta);
	auto tta = tangent_at(ta);
	return {
		c[0], 
		c[0] + ta*(c[1] - c[0]), 
		cta - (ta/3.0)*tta,
		cta
	};
}

/// Get the curve from ta to 1.
inline CubicBezier CubicBezier::rsplit(double ta) const{
	if(ta <= 0.0)
		return {c[0], c[1], c[2], c[3]};
	if(ta >= 1.0)
		return {c[3], c[3], c[3], c[3]};

	auto cta = point_at(ta);
	auto tta = tangent_at(ta);
	return {
		cta, 
		cta + ((1-ta)/3.0)*tta,
		c[2] + ta*(c[3] - c[2]),
		c[3]
	};
}

/// Get the curve from ta to tb.
inline CubicBezier CubicBezier::split(double ta, double tb) const{
	if(ta > tb)
		std::swap(ta, tb);

	if(ta <= 0.0)
		return split(tb);
	if(tb >= 1.0)
		return rsplit(ta);

	auto cta = point_at(ta);
	auto tta = tangent_at(ta);
	
	auto ctb = point_at(tb);
	auto ttb = tangent_at(tb);

	double alpha = (tb - ta)/3.0;

	return {
		cta,
		cta + alpha*tta,
		ctb - alpha*ttb,
		ctb
	};
}

/// Get the curves from 0 to t and from t to 1.
inline loc::pair_of<CubicBezier> CubicBezier::split2(double t) const{
	return {split(t), rsplit(t)};
}

/// Calculate curve lenght.
inline double CubicBezier::polygon_length() const{
	return dist(c[0], c[1]) + dist(c[1], c[2]) + dist(c[2], c[3]);
}

/// Calculate chord.
inline double CubicBezier::chord_length() const{
	return dist(c[0], c[3]);
}

#endif
