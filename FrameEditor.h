#ifndef FRAME_EDITOR_H
#define FRAME_EDITOR_H

#include <bitset>
#include <QtWidgets>
#include <Eigen/Dense>
#include <Eigen/SVD>
#include "Animation.h"
#include "FittingCurve.h"
#include "ColorWidget.h"

class AnimationEditor;
using QCursor_up = std::unique_ptr<QCursor>;

/// A set of cursors.
struct CursorSet{
	/// Eraser cursor.
	QCursor_up eraser;

	/// Pencil cursor.
	QCursor_up pencil;

	/// Sketch eraser cursor.
	QCursor_up eraser_sketch;

	/// Sketch pencil cursor.
	QCursor_up pencil_sketch;

	/// Curve selector cursor.
	QCursor_up curve_selector;

	/// Region selector cursor.
	QCursor_up region_selector;

	/// Control point editor cursor.
	QCursor_up cp_editor;
};

/// Qt Widget to edit frames from an animation.
class FrameEditor : public QWidget{
	Q_OBJECT

	public:
		////////////////////////////////////////////////////////////////
		// Types for editor status.

		/// States.
		enum State{
			drawing,
			sketching,
			experimental
		};

		/// Tools.
		enum Tool{
			pencil,
			eraser,
			curve_selector,
			region_selector,
			center_editor,
			cp_editor
		};

		/// Possible flags.
		enum Flag{
			show_drawing,
			show_sketch,
			show_points,
			show_distance_field,
			show_auxiliary,
			show_previous_guides,
			show_subcurves,
			show_bg_image,
			tool_active,
			translating_view,
			show_center_point,
			clear_paint
		};

	public:
		////////////////////////////////////////////////////////////////

		/// Constructor.
		FrameEditor(AnimationEditor& editor);

		////////////////////////////////////////////////////////////////
		// Methods that define editor status.

		/// Define the color widget
		void set_color_tool(ColorWidget* color_tool);

		/// Change the status of flag f.
		void flip(Flag f);

		/// Define the current state.
		void set(State s);

		/// Define the current tool.
		void set_tool(Tool t);

		/// Load the background image.
		void load_image(const QString& filename);

		/// Reset data.
		void reset();

		/// Define the error parameter.
		void set_error(double e);

		/// Undo last action.
		void undo();

		/// Redo last action.
		void redo();

		/// Update colors.
		void update_colors();

		/// Save the frame data into an eps file.
		void save_eps();

		/// Draw the fitting curve.
		void draw_fitting_curve();

		/// Draw the distance field.
		void draw_distance_field();

		////////////////////////////////////////////////////////////////
		// Methods to draw data from a frame.

		/// Draw the drawing of frame.
		void draw_drawing(const Drawing& drawing);

		/// Draw the guidelines of frame.
		void draw_sketch(const Drawing& drawing, double opacity = 1.0);

		////////////////////////////////////////////////////////////////
		// Methods to draw data from curves.

		/// Draw a Drawable object.
		void draw_drawable(const Drawable& d);

		/// Draw a cubic bezier curve.
		void draw_cubic_bezier(QPainter& painter, const BezierPath& c);

		/// Draw the control points of a BezierPath object.
		void draw_control_points(
			QPainter& painter,
			const BezierPath& c
		);

		/// Draw all the control points of a Drawing.
		void draw_control_points(Drawing& drawing);

		/// Draw all the control points of the current frame/layer.
		void draw_control_points();

		/// Draw layer at frame f.
		void draw(const Node<Layer>& layer, int f, const Node<Layer>* except = nullptr);

		/// Set cursor according to current tool.
		void update_cursor();

		/// Draw the current frame.
		void draw_current_frame();

		/// Draw the center of rotation point.
		void draw_center_point();

		/// Draw the background image of the current frame.
		void draw_bg_image();

		/// Remove curves that are crossing segment from
		/// last_point to current_point.
		void remove_curves_crossing(const Point& current_point);

		/// Insert new frame at position i.
		void insert_frame(size_t i);
		
		/// Remove frame at position i.
		void remove_frame(size_t i);

		/// Action done when tablet pen enters.
		void tablet_enter(QTabletEvent* e);
	protected:
		////////////////////////////////////////////////////////////////
		// Events

		/// Paint event.
		void paintEvent(QPaintEvent* event);

		/// Mouse press event.
		void mousePressEvent(QMouseEvent* event);

		/// Mouse release event.
		void mouseReleaseEvent(QMouseEvent* event);

		/// Mouse move event.
		void mouseMoveEvent(QMouseEvent* event);

		/// Mouse wheel event.
		void wheelEvent(QWheelEvent* event);
		
		/// Touch event.
		void touchEvent(QTouchEvent* event);
		
		/// Tablet event.
		void tabletEvent(QTabletEvent* event);

		/// 
		bool event(QEvent* event);

	private:
		/// What is done when pressing right button.
		void pressed_right_button(const QPointF& pos);
		
		/// What is done when pressing left button.
		void pressed_left_button(const QPointF& pos, bool shift);
		
		/// What is done when pressing middle button.
		void pressed_middle_button(const QPointF& pos);

		////////////////////////////////////////////////////////////////
		// Editor status.

		/// Flags.
		std::bitset<12> flag;

		/// Active tool.
		Tool tool;

		/// Editor state.
		State curve_state;

		/// Maximum error parameter used with the FittingCurve.
		double max_error;

		/// Fitting curve that transforms polygonal
		/// input data into BezierPath objects.
		FittingCurve fitting_curve;

		/// Animation that contains the frame.
		AnimationEditor& editor;

		/// Painter object.
		QPainter painter;

		/// Last point inserted.
		Point last_point;

		/// Background images.
		std::vector<QImage> images;

		/// Widget to choose color.
		ColorWidget* color_tool;

		/// View transformation.
		QTransform T;

		/// Scale.
		double s;

		/// Control point being edited.
		std::pair<size_t, BezierPath_sp> control_point;

		/// Set of cursors.
		CursorSet cursors;
		
		/// Timer to control frame auto-update.
		QTimer timer;

		/// Tablet device, if any.
		bool has_tablet = false;

		/// Tablet active.
		bool tablet_down = false;
};

/////////////////////////////////////////////////////////////////
// Methods that define editor status.

/// Define the color widget
inline void FrameEditor::set_color_tool(ColorWidget* _color_tool){
	color_tool = _color_tool;
}

/// Change the status of flag f.
inline void FrameEditor::flip(Flag f){
	flag.flip(f);
	repaint();
}

/// Define the current state.
inline void FrameEditor::set(State s){
	curve_state = s;
}

/// Define the error parameter.
inline void FrameEditor::set_error(double e){
	max_error = e;
}

#endif
