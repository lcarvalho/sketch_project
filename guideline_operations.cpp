#include "guideline_operations.h"

using std::max;
using std::vector;

BezierPathList normalized_points(const BezierPathList& paths){
	BezierPathList result;
	result.reserve(paths.size());

	Point barycenter;
	int n = 0;

	// Copy curves.
	for(auto& path: paths){
		result.push_back(loc::copy_shared(path));

		const auto& cps = path->control_points();

		barycenter = loc::accumulate(cps, barycenter);

		n += cps.size();
	}

	if(n != 0)
		barycenter = (1.0/n)*barycenter;

	// Normalize points.
	for(auto& path: result){
		auto& pts = path->control_points();
		for(Point& p: pts)
			p = p - barycenter;
	}

	return result;
}

double get_scale(const BezierPathList& paths){
	auto bb = min_bounding_box(all_control_points(paths));
	double w = dist(bb[0], bb[1]);
	double h = dist(bb[0], bb[3]);
	return max(w, h);
}

void normalize_scale(BezierPathList& paths0, const BezierPathList& paths1){
	double scale0 = get_scale(paths0);
	double scale1 = get_scale(paths1);

	double s = scale1/scale0;

	// Apply scale to the control points of paths0.
	for(auto& path: paths0){
		auto& pts = path->control_points();
		for(Point& p: pts)
			p = s*p;
	}
}

void rotate_45degrees(BezierPathList& paths){
	constexpr double s = 0.707106781;
	for(auto& path: paths)
		for(Point& p: path->control_points())
			p = s*Point{p[0]-p[1], p[0]+p[1]};
}

bipartite_matching::Edges best_match(
	const BezierPathList& paths0, 
	const BezierPathList& paths1
){
	auto df = calculate_distances(paths0, paths1);

	auto dist_info = [&](size_t i, size_t j) -> DistanceInfo&{
		BezierPair p{paths0[i].get(), paths1[j].get()};
		return df[p];
	};

	size_t n0 = paths0.size();
	size_t n1 = paths1.size();

	bipartite_matching::BipartiteGraph graph{paths0.size(), paths1.size()};

	for(size_t i0 = 0; i0 < n0; ++i0)
		for(size_t i1 = 0; i1 < n1; ++i1){
			auto& d = dist_info(i0, i1);
			graph.add_edge(i0, i1, -d.dist);
		}

	return graph.maximum_weight_matching();
}


CostInfo get_cost(
	const bipartite_matching::Edges& bimatch,
	const BezierPathList& paths0,
	const BezierPathList& paths1
){

	double cost = 0.0;
	vector<int> permut_indices(paths0.size());
	vector<bool> curves_orientation(paths1.size());

	for(auto& p: bimatch){
		int i0 = p.first;
		int i1 = p.second;
		permut_indices[i0] = i1;

		auto d = distance_info(*paths0[i0], *paths1[i1]);

		cost += d.dist;
		curves_orientation[i1] = d.fw;
	}

	return CostInfo{cost, move(permut_indices), move(curves_orientation)};
}


