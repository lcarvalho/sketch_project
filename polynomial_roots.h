#ifndef POLYNOMIAL_ROOTS_H
#define POLYNOMIAL_ROOTS_H

#include <vector>

/// Calculate the roots of equation ax+b = 0.
std::vector<double> real_roots(double a, double b);

/// Calculate the roots of equation ax² + bx + c = 0.
std::vector<double> real_roots(double a, double b, double c);

/// Calculate the roots of equation ax³ + bx² + cx + d = 0.
std::vector<double> real_roots(double a, double b, double c, double d);

#endif
