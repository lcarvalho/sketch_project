#include "CubicBezier.h"
#include <Eigen/Dense>
#include <Eigen/SVD>

using std::get;

/// Get the arc length of cubic c.
double CubicBezier::arc_length(double eps, int level) const{
	//return length(sample(10)); // simple lenght
	
	auto Lp = polygon_length();
	auto Lc = chord_length();

	auto l = .5*(Lp + Lc);
	
	auto err = Lp - Lc;
	
	if(err < eps or level > 3)
		return l;

	auto x = split2(0.5);
	eps = 0.75*eps;

	return 
		get<0>(x).arc_length(eps, level+1) + 
		get<1>(x).arc_length(eps, level+1);
}

/// Get a uniform sample of the cubic.
PointList CubicBezier::sample(size_t N) const{
	PointList pts(N);
	double dt = 1.0/(N-1);
	for(size_t i = 0; i < N; ++i)
		pts[i] = point_at(i*dt);
	return pts;
}

auto get_svd(int N){
	Eigen::MatrixXd A{2*N, 4};
	
	double t = 0.0;
	double dt = 1.0/(N-1);
	for(int i = 0; i < N; ++i, t+=dt){
		double t2 = t*t;
		double ti = (1-t);
		double ti2 = ti*ti;

		A.row(2*i)   << 3*ti2*t, 0.0, 3*ti*t2, 0.0;
		A.row(2*i+1) << 0.0, 3*ti2*t, 0.0, 3*ti*t2;
	}

	return A.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV);
}

/// Fit curve to a set of points.
double CubicBezier::fit(const PointList& target){
	if(target.empty())
		return loc::infinity<double>();

	auto norm2 = [&](const Point& p){ return p.norm2(); };

	double max_norm = loc::max_val_element(target, norm2)->norm();

	static size_t N = 10;
	static auto svd = get_svd(N);

	if(N != target.size()){
		N = target.size();
		svd = get_svd(N);
	}

	Eigen::VectorXd b{int(2*N)};
	double t = 0.0;
	double dt = 1.0/(N-1);
	for(size_t i = 0; i < N; ++i, t += dt){
		const Point& p = target[i] - pow(1-t, 3)*c[0] - pow(t, 3)*c[3];
		b(2*i) = p[0]/max_norm;
		b(2*i+1) = p[1]/max_norm;
	}

	Eigen::VectorXd x = svd.solve(b);
	
	c[1] = max_norm*Point{x[0], x[1]};
	c[2] = max_norm*Point{x[2], x[3]};

	// Calculate the relative error.
	double err = 0.0;
	auto it = target.begin();
	for(Point p: sample(N))
		err += dist2(p, *it++);
	return sqrt(err)/b.norm();
}
