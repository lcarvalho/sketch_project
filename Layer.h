#ifndef LAYER_H
#define LAYER_H

#include <string>
#include <vector>
#include <list>
#include <set>
#include <memory>
#include "loc.h"
#include "Drawing.h"
#include "Interpolator.h"

class Animation;

class Layer;

using DrawingPair = loc::pair_of<const Drawing*>;

/// A layer of drawings.
class Layer{
	public:
		/// Constructor.
		Layer() = default;

		/// Constructor from Json object.
		Layer(const json11::Json& json, std::set<size_t>& keyframes);

		/// Reset the drawing list.
		void reset_drawings(size_t n_frames);

		/// Set the curve distance tolerance.
		void set_tolerance(double tol);

		/// Get the curve distance tolerance.
		double get_tolerance() const;

		/// Return the name of the layer.
		const std::string& get_name() const;

		/// Define the name of the layer.
		void set_name(const std::string& name);

		/// Set visibility.
		void set_visibility(bool v);

		/// Get visibility.
		bool is_visible() const;

		/// Return layer status.
		loc::pair_of<size_t> status() const;

		/// Get the n-th drawing.
		Drawing* get_drawing(size_t d);

		/// Insert frame at position i.
		void insert_frame(size_t i);
		
		/// Remove frame at position i.
		void remove_frame(size_t i);

		/// Get the n-th drawing.
		const Drawing* get_drawing_const(size_t d) const;

		/// Remove empty curves.
		void clear_empty_curves();
		
		/// Remove auxiliary curves at frame f.
		void clear_auxiliary_curves(size_t f);

		/// Get a list with all drawable objects at frame f.
		DrawableList get_all_drawables(size_t f) const;

		/// Get a list with all guidelines at frame f.
		BezierPathList get_all_guidelines(size_t f) const;

		/// Get a list with all auxiliary curves at frame f.
		BezierPathList get_all_auxiliary_curves(size_t f) const;

		/// Get a list with all auxiliary curves at frame f.
		std::vector<BezierPathList> get_all_clusters(size_t f) const;

		/// Calculate morph from frame f0 to frame f1.
		SimilarityMorph_sp calculate_morph(
			size_t f0,
			size_t f1,
			const SimilarityMorph_sp& base_morph = nullptr
		);

		/// Mix object from frame f_start to f_end at frame f.
		loc::pair_of<SimilarityMorph> mix_objects(
			size_t f_start,
			size_t f_end,
			size_t f,
			size_t f_end_actual,
			std::function<double(double)> speed,
			const SimilarityMorph& Sp,
			const SimilarityMorph& Spia 
		);
		
		/// Get auxiliary curves transforming curves from
		/// keyframe f0 to the keyframe f1.
		void calculate_auxiliary_curves(size_t f0, size_t f1);

		/// Update inbetween info.
		void update_inbetween_info();

		/// Clear inbetween info.
		void clear_inbetween_info();

		/// Get a list with all the control points of this layer
		/// and sublayers.
		PointList all_control_points() const;

		/// Get the interpolator object associated with
		/// a pair of drawings.
		Interpolator* get_interpolator(size_t f0, size_t f1);

		/// Update curve proportions.
		void update_proportions();

		/// Ostream operator.
		template<class Op>
		friend std::ostream& output(
			std::ostream& out,
			const Layer& layer,
			Op op
		);

		/// Istream operator.
		friend std::istream& operator>>(
			std::istream& in,
			Layer& layer
		);

		/// Convert to Json object.
		template<class Op>
		friend json11::Json output_json(const Layer& layer, Op op);

		// Remove all interpolators using drawing d.
		void clear_interpolators_using(const Drawing& d);

	private:
		////////////////////////////////////////////////////////////////////
		// Parameters.
		
		/// Drawings of each frame.
		std::list<Drawing> drawings;

		/// Layer name.
		std::string name = "default";

		/// Marks wether this layer is visible or not.
		bool visible = true;

		/// Map for finding the interpolator used for each
		/// pair of drawings.
		std::map<DrawingPair, Interpolator*> interp_map;

		/// List of interpolator objects.
		std::list<Interpolator> interpolators;
		
		/// Curve distance tolerance.
		double tolerance = 0.2;
};


/// Get the curve distance tolerance.
inline double Layer::get_tolerance() const{
	return tolerance;
}

inline const std::string& Layer::get_name() const{
	return name;
}

inline void Layer::set_name(const std::string& _name){
	name = _name;
}

/// Set visibility.
inline void Layer::set_visibility(bool v){
	visible = v;
}

/// Get visibility.
inline bool Layer::is_visible() const{
	return visible;
}

/// Get the n-th drawing.
inline Drawing* Layer::get_drawing(size_t d){
	if(d >= drawings.size())
		drawings.resize(d+1);

	auto it = drawings.begin();
	advance(it, d);
	return &*it;
}

/// Get the n-th drawing.
inline const Drawing* Layer::get_drawing_const(size_t d) const{
	if(d >= drawings.size())
		return nullptr;

	auto it = drawings.begin();
	advance(it, d);
	return &*it;
}

/// Ostream operator.
template<class Op>
std::ostream& output(std::ostream& out, const Layer& layer, Op op){
	out << layer.name << '\n';

	// Get non-empty frames.
	loc::vector_pair<size_t, const Drawing*> drawings;

	for(size_t f = 0; f < layer.drawings.size(); ++f){
		if(!op(f))
			continue;

		auto d = layer.get_drawing_const(f);
		if(d != nullptr and d->empty() == false)
			drawings.emplace_back(f, d);
	}

	// Write frames data.
	out << drawings.size() << '\n';
	for(auto& d: drawings)
		out << "frame "
			<< std::get<0>(d) << '\n'
			<< *std::get<1>(d) << '\n';

	return out;
}
		
/// Remove auxiliary curves.
inline void Layer::clear_auxiliary_curves(size_t f){
//	auto& d = drawings.at(f);
	auto d = get_drawing(f);
	if(d != nullptr)
		d->clear_auxiliary_curves();
}
		
/// Convert to Json object.
template<class Op>
json11::Json output_json(const Layer& layer, Op op){
	using namespace json11;

	// Get non-empty frames.
	loc::vector_pair<size_t, const Drawing*> drawings;

	for(size_t f = 0; f < layer.drawings.size(); ++f){
		if(!op(f))
			continue;

		auto d = layer.get_drawing_const(f);
		if(d != nullptr)
			drawings.emplace_back(f, d);
	}

	// Write frames data.
	Json::array frames;
	for(auto& d: drawings)
		frames.push_back(
			Json::object{
				{"frame_index", int(std::get<0>(d))},
				{"drawings", *std::get<1>(d)},
			}
		);

	return Json::object{
		{"name", layer.name},
		{"frames", frames}
	};
}

#endif

