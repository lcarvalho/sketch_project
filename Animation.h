#ifndef ANIMATION_H
#define ANIMATION_H

#include "Drawing.h"
#include "Layer.h"
#include "Interpolator.h"
#include "Node.h"
#include "NodeLayer.h"
#include "SpeedControl.h"

class AnimationViewer;

/// Keyframe animation object.
class Animation{
	public:
	////////////////////////////////////////////////////////////////////////

	/// Constructor.
	Animation(size_t n_frames = 100);

	/// Constructor from file.
	Animation(const std::string& filename);

	/// Constructor form json object.
	explicit Animation(const json11::Json& json);

	////////////////////////////////////////////////////////////////////////

	/// Set or unset frame i as a keyframe.
	void set_keyframe(size_t i);

	/// Define keyframe flag value for frame i.
	void set_keyframe(size_t i, bool value);

	/// Set the curve distance tolerance.
	void set_tolerance(double tol);

	/// Get the curve distance tolerance.
	double get_tolerance() const;

	/// Check if frame i is empty.
	bool is_empty_frame(size_t f) const;

	/// Check if frame i is a keyframe.
	bool is_keyframe(size_t i) const;

	/// Check if animation is cyclic.
	bool is_cyclic() const;
	
	/// Set animation as cyclic or not.
	void change_cyclic(bool c);

	/// Get the index of last keyframe.
	size_t last_keyframe_index() const;
	
	/// Get the index of first keyframe.
	size_t first_keyframe_index() const;

	/// Check if frame is between first and last keyframe.
	bool inside_range(size_t f) const;

	/// Number of frames.
	size_t n_frames() const;

	/// Find the next keyframe after position i (inclusive).
	bool next_keyframe(size_t& i);

	/// Find the previous keyframe before position i. 
	bool prev_keyframe(size_t& i);

	/// Clear interpolated frames.
	void clear_interpolated_frames();
	
	/// Clear inbetween info.
	void clear_inbetween_info();

	/// Remove empty curves.
	void clear_empty_curves();

	/// Return animation status.
	std::string status() const;

	/// Insert frame at position i.
	void insert_frame(size_t i);
	
	/// Remove frame at position i.
	void remove_frame(size_t i);

	////////////////////////////////////////////////////////////////////////
	// Layer operations.

	/// Get root layer.
	Node<Layer>& get_root_layer();

	/// Get root layer (const version).
	const Node<Layer>& get_root_layer() const;

	/// Update all curve proportions.
	void update_proportions();

	/// Calculate all morphs.
	void calculate_morphs();

	/// Get auxiliary curves transforming curves from each keyframe
	/// to the next keyframe.
	void calculate_auxiliary_curves();

	/// Preprocessing frames.
	void preprocessing();

	/// Calculate the whole animation by inbetweening keyframes.
	void generate_animation();

	/// Save the animation into svg file.
	void save_animation_svg(const std::string& filename);

	/// Save the animation into eps file.
	void save_eps(const std::string& filename);

	/// Get the minimum axis aligned bounding box of all frames.
	Point2 axis_aligned_bb() const;

	/// Get an animation viewer bound to this animation.
	AnimationViewer get_viewer();

	/// Update all auxiliary drawings.
	void update_all_auxiliary();

	/// Apply operation to every pair of keyframes.
	void apply_to_keyframes(std::function<void(size_t, size_t)> f);

	/// Get all drawables of frame f.
	DrawableList get_all_drawables(size_t f);

	/// Get the speed control function for keyframe f.
	std::function<double(double)> get_speed(size_t f);
	
	/// Get the speed control function for keyframe f.
	SpeedControl* get_speed_controller(size_t f);

	////////////////////////////////////////////////////////////////////////

	/// Convert to Json object.
	json11::Json to_json() const;

	/// Convert speed_controllers map to json.
	json11::Json get_speed_json() const;

	/// Output stream operator.
	friend std::ostream& operator<<(std::ostream& out, const Animation& a);

	private:
	////////////////////////////////////////////////////////////////////////
	// Parameters.

	/// Total number of frames.
	size_t total_frames = 200;

	/// Cyclic animation flag.
	bool cyclic = false;

	/// List of keyframes.
	std::set<size_t> keyframe_set;

	/// List of speed controllers.
	std::map<size_t, SpeedControl> speed_controllers;

	/// Root base layer.
	Node<Layer> root_layer;
};

////////////////////////////////////////////////////////////////////////////////
// Implementation of inline methods.

/// Define keyframe flag value for frame i.
inline void Animation::set_keyframe(size_t i, bool value){
	if(value){
		keyframe_set.insert(i);
		speed_controllers[i] = {};
	}else{
		keyframe_set.erase(i);
		speed_controllers.erase(i);
	}
}

/// Set the curve distance tolerance.
inline void Animation::set_tolerance(double tol){
	root_layer.apply(
		[&](Layer& l){
			l.set_tolerance(tol);
		}
	);
}

/// Get the curve distance tolerance.
inline double Animation::get_tolerance() const{
	return root_layer.get_tolerance();
}


/// Check if frame i is a keyframe.
inline bool Animation::is_keyframe(size_t i) const{
	return keyframe_set.count(i) == 1;
}

/// Get the index of last keyframe.
inline size_t Animation::last_keyframe_index() const{
	if(keyframe_set.empty())
		return 0;

	return *keyframe_set.rbegin();
}

/// Get the index of first keyframe.
inline size_t Animation::first_keyframe_index() const{
	if(keyframe_set.empty())
		return 0;

	return *keyframe_set.begin();
}
	
/// Check if frame is between first and last keyframe.
inline bool Animation::inside_range(size_t f) const{
	return f >= first_keyframe_index() and f <= last_keyframe_index();
}

/// Number of frames.
inline size_t Animation::n_frames() const{
	return total_frames;
}

/// Find the next keyframe after position i (inclusive).
inline bool Animation::next_keyframe(size_t& i){
	if(i > last_keyframe_index())
		return false;

	i = *keyframe_set.lower_bound(i);
	return true;
}

/// Find the previous keyframe before position i.
inline bool Animation::prev_keyframe(size_t& i){
	if(keyframe_set.empty() or i == *keyframe_set.begin())
		return false;

	i = *std::prev(keyframe_set.lower_bound(i));
	return true;
}

/// Get root layer.
inline Node<Layer>& Animation::get_root_layer(){
	return root_layer;
}
/// Get root layer (const version).
inline const Node<Layer>& Animation::get_root_layer() const{
	return root_layer;
}

/// Remove empty curves.
inline void Animation::clear_empty_curves(){
	root_layer.clear_empty_curves();
}

/// Check if animation is cyclic.
inline bool Animation::is_cyclic() const{
	return cyclic;
}
	
/// Set animation as cyclic or not.
inline void Animation::change_cyclic(bool c){
	cyclic = c;

	if(!cyclic)
		root_layer.clear_auxiliary_curves(first_keyframe_index());
	else
		calculate_auxiliary_curves();
}


#endif
