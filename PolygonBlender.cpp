#include "PolygonBlender.h"
#include "polynomial_roots.h"
#include <cmath>
#include <chrono>
	
double ks = 3.19575;
double es = 0.781614;
double cs = 0.5;

double kb = 7.74928;
double eb = 0.283072;
double mb = 35.0892;
double pb = 9227.17;


using std::min;
using std::max;
using std::tie;
using std::minmax;
using std::vector;
using std::tuple;
using std::make_tuple;
using std::get;
using std::function;
using std::cout;

/// Returns a point containing the dot product and z-coordinate of cross product of F and B.
inline Point dot_cross(const Point& F, const Point& B){
	return {dot(F, B), cross(F, B)};
}

/// Returns points that define the function that describes the angle change between two segments.
Point3 get_Q(const Point3& Pi, const Point3& Pj){
	Point 
		Bi = Pi[0] - Pi[1], 
		Fi = Pi[2] - Pi[1],
		Bj = Pj[0] - Pj[1],
		Fj = Pj[2] - Pj[1];

	bool bi = Bi.norm2() < 1e-8;
	bool fi = Fi.norm2() < 1e-8;
	bool bj = Bj.norm2() < 1e-8;
	bool fj = Fj.norm2() < 1e-8;

	double s = 0.01;

	// most extreme case, both sets collapse to a point.
	if(bi and fi and bj and fj){
		Point d = rotate90(Pi[0] - Pj[0]);
		double n = d.norm();
		if(fabs(n) < 1e-8){
			//cout << "are you f***ing kidding with me?\n";
			Bi = {0.0,  1.0};
			Fi = {0.0, -1.0};
			Bj = {0.0,  1.0};
			Fj = {0.0, -1.0};
		}else{
			d = (1.0/n)*d;
			Bi = Bj = (-1.0)*d;
			Fi = Fj = d;
		}
	}else{
		if(bi and !fi)
			Bi = (-s/Fi.norm())*Fi;
		else if(!bi and fi)
			Fi = (-s/Bi.norm())*Bi;
		else if(bi and fi){
			if(!bj and !fj)
				Bi = Bj - Fj;
			else if(bj and !fj)
				Bi = (-s/Fj.norm())*Fj;
			else// if(!bj and fj)
				Bi = (s/Bj.norm())*Bj;
			Fi = -1.0*Bi;
		}

		if(bj and !fj)
			Bj = (-s)*Fj;
		else if(!bj and fj)
			Fj = (-s)*Bj;
		else if(bj and fj){
			if(!bi and !fi)
				Bj = Bi - Fi;
			else if(bi and !fi)
				Bj = (-s/Fi.norm())*Fi;
			else// if(!bi and fi)
				Bj = s/Bi.norm()*Bi;
			Fj = -1.0*Bj;
		}
	}


	return {
		dot_cross(Fi, Bi), 
		0.5*(dot_cross(Fj, Bi) + dot_cross(Fi, Bj)), 
		dot_cross(Fj, Bj)
	};
}

/// Calculate the work required to bend one segment to the other.
double PolygonBlender::bending_work(const Point3& Pi, const Point3& Pj){
	const auto Q = get_Q(Pi, Pj);

	double dtheta = angle(Q[0], Q[2]);

	if(dtheta < 1e-5)
		return 0.0;

	auto find_roots = [](double d0, double d1, double d2){
		auto ts = real_roots(d0-2*d1+d2, 2*(d1-d0), d0);
		loc::remove_if_and_erase(ts, [](double t){ return t<=0 or t>=1; });
		return ts;
	};

	const auto roots = find_roots(
		cross(Q[0], Q[1]),
		cross(Q[0], Q[2])/2.0,
		cross(Q[1], Q[2])
	);

	if(roots.empty() and inside_triangle({0.0, 0.0}, Q))
		dtheta = 2*loc::pi - dtheta;
	
	double dthetaX = 0.0;
	for(double t: roots){
		double ti = 1.0 - t;
		const Point Qt = (ti*ti)*Q[0] + (2*t*ti)*Q[1] + (t*t)*Q[2];
		dthetaX += min(angle(Qt, Q[0]), angle(Qt, Q[2]));
	}


	const double Wb = kb*pow(dtheta + mb*dthetaX, eb);

	// Check if there is some t such that theta(t) == 0.
	for(double t: find_roots(Q[0][1], Q[1][1], Q[2][1])){
		double ti = 1.0 - t;
		double x = (ti*ti)*Q[0][0] + (2*t*ti)*Q[1][0] + (t*t)*Q[2][0];
		if(x >= 0.0)
			return Wb + pb;
	}

	return Wb;
}

/// Calculate the work required to stretch one segment to the other.
double PolygonBlender::stretching_work(const Point2& Pi, const Point2& Pj){
	double Lmin, Lmax;
	tie(Lmin, Lmax) = minmax(
		dist(Pi[1], Pi[0]), 
		dist(Pj[1], Pj[0])
	);

	if(Lmax-Lmin <= 1e-8)
		return 0.0;

	return ks*pow(Lmax - Lmin, es)/loc::linerp(cs, Lmin, Lmax);
}

/// Get the path connecting 0,0 to ci,cj.
template<class Fw, class Fn>
vector<int2> backtrack_path(int ci, int cj, Fw west, Fn north){
	vector<int2> res{{ci, cj}};
	while(ci != 0 or cj != 0){
		int w = west(ci, cj);
		int n = north(ci, cj);
		if(w > ci)
			ci = 0;
		else
			ci -= w;
		if(n > cj)
			cj = 0;
		else
			cj -= n;
		res.emplace_back(ci, cj);
	}
	loc::reverse(res);
	return res;
}

/// Get a normalized version of points in P, making points inside unit square.
PointList normalize(const PointList& P){
	if(P.empty())
		return {};

	auto bb = axis_aligned_bounding_box(P);
	Point pa = bb[0];
	Point pb = bb[1];

	double m = max(pb[0]-pa[0], pb[1] - pa[1]);

	PointList res = P;

	double s = (m < 1e-5)? 0.0: 1.0/m;

	for(Point& p: res)
		p = s*(p - pa);

	return res;
}

/// Define the parameters used in stretching calculation.
void PolygonBlender::set_stretch_parameters(double lks, double les, double lcs){
	ks = lks;
	es = les;
	cs = lcs;
}

/// Define the parameters used in bending calculation.
void PolygonBlender::set_bend_parameters(double lkb, double leb, double lmb, double lpb){
	kb = lkb;
	eb = leb;
	mb = lmb;
	pb = lpb;
}

/// Calculate random parameters.
void randomize_parameters(){
#if 0
	using namespace std::chrono;

	auto now = std::chrono::system_clock::now();
	auto seed = now.time_since_epoch().count();
	std::default_random_engine generator(seed);
	std::uniform_real_distribution<double> dist(0.0, 1.0);

	double alpha = dist(generator);

	ks = loc::linerp(alpha, 0.0, 10.0);

	alpha = dist(generator);
	es = loc::linerp(alpha, 0.0, 10.0);

	alpha = dist(generator);
	kb = loc::linerp(alpha, 0.0, 10.0);

	alpha = dist(generator);
	eb = loc::linerp(alpha, 0.0, 10.0);

	alpha = dist(generator);
	mb = loc::linerp(alpha, 0.0, 50.0);

	alpha = dist(generator);
	pb = loc::linerp(alpha, 0.001, 10000.0);
#endif
	/*
	cout << "Parameters:\n";
	cout << "ks = " << ks << ";\n";
	cout << "es = " << es << ";\n";
	cout << "kb = " << kb << ";\n";
	cout << "eb = " << eb << ";\n";
	cout << "mb = " << mb << ";\n";
	cout << "pb = " << pb << ";\n";
	*/
}

/// Find path of pair of points reducing the work necessary to bend and stretch wires Ci and Cj.
vector<int2> PolygonBlender::find_path(const PointList& oCi, const PointList& oCj){
	auto Ci = normalize(oCi);
	auto Cj = normalize(oCj);

	size_t m = Ci.size();
	size_t n = Cj.size();

	if(m == 0 or n == 0)
		return {};

	vector<vector<tuple<double, int, int>>> data(m);
	
	auto W = [&](size_t i, size_t j) -> double&{
		return get<0>(data[i][j]);
	};

	auto north = [&](size_t i, size_t j) -> int&{
		return get<1>(data[i][j]);
	};
	
	auto west = [&](size_t i, size_t j) -> int&{
		return get<2>(data[i][j]);
	};

	const double inf = loc::infinity<double>();
	auto inf_data = make_tuple(inf, 0, 0);

	for(size_t i = 0; i < m; ++i){
		data[i].resize(n, inf_data);
		for(size_t j = 0; j < n; ++j){
			double dd = fabs(double(i)/m - double(j)/n);
			if(dd < .3)
				calculate_weight(Ci, Cj, i, j, W, west, north);
		}
	}

	auto path = backtrack_path(m-1, n-1, west, north);

	/*
	for(auto p: path)
		std::cout << p.first << "-" << p.second << '\n';
//		std::cout << "M " << Ci[p.first] << " L " << Cj[p.second] + displacement << '\n';
	*/

	return path;
}

/// Get a clean version of the path, removing all pairs of non-terminal vertices.
vector<int2> PolygonBlender::clear_path(const vector<int2>& path, function<bool(int, size_t)> is_terminal){
	if(path.empty())
		return {};

	vector<int2> res{path.front()};

	int i, j;
	for(int2 p: path){
		tie(i, j) = p;
		if(p == path.front())
			continue;

		int2& back = res.back();
		
		bool ti = is_terminal(i, 0);
		bool tj = is_terminal(j, 1);

		if(ti and tj){
			int li = back.first;
			int lj = back.second;

			if((i == li or j == lj) and !(is_terminal(li, 0) and is_terminal(lj, 1)))
				back = p;
			else
				res.push_back(p);
		}else if(ti){
			if(i == back.first){
				if(!is_terminal(back.second, 1))
					back = p;
			}else
				res.push_back(p);
		}else if(tj){
			if(j == back.second){
				if(!is_terminal(back.first, 0))
					back = p;
			}else
				res.push_back(p);
		}
	}

	return res;
}


