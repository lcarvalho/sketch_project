#include "polynomial_roots.h"

#include <cmath>
#include <complex>
#include <vector>
#include <set>

inline bool is_zero(double x){ 
	return std::fabs(x) < 1e-8; 
}
	
struct is_less{
	bool operator()(double u, double v){ 
		return !is_zero(u-v) && u < v; 
	}
};

/// Calculate the roots of equation ax+b = 0.
std::vector<double> real_roots(double a, double b){
	if(is_zero(a))
		return {};

	return {-b/a};
}

/// Calculate the roots of equation ax² + bx + c = 0.
std::vector<double> real_roots(double a, double b, double c){
	if(is_zero(a))
		return real_roots(b, c);

	double delta = b*b - 4*a*c;

	if(delta < 0)
		return {};

	if(delta < 1e-8)
		return {-b/(2*a)};

	double x0 = (-b - std::sqrt(delta))/(2*a);
	double x1 = (-b + std::sqrt(delta))/(2*a);

	return {x0, x1};
}

/// Find the root of ax^3 + bx^2 + cx + d.
std::vector<double> real_roots(double a_, double b_, double c_, double d_){
	if(is_zero(a_))
		return real_roots(b_, c_, d_);

	double c = d_/a_;
	double b = c_/a_;
	double a = b_/a_;

	double p = b - a*a/3.;
	double q = c + (2.*a*a*a - 9.*a*b)/27.;

	if(is_zero(p)){
		if(is_zero(q))
			return {-a/3.};

		double u = pow(q, 1./3.);
		return {p/(3.*u) - u - a/3.};
	}
	
	using complex = std::complex<double>;

	complex u0 = pow(q/2. + sqrt(complex{q*q/4. + p*p*p/27.}), 1./3.);

	constexpr complex v{-.5, -sqrt(3.)/2.};

	std::set<double, is_less> t;

	for(auto u: {u0, u0*v, u0*conj(v)}){
		complex x =  p/(3.*u) - u - a/3.;
		if(is_zero(x.imag()))
			t.insert(x.real());
	}

	return {t.begin(), t.end()};
}

