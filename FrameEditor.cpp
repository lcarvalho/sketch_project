#include "FrameEditor.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cmath>
#include <chrono>
#include <random>

#include "AnimationEditor.h"
#include "PostScriptWriter.h"
#include "SVGWriter.h"
#include "NodeLayer.h"

using std::cout;
using std::bind;
using std::string;
using std::map;
using std::make_shared;
using loc::make_unique;
using loc::to_string;

///////////////////////////////////////////////////////////////////////////////
// Auxiliary functions.

/// Convert a Point into QPointF.
inline QPointF toQPoint(const Point& p){
	return {p[0], p[1]};
}

/// Convert a QPointF into Point.
inline Point toPoint(const QPointF& p){
	return {p.x(), p.y()};
}

///////////////////////////////////////////////////////////////////////////////
/// Constructor.
FrameEditor::FrameEditor(AnimationEditor& _editor):
	editor(_editor)
{
	setGeometry(0,0,100,100);
	max_error = 20;

	// Set flags
	flag.set(show_drawing);
	flag.set(show_sketch);
	flag.set(show_auxiliary);
	flag.set(show_previous_guides);
	flag.set(show_subcurves);
	flag.set(show_bg_image);

	reset();
	// Set state
	curve_state = drawing;

	// Current tool.
	tool = pencil;

	auto new_cursor = [](const char* filename){
		return make_unique<QCursor>(QPixmap::fromImage(QImage{filename}));
	};

	// Read cursor icons.
	cursors.eraser = new_cursor(":/images/eraser.svg");
	cursors.pencil = new_cursor(":/images/pencil.svg");
	cursors.eraser_sketch = new_cursor(":/images/eraser_sketch.svg");
	cursors.pencil_sketch = new_cursor(":/images/pencil_sketch.svg");
	cursors.curve_selector = new_cursor(":/images/hand_point_curve.svg");
	cursors.region_selector = new_cursor(":/images/hand_point_region.svg");
	cursors.cp_editor = new_cursor(":/images/cp_editor_cursor.png");

	setAttribute(Qt::WA_AcceptTouchEvents);

	color_tool = nullptr;

	s = 1.0;

	connect(
		&timer, 
		&QTimer::timeout,
		[&]{
			repaint();
		}
	);
}

/// Define the current tool.
void FrameEditor::set_tool(Tool t){
	tool = t;
	flag[show_center_point] = (tool == center_editor);
	flag[show_points] = (tool == cp_editor);

	if(tool == center_editor)
		editor.get_viewer().update_center_point();
		
	repaint();
}

/// Load the background image.
void FrameEditor::load_image(const QString& filename){
	images[editor.get_viewer().current_frame_index()].load(filename);
	repaint();
}

/// Reset data.
void FrameEditor::reset(){
	images.clear();
	images.resize(editor.get_animation().n_frames());
	T = QTransform{};
	s = 1.0;
	repaint();
}

/// Undo last action.
void FrameEditor::undo(){
	auto& viewer = editor.get_viewer();
	Drawing* d = viewer.get_drawing(false);
	if(d == nullptr)
		return;

	d->undo();
	switch(curve_state){
		case drawing: case sketching:
			viewer.update_auxiliary_curves();
			repaint();
			break;

		case experimental:
			break;
	}
}

/// Redo last action.
void FrameEditor::redo(){
	auto& viewer = editor.get_viewer();
	Drawing* d = viewer.get_drawing(false);
	if(d == nullptr)
		return;

	d->redo();

	switch(curve_state){
		case drawing: case sketching:
			viewer.update_auxiliary_curves();
			repaint();
			break;

		case experimental:
			break;
	}
}

///////////////////////////////////////////////////////////////////////////////
/// Save the frame data into an eps file.
void FrameEditor::save_eps(){
	auto& animation = editor.get_animation();
	auto& viewer = editor.get_viewer();
	for(size_t f = 0; f <= animation.last_keyframe_index(); ++f){
		viewer.set_current_frame(f);
//		int f = viewer.current_frame_index();
//		if(animation.is_empty_frame(f))
//			return;

		std::ostringstream filename_out;
		filename_out << "out" << std::setfill('0') << std::setw(4) << viewer.current_frame_index() << ".eps";
		string filename = filename_out.str();
		//string filename = "out" + to_string(viewer.current_frame_index()) + ".eps";
		cout << "Saving file " << filename << '\n';

		auto bb = animation.axis_aligned_bb();
		PostScriptWriter psout{filename, bb[0], bb[1] - bb[0]};

		// Use square line cap.
		psout << "2 setlinecap";

		const auto& root_layer = animation.get_root_layer();

		auto draw_curve_list = [&](const BezierPathList& curves, std::array<double, 3> c){
			if(curves.empty())
				return;

			psout.set_color(c[0], c[1], c[2]);
			for(const auto& d: curves)
				psout << d->front() << "moveto" << *d;
			psout << "stroke";
		};

		if(flag[show_auxiliary])
			draw_curve_list(get_all_auxiliary_curves(root_layer, f), {.7, .7, .7});

		if(flag[show_drawing])
			for(auto& drawable: animation.get_all_drawables(f))
				psout << *drawable;

		if(flag[show_sketch])
			draw_curve_list(get_all_guidelines(root_layer, f), {.47, .66, 1.});
	}
}

template<class Iterator>
void add_cubics_to_path(
	QPainterPath& myPath,
	Iterator pc,
	int cs,
	int ic = 0
){
	if(ic == 0)
		myPath.moveTo(pc[0][0], pc[0][1]);
	else
		myPath.lineTo(pc[0][0], pc[0][1]);

	for(auto i = 1; i < cs; i+=3)
		myPath.cubicTo(
			pc[i][0],   pc[i][1],
			pc[i+1][0], pc[i+1][1],
			pc[i+2][0], pc[i+2][1]
		);
}

/////////////////////////////////////////////////////////////////
/// Draw the fitting curve.
void FrameEditor::draw_fitting_curve(){
	if(fitting_curve.empty())
		return;
	
	draw_distance_field();

	static map<State, QColor> color_map{
		{drawing, QColor{0, 0, 0}},
		{sketching, QColor{120, 170, 255}},
		{experimental, QColor{0, 0, 180}}
	};
	color_map[drawing] = color_tool->get_bg_color();

	painter.setPen(color_map[curve_state]);

	painter.resetTransform();

	double cs = s;
	s = 1.0;
	draw_cubic_bezier(painter, fitting_curve.fitted_curve());
	if(flag[show_points]){
		painter.setBrush(QColor{0,0,0,0});
		painter.setPen(QColor{40, 40, 250});
		draw_control_points(painter, fitting_curve.fitted_curve());
	}

	QPainterPath myPath;
	bool first = true;
	for(const auto& p: fitting_curve.input_curve()){
		if(first)
			myPath.moveTo(p[0], p[1]);
		else
			myPath.lineTo(p[0], p[1]);
		first = false;
	}

	painter.setBrush(QColor{0,0,0,0});
	painter.setPen(QColor{250, 40, 40});
	painter.drawPath(myPath);

	s = cs;
	painter.setTransform(T);

}

// Draw the distance field.
void FrameEditor::draw_distance_field(){
	if(flag[show_distance_field] == false)
		return;

	int w_dist = fitting_curve.distance_field_width();
	QImage field_img{w_dist, w_dist, QImage::Format_RGB32};
	for(int i = 0; i < w_dist; ++i)
		for(int j = 0; j < w_dist; ++j){
			const Point& d = fitting_curve.distance(i,j);
			//double r = 255.0/(0.01*fabs(d[0])+1);
			double r = 255.0/(0.01*d.norm()+1);
			double g = 0;//255.0/(0.01*fabs(d[1])+1);
			double b = 0;
			field_img.setPixel(i, j, qRgb(r,g,b));
		}

	auto p = fitting_curve.field_geometry();

	painter.resetTransform();
	painter.drawImage(p[0], p[1], field_img);
	painter.setTransform(T);
}

///////////////////////////////////////////////////////////////////////////////
// Methods to draw data from a frame.

/// Draw the drawing of frame.
void FrameEditor::draw_drawing(const Drawing& drawing){
	if(flag[show_auxiliary]){
		painter.setPen(QColor{180, 180, 180});

		// Draw auxiliary curves.
		for(auto& c: drawing.get_auxiliary_curves())
			draw_cubic_bezier(painter, *c);
	}

	if(flag[show_drawing]){
		QPen pen;
		pen.setWidthF(1.0);
		painter.setPen(pen);

		// Draw curves.
		for(auto& d: drawing.get_drawables())
			if(d != nullptr)
				draw_drawable(*d);
	}
}

/// Draw all the control points of a Drawing.
void FrameEditor::draw_control_points(Drawing& drawing){
	QPen pen{QColor{40, 40, 250}};
	pen.setWidthF(1.0/s);
	painter.setBrush(QColor{0,0,0,0});
	painter.setPen(pen);

	// Draw control points.
	if(flag[show_drawing])
		for(auto& d: drawing.get_drawables())
			if(d->type() == 1)
				draw_control_points(
					painter,
					toBezierPath(*d)
				);

	if(flag[show_sketch])
		for(auto& c: drawing.get_guidelines())
			draw_control_points(painter, *c);
}

/// Draw the guidelines of frame.
void FrameEditor::draw_sketch(const Drawing& drawing, double opacity){
	QPen pen;
	pen.setWidthF(1.0);
	painter.setPen(pen);

	for(auto& c: drawing.get_guidelines()){
		pen.setColor(QColor::fromRgbF(120/255., 170/255., 1.0, opacity));
		painter.setPen(pen);

		draw_cubic_bezier(painter, *c);
	}
}

///////////////////////////////////////////////////////////////////////////////
// Methods to draw data from curves.

/// Draw a BezierPath.
void draw_bezier_path(const BezierPath& curve, QPainter& painter, bool clear_paint){
	const auto& c = curve.control_points();
	if(c.empty())
		return;

	QPen pen;
	pen.setWidthF(1.0);
	auto color = curve.get_color();

	if(clear_paint)
		pen.setColor(QColor{150, 150, 150});
	else if(!curve.is_selected())
		pen.setColor(QColor{color[0], color[1], color[2]});
	else
		pen.setColor(QColor{0, 200, 0});

	painter.setPen(pen);

	QPainterPath myPath;
	add_cubics_to_path(myPath, c.data(), c.size()-2);

	painter.drawPath(myPath);
}

/// Draw a Region.
void draw_region2d(
	const Region& region,
	QPainter& painter,
	bool clear_paint
){
	if(region.empty())
		return;

	auto color = region.get_color();

	QColor qcolor = clear_paint?
		QColor{200, 200, 200}:
		QColor{color[0], color[1], color[2]};

	int kmax = region.is_selected()? 2: 1;

	for(int k = 0; k < kmax; ++k){
		QBrush brush =
			(k == 0)?
			QBrush{qcolor, Qt::SolidPattern}:
			QBrush{Qt::black, Qt::Dense6Pattern};

		QPainterPath myPath;

		int ic = 0;
		for(const auto& bbc: region.get_curves()){
			const auto& curve = bbc.curve;
			const auto& c = curve->control_points();
			if(c.size() == 0)
				continue;

			auto cs = c.size()-2u;

			if(bbc.fw)
				add_cubics_to_path(myPath, c.begin(), cs, ic);
			else
				add_cubics_to_path(myPath, c.rbegin(), cs, ic);
			++ic;
		}

		/////////////////////////////////////////////////////
		painter.fillPath(myPath, brush);
	}
}

/// Draw a Drawable object.
void FrameEditor::draw_drawable(const Drawable& d){
	if(d.is_visible() == false)
		return;

	switch(d.type()){
		case 1:
			draw_bezier_path(
				static_cast<const BezierPath&>(d),
				painter,
				flag[clear_paint]
			);
			break;
		case 2:
			draw_region2d(
				static_cast<const Region&>(d),
				painter,
				flag[clear_paint]
			);
			break;
	}
}

/// Draw a cubic bezier curve.
void FrameEditor::draw_cubic_bezier(
	QPainter& painter,
	const BezierPath& stroke
){
	QPainterPath myPath;
	if(stroke.empty() == false){
		const auto& c = stroke.control_points();
		add_cubics_to_path(myPath, &c[0], c.size()-2);
	}

	painter.drawPath(myPath);
}

/// Draw the control points of a BezierPath object.
void FrameEditor::draw_control_points(
	QPainter& painter,
	const BezierPath& stroke
){
	QPainterPath myPath;

	const auto& points = stroke.control_points();

	double pSize = 5/s;

	auto ps = points.size();
	if(ps == 0)
		return;

	for(auto i = 0u; i < ps; ++i)
		painter.drawRect(
			QRectF{
				points[i][0] - pSize,
				points[i][1] - pSize,
				pSize*2, pSize*2
			}
		);

	auto draw_line = [&](const Point& p, const Point& q){
		painter.drawLine(QLineF{p[0], p[1], q[0], q[1]});
	};

	for(auto i = 0u; i < ps-1; i+=3){
		draw_line(points[ i ], points[i+1]);
		draw_line(points[i+2], points[i+3]);
	}

	painter.drawPath(myPath);
}

/// Draw all layers except specified layer at frame f.
void FrameEditor::draw(
	const Node<Layer>& layer, 
	int f, 
	const Node<Layer>* except
){
	if(&layer == except)
		return;

	auto& viewer = editor.get_viewer();
	auto drawings = viewer.get_visible_drawings(layer, f, except);

	for(auto d: drawings)
		draw_drawing(*d);

	if(flag[show_sketch])
		for(auto d: drawings)
			draw_sketch(*d);

	/////////////// Draw onion skin.
	if(!flag[show_previous_guides] or !viewer.is_current_keyframe())
		return;

	auto& animation = editor.get_animation();
	size_t p = f;
	if(!animation.prev_keyframe(p))
		return;

	drawings = viewer.get_visible_drawings(layer, p, except);
	for(auto d: drawings)
		draw_sketch(*d, .4);
}

/// Set cursor according to current tool.
void FrameEditor::update_cursor(){
	QCursor cursor = Qt::ArrowCursor;
	switch(tool){
		case pencil:
			cursor = (curve_state==drawing)?
				*cursors.pencil:
				*cursors.pencil_sketch;
			break;
		case eraser:
			cursor = (curve_state==drawing)?
				*cursors.eraser:
				*cursors.eraser_sketch;
			break;
		case curve_selector:
			cursor = *cursors.curve_selector;
			break;
		case region_selector:
			cursor = *cursors.region_selector;
			break;
		case center_editor:
			cursor = Qt::CrossCursor;
			break;
		case cp_editor:
			cursor = *cursors.cp_editor;
			break;
		default:
			break;
	}
	setCursor(cursor);
}

/// Draw the current frame.
void FrameEditor::draw_current_frame(){
	auto& viewer = editor.get_viewer();
	auto& animation = editor.get_animation();
	int f = viewer.current_frame_index();

	const auto& layer = viewer.get_base_layer();
	const auto& root = animation.get_root_layer();

	if(&layer != &root){
		flag.set(clear_paint);
		draw(root, f, &layer);
		flag.reset(clear_paint);
	}

	draw(layer, f);

	draw_center_point();

	draw_control_points();
}

/// Draw the center of rotation point.
void FrameEditor::draw_center_point(){
	if(!flag[show_center_point])
		return;
	auto& viewer = editor.get_viewer();

	auto drawing = viewer.get_drawing(false);
	if(drawing != nullptr and viewer.is_current_keyframe()){
		viewer.update_center_point();
		Point c = viewer.get_center_point();
		double r = 10.0/s;
		QRectF rect{c[0]-r, c[1]-r, 2*r, 2*r};
		QPen pen;
		pen.setWidthF(1.0/s);
		if(drawing->using_auto_center())
			pen.setColor(Qt::black);
		else
			pen.setColor(Qt::red);
		painter.setPen(pen);
		painter.drawArc(rect, 0, 360*16);
	}
}

/// Draw all the control points of the current frame/layer.
void FrameEditor::draw_control_points(){
	if(!flag[show_points])
		return;

	auto& viewer = editor.get_viewer();
	auto& layer = viewer.current_layer();
	if(layer.is_visible()){
		size_t f = viewer.current_frame_index();
		size_t l = viewer.current_layer_index();
		Drawing* d = viewer.get_drawing(f, l);
		if(d != nullptr)
			draw_control_points(*d);
	}
}

/// Draw the background image of the current frame.
void FrameEditor::draw_bg_image(){
	if(!flag[show_bg_image])
		return;

	auto& viewer = editor.get_viewer();
	painter.setOpacity(0.4);
	painter.drawImage(100, 100, images[viewer.current_frame_index()]);
	painter.setOpacity(1.0);
}

/////////////////////////////////////////////////////////////////
// Events

/// Paint event.
void FrameEditor::paintEvent(QPaintEvent* event){
	// Init painter.
	painter.begin(this);
	painter.fillRect(0, 0, width(), height(), Qt::white);
	painter.setRenderHint(QPainter::Antialiasing);
	painter.setTransform(T);

	update_cursor();

	draw_bg_image();

	draw_current_frame();

	draw_fitting_curve();

	painter.end();
}

void FrameEditor::pressed_right_button(const QPointF& pos){
	auto& viewer = editor.get_viewer();
	auto d = viewer.get_drawing(false);
	if(d == nullptr)
		return;

	if(!viewer.is_current_keyframe()){
		QMessageBox{
			QMessageBox::Warning,
				"Warning!",
				"Current frame is not a keyframe!",
				QMessageBox::Ok
		}.exec();
		return;
	}
	
	const Point p = toPoint(T.inverted()*pos);

	viewer.update_center_point();
	if(
		!flag[translating_view] and
		tool == center_editor and
		dist(p, viewer.get_center_point()) < 10.0/s
	){
		d->set_automatic_center(true);
		viewer.update_center_point();
		repaint();
		return;
	}
	
	if(tool == cp_editor){
		auto& viewer = editor.get_viewer();
		auto d = viewer.get_drawing(false);
		if(d == nullptr)
			return;

		// Remove control points near selected point.
		const Point p = toPoint(T.inverted()*pos);
		auto pt = d->control_point_at(p, 5.0/s);
		auto& curve = pt.second;
		if(curve == nullptr)
			return;

		int i = pt.first;
		if(i%3 == 0){
			curve->remove_control_point(i/3);
			if(curve->empty())
				d->remove_curve_or_guideline(curve);
			repaint();
		}
	}
}

void FrameEditor::pressed_left_button(const QPointF& pos, bool shift){
	// Do nothing if pressed another button while translating view.
	if(flag[translating_view])
		return;

	auto& viewer = editor.get_viewer();
	if(!viewer.is_current_keyframe()){
		QMessageBox{
			QMessageBox::Warning,
			"Warning!",
			"Current frame is not a keyframe!",
			QMessageBox::Ok
		}.exec();
		return;
	}
	
	if(!has_tablet or tablet_down or tool != pencil){
		const double fps = 120.0;
		const double time = 1000.0/fps;
		timer.start(time);
		flag.set(tool_active);
	}

	const Point p = toPoint(T.inverted()*pos);
	auto& selection = editor.get_selection();

	switch(tool){
		case pencil:
			if(has_tablet and !tablet_down)
				pressed_middle_button(pos);
			else{
				// Start curve input.
				fitting_curve.clear();
				fitting_curve.maximum_error(max_error);
				fitting_curve.insert_point(toPoint(pos));
			}
			break;

		case eraser:
			// Keep the current position for later use.
			last_point = p;
			break;

		case curve_selector:
			if(shift)
				// Deselect curves near p.
				selection.deselect_curves_at(p, 1.0/s);
			else
				// Select curves near p.
				selection.select_curves_at(p, 1.0/s);
			break;

		case region_selector:
			if(shift)
				// Deselect regions at p.
				selection.deselect_regions_at(p);
			else
				// Select regions at p.
				selection.select_regions_at(p);
			break;

		case center_editor:
			// Set rotation center.
			viewer.get_drawing(true)->set_center_point(p);
			viewer.update_center_point();
			break;

		case cp_editor:
			{
				// Set current control point.
				auto d = viewer.get_drawing(false);
				if(d != nullptr)
					control_point = d->control_point_at(p, 5.0/s);
			}
			break;
		default:
			break;
	}
	repaint();

}

void FrameEditor::pressed_middle_button(const QPointF& pos){
	if(flag[tool_active])
		return;
	
	// Start moving canvas.
	last_point = toPoint(pos);
	flag.set(translating_view);
	
	const double fps = 120.0;
	const double time = 1000.0/fps;
	timer.start(time);

	if(tool == cp_editor){
		auto& viewer = editor.get_viewer();
		auto d = viewer.get_drawing(false);
		if(d == nullptr)
			return;

		// Force G1 continuity at selected point.
		const Point p = toPoint(T.inverted()*pos);
		auto pt = d->control_point_at(p, 5.0/s);
		auto& curve = pt.second;
		if(curve == nullptr)
			return;

		int i = pt.first;
		if(i%3 == 0){
			curve->force_g1(i/3);
			repaint();
		}
	}
}

/// Mouse press event.
void FrameEditor::mousePressEvent(QMouseEvent* event){
	auto& viewer = editor.get_viewer();
	if(!viewer.current_layer().is_visible()){
		QMessageBox{
			QMessageBox::Warning,
			"Warning!",
			"Current layer is invisible!",
			QMessageBox::Ok
		}.exec();
		return;
	}

	bool shift = event->modifiers() == Qt::ShiftModifier;

	switch(event->button()){
		case Qt::RightButton:
			pressed_right_button(event->pos());
			break;
		case Qt::LeftButton:
			pressed_left_button(event->pos(), shift);
			break;
		case Qt::MiddleButton:
			pressed_middle_button(event->pos());
			break;
		default:
			break;
	}
}

/// Mouse release event.
void FrameEditor::mouseReleaseEvent(QMouseEvent* event){
	// Do nothing if released another button while translating view.
	if(flag[translating_view] and event->button() != Qt::MiddleButton){
		if(!has_tablet or event->button() != Qt::LeftButton)
			return;
	}
	
	// Do nothing if pressed another button while using some tool.
	if(flag[tool_active] and event->button() != Qt::LeftButton)
		return;
	
	timer.stop();

	/// Release control point (if any is selected).
	control_point = {0, nullptr};

	auto& viewer = editor.get_viewer();
	if(!viewer.current_layer().is_visible())
		return;

	flag.reset(tool_active);

	if(flag[translating_view]){
		QPointF ep = event->pos();
		last_point = toPoint(ep);
		flag.reset(translating_view);
		return;
	}

	if(event->button() != Qt::LeftButton)
		return;

	if(tool == cp_editor){
		viewer.update_auxiliary_curves();
		return;
	}

	if(tool != pencil)
		return;

	const auto& fc = fitting_curve.fitted_curve();
	if(fc.empty())
		return;

	auto newc = make_shared<BezierPath>(fc);
	fitting_curve.clear();

	QColor col = color_tool->get_bg_color();
	newc->set_color({col.red(), col.green(), col.blue()});
	newc->prune(2.0);

	if(newc->empty()){
		cout << "ignoring empty curve\n";
		repaint();
		return;
	}

	for(auto& p: newc->control_points())
		p = toPoint(T.inverted()*toQPoint(p));

	auto current_drawing = viewer.get_drawing(true);

	switch(curve_state){
		case drawing:
			current_drawing->hist_add(newc);
			viewer.update_auxiliary_curves();
			break;

		case sketching:
			current_drawing->add_guideline(newc);

			editor.get_animation().calculate_morphs();
			viewer.update_auxiliary_curves();
			break;
		case experimental:
			{
				/*
				   BezierPath_sp newc_shared = move(newc);
				   create_eps_lines(newc_shared, 3, eps_lines);
				   current_drawing->add_drawing(newc_shared);
				 */
			}
			break;
	}
	repaint();
}

/// Mouse move event.
void FrameEditor::mouseMoveEvent(QMouseEvent* event){
	auto& viewer = editor.get_viewer();
	if(!viewer.current_layer().is_visible())
		return;

	if(flag[translating_view]){
		Point p = toPoint(event->pos());

		Point dif = (1.0/s)*(p - last_point);

		T.translate(dif[0], dif[1]);
		last_point = p;
		return;
	}

	if(!flag[tool_active])
		return;

	const QPointF& qp = T.inverted()*QPointF{event->pos()};

	switch(tool){
		case pencil:
			fitting_curve.insert_point(
				toPoint(event->pos())
			);
			break;

		case eraser:
			{
				Point q = toPoint(qp);
				remove_curves_crossing(q);
				last_point = q;
			}
			break;

		case cp_editor:
			if(control_point.second != nullptr){
				auto i = control_point.first;
				auto curve = control_point.second;
				PointList& pts = curve->control_points();
				Point diff = toPoint(qp) - pts[i];
				pts[i] += diff;
				if(i%3 == 0){
					if(i != 0)
						pts[i-1] += diff;
					if(i != pts.size()-1)
						pts[i+1] += diff;
				}
			}

		default:
			break;
	}
}

/// Mouse wheel event.
void FrameEditor::wheelEvent(QWheelEvent* event){
	constexpr double ds = 1.1;
	constexpr double dsi = 1.0/ds;
	QPointF ep = event->pos();
	QPointF p = T.inverted()*QPointF(ep);

	if(event->delta() > 0){
		s *= ds;
		T.scale(ds, ds);
	}else{
		s /= ds;
		T.scale(dsi, dsi);
	}

	QPointF dif = T.inverted()*QPointF(ep) - p;
	T.translate(dif.x(), dif.y());

	repaint();
}
		
/// Touch event.
void FrameEditor::touchEvent(QTouchEvent* event){
	std::cout << "touch events are not supported yet\n";
}

/// Remove curves that are crossing segment from last_point to current_point.
void FrameEditor::remove_curves_crossing(const Point& current_point){
	auto& viewer = editor.get_viewer();
	Drawing* d = viewer.get_drawing(false);
	if(d == nullptr)
		return;

	switch(curve_state){
		case drawing:
			if(
				d->remove_curves_crossing(
					last_point,
					current_point
				)
			){
				viewer.update_auxiliary_curves();
				repaint();
			}
			break;
		case sketching:
			if(
				d->remove_guidelines_crossing(
					last_point,
					current_point
				)
			){
				viewer.update_auxiliary_curves();
				repaint();
			}
			break;
		case experimental:
			break;
	}
}

/// Update colors.
void FrameEditor::update_colors(){
	QColor fg_color = color_tool->get_color();
	QColor bg_color = color_tool->get_bg_color();

	auto f = [&](Drawable& d){
		QColor& color = (d.type() == 1)? bg_color: fg_color;
		d.set_color({color.red(), color.green(), color.blue()});
	};

	editor.get_selection().apply_to_selection(f);
}

/// Insert new frame at position i.
void FrameEditor::insert_frame(size_t i){
	if(i >= images.size())
		throw std::runtime_error{"Invalid index when inserting frame."};

	auto it = images.begin() + i;
	images.emplace(it);
	repaint();
}

/// Remove frame at position i.
void FrameEditor::remove_frame(size_t i){
	if(i >= images.size())
		throw std::runtime_error{"Invalid index when removing frame."};

	auto it = images.begin() + i;
	images.erase(it);
	repaint();
}
		
/// Tablet event.
void FrameEditor::tabletEvent(QTabletEvent* event){
	has_tablet = true;
			

	switch(event->type()){
		case QEvent::TabletPress:
			if(!tablet_down)
				tablet_down = true;
			break;

		case QEvent::TabletRelease:
			if(tablet_down)
				tablet_down = false;
			break;

		case QEvent::TabletMove:
			break;
		default:
			break;
	}
}

/// Action done when tablet pen enters.
void FrameEditor::tablet_enter(QTabletEvent* e){
	switch(e->pointerType()){
		case QTabletEvent::Pen:
			set_tool(pencil);
			break;
		case QTabletEvent::Eraser:
			set_tool(eraser);
			break;
		default:
			break;
	}
	update();
}


bool FrameEditor::event(QEvent* event){
	switch(event->type()){
		case QEvent::TabletEnterProximity:
		case QEvent::TabletLeaveProximity:
			std::cout << "tetete\n";
			break;
		case QEvent::TouchBegin:
		case QEvent::TouchUpdate:
		case QEvent::TouchEnd:
		case QEvent::TouchCancel:
			touchEvent(static_cast<QTouchEvent*>(event));
			return true;
		default:
			break;
	}
	return QWidget::event(event);
}
