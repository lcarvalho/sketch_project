#include "PostScriptWriter.h"

#include <iostream>

using std::string;

/// Constructor.
PostScriptWriter::PostScriptWriter(
	const string& filename,
	double _x0,
	double _y0,
	double _w,
	double _h
):
	out{filename}, 
	x0{_x0}, y0{_y0}, w{_w}, h{_h}
{
	out << std::fixed;
	out << "%!PS-Adobe-3.0 EPSF-3.0\n";
	out << "%%BoundingBox: " << x0 << ' ' << -y0-h << ' '
	    << x0+w << ' ' << -y0 << '\n';
	out << "1 -1 scale\n";
	
	closer = {nullptr, [&](void*){ out << "showpage\n"; }};
}

/// Constructor.
PostScriptWriter::PostScriptWriter(
	const string& filename,
	const Point& p0,
	const Point& d
) : PostScriptWriter{filename, p0[0], p0[1], d[0], d[1]}
{}

/// Define the color.
void PostScriptWriter::set_color(double r, double g, double b){
	out << r << ' ' << g << ' ' << b << " setrgbcolor\n";
}

/// Apply a translation.
void PostScriptWriter::translate(double dx, double dy){
	out << dx << ' ' << dy << " translate\n";
}

/// Draw a box.
void PostScriptWriter::box(const Point2& b){
	Point d{get_x(b[1] - b[0]), 0.0};

	*this << b[0] << "moveto"
		<< b[0] + d << "lineto"
		<< b[1] << "lineto"
		<< b[1] - d << "lineto"
		<< " closepath stroke";
}

/// Write a command from a string.
PostScriptWriter& PostScriptWriter::operator<<(const string& s){
	out << s << '\n';
	return *this;
}

/// Draw a point.
PostScriptWriter& PostScriptWriter::operator<<(const Point& p){
	out << p[0] << ' ' << p[1] << ' ';
	current = p;
	return *this;
}
		
/// Draw a cubic Bezier curve..
PostScriptWriter& PostScriptWriter::operator<<(const CubicBezier& c){
	if(current != c[0])
		*this << c[0] << "lineto";
	return *this << c[1] << c[2] << c[3] << "curveto";
}

/// Draw a path.
PostScriptWriter& PostScriptWriter::operator<<(const BezierPath& c){
	for(size_t i = 0u, nc = c.n_cubics(); i < nc; ++i)
		*this << c.get_cubic(i);
	return *this;
}
		
/// Draw a BezierBool.
PostScriptWriter& PostScriptWriter::operator<<(const BezierBool& bb){
	if(bb.fw)
		return *this << *bb.curve;

	const auto& p = bb.curve->control_points();
	return *this << BezierPath{{p.rbegin(), p.rend()}};
}

/// Draw a 2d region.
PostScriptWriter& PostScriptWriter::operator<<(const Region& c){
	const auto& curves = c.get_curves();

	if(curves.empty())
		return *this;

	// Move to the first point of first curve.
	*this << curves.front().front() << "moveto";

	// Draw all curves.
	for(const auto& bb: curves)
		*this << bb;

	return *this << "closepath eofill";
}

/// Draw a drawable.
PostScriptWriter& PostScriptWriter::operator<<(const Drawable& drawable){
	if(drawable.is_visible() == false)
		return *this;

	auto col = drawable.get_color();

	set_color(col[0]/255.0, col[1]/255.0, col[2]/255.0);

	switch(drawable.type()){
		case 1:
		{
			const auto& c = static_cast<const BezierPath&>(drawable);
			*this << c.front() << "moveto" << c << "stroke";
			break;
		}
		case 2:
			*this << static_cast<const Region&>(drawable);
			break;
	};
	return *this;
}

