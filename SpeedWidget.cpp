#include "SpeedWidget.h"
#include <iostream>
#include <iomanip>
#include "AnimationEditor.h"


/////////////////////////////////////////////////////////////
/// Constructor.
SpeedWidget::SpeedWidget(AnimationEditor& ed):
	editor(ed)
{
	setFixedSize(QSize{300, 300});
	setWindowTitle("Acceleration control");
	
	T.translate(0, 300);
	T.scale(300, -300);
}

/// Update curve.
void SpeedWidget::update_curve(){
	auto& animation = editor.get_animation();
	auto& viewer = editor.get_viewer();
	size_t f = viewer.current_frame_index();
	
	speed_control = animation.get_speed_controller(f);

	repaint();
}

/// Paint event.
void SpeedWidget::paintEvent(QPaintEvent* event){
	if(speed_control == nullptr)
		return;

	QPainter painter;
	painter.begin(this);
	
	painter.fillRect(0, 0, width(), height(), Qt::white);

	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.setTransform(T);
	QPen pen;
	pen.setWidth(0);
	painter.setPen(pen);

	Point p[] = {speed_control->get_p1(), speed_control->get_p2()};

	// Draw bezier curve.
	QPainterPath path;
	path.moveTo(0.0, 0.0);
	path.cubicTo(
		p[0][0], p[0][1], 
		p[1][0], p[1][1], 
		1.0, 1.0
	);
	painter.drawPath(path);

	for(int k: {0, 1}){
		QPointF c{p[k][0], p[k][1]};
		painter.drawLine(QPointF(k, k), c);
		painter.setBrush(Qt::black);
		painter.drawEllipse(c, r, r);
		painter.setBrush(QPalette().light());
		painter.drawEllipse(c, .9*r, .9*r);
	}
		
	if(selection != nullptr){
		const Point& pt = *selection;
		painter.setBrush(QPalette().highlight());
		QPointF c{pt[0], pt[1]};
		painter.drawEllipse(c, .9*r, .9*r);
	}
	
	painter.end();
}

/// Mouse press event.
void SpeedWidget::mousePressEvent(QMouseEvent* event){
	if(speed_control == nullptr)
		return;

	QPointF qp = T.inverted()*QPointF(event->pos());
	Point p{qp.x(), qp.y()};

	Point& c1 = speed_control->get_p1();
	Point& c2 = speed_control->get_p2();

	switch(event->button()){
		case Qt::RightButton:
			c1 = {0.3333, 0.3333};
			c2 = {0.6666, 0.6666};
			repaint();
			break;

		case Qt::LeftButton:
			selection = nullptr;

			if(dist(p, c1) <= r)
				selection = &c1;
			else if(dist(p, c2) <= r)
				selection = &c2;

			if(selection)
				last_pos = p;
			break;

		default:
			break;
	}

}

/// Mouse move event.
void SpeedWidget::mouseMoveEvent(QMouseEvent* event){
	if(selection == nullptr)
		return;

	QPointF qp = T.inverted()*QPointF(event->pos());
	
	Point p{qp.x(), qp.y()};

	Point newc = *selection + p - last_pos;
	
	if(newc[0] < 0.0 or newc[0] > 1.0 or newc[1] < 0.0 or newc[1] > 1.0)
		return;

	*selection = newc;
	
	last_pos = p;
	repaint();
}

/// Mouse move event.
void SpeedWidget::mouseReleaseEvent(QMouseEvent* event){
	selection = nullptr;
	repaint();
}
