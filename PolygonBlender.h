#ifndef POLYGON_BLENDER_H
#define POLYGON_BLENDER_H

#include <iomanip>
#include <vector>
#include "loc.h"
#include "Point.h"

using int2 = loc::pair_of<int>;

void randomize_parameters();

class PolygonBlender{
	public:
		
		/// Calculate the work required to bend one segment to the other.
		double bending_work(const Point3& Pi, const Point3& Pj);

		/// Calculate the work required to stretch one segment to the other.
		double stretching_work(const Point2& Pi, const Point2& Pj);

		/// Calculate work necessary to stretch and bend a portion of wires Ci and Cj.
		template<class FW, class Fw, class Fn>
		void calculate_weight(
			const PointList& Ci, 
			const PointList& Cj, 
			size_t i, size_t j, 
			FW W, 
			Fw west, 
			Fn north
		);

		/// Find path of pair of points reducing the work necessary to bend and stretch Ci and Cj.
		std::vector<int2> find_path(const PointList& Ci, const PointList& Cj);

		/// Get a clean version of the path, removing all pairs of non-terminal vertices.
		std::vector<int2> clear_path(
			const std::vector<int2>& path, 
			std::function<bool(int, size_t)> is_terminal
		);

		/// Define the parameters used in stretching calculation.
		void set_stretch_parameters(double ks, double es, double cs);
		
		/// Define the parameters used in bending calculation.
		void set_bend_parameters(double kb, double eb, double mb, double pb);
};

/// Calculate work necessary to stretch and bend a portion of wires Ci and Cj.
template<class FW, class Fw, class Fn>
void PolygonBlender::calculate_weight(
	const PointList& Ci, 
	const PointList& Cj, 
	size_t i, size_t j, 
	FW W, 
	Fw west, 
	Fn north
){
	if(i == 0 and j == 0){
		W(i, j) = 0.0;
		west(i, j) = 0;
		north(i, j) = 0;
		return;
	}

	const double inf = loc::infinity<double>();

	auto calculate_W = [&](size_t ni, size_t nj){
		return W(ni, nj)+
			stretching_work(
				{Ci[ni], Ci[i]},
				{Cj[nj], Cj[j]}
			) + 
			bending_work(
				{Ci[ni-west(ni, nj)], Ci[ni], Ci[i]},
				{Cj[nj-north(ni, nj)], Cj[nj], Cj[j]}
			);
	};

	double w[] = {
		(i == 0)? inf: calculate_W(i-1, j),
		(j == 0)? inf: calculate_W(i, j-1),
		(i == 0 or j == 0)? inf: calculate_W(i-1, j-1)
	};

	auto it = std::min_element(w, w+3);
	W(i, j) = *it;
	north(i, j) = it!=w;
	west(i, j) = it!=w+1;
}

#endif
