#ifndef SELECTION_H
#define SELECTION_H

#include <functional>
#include "AnimationViewer.h"

/// Manage selection and clipboard of data from Animation.
class Selection{
	public:
		/// Constructor.
		Selection(AnimationViewer& viewer);

		/// Select curves that are close to point p.
		void select_curves_at(const Point p, double scale = 1.0);

		/// Select regions that contain point p.
		void select_regions_at(const Point p);

		/// Deselect curves that are close to point p.
		void deselect_curves_at(const Point p, double scale = 1.0);

		/// Deselect regions that contain point p.
		void deselect_regions_at(const Point p);

		/// Select all objects.
		void select_all();

		/// Deselect all objects.
		void select_none();

		/// Copy selection.
		void copy_selection_to_clipboard();

		/// Cut selection.
		void selection_to_clipboard();

		/// Paste clipboard into current frame/layer.
		void paste_clipboard();

		/// Bring selected objects up.
		void uplevel_selected();

		/// Bring selected objects down.
		void downlevel_selected();

		/// Create a region from the selected curves.
		void make_region_from_selection(const Color& color, const Color& curves_color);

		/// Remove everything that is selected.
		void remove_selected();

		/// Apply a function to all selected objects.
		void apply_to_selection(std::function<void(Drawable&)> function);

		/// Copy guidelines from current frame and layer to clipboard.
		void copy_guidelines();

		/// Paste guidelines from clipboard to current frame and layer.
		void paste_guidelines();

		/// Update the current drawing (deselect everything if drawing changed).
		void update_drawing();

	private:
		/// Viewer bounded to this Selection object.
		AnimationViewer& viewer;

		/// List of selected objects.
		DrawableList selected;

		/// Drawing that contains the selection.
		Drawing* selection_drawing = nullptr;

		/// Objects in clipboard.
		DrawableList clipboard;

		/// List of selected objects.
		std::vector<BezierPath> guidelines_clipboard;
};

/// Constructor
inline Selection::Selection(AnimationViewer& _viewer):
	viewer(_viewer)
{}


/// Apply a function to all selected objects.
inline void Selection::apply_to_selection(std::function<void(Drawable&)> function){
	for(auto& d: selected)
		function(*d);
}
#endif
