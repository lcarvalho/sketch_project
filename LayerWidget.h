#ifndef LAYER_WIDGET_H
#define LAYER_WIDGET_H

#include <QtWidgets>
#include "Animation.h"

class AnimationEditor;

/// An item with Layer information.
struct LayerItem{
	/// Layer.
	Node<Layer>* layer;

	/// Rectangle where the layer appears.
	QRect rect;
};

/// Qt Widget to edit frames from an animation.
class LayerWidget : public QWidget{
	Q_OBJECT

	public:
		/// Constructor.
		LayerWidget(AnimationEditor* editor);

		/// Prepare data.
		void update_data();

	private:
		/// Resize event.
		void resizeEvent(QResizeEvent* event);

		/// Paint event.
		void paintEvent(QPaintEvent* event);

		/// Mouse press event.
		void mousePressEvent(QMouseEvent* event);

		/// Mouse double click event.
		void mouseDoubleClickEvent(QMouseEvent* event);
		
		/// Define current layer name from text_editor.
		void set_layer_name();

		/// Create the menu items.
		void setup_menu();

		////////////////////////////////////////////////////////////////
		/// Animation editor.
		AnimationEditor* editor;

		/// Layer name editor.
		QLineEdit* text_editor;

		/// Width of the label blocks.
		double w_label;

		/// Width of the shift of the sub layers blocks.
		double w_shift;

		/// Width reserved for the plus sign.
		double w_plus;

		// width reserved for the arrow.
		double w_arrow;

		/// Vertical size of label blocks.
		double h_label;

		/// Vertical separation between label blocks.
		double h_sep;

		/// Separation between text and blocks.
		double text_sep;

		/// Background color of base layer.
		QColor color_bg_base;

		/// Background color of sub layers.
		QColor color_bg_sublayer;

		/// Background color of 'add layer' button.
		QColor color_bg_add_layer;

		/// Color of selected layer.
		QColor color_selected;

		/// List of rectangles that contain the layers.
		std::vector<LayerItem> layer_items;

		/// List of rectangles that contain the layers.
		std::vector<QRectF> circle_rects;
		
		/// Menu to control frames.
		QMenu* menu;
};

#endif

