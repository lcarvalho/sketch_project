#include "Point.h"
#include <limits>

using std::vector;
using std::max;

/// Get the Hausdorff distance between two point lists.
double hausdorff_distance(const PointList& pts1, const PointList& pts2){
	if(pts1.empty() and pts2.empty())
		return 0.0;

	if(pts1.empty() or pts2.empty())
		return loc::infinity<double>();

	auto asym_hausdorff = [](const PointList& pts1, const PointList& pts2){
		vector<double> ds(pts2.size()), min_ds;
		min_ds.reserve(pts1.size());

		for(const Point& p1: pts1){
			loc::transform(
					pts2, ds,
					[&](const Point& p2){ return dist2(p1, p2); }
				      );
			min_ds.push_back(*loc::min_element(ds));
		}
		return *loc::max_element(min_ds);
	};

	return sqrt(
		max(
			asym_hausdorff(pts1, pts2), 
			asym_hausdorff(pts2, pts1)
		)
	);
}

/// Calculate the length of a polygonal curve.
double length(const PointList& p){
	double l = 0.0;

	for(size_t i = 1; i < p.size(); ++i)
		l += dist(p[i], p[i-1]);

	return l;
}
