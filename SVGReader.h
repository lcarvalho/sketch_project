#ifndef SVG_READER_H
#define SVG_READER_H

#include "Xml.h"
#include "BezierPath.h"
#include "Region.h"

/// Class that read and process svg files.
class SVGReader{
	public:
		/// Constructor.
		SVGReader(const std::string& filename);

		/// Get drawable objects from svg file.
		DrawableList get_drawables() const;

	private:
		/// Xml object.
		Xml xml;
};

#endif

