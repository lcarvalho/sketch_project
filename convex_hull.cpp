#include "loc.h"
#include "Point.h"
#include <functional>


inline double sign_area(Point a, Point b, Point c){
	return cross(b-a, c-a);
}

PointList convex_hull(const Point& a, const Point& b, const PointList& pts){
	if(pts.empty())
		return {a};

	using namespace std::placeholders;
	using std::bind;

	Point max = *loc::max_val_element(pts, bind(sign_area, a, b, _1));

	PointList ac, cb;
	for(const Point& p: pts)
		if(sign_area(a, max, p) > 0)
			ac.push_back(p);
		else if(sign_area(max, b, p) > 0)
			cb.push_back(p);

	PointList ac_hull = convex_hull(a, max, ac);
	PointList cb_hull = convex_hull(max, b, cb);

	loc::insert_back(ac_hull, cb_hull);

	return ac_hull;
}

PointList convex_hull(const PointList& pts){
	if(pts.empty())
		return {};

	auto x_value = [](const Point& p) { return p[0]; };
	Point min = *loc::min_val_element(pts, x_value);
	Point max = *loc::max_val_element(pts, x_value);

	PointList up, down;
	for(const Point& p: pts){
		double s = sign_area(min, max, p);
		if(s > 0)
			up.push_back(p);
		else if(s < 0)
			down.push_back(p);
	}

	PointList hull_up = convex_hull(min, max, up);

	loc::insert_back(hull_up, convex_hull(max, min, down));

	return hull_up;
}
