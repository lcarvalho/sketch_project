#include "Xml.h"
#include "loc.h"

using std::istream;
using std::ostream;

/// Read data from input stream.
istream& operator>>(istream& is, Xml& xml){
	loc::ignore_spaces(is);

	auto& tags = xml.tags;

	while(is.peek() == '<'){
		tags.emplace_back();
		is >> tags.back();
		loc::ignore_spaces(is);
	}

	return is;
}

/// Write data to output stream.
ostream& operator<<(ostream& os, const Xml& xml){
	for(const auto& tag: xml.tags)
		os << tag;
	return os;
}
