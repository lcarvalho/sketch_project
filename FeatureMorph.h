#ifndef FEATURE_MORPH_H
#define FEATURE_MORPH_H

#include "Morph.h"

class FeatureMorph : public Morph{
	public:
	////////////////////////////////////////////////////////////////////////
	/// Calculte morph transforming ps to qs.
	/** Calculate the morph that transforms the curves
	* in ps to points into qs.
	*/
	FeatureMorph(const loc::vector_pair<PointList>& data = {});

	/// Apply morph to a single point.
	Point morph(const Point& c) const override;

	private:
		
	std::pair<Point, double> calculate_displacements(
		const Point& c, 
		const loc::pair_of<PointList>& p
	) const;

	std::pair<Point, double> displacement(
		const Point& c, 
		const Point& P, const Point& Q,
		const Point& Px, const Point& Qx
	) const;
	
	////////////////////////////////////////////////////////////////////////
	// Parameters

	loc::vector_pair<PointList> data;
};



inline FeatureMorph::FeatureMorph(const loc::vector_pair<PointList>& data):
	data{data}
{}


/// Apply morph to a single point.
inline Point FeatureMorph::morph(const Point& c) const{
	Point dsum;
	double weight_sum = 0.0;
	for(const auto& p: data){
		auto x = calculate_displacements(c, p);
		dsum += x.first;
		weight_sum += x.second;
	}

	return (weight_sum == 0.0)? c: c + (1.0/weight_sum)*dsum;
}
	

inline std::pair<Point, double> FeatureMorph::calculate_displacements(
	const Point& c, 
	const loc::pair_of<PointList>& p
) const{
	const auto& P0 = p.first;
	const auto& P1 = p.second;

	size_t n = std::min(p.first.size(), p.second.size());
	
	Point dsum;
	double weight_sum = 0.0;
	
	for(size_t i = 1; i < n; ++i){
		auto x = displacement(c, P0[i-1], P0[i], P1[i-1], P1[i]);
		dsum += x.first;
		weight_sum += x.second;
	}

	return {dsum, weight_sum};
}

inline std::pair<Point, double> FeatureMorph::displacement(
	const Point& X, 
	const Point& P, const Point& Q,
	const Point& Px, const Point& Qx
) const{
	Point PX = X-P;
	Point PQ = Q-P;
	double u = dot(PX, PQ)/PQ.norm2();
	double v = dot(PX, PQ.perp())/PQ.norm();

	Point PQx = Qx - Px;
	Point Xx = Px + u*PQx + (v/PQx.norm())*PQx.perp();

	double a = 1e-5;
	double b = 0.5;
	double p = 1.0;

	double lenght = PQ.norm();
	double d = (u < 0)? dist(P, X): (u > 1)? dist(Q, X): std::fabs(v);

	double weight = std::pow(std::pow(lenght, p)/(a + d), b);

	return {weight*(Xx - X), weight};
}

#endif
