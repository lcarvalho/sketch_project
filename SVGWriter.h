#ifndef SVG_WRITER_H
#define SVG_WRITER_H

#include <fstream>
#include <vector>
#include "BezierPath.h"
#include "Region.h"
#include "Drawable.h"

/// Object to write svg files.
class SVGWriter{

	public:
		/// Constructor.
		SVGWriter(
			const std::string& filename,
			double x0,
			double y0,
			double w,
			double h
		);

		/// Create a group with identifier gid, and flag 'visible'.
		void create_group(int gid, bool visible = true);

		/// Finishes a group.
		void end_group();

		/// Define the visibility duration of groups from 0 to maxid.
		void set_visibility(int maxgid, double dur);

		/// Create path/region from a Drawing.
		void create(const Drawable& d);

		/// Create a path from a curve.
		void create_path(const BezierPath& path);

		/// Write the path info from a curve.
		void write_path_d(const BezierPath& curve, bool start = true);

		/// Create a region.
		void create_region(const Region& region);

		/// Create a list of paths from curves.
		void create_paths(const BezierPathList& curves);

		/// Create a list of paths from regions.
		void create_paths(const RegionList& regions);

		/// Create animation for a path.
		void animate_path(
			const std::vector<BezierPath*>& paths,
			int id,
			double begin,
			double end
		);
	private:

		/// Output stream.
		std::ofstream out;
		
		/// File closer.
		std::shared_ptr<void> closer;

		/// Bounding box minimum x.
		double x0;

		/// Bounding box minimum y.
		double y0;

		/// Bounding box width.
		double w;

		/// Bounding box height.
		double h;
};

/// Create path/region from a Drawing.
inline void SVGWriter::create(const Drawable& d){
	if(d.type() == 1)
		create_path(static_cast<const BezierPath&>(d));
	else
		create_region(static_cast<const Region&>(d));
}


#endif

