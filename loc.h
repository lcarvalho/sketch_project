#ifndef LOC_H
#define LOC_H

#include <iostream>
#include <algorithm>
#include <numeric>
#include <vector>
#include <array>
#include <memory>
#include <iterator>
#include <limits>
#include <cmath>
#include <fstream>
#include <sstream>
#include "json11.hpp"

/// Utilities definitions.
namespace loc{
	/// Math pi constant.
	constexpr double pi = 3.141592653589793;

	/// Literals.
	namespace literals{
		/// String literals
		inline std::string operator"" _s(const char* p, size_t n){
			return {p, n};
		}
	}

	template<class T>
	auto linerp(double alpha, const T& t0, const T& t1){
		return (1.0-alpha)*t0 + alpha*t1;
	}

	/// Pair of objects of the same type.
	template<class T>
	using pair_of = std::pair<T, T>;

	/// Vector of a pair of objects.
	template<class T1, class T2=T1>
	using vector_pair = std::vector<std::pair<T1, T2>>;

	/// Template alias for the value type of a container.
	template<class Cont>
	using ValueType = typename Cont::value_type;

	/// Template alias for enable_if
	template<bool cond, class T>
	using Enable_if = typename std::enable_if<cond, T>::type;

	/// Construct an object of type T and wrap it in a std::unique_ptr.
	template<class T, class... Args>
	inline std::unique_ptr<T> make_unique(Args&&... args){
		return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
	}

	/// Infinity of type T.
	template<class T>
	constexpr Enable_if<std::numeric_limits<T>::has_infinity, T> infinity(){
		return std::numeric_limits<T>::infinity();
	}

	/// Create a shared_ptr with a copy of the original pointee.
	template<class T>
	inline std::shared_ptr<T> copy_shared(const std::shared_ptr<T>& p){
		return std::make_shared<T>(*p);	
	}
	
	/// Create a shared pointer by copying/moving an object.
	template<class T>
	inline auto shared(T&& p){
		using TT = typename std::decay<T>::type;
		return std::make_shared<TT>(std::forward<T>(p));	
	}
	
	/// Ignore spaces from an input stream.
	inline void ignore_spaces(std::istream& is){
		while(std::isspace(is.peek()))
			is.ignore();
	}

	/// Insert elements from parameters into container, properly converted to
	/// the value type used by the container.
	template<class Container, class... T>
	inline void insert_values(Container& c, T&&... t){
		c.insert(c.end(), {typename Container::value_type(t)...});
	}

	/// Insert elements from second container into first one.
	template<class Container1, class Container2>
	inline void insert_back(Container1& c1, const Container2& c2){
		c1.insert(c1.end(), begin(c2), end(c2));
	}

	/// Create a vector with all the elements from a vector of containers.
	template<class Container>
	inline Container join_all(const std::vector<Container>& sets){
		Container ret;
		for(const Container& c: sets)
			insert_back(ret, c);
		return ret;
	}

	/// Standard function begin.
	using std::begin;

	/// Standard function end.
	using std::end;

	////////////////////////////////////////////////////////////////////////
	// Functions in <algorithm>
	// Non-modifying sequence operations:

	/// Test condition on all elements in container.
	template<class Cont, class Predicate>
	inline bool all_of(Cont&& c, Predicate p){
		return std::all_of(begin(c), end(c), p);
	}

	/// Test if any element in container fulfills condition.
	template<class Cont, class Predicate>
	inline bool any_of(Cont&& c, Predicate p){
		return std::any_of(begin(c), end(c), p);
	}

	/// Apply operation to each element of a container.
	template<class Container, class Op>
	inline void for_each(Container&& c, Op op){
		std::for_each(begin(c), end(c), op);
	}

	/// Find value in container.
	template<class Cont, class T>
	inline auto find(Cont&& c, const T& val){
		return std::find(begin(c), end(c), val);
	}

	/// Find element in container.
	template<class Cont, class Predicate>
	inline auto find_if(Cont&& c, Predicate p){
		return std::find_if(begin(c), end(c), p);
	}

	/// Find element from set in container.
	template <class Container1, class Container2>
	inline auto find_first_of(Container1&& c1, Container2&& c2){
		return std::find_first_of(begin(c1), end(c1), begin(c2), end(c2));

	}
	/// Find element from set in container with predicate.
	template <class Container1, class Container2, class BinPredicate>
	inline auto find_first_of(
		Container1&& c1,
		Container2&& c2,
		BinPredicate pred
	){
		return std::find_first_of(
			begin(c1), end(c1),
			begin(c2), end(c2),
			pred
		);
	}

	/// Count appearances of value in container.
	template <class Container, class T>
	inline auto count(Container&& c, const T& val){
		return std::count(begin(c), end(c), val);
	}

	////////////////////////////////////////////////////////////////////////
	// Functions in <algorithm>
	// Modifying sequence operations:

	/// Transform container.
	template<class Cont1, class Cont2, class Predicate>
	inline void transform(const Cont1& c_in, Cont2& c_out, Predicate p){
		std::transform(begin(c_in), end(c_in), begin(c_out), p);
	}

	/// Replace value in container.
	template<class Cont, class T>
	inline void replace(Cont& c, const T& old_value, const T& new_value){
		std::replace(begin(c), end(c), old_value, new_value);
	}

	/// Generate values for a container with function.
	template<class Cont, class Generator>
	inline void generate(Cont& c, Generator gen){
		std::generate(begin(c), end(c), gen);
	}
	
	/// Remove consecutive duplicates in a container.
	template<class Container>
	inline auto unique(Container& c){
		return std::unique(begin(c), end(c));
	}

	/// Remove consecutive duplicates in a vector and resize it.
	template<class T>
	inline void remove_duplicates(std::vector<T>& v){
		v.resize(
			std::distance(v.begin(), unique(v))
		);
	}

	/// Reverse the elements of a container.
	template<class Container>
	inline void reverse(Container& c){
		std::reverse(begin(c), end(c));
	}

	////////////////////////////////////////////////////////////////////////
	// Functions in <algorithm>
	// Partition.
	template<class Container, class UnaryPredicate>
	inline auto partition(Container& c, UnaryPredicate pred){
		return std::partition(begin(c), end(c), pred);
	}

	////////////////////////////////////////////////////////////////////////
	// Functions in <algorithm>
	// Sorting.

	/// Sort elements in container.
	template<class Cont>
	inline void sort(Cont& c){
		std::sort(begin(c), end(c));
	}

	/// Sort elements in container using comp to compare objects.
	template<class Cont, class Compare>
	inline void sort(Cont& c, Compare&& comp){
		std::sort(begin(c), end(c), comp);
	}
	
	/// Sort elements preserving order of equivalents.
	template<class Cont>
	inline void stable_sort(Cont& c){
		std::stable_sort(begin(c), end(c));
	}
	
	/// Sort elements preserving order of equivalents, using comp to compare objects.
	template<class Cont, class Compare>
	inline void stable_sort(Cont& c, Compare&& comp){
		std::stable_sort(begin(c), end(c), comp);
	}

	/// Check whether container is sorted.
	template<class Cont>
	inline bool is_sorted(const Cont& c){
		return std::is_sorted(begin(c), end(c));
	}
	
	////////////////////////////////////////////////////////////////////////
	// Functions in <algorithm>
	// Binary search.
	template<class Cont, class T>
	inline auto upper_bound(Cont&& c, T val){
		return std::upper_bound(begin(c), end(c), val);
	}
	
	// Binary search.
	template<class Cont, class T>
	inline auto lower_bound(Cont&& c, T val){
		return std::lower_bound(begin(c), end(c), val);
	}
	
	////////////////////////////////////////////////////////////////////////
	// Functions in <algorithm>
	// Merge.

	/// Merge sorted containers.
	template<class Cont1, class Cont2, class Cont3>
	inline void merge(const Cont1& c1, const Cont2& c2, Cont3& r){
		std::merge(begin(c1), end(c1), begin(c2), end(c2), begin(r));
	}

	////////////////////////////////////////////////////////////////////////
	// Functions in <algorithm>
	// Min/max.

	/// Return smallest element in container.
	template<class Cont>
	inline auto min_element(Cont&& c){
		return std::min_element(begin(c), end(c));
	}

	/// Return largest element in container.
	template<class Cont>
	inline auto max_element(Cont&& c){
		return std::max_element(begin(c), end(c));
	}

	/// Return smallest and largest elements in container.
	template<class Cont>
	inline auto minmax_element(Cont&& c){
		return std::minmax_element(begin(c), end(c));
	}

	////////////////////////////////////////////////////////////////////////
	// Functions in <numeric>

	/// Accumulate values in container.
	template<class Cont, class T>
	inline T accumulate(const Cont& c, T init){
		return std::accumulate(begin(c), end(c), init);
	}

	/// Accumulate values in container.
	template<class Cont, class T, class Op>
	inline T accumulate(const Cont& c, T init, Op op){
		return std::accumulate(begin(c), end(c), init, op);
	}

	/// Compute cumulative inner product of container.
	template<class Cont1, class Cont2, class T>
	inline T inner_product(const Cont1& c1, const Cont2& c2, T init){
		return std::inner_product(begin(c1), end(c1), begin(c2), init);
	}

	/// Store increasing sequence in container.
	template <class Cont, class T>
	void iota(Cont& cont, T val){
		std::iota(begin(cont), end(cont), val);
	}

	////////////////////////////////////////////////////////////////////////
	// Functions created by me.

	/// Assign all elements from a input stream to a container.
	template<class Container>
	inline void assign(std::istream& in, Container& v){
		using T = ValueType<Container>;
		std::istream_iterator<T> b{in}, e;
		v.assign(b, e);
	}

	/// Average of elements in container.
	template <class Container>
	inline auto average(const Container& vals){
		ValueType<Container> z = 0;
		return (1.0/vals.size())*accumulate(vals, z);
	}

	/// Standar deviation of values in container.
	template<class Container>
	double standard_deviation(const Container& vals){
		const auto n = vals.size();
		const auto avg = average(vals);

		double sum = 0.0;
		for(auto v: vals)
			sum += std::pow(v - avg, 2);

		return std::sqrt(sum/n);
	}

	/// Weighted average of elements in container.
	template <class Container, class WContainer>
	inline auto weighted_average(const Container& vals, const WContainer& weights){
		ValueType<WContainer> zw = 0;
		auto ws = accumulate(weights, zw);

		ValueType<Container> z = 0;
		return (1.0/ws)*inner_product(weights, vals, z);
	}

	/// Find the element with minimum value in range defined by operator f.
	template <class It, class Operator>
	It min_val_element(It first, It last, Operator f){
		if(first == last)
			return last;

		auto smallest = first;
		auto min_val = f(*smallest);

		while(++first != last){
			auto fp = f(*first);
			if(fp < min_val){
				smallest = first;
				min_val = fp;
			}
		}

		return smallest;
	}

	/// Find the element with minimum value in container defined by operator f.
	template <class Container, class Operator>
	inline auto min_val_element(Container&& c, Operator f){
		return min_val_element(begin(c), end(c), f);
	}

	/// Find the elements with minimum and maximum value in range defined by operator f.
	template <class Iterator, class Operator>
	pair_of<Iterator> minmax_val_element(
		Iterator first,
		Iterator last,
		Operator f
	){
		if(first == last)
			return {last, last};

		auto smallest = first;
		auto min_val = f(*smallest);

		auto largest = smallest;
		auto max_val = min_val;

		while(++first != last){
			auto fp = f(*first);
			if(fp < min_val){
				smallest = first;
				min_val = fp;
			}else if(fp > max_val){
				largest = first;
				max_val = fp;
			}
		}

		return {smallest, largest};
	}

	/// Find the elements with minimum and maximum value in container defined by operator f.
	template <class Container, class Operator>
	inline auto minmax_val_element(Container& c, Operator f){
		return minmax_val_element(begin(c), end(c), f);
	}

	/// Find the element with maximum value in range defined by operator f.
	template <class ForwardIterator, class Operator>
	ForwardIterator max_val_element(
		ForwardIterator first,
		ForwardIterator last,
		Operator&& f
	){
		if(first == last)
			return last;

		ForwardIterator maximum = first;
		auto max_val = f(*maximum);

		while(++first != last){
			auto fp = f(*first);
			if(fp > max_val){
				maximum = first;
				max_val = fp;
			}
		}

		return maximum;
	}

	/// Find the element with maximum value in container defined by operator f.
	template <class Container, class Operator>
	inline auto max_val_element(Container&& c, Operator&& f){
		return max_val_element(begin(c), end(c), std::forward<Operator>(f));
	}

	/// Remove specific element from a container
	/// the element type  must be convertible to the type of elements in container.
	template<class Container, class D>
	inline void remove_element(Container& v, const D& t){
		auto it = find(v, t);
		if(it != end(v))
			v.erase(it);
	}

	/// Remove all elements in container that satisfy operation op.
	template<class Container, class Op>
	bool remove_if_and_erase(Container& v, Op op){
		auto b = begin(v), e = end(v);
		return e != v.erase(std::remove_if(b, e, op), e);
	}


	/// Read json data from file.
	inline json11::Json read_file(const std::string& file){
		std::ifstream in{file};
		std::string buf, line, err;
		while(std::getline(in, line))
			buf += line + '\n';

		return json11::Json::parse(buf, err);
	}

	/// Convert object to string.
	template<class T>
	inline auto to_string(const T& t){
	    std::ostringstream out;
	    out << t;
	    return out.str();
	}
}

#endif

