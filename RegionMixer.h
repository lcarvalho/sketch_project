#ifndef REGION_MIXER_H
#define REGION_MIXER_H

#include "Region.h"
#include "SimilarityMorph.h"
#include "loc.h"
#include "CurveMixer.h"
#include <vector>

class RegionMixer;
using RegionMixer_up = std::unique_ptr<RegionMixer>;
using vector_int2 = loc::vector_pair<int>;

/// Region created as a mix of two other regions.
class RegionMixer{
	public:
		/// Constructor.
		RegionMixer(
			const Region& region1,
			const Region& region2,
			const vector_int2& corresps,
			const Morph& morph,
			const std::vector<CurveMixer_sp>& curve_mixers
		);

		/// Constructs a new region at parameter alpha in [0,1].
		//Region_sp region_at(double alpha);

		/// Constructs a new region at parameter alpha in [0,1].
		Region_sp region_at(
			double alpha,
			const SimilarityMorph& M0,
			const SimilarityMorph& M1,
			const SimilarityMorph& MR = {}
		);

		/// Calculate distance between regions H(region1) and region2.
		double distance(Morph& H) const;

	private:
		/// Check which direction is better for correspondence.
		bool correspondence_distance(
			const loc::vector_pair<int,int>& corresps,
			const Morph& morph
		);

		/// Reverse curves from region 2.
		void reverse_curves2(vector_int2& corresps);

		/// Create curve mixers.
		void create_mixers(
			const vector_int2& corresps,
			const std::vector<CurveMixer_sp>& curve_mixers
		);

		/// Rearrange curves.
		template<size_t I>
		std::map<int, int> rearrange_curves(
			const vector_int2& corresps
		);

		// Check order of curves.
		void check_order(const vector_int2& new_corresps);

		////////////////////////////////////////////////////////////////
		// Parameters.

		// Region 1.
		const Region& region1;

		// Region 2.
		const Region& region2;

		/// Curves from region 1.
		BBList bbs1;

		/// Curves from region 2.
		BBList bbs2;

		/// Color of region 1.
		Color color1;

		/// Color of region 2.
		Color color2;
		
		/// Curve mixers.
		loc::vector_pair<CurveMixer_sp, bool> mixers;

		/// Last alpha used to create a region.
		double last_alpha;

		/// Region created using last alpha.
		Region_sp region;
};

#endif

