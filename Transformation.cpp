#include "Transformation.h"
#include "loc.h"

using std::array;

/// Constructor from parameters.
Transformation::Transformation(
	const array<double, 4>& _A,
	const Point& _a
)
	:A(_A), a(_a)
{}

/// Apply transformation to point p.
Point Transformation::operator()(const Point& p) const{
	return {A[0]*p[0] + A[2]*p[1] + a[0], A[1]*p[0] + A[3]*p[1] + a[1]};
}

/// Apply transformation to list of points.
PointList& Transformation::operator()(PointList& pts) const{
	const Transformation& T = *this;
	for(Point& p: pts)
		p = T(p);
	return pts;
}

/// Combine two transformations.
Transformation operator*(const Transformation& T1, const Transformation& T2){
	const auto& A = T1.A;

	const auto& B = T2.A;
	const auto& b = T2.a;

	array<double, 4> R;

	R[0] = A[0]*B[0] + A[2]*B[1];
	R[1] = A[1]*B[0] + A[3]*B[1];
	R[2] = A[0]*B[2] + A[2]*B[3];
	R[3] = A[1]*B[2] + A[3]*B[3];

	Point r = T1(b);

	return {R, r};
}

/// Get an identity transformation.
Transformation Transformation::identity(){
	array<double, 4> A;
	A[0] = A[3] = 1.0;
	A[1] = A[2] = 0.0;
	return {A, {0.0, 0.0}};
}

/// Get a scale transformation.
Transformation Transformation::scale(double sx, double sy){
	array<double, 4> A;
	A[0] = sx;
	A[3] = sy;
	A[1] = A[2] = 0.0;
	return {A, {0.0, 0.0}};
}

/// Get a translate transformation.
Transformation Transformation::translate(double tx, double ty){
	array<double, 4> A;
	A[0] = A[3] = 1.0;
	A[1] = A[2] = 0.0;
	return {A, {tx, ty}};
}

/// Get a rotation transformation.
Transformation Transformation::rotate(double angle, const Point& c){
	double angle_rad = angle*loc::pi/180.0;

	double cosa = cos(angle_rad);
	double sina = sin(angle_rad);

	array<double, 4> R;
	R[0] = cosa;
	R[1] = sina;
	R[2] = -sina;
	R[3] = cosa;

	return translate(c[0], c[1])*Transformation{R}*translate(-c[0], -c[1]);
}

/// Get a skew in x transformation.
Transformation Transformation::skewX(double angle){
	auto T = identity();
	T.A[2] = tan(angle*loc::pi/180.0);
	return T;
}

/// Get a skew in x transformation.
Transformation Transformation::skewY(double angle){
	auto T = identity();
	T.A[1] = tan(angle*loc::pi/180.0);
	return T;
}

