#ifndef DRAWING_OPERATIONS_H
#define DRAWING_OPERATIONS_H

#include "Drawing.h"

////////////////////////////////////////////////////////////////////////////////
/// Operation related to the addition of a curve into a drawing.
class AddCurveOperation : public Operation{
	public:
		/// Constructor.
		AddCurveOperation(const BezierPath_sp& _curve, Drawing& _d);

		/// Undo operation.
		void undo() override;

		/// Redo operation.
		void redo() override;
	private:
		/// Drawing.
		Drawing& d;

		/// Curve added into drawing.
		BezierPath_sp curve;
};

////////////////////////////////////////////////////////////////////////////////
/// Operation related to the addition of a region into a drawing.
class AddRegionOperation : public Operation{
	public:
		/// Constructor.
		AddRegionOperation(const Region_sp& _region, Drawing& d);

		/// Undo operation.
		void undo() override;

		/// Redo operation.
		void redo() override;
	private:
		/// Drawing.
		Drawing& d;
		
		/// Region added into drawing.
		Region_sp region;
};

////////////////////////////////////////////////////////////////////////////////
/// Operation related to the removal of some curves from a drawing.
class RemoveCurvesOperation : public Operation{
	public:
		/// Constructor.
		RemoveCurvesOperation(Drawing& d);

		/// Undo operation.
		void undo() override;

		/// Redo operation.
		void redo() override;
	private:
		/// Drawing.
		Drawing& d;

		/// List of drawables before the deletion.
		DrawableList drawables;
};

////////////////////////////////////////////////////////////////////////////////
/// Operation related to the removal of some regions from a drawing.
class RemoveRegionsOperation : public Operation{
	public:
		/// Constructor.
		RemoveRegionsOperation(Drawing& d);

		/// Undo operation.
		void undo() override;

		/// Redo operation.
		void redo() override;
	private:
		/// Drawing.
		Drawing& d;

		/// List of drawables before the deletion.
		DrawableList drawables;
};

////////////////////////////////////////////////////////////////////////////////
/// Operation related to the removal of some drawables from a drawing.
class RemoveDrawablesOperation : public Operation{
	public:
		/// Constructor.
		RemoveDrawablesOperation(Drawing& d);

		/// Undo operation.
		void undo() override;

		/// Redo operation.
		void redo() override;
	private:
		/// Drawing.
		Drawing& d;

		/// List of drawables before the deletion.
		DrawableList drawables;
};

////////////////////////////////////////////////////////////////////////////////
/// Operation related to the addition of a guideline into a drawing.
class AddGuidelineOperation : public Operation{
	public:
		/// Constructor.
		AddGuidelineOperation(const BezierPath_sp& _curve, Drawing& d);

		/// Undo operation.
		void undo() override;

		/// Redo operation.
		void redo() override;
	private:
		/// Drawing.
		Drawing& d;

		/// Curve removed.
		BezierPath_sp curve;
};

////////////////////////////////////////////////////////////////////////////////
/// Operation related to the removal of a guideline from a drawing.
class RemoveGuidelinesOperation : public Operation{
	public:
		/// Constructor.
		RemoveGuidelinesOperation(Drawing& d);

		/// Undo operation.
		void undo() override;

		/// Redo operation.
		void redo() override;
	private:
		/// Drawing.
		Drawing& d;

		/// Guidelines before the removal.
		BezierPathList guidelines;
};

////////////////////////////////////////////////////////////////////////////////
/// Constructor.
inline AddCurveOperation::AddCurveOperation(
	const BezierPath_sp& _curve, 
	Drawing& _d
):
	d(_d), 
	curve{_curve}
{}

/// Undo operation.
inline void AddCurveOperation::undo(){
	d.remove(curve);
}
/// Redo operation.
inline void AddCurveOperation::redo(){
	d.add(curve);
}

////////////////////////////////////////////////////////////////////////////////
/// Constructor.
inline AddRegionOperation::AddRegionOperation(
	const Region_sp& _region, 
	Drawing& _d
): 
	d(_d), 
	region{_region}
{}

/// Undo operation.
inline void AddRegionOperation::undo(){
	d.remove(region);
}

/// Redo operation.
inline void AddRegionOperation::redo(){
	d.add(region);
}

////////////////////////////////////////////////////////////////////////////////
/// Constructor.
inline RemoveCurvesOperation::RemoveCurvesOperation(Drawing& _d): 
	d(_d), 
	drawables{d.get_drawables()}
{}

/// Undo operation.
inline void RemoveCurvesOperation::undo(){
	swap(drawables, d.get_drawables());
}

/// Redo operation.
inline void RemoveCurvesOperation::redo(){
	swap(drawables, d.get_drawables());
}

////////////////////////////////////////////////////////////////////////////////
/// Constructor.
inline RemoveRegionsOperation::RemoveRegionsOperation(Drawing& _d):
	d(_d), 
	drawables{d.get_drawables()}
{}

/// Undo operation.
inline void RemoveRegionsOperation::undo(){
	swap(drawables, d.get_drawables());
}

/// Redo operation.
inline void RemoveRegionsOperation::redo(){
	swap(drawables, d.get_drawables());
}

////////////////////////////////////////////////////////////////////////////////
/// Constructor.
inline RemoveDrawablesOperation::RemoveDrawablesOperation(Drawing& _d):
	d(_d), 
	drawables{d.get_drawables()}
{}

/// Undo operation.
inline void RemoveDrawablesOperation::undo(){
	swap(drawables, d.get_drawables());
}

/// Redo operation.
inline void RemoveDrawablesOperation::redo(){
	swap(drawables, d.get_drawables());
}

////////////////////////////////////////////////////////////////////////////////
/// Constructor.
inline AddGuidelineOperation::AddGuidelineOperation(
	const BezierPath_sp& _curve, 
	Drawing& _d
):
	d(_d), 
	curve{_curve}
{}

/// Undo operation.
inline void AddGuidelineOperation::undo(){
	d.remove_guideline(curve, false);
}

/// Redo operation.
inline void AddGuidelineOperation::redo(){
	d.add_guideline(curve, false);
}

////////////////////////////////////////////////////////////////////////////////
/// Constructor.
inline RemoveGuidelinesOperation::RemoveGuidelinesOperation(Drawing& _d):
	d(_d), 
	guidelines{d.get_guidelines()}
{}

/// Undo operation.
inline void RemoveGuidelinesOperation::undo(){
	swap(guidelines, d.get_guidelines());
}

/// Redo operation.
inline void RemoveGuidelinesOperation::redo(){
	swap(guidelines, d.get_guidelines());
}

#endif
