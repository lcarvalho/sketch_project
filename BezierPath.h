#ifndef BEZIER_PATH_H
#define BEZIER_PATH_H

#include "CubicBezier.h"
#include "Morph.h"
#include "Drawable.h"
#include "loc.h"

class BezierPath;
using BezierPath_sp = std::shared_ptr<BezierPath>;
using BezierPath_wp = std::weak_ptr<BezierPath>;
using BezierPathList = std::vector<BezierPath_sp>;

struct DistanceInfo;
		
// Parametrization methods.
enum class Parametrization{ simple, arc_length_aware };

/// PointList formed by a sequence of Bezier subcurves.
class BezierPath : public Drawable{
	public:
		////////////////////////////////////////////////////////////////

		/// Constructor from a list of control points.
		BezierPath(const PointList& pts = {});
		
		/// Constructor from an rvalue of control points.
		BezierPath(PointList&& pts);

		/// Constructor from a single point.
		BezierPath(const Point& p);
		
		/// Constructor from Json object.
		BezierPath(const json11::Json& json);

		////////////////////////////////////////////////////////////////
		// Methods that set data.

		/// Set the current parametrization.
		void set_parametrization(Parametrization p);

		////////////////////////////////////////////////////////////////
		// Methods that get data.

		/// Check if the curve is empty.
		bool empty() const;

		/// Number of control points.
		size_t size() const;

		/// Number of cubic bezier curves.
		size_t n_cubics() const;

		/// Get the first point of the curve.
		const Point& front() const;

		/// Get the last point of the curve.
		const Point& back() const;
		
		/// Get the control points of the cubic bezier.
		PointList& control_points();

		/// Get the control points of the cubic bézier (const version).
		const PointList& control_points() const;

		/// Get the i-th control point.
		Point& point(size_t i);

		/// Get the i-th control point (const version).
		const Point& point(size_t i) const;

		/// Get i-th cubic segment.
		CubicBezier get_cubic(int i);
		
		/// Get i-th cubic segment (const version).
		CubicBezier get_cubic(int i) const;

		/// Get the i-th control point in reverse order.
		Point& rpoint(size_t i);

		/// Get the i-th control point in reverse order (const version).
		const Point& rpoint(size_t i) const;

		/// Check if joint j is G1 continuous
		/** where j is joint between cubics j and j+1. */
		bool is_g1(size_t j) const;

		/// Get the point of the curve at parameter t.
		Point point_at(double t) const;

		/// Get the tangent of the curve at parameter t.
		Point tangent_at(double t) const;

		/// Get the curve point at the last cubic with parameter t.
		CubicBezier last_cubic();

		/// Get the curve point at the last cubic with parameter t (const version).
		CubicBezier last_cubic() const;

		/// Get the curve point at the last cubic with parameter t.
		Point last_cubic_at(double t) const;

		/// Get the tangent at the last cubic with parameter t.
		Point last_tangent_at(double t) const;

		/// Sample the curve with exactly N points.
		PointList uniform_sample(size_t N);

		/// Sample the curve with exactly N points (const version).
		PointList uniform_sample(size_t N) const;
		
		/// Sample with npc points per cubic.
		PointList sample(size_t npc);
		
		/// Sample with npc points per cubic.
		PointList sample(size_t npc) const;

		/// Get the (previously) sampled curve.
		const PointList& get_sample() const;

		/// Check if the curve is close to the point (x,y).
		bool is_close_to(double x, double y, double r) const;

		/// Check if the curve crosses segment (xp,yp)-(xq, yq).
		bool cross_segment(const Point& p, const Point& q) const;

		/// Calculate the distance between control point of
		/// this curve with curve c.
		double distance2(const BezierPath& c) const;

		/// Reverse distance between curves.
		/** 
		* Calculate the distance between control point of
		* this curve (in reverse order) with curve c.
		*/
		double reverse_distance2(const BezierPath& c) const;

		/// Calculate curve lenght.
		double lenght() const;

		/// Split the curve in two parts, one from parameters 0.0 to t,
		/// and the other from t to 1.0.
		loc::pair_of<BezierPath> split(double t, bool fw = true) const;
		
		/// Get the result of the curve between parameters ta and tb.
		BezierPath cut(double ta, double tb) const;

		////////////////////////////////////////////////////////////////
		// Methods from Drawable interface.

		/// Check if the curve is next to point p.
		bool is_next_to(const Point& p, double scale = 1.0) override;

		////////////////////////////////////////////////////////////////
		// Friend functions.
		
		/// Get a uniform sample of two curves.
		friend loc::pair_of<PointList> sample_curves(
			const BezierPath& c1, 
			const BezierPath& c2,
			int nk
		);

		/// Calculate a distance info between two curves.
		friend DistanceInfo distance_info(
			const BezierPath& c1,
			const BezierPath& c2
		);

		/// Find the cubic path that contains the parameter t.
		std::pair<size_t, double> find_cubic(double t) const;
		
		/// Find the cubic path that contains the parameter t
		/** (if parameter is close to extreme, it is collapsed to 0.0).*/
		std::pair<size_t, double> find_cubic(double t, double tol) const;

		/// Calculate a list with values related to proportions of each cubic.
		/**
		  Let li be the lenght of i-th curve in bbs,
		  and L = sum(li),
		  the return vector contains values:
		  [0.0, l0/L, (l0+l1)/L, (l0+l1+l2)/L, ..., 1.0]
		*/
		std::vector<double> calculate_proportions() const;

		/// Update proportions.
		void update_proportions();
		
		/// Get the proportions.
		const std::vector<double>& get_proportions();
		
		const std::vector<double>& get_proportions() const;
		
		/// Add a new control point.
		void add(const Point& p);

		/// Create a morphed curve.
		BezierPath morph(const Morph& H) const;

		/// Force G1 continuity at the beginning of cubic c.
		void force_g1(size_t c);
		
		/// Remove controls points at the beginning of cubic c.
		void remove_control_point(size_t c);

		/// Remove small components.
		void prune(double mind);

		/// Convert to Json object.
		json11::Json to_json() const;

		/// Merge two curves.
		friend BezierPath merge_curves(
			const BezierPath& c0,
			const BezierPath& c1,
			double alpha,
			bool fw1,
			bool fw2
		);

		/// Get the Hausdorff distance between two curves.
		friend double hausdorff_distance(const BezierPath& c1, const BezierPath& c2);

		/// Output stream operator <<.
		friend std::ostream& operator<<(
			std::ostream& out, 
			const BezierPath& path
		);

		/// Input stream operator >>.
		friend std::istream& operator>>(
			std::istream& out,
			BezierPath& path
		);

	protected:
		////////////////////////////////////////////////////////////////
		// Parameters.
		
		/// Control points.
		PointList points;

		/// Sample of the curve.
		PointList sample_points;

		/// Parametrization.
		Parametrization parametrization = Parametrization::arc_length_aware;

		/// Proportion data info.
		struct ProportionsData{
			/// List of proportions.
			std::vector<double> proportions;

			/// Control points used when proportions were calculated.
			std::vector<Point> control_points;
		};

		/// Proportions.
		ProportionsData proportion_data;
		
};

/// Information about the distance between two curves.
struct DistanceInfo{
	/// First curve.
	const BezierPath* c0;

	/// Second curve.
	const BezierPath* c1;

	/// Distance between curves.
	double dist;

	/// Orientation between curves.
	bool fw;
};

/// Info used to get a subcurve from parameter t0 to t1.
struct SubcurveInfo{
	/// First parameter in subcurve interval.
	double t0;

	/// Second parameter in subcurve interval.
	double t1;
};

using BezierPair = loc::pair_of<const BezierPath*>;

/// Decide if two sets best fit in forward direction.
bool is_forward(const PointList& pts1, const PointList& _pts2);

/// Calculate all distances between curves in two vectors.
inline std::map<BezierPair, DistanceInfo> calculate_distances(const BezierPathList& curves1, const BezierPathList& curves2){
	std::map<BezierPair, DistanceInfo> df;
	for(const auto& c1: curves1)
		for(const auto& c2: curves2)
			df[std::make_pair(c1.get(), c2.get())] = distance_info(*c1, *c2);
	return df;
}

/// Merge two curves.
BezierPath merge_curves(
	const BezierPath& c0,
	const BezierPath& c1,
	double alpha,
	bool fw1 = true,
	bool fw2 = true
);

/// An curve with a boolean flag.
struct BezierBool{
	/// Curve.
	BezierPath_sp curve;

	/// Flag indicating if curve should be handled in forward direction.
	bool fw;

	/// Get the first control point of the curve, according to fw.
	Point front() const{
		return fw? curve->front(): curve->back();
	}
	
	/// Get the last control point of the curve, according to fw.
	Point back() const{
		return fw? curve->back(): curve->front();
	}
};

inline bool operator==(const BezierBool& bb, BezierPath* c){
	return bb.curve.get() == c;
}

inline const BezierPath& toBezierPath(const Drawable& d){
	return static_cast<const BezierPath&>(d);
}

inline BezierPath* toBezierPath(Drawable* ptr){
	return static_cast<BezierPath*>(ptr);
}

inline BezierPath* toBezierPath(const Drawable_sp& ptr){
	return static_cast<BezierPath*>(ptr.get());
}

inline BezierPath_sp toBezierPath_s(const Drawable_sp& ptr){
	return std::static_pointer_cast<BezierPath>(ptr);
}

/// Get all the control points of a list of BezierPaths.
PointList all_control_points(const BezierPathList& paths);

////////////////////////////////////////////////////////////////////////////////
// Implementation of inline methods.

/// Constructor from a list of control points.
inline BezierPath::BezierPath(const PointList& pts):
	Drawable{1},
	points{pts}
{
	update_proportions();
}
		
/// Constructor from an rvalue of control points.
inline BezierPath::BezierPath(PointList&& pts):
	Drawable{1},
	points{std::move(pts)}
{
	update_proportions();
}

/// Constructor from a single point.
inline BezierPath::BezierPath(const Point& p):
	BezierPath({p, p, p, p})
{}
		
////////////////////////////////////////////////////////////////////////////////
// Methods that set data.

/// Set the current parametrization.
inline void BezierPath::set_parametrization(Parametrization p){
	parametrization = p;
}

/// Convert to Json object.
inline json11::Json BezierPath::to_json() const{
	return points;
}

/// Number of cubic bezier curves.
inline size_t BezierPath::n_cubics() const{
	size_t s = size();
	return (s == 0)? 0: (s - 1)/3;
}
		
/// Get the (previously) sampled curve.
inline const PointList& BezierPath::get_sample() const{
	return sample_points;
}

/// Get the i-th control point in reverse order.
inline Point& BezierPath::rpoint(size_t i){
	return point(size() - 1 - i);
}

/// Get the i-th control point in reverse order (const version).
inline const Point& BezierPath::rpoint(size_t i) const{
	return point(size() - 1 - i);
}

/// Find the cubic path that contains the parameter t
/** (if parameter is close to extreme, it is collapsed to 0.0).*/
inline std::pair<size_t, double> BezierPath::find_cubic(
	double t, 
	double tol
) const{
	auto cs = find_cubic(t);
	auto& c = cs.first;
	auto& s = cs.second;

	if(s < tol)
		s = 0.0;
	else if(1.0 - s < tol){
		if(c == n_cubics()-1)
			s = 1.0;
		else{
			++c;
			s = 0.0;
		}
	}
	return cs;
}

/// Get the point of the curve at parameter t.
inline Point BezierPath::point_at(double t) const{
	if(t <= 0.0)
		return front();

	if(t >= 1.0)
		return back();

	auto r = find_cubic(t);

	return get_cubic(r.first).point_at(r.second);
}

/// Get the tangent of the curve at parameter t.
inline Point BezierPath::tangent_at(double t) const{
	if(t <= 0.0)
		return 3*(point(1) - point(0));

	if(t >= 1.0)
		return 3*(rpoint(0) - rpoint(1));

	auto r = find_cubic(t);

	return get_cubic(r.first).tangent_at(r.second);
}

/// Get the curve point at the last cubic with parameter t.
inline CubicBezier BezierPath::last_cubic(){
	return get_cubic(-1);
}

/// Get the curve point at the last cubic with parameter t.
inline CubicBezier BezierPath::last_cubic() const{
	return get_cubic(-1);
}

/// Get the curve point at the last cubic with parameter t.
inline Point BezierPath::last_cubic_at(double t) const{
	return last_cubic().point_at(t);
}

/// Get the tangent at the last cubic with parameter t.
inline Point BezierPath::last_tangent_at(double t) const{
	return last_cubic().tangent_at(t);
}

/// Check if the curve is empty.
inline bool BezierPath::empty() const{
	return points.size() < 4;
}

/// Number of control points.
inline size_t BezierPath::size() const{
	return points.size();
}
		
/// Get i-th cubic segment.
inline CubicBezier BezierPath::get_cubic(int i){
	if(i < 0)
		i += n_cubics();

	return {points.data()+3*i};
}

/// Get i-th cubic segment (const version).
inline CubicBezier BezierPath::get_cubic(int i) const{
	if(i < 0)
		i += n_cubics();

	return {points.data()+3*i};
}


/// Get the control points of the cubic bezier.
inline PointList& BezierPath::control_points(){
	return points;
}

/// Get the control points of the cubic bézier (const version).
inline const PointList& BezierPath::control_points() const{
	return points;
}

/// Get the first point of the curve.
inline const Point& BezierPath::front() const{
	return points.front();
}

/// Get the last point of the curve.
inline const Point& BezierPath::back() const{
	return points.back();
}

/// Get the i-th control point.
inline Point& BezierPath::point(size_t i){
	return points[i];
}

/// Get the i-th control point (const version).
inline const Point& BezierPath::point(size_t i) const{
	return points[i];
}

/// Add a new control point.
inline void BezierPath::add(const Point& p){
	points.push_back(p);
}
		
/// Get the proportions obtained by proportions_manager.
inline const std::vector<double>& BezierPath::get_proportions(){
	update_proportions();
	return proportion_data.proportions;
}

#endif //BEZIER_PATH_H

