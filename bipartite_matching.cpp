#include "bipartite_matching.h"
#include <algorithm>
#include <limits>
#include <iostream>

using namespace bipartite_matching;
using std::min;

/// Constructor.
BipartiteGraph::BipartiteGraph(size_t _nu, size_t _nv):
	nu{_nu}, nv{_nv}, us(nu), vs(nv)
{
	int i = 0;
	for(auto& u: us)
		u.index = i++;

	int j = 0;
	for(auto& v: vs){
		v.index = j++;
		v.adj = {-1, -1};
	}
}

/// Add an edge.
void BipartiteGraph::add_edge(int i, int j, double weight){
	auto& adj = us[i].adj;
	
	auto it = loc::find_if(
		adj, 
		[&](const WeightedEdge& e){ 
			return e.index == j; 
		}
	);

	if(it == adj.end())
		adj.push_back({j, weight});
	else
		it->weight = weight;
}

/// Init vertices.
void BipartiteGraph::init_vertices(){
	for(auto& u: us){
		u.is_free = true;
		u.adj.reserve(vs.size());
	}

	for(auto& v: vs){
		v.is_free = true;
		if(v.adj.index != -1)
			flip_edge(v, us[v.adj.index]);
	}
}

/// Extract matching.
Edges BipartiteGraph::extract_matching(){
	Edges out;
	out.reserve(min(nu, nv));
	for(auto& v: vs)
		if(v.adj.index != -1)
			out.emplace_back(v.adj.index, v.index);

	return out;
}

/// Find augmenting path and flip edges.
bool BipartiteGraph::augment(){
	Edges path = find_augmenting_path();

	if(path.empty())
		return false;

	for(size_t i = 0; i+1 < path.size(); ++i)
		// flip all edges out of match.
		flip_edge(vs[path[i+1].second], us[path[i].first]);

	for(const auto& p: path)
		// flip all edges into the match
		flip_edge(us[p.first], vs[p.second]);

	vs[path.front().second].is_free = false;
	us[path.back().first].is_free = false;

	return true;
}

/// Get the maximum_weigth_matching.
Edges BipartiteGraph::maximum_weight_matching(){
	init_vertices();

	for(int r = min(nu, nv); r != 0; --r)
		if(!augment())
			break;
	
	return extract_matching();
}

/// Flip an edge connecting U vertex and V vertex.
void BipartiteGraph::flip_edge(UVertex& u, VVertex& v){
	// add edge u,v to the matching
	auto it = loc::find_if(
		u.adj, 
		[&](const WeightedEdge& e){
			return e.index == v.index;
		}
	);

	v.adj = {u.index, it->weight};

	u.adj.erase(it);
}

/// Flip an edge connecting V vertex and U vertex.
void BipartiteGraph::flip_edge(VVertex& v, UVertex& u){
	// remove edge u,v from the matching
	u.adj.push_back({v.index, v.adj.weight});
	v.adj = {-1, -1.0};
}

/// Relax a U vertex.
bool BipartiteGraph::relax_us(){
	bool changed = false;
	for(auto& u: us){
		for(const auto& uadj: u.adj){
			int j = uadj.index;

			double cost = u.aug_cost + uadj.weight;

			auto& vj = vs[j];

			if(cost > vj.aug_cost){
				vj.aug_cost = cost;
				vj.last_vertex = &u;
				changed = true;
			}
		}
	}

	return changed;
}

/// Relax a V vertex.
bool BipartiteGraph::relax_vs(){
	bool changed = false;

	for(auto& v: vs){
		int i = v.adj.index;

		if(i == -1)
			continue; // there is no incoming edge

		double cost = v.aug_cost - v.adj.weight;

		auto& ui = us[i];

		if(cost > ui.aug_cost){
			ui.aug_cost = cost;
			ui.last_vertex = &v;
			changed = true;
		}
	}

	return changed;
}

/// Init distance info.
void BipartiteGraph::init_distances(){
	constexpr double w_min = -loc::infinity<double>();

	for(auto& u: us){
		u.aug_cost = u.is_free? 0.0: w_min;
		u.last_vertex = nullptr;
	}

	for(auto& v: vs){
		v.aug_cost = w_min;
		v.last_vertex = nullptr;
	}
}
	
/// Relax vertices.
void BipartiteGraph::relax_vertices(){
	for(auto k = 0u; k < nu + nv - 1; k+=2)
		if(!relax_us() or !relax_vs())
			// There's no need to continue if anything has changed.
			return;
}
	
/// Find VVertex with maximum augmenting path cost.
BipartiteGraph::VVertex* BipartiteGraph::find_maximum_dist(){
	double maxd = -loc::infinity<double>();
	VVertex* out_v = nullptr;

	for(auto& v: vs)
		if(v.is_free and v.aug_cost > maxd){
			maxd = v.aug_cost;
			out_v = &v;
		}

	return out_v;
}

/// Find augmenting path.
Edges BipartiteGraph::find_augmenting_path(){
	init_distances();

	relax_vertices();

	Vertex* v = find_maximum_dist();
	if(v == nullptr)
		return {};

	// extract path
	Vertex* u = v->last_vertex;
	Edges out{ {u->index, v->index} };

	while(u->last_vertex != nullptr){
		v = u->last_vertex;
		u = v->last_vertex;
		out.emplace_back(u->index, v->index);
	}

	return out;
}

