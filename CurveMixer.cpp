#include "CurveMixer.h"

#include <fstream>
#include <iomanip>
#include <chrono>

#include "polynomial_roots.h"
#include "FittingCurve.h"

using std::fabs;
using std::pow;
using std::cout;
using std::min;
using std::max;
using std::minmax;
using std::tie;
using std::min_element;
using std::vector;
using std::tuple;
using std::function;
using std::get;
using std::make_shared;


/// Constructor.
CurveMixer::CurveMixer(
	const BezierPath_sp& c0,
	const BezierPath_sp& c1,
	bool fw0,
	bool fw1
):
	CurveMixer{BezierBool{c0, fw0}, BezierBool{c1, fw1}}
{}

/// Constructor from BezierBool objects.
CurveMixer::CurveMixer(const BezierBool& bb0, const BezierBool& bb1){
	//cout << *bb0.curve << '\n' << *bb1.curve;
	curve0.bb = bb0;
	curve1.bb = bb1;
	
	// Subdivide the curves.
	calculate_subcurves();

	// Check which joints should be forced to be g1.
	check_joints();
}
		
/// Get data necessary for calculate subcurves.
BezierPath CurveMixer::get_data(CurveInfo& info){
	// Copy curves.
	BezierPath c = *info.bb.curve;

	// Reverse if necessary.
	if(!info.bb.fw)
		loc::reverse(c.control_points());
	
	auto& sc = info.sc;

	sc.set_index(c.get_index());
	sc.set_visibility(c.is_visible());

	return c;
}

std::vector<double> find_new_ts(const std::vector<double>& ts0, const std::vector<double>& ts1){
	std::vector<double> new_ts;

	size_t j = 0;
	for(double t: ts1){
		while(j < ts0.size() and ts0[j] <= t)
			++j;
		
		if(j == ts0.size())
			break;

		size_t i = j-1;

		double d = ts0[j] - ts0[i];

		double dif = std::min(ts0[j] - t, t - ts0[i]);

		if(dif > .30*d)
			new_ts.push_back(t);
	}

	return new_ts;
}

		
void join_proportions(std::vector<double>& ts0, std::vector<double>& ts1){
	loc::remove_duplicates(ts0);
	loc::remove_duplicates(ts1);
	
#if 0
	auto new_ts0 = find_new_ts(ts0, ts1);
	auto new_ts1 = find_new_ts(ts1, ts0);

	size_t n0 = ts0.size();
	size_t n1 = ts1.size();

	loc::insert_back(ts0, new_ts0);
	loc::insert_back(ts1, new_ts1);
	
	std::inplace_merge(ts0.begin(), ts0.begin()+n0, ts0.end());
	std::inplace_merge(ts1.begin(), ts1.begin()+n1, ts1.end());
#else
	size_t m0 = ts0.size();
	size_t m1 = ts1.size();
	size_t i0 = 1;
	size_t i1 = 1;
	while(i0 < m0 and i1 < m1){
		double a0 = ts0[i0] - ts0[i0-1];
		double a1 = ts1[i1] - ts1[i1-1];
		double p = min(a0, a1)/max(a0, a1);
		if(p < .6){
			if(a1 > a0){
				ts1.insert(ts1.begin()+i1, loc::linerp(p, ts1[i1-1], ts1[i1]));
				++m1;
			}else{
				ts0.insert(ts0.begin()+i0, loc::linerp(p, ts0[i0-1], ts0[i0]));
				++m0;
			}
		}
		++i0;
		++i1;
	}
#endif 
}

/// Greatest common divisor of a and b.
int gcd(int a, int b){
	int d = 0;
	while(a%2==0 and b%2==0){
		a /= 2;
		b /= 2;
		++d;
	}

	while(a!=b){
		if(a%2==0)
			a /= 2;
		else if(b%2==0)
			b /= 2;
		else if(a>b)
			a = (a-b)/2;
		else
			b = (b-a)/2;
	}
	return a*std::pow(2, d);
}
		
/// Information about one sample point from a BezierPath.
struct SampleInfo{
	/// Position.
	Point pos;

	/// Parameter.
	double t;

	/// Flag indicating if this is an end-point.
	bool end_point;
};

/// Comparison between two SampleInfo objects.
bool operator<(const SampleInfo& a, const SampleInfo& b){
	return a.t < b.t;
}

void insert_sample(vector<SampleInfo>& samples, const SampleInfo& s, double tol){
	// Find b and e such that s.t is in the interval [b, e).
	auto e = loc::upper_bound(samples, s);
	auto b = prev(e);

	if(!b->end_point and (e == samples.end() or s.t - b->t < tol))
		*b = s;
	else if(e != samples.end() and !e->end_point and e->t - s.t < tol)
		*e = s;
	else
		samples.insert(e, s);
}

vector<SampleInfo> get_sample(size_t n, const BezierPath& curve){
	vector<SampleInfo> res(n);
	double dt = 1.0/(n-1);
	
	PointList sample = curve.uniform_sample(n);

	for(size_t i = 0; i < n; ++i)
		res[i] = SampleInfo{sample[i], i*dt, false};
	
	const auto& ts = curve.get_proportions();
	const auto& cps = curve.control_points();
	size_t ns = ts.size();
	for(size_t i = 0; i < ns; ++i)
		insert_sample(res, SampleInfo{cps[3*i], ts[i], true}, .2*dt);

	return res;
}
	
PointList get_points(const vector<SampleInfo>& samples){
	PointList pts;
	pts.reserve(samples.size());
	for(auto s: samples)
		pts.push_back(s.pos);
	return pts;
}

#include "PostScriptWriter.h"
/// Subdivide curves such that they have the same number of control points.
void CurveMixer::calculate_subcurves(){
	BezierPath curve[] = {get_data(curve0), get_data(curve1)};
	curve[0].update_proportions();
	curve[1].update_proportions();

	size_t nc0 = curve[0].n_cubics();
	size_t nc1 = curve[1].n_cubics();
	size_t n_samples = 5*max(nc0, nc1);

	vector<SampleInfo> info[2];
	info[0] = get_sample(n_samples, curve[0]);
	info[1] = get_sample(n_samples, curve[1]);

	PointList pts[2] = {
		{curve[0].front()},
		{curve[1].front()}
	};

	auto is_terminal = [&](int i, size_t k){ 
		return info[k][i].end_point; 
	};

	auto path = curve_blender.find_path(
		get_points(info[0]),
		get_points(info[1])
	);

	path = curve_blender.clear_path(path, is_terminal);
	
	double a[] = {0.0, 0.0};
	int i[2];
	for(auto p: path){
		if(p == path.front())
			continue;
		
		tie(i[0], i[1]) = p;

		for(size_t k: {0, 1}){
			double b = info[k][i[k]].t;
			auto ck = curve[k].cut(a[k], b);
			a[k] = b;
			pts[k].pop_back();
			loc::insert_back(pts[k], ck.control_points());
		}
	}

	curve0.sc.control_points() = move(pts[0]);
	curve1.sc.control_points() = move(pts[1]);
	check_joints();
}

/// Check if each joint should be forced to be G1 continuous.
void CurveMixer::check_joints(){
	auto& sc0 = curve0.sc;
	auto& sc1 = curve1.sc;

	// Number of joints.
	size_t nj = sc0.n_cubics()-1;

	g1_joints.reserve(nj);

	for(size_t j = 0; j < nj; ++j)
		g1_joints.push_back(sc0.is_g1(j) and sc1.is_g1(j));
}

/// Get a curve at parameter alpha in [0.0, 1.0].
BezierPath_sp CurveMixer::curve_at(double alpha){
	if(last_curve != nullptr and fabs(alpha - last_alpha) < 1e-5)
		return last_curve;

	auto curve = loc::shared(merge_curves(curve0.sc, curve1.sc, alpha));

	for(auto j = 0u; j < g1_joints.size(); ++j)
		if(g1_joints[j])
			curve->force_g1(j+1);

	last_curve = curve;
	last_alpha = alpha;

	return curve;
}

/// Get a curve at parameter alpha in [0.0, 1.0].
BezierPath_sp CurveMixer::curve_at(
	double alpha,
	const SimilarityMorph& M0,
	const SimilarityMorph& M1,
	const SimilarityMorph& MR
){
	if(last_curve != nullptr and fabs(alpha - last_alpha) < 1e-5)
		return last_curve;

	auto curve = loc::shared(merge_curves(curve0.sc.morph(M0), curve1.sc.morph(M1), alpha));

	MR.transform(curve->control_points());

	for(auto j = 0u; j < g1_joints.size(); ++j)
		if(g1_joints[j])
			curve->force_g1(j+1);

	last_curve = curve;
	last_alpha = alpha;

	Color color1 = curve0.bb.curve->get_color();
	Color color2 = curve1.bb.curve->get_color();
	int r = (1-alpha)*color1[0] + alpha*color2[0];
	int g = (1-alpha)*color1[1] + alpha*color2[1];
	int b = (1-alpha)*color1[2] + alpha*color2[2];
	curve->set_color({r, g, b});	

	curve->set_index(curve0.sc.get_index());

	return curve;
}
