#include "ColorWidget.h"
#include "FrameEditor.h"

/// Constructor.
ColorWidget::ColorWidget(FrameEditor& ed, QWidget* parent):
	QWidget{parent},
	editor(ed)
{
	bg_color.setRgb(0,0,0);
	main_color.setRgb(255, 255, 127);
}

/// Paint event.
void ColorWidget::paintEvent(QPaintEvent* event){
	QPainter painter{this};

	double w = width();
	double h = height();

	painter.fillRect(0.1*w, 0.4*h, 0.5*w, 0.5*h, bg_color);
	painter.drawRect(0.1*w, 0.4*h, 0.5*w, 0.5*h);
	painter.fillRect(0.4*w, 0.1*h, 0.5*w, 0.5*h, main_color);
	painter.drawRect(0.4*w, 0.1*h, 0.5*w, 0.5*h);

	QPen pen;
	pen.setWidth(1.0);
	pen.setColor(QColor{0, 0, 0});

	painter.setPen(pen);
}

/// Mouse press event.
void ColorWidget::mousePressEvent(QMouseEvent* event){
	QPointF ep = event->pos();
	int x = ep.x();
	int y = ep.y();

	double w = width();
	double h = height();

	if(x >= 0.4*w and x <= 0.9*w and y >= 0.1*h and y <= 0.6*h){
		// Get the color from the dialog.
		QColor col = QColorDialog::getColor(main_color);
		if(col.isValid() == false)
			return;

		// Set the color as the main color.
		main_color = col;

	}else if(x >= 0.1*w and x <= 0.6*w and y >= 0.4*h and y <= 0.9*h){
		// Get the color from the dialog.
		QColor col = QColorDialog::getColor(bg_color);
		if(col.isValid() == false)
			return;

		// Set the color as the main color.
		bg_color = col;
	}else
		return;

	// Set the color attributes of the frame.
	editor.update_colors();
}

