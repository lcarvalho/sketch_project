#ifndef XML_TAG_H
#define XML_TAG_H

#include <vector>
#include <string>
#include <iostream>
#include "loc.h"

/// An Xml tag
class XmlTag{
	using Property = loc::pair_of<std::string>;

	/// The name of the tag.
	std::string tag;

	/// List of properties.
	std::vector<Property> properties;

	/// List of subtags.
	std::vector<XmlTag> subtags;

	/// Text between tag.
	std::string text;

	public:

	/// Return the name of the tag.
	const std::string& name() const;

	/// Get the properties of the tag.
	const std::vector<Property>& get_properties() const;

	/// Get a property with given name.
	const Property* find_property(const std::string& name) const;

	/// Get the list of subtags.
	const std::vector<XmlTag>& get_subtags() const;

	/// Get the text.
	const std::string& get_text() const;

	/// Get data from input stream.
	friend std::istream& operator>>(std::istream& is, XmlTag& xmltag);

	/// Write data to output stream.
	friend std::ostream& operator<<(std::ostream& os, const XmlTag& xmltag);
};

/// Return the name of the tag.
inline const std::string& XmlTag::name() const{
	return tag;
}

/// Get the properties of the tag.
inline auto XmlTag::get_properties() const -> const std::vector<Property>&{
	return properties;
}

/// Get the list of subtags.
inline const std::vector<XmlTag>& XmlTag::get_subtags() const{
	return subtags;
}

/// Get the text.
inline const std::string& XmlTag::get_text() const{
	return text;
}

#endif
