cmake_minimum_required(VERSION 2.8.9)
project(animator)

cmake_policy(VERSION 2.8.9)

# Find Qt 5.
find_package(Qt5Widgets REQUIRED)

# Use the compile definitions defined in the Qt 5 Widgets module
add_definitions(${Qt5Widgets_DEFINITIONS})

#cmake_policy(SET CMP0043 OLD)

# Add compiler flags for building executables (-fPIE)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${Qt5Widgets_EXECUTABLE_COMPILE_FLAGS}")

set(CMAKE_MODULE_PATH ${animator_SOURCE_DIR})

# Find Eigen3.
find_package(Eigen3 REQUIRED)

message("Compiling with " ${CMAKE_CXX_COMPILER_ID})
# Build type.
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
	set(CMAKE_CXX_FLAGS_DISTRIBUTION
		"-march=native -O3 -Wall -Wpedantic -Wfatal-errors -Wno-missing-braces -std=c++14 -stdlib=libc++"
	)
	set(CMAKE_CXX_FLAGS_RELEASE 
#		"-march=native -O3 -Wall -Wpedantic -Wfatal-errors -Wno-missing-braces -std=c++14 -stdlib=libc++"
		${CMAKE_CXX_FLAGS_DISTRIBUTION}
	)
	set(CMAKE_CXX_FLAGS_DEBUG   
		"-g -Wfatal-errors -O0 -std=c++14 -stdlib=libc++"
	)
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
	set(CMAKE_CXX_FLAGS_DISTRIBUTION
		"-O3 -s -static-libstdc++ -fPIC -fopenmp -pthread -Wall -Wpedantic -Wfatal-errors -std=c++14"
	)
	set(CMAKE_CXX_FLAGS_RELEASE 
		"-O3 -fopenmp -pthread -fPIC -Wall -Wpedantic -Wfatal-errors -std=c++14"
	)
	set(CMAKE_CXX_FLAGS_DEBUG   
		"-g -Wfatal-errors -O0 -fPIC -fopenmp -pthread -std=c++14"
	)
endif()

set(CMAKE_BUILD_TYPE distribution)

# Output directory.
set(EXECUTABLE_OUTPUT_PATH ${animator_SOURCE_DIR}/bin)

# Define the include directories for the current folder.
include_directories(${animator_SOURCE_DIR})

message(
	${EIGEN3_INCLUDE_DIR})
# System directories.
include_directories(SYSTEM 
	${EIGEN3_INCLUDE_DIR}
	${Qt5Widgets_INCLUDE_DIRS}
)

add_library(animation
json11
Point
polynomial_roots
CubicBezier
BezierPath CurveMixer PolygonBlender
DrawableSet
Animation 
AnimationViewer
Selection
Drawing Layer 
Interpolator
guideline_operations
Region RegionMixer
PostScriptWriter SVGWriter
XmlTag Xml
SVGReader
Transformation
SimilarityMorph CombMorph 
convex_hull min_bounding_box
bipartite_matching
FittingCurve
)

qt5_wrap_cpp(Editor_HEADERS_MOC AnimationEditor.h FrameEditor.h TimelineWidget.h LayerWidget.h ColorWidget.h SpeedWidget.h)
qt5_add_resources(Editor_RESOURCES_RCC resources.qrc)

add_library(editor 
AnimationEditor.cpp FrameEditor.cpp 
TimelineWidget.cpp ColorWidget.cpp 
SpeedWidget.cpp
LayerWidget.cpp
${Editor_HEADERS_MOC}
)

# Define executable filename.
add_executable(dilight main.cpp ${Editor_RESOURCES_RCC})


# Link libraries to executable.
target_link_libraries(dilight 
	${QT_LIBRARIES}
	editor
	animation
)


qt5_use_modules(dilight Widgets)

# Define executable filename.
add_executable(tester test)

# Link libraries to executable.
target_link_libraries(tester
	animation
)

