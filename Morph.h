#ifndef MORPH_H
#define MORPH_H

#include "Point.h"
#include <memory>

/// Transformation that can be applied to a set of points.
class Morph{
	public:
		/// Apply morph to points in, store results into out.
		PointList morph_list(const PointList& in) const;
		
		/// Apply morph to points in, store results into out.
		PointList& morph_list(PointList&& in) const;

		/// Apply morph to points.
		void transform(PointList& points) const;

		/// Apply morph to a single point.
		virtual Point morph(const Point& c) const = 0;

		/// Parenthesis operator.
		Point operator()(const Point& c) const;
};

/// Shared ptr of Morph. 
using Morph_sp = std::shared_ptr<Morph>;

/// Important! Don't use std::unique_ptr<Morph>, for it doesn't have virtual destructor..

/// Apply morph to points in, store results into out.
inline PointList Morph::morph_list(const PointList& in) const{
	PointList out(in.size());
	loc::transform(in, out, [&](const Point& p){ return morph(p); });
	return out;
}
		
/// Apply morph to points in, store results into out.
inline PointList& Morph::morph_list(PointList&& in) const{
	loc::transform(in, in, [&](const Point& p){ return morph(p); });
	return in;
}

inline void Morph::transform(PointList& points) const{
	morph_list(std::move(points));
}

/// Parenthesis operator.
inline Point Morph::operator()(const Point& c) const{
	return morph(c);
}

#endif
