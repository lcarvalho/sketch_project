#include "RegionMixer.h"
#include <fstream>

using std::vector;
using std::make_shared;
using std::map;
using std::max;
using std::get;
using std::logic_error;
using std::tie;

/// Constructor.
RegionMixer::RegionMixer(
	const Region& _region1,
	const Region& _region2,
	const vector_int2& _corresps,
	const Morph& morph,
	const vector<CurveMixer_sp>& curve_mixers
):
	region1(_region1),
	region2(_region2),
	bbs1(region1.get_curves()),
	bbs2(region2.get_curves()),
	color1(region1.get_color()),
	color2(region2.get_color())
{
	auto corresps = _corresps;

	// Check which orientation makes the closest match.
	if(!correspondence_distance(corresps, morph))
		reverse_curves2(corresps);

	// Check order of curves.
	check_order(corresps);

	// Adjust the list of curves of each region.
	auto new_index1 = rearrange_curves<0>(corresps);
	auto new_index2 = rearrange_curves<1>(corresps);

	// Adjust correspondences indices.
	for(auto& p: corresps)
		p = {new_index1.at(p.first), new_index2.at(p.second)};

	create_mixers(corresps, curve_mixers);
}

/// Check which direction is better for correspondence.
bool RegionMixer::correspondence_distance(
	const vector_int2& corresps,
	const Morph& morph
){
	double sum = 0.0;
	double rsum = 0.0;

	BezierPath mc1;
	for(const auto& p: corresps){
		auto& bb1 = bbs1[p.first];
		auto& bb2 = bbs2[p.second];

		auto mc1 = bb1.curve->morph(morph);

		mc1.sample(10);
		auto& c2 = *bb2.curve;
		c2.sample(10);

		double dn = mc1.distance2(c2);
		double dr = mc1.reverse_distance2(c2);

		if(bb1.fw == bb2.fw){
			sum += dn;
			rsum += dr;
		}else{
			sum += dr;
			rsum += dn;
		}
	}

	return sum < rsum;
}

/// Reverse curves from region 2.
void RegionMixer::reverse_curves2(vector_int2& corresps){
	int n2 = bbs2.size();

	BBList reverse_bbs2(n2);
	reverse_bbs2[0] = {bbs2[0].curve, !bbs2[0].fw};

	for(int i = 1; i < n2; ++i)
		reverse_bbs2[i] = {bbs2[n2 - i].curve, !bbs2[n2-i].fw};

	bbs2 = move(reverse_bbs2);

	for(auto& p: corresps)
		if(p.second != 0)
			p.second = n2 - p.second;
}

/// Constructs a new region at parameter alpha in [0,1].
Region_sp RegionMixer::region_at(
	double alpha,
	const SimilarityMorph& M0,
	const SimilarityMorph& M1,
	const SimilarityMorph& MR
){
	if(region != nullptr and fabs(last_alpha - alpha) < 1e-5)
		return region;

	region = make_shared<Region>();

	for(const auto& m: mixers)
		region->add_path(
			m.first->curve_at(alpha, M0, M1, MR),
			m.second
		);

	int r = (1-alpha)*color1[0] + alpha*color2[0];
	int g = (1-alpha)*color1[1] + alpha*color2[1];
	int b = (1-alpha)*color1[2] + alpha*color2[2];
	region->set_color({r, g, b});

//	int i = (1-alpha)*region1.get_index() + alpha*region2.get_index();
	int i = region1.get_index();
	region->set_index(i);

	last_alpha = alpha;

	return region;
}

/// Create a list of control points from curves between bbs[i] and bbs[j].
PointList control_points_in_range(int i, int j, BBList& bbs){
	auto bs = bbs.size();

	PointList points = {bbs[i].back()};

	for(int k = (i+1)%bs;; k=(k+1)%bs){
		Point p0 = points.back();

		const auto& curve_k = bbs[k];
		Point p1 = curve_k.front();

		if(dist2(p0, p1) > 1e-5)
			loc::insert_values(
				points,
				.75*p0 + .25*p1,
				.25*p0 + .75*p1,
				p1
			);

		if(k == j)
			break;

		const auto& c = curve_k.curve->control_points();
		if(curve_k.fw)
			points.insert(points.end(), next(c.begin()), c.end());
		else
			points.insert(points.end(), next(c.rbegin()), c.rend());
	}

	return points;
}

/// Rearrange curves.
template<size_t I>
map<int, int> RegionMixer::rearrange_curves(
	const vector_int2& corresps
){
	auto s = corresps.size();

	BBList& bbs = (I == 0)? bbs1: bbs2;

	auto element = [&](int ic){
		return get<I>(corresps[ic%s]);
	};
	
	BBList bbsx;
	bbsx.reserve(2*bbs.size());
	map<int, int> new_index;
	for(auto ic = 0u; ic < s; ++ic){
		int i = element(ic);
		int j = element(ic+1);

		new_index[i] = bbsx.size();
		bbsx.push_back(bbs[i]);

		auto points = control_points_in_range(i, j, bbs);

		if(points.size() <= 1)
			continue;

		auto new_curve = make_shared<BezierPath>(points);
		new_curve->set_visibility(false);
		bbsx.push_back({new_curve, true});

	}

	bbs = move(bbsx);
	
	return new_index;
}

/// Find a mixer between curves c1 and c2.
const CurveMixer_sp& find_mixer(
	const vector<CurveMixer_sp>& mixers,
	const BezierPath& c1,
	const BezierPath& c2
){
	auto it = loc::find_if(
		mixers,
		[&](const CurveMixer_sp& cm){
			return &cm->get_c0() == &c1 and
				&cm->get_c1() == &c2;
		}
	);

	if(it == mixers.end())
		throw logic_error("Error! Couldn't find mixer.");

	return *it;
}

/// Create curve mixers.
void RegionMixer::create_mixers(
	const vector_int2& corresps,
	const vector<CurveMixer_sp>& curve_mixers
){

	auto cor_b = corresps.begin();
	auto cor_e = corresps.end();

	for(auto i = cor_b; i != cor_e; ++i){
		int pi1, pi2, pk1, pk2, pj1, pj2;

		tie(pi1, pi2) = *i;

		auto& bbs_i1 = bbs1[pi1];
		auto& bbs_i2 = bbs2[pi2];

		const auto& mixer = find_mixer(curve_mixers, *bbs_i1.curve, *bbs_i2.curve);

		mixers.emplace_back(mixer, bbs_i1.fw == mixer->is_fw0());

		// Index of next curve.
		pk1 = (pi1+1)%bbs1.size();
		pk2 = (pi2+1)%bbs2.size();

		auto ni = next(i);
		tie(pj1, pj2) = (ni != cor_e? *ni: *cor_b);

		// Check if curves at pk1/2 are between pi and pj.
		bool x1 = pk1 != pj1;
		bool x2 = pk2 != pj2;

		if(!x1 and !x2)
			continue;

		auto& b1 = bbs1[pk1];
		auto& b2 = bbs2[pk2];

		loc::pair_of<BezierBool> ab;

		if(x1 and x2)// Both curves exist.
			ab = {b1, b2};
		else if (x1 and !x2) // There is only curve at pk1.
			ab = {b1, {make_shared<BezierPath>(bbs2[pi2].back()), b1.fw}};
		else // There is only curve at pk2.
			ab = {{make_shared<BezierPath>(bbs1[pi1].back()), b2.fw},  b2};

		mixers.emplace_back(make_shared<CurveMixer>(ab.first, ab.second), true);
	}
}

/// Convert points of curves k to Morph format.
PointList convert_points(const BBList& bbs){
	PointList pts;
	for(const auto& bb: bbs){
		const auto& cpts = bb.curve->control_points();

		if(bb.fw)
			pts.insert(pts.end(), cpts.begin(), cpts.end());
		else
			pts.insert(pts.end(), cpts.rbegin(), cpts.rend());
	}

	return pts;
}

/// Calculate distance between regions H(region1) and region2.
double RegionMixer::distance(Morph& H) const{
	double max_dist = 0.0;

	for(const auto& mix: mixers){
		const auto& curve_mixer = mix.first;
		const auto& c0 = curve_mixer->get_c0().morph(H);
		const auto& c1 = curve_mixer->get_c1();
		max_dist = max(max_dist, hausdorff_distance(c0, c1));
	}


	return max_dist;
}

template<int I>
inline int pair_element(const loc::pair_of<int>& p){
	return get<I>(p);
}
	
template<int I>
bool test_order(const vector_int2& corresps){
	vector<int> vs(corresps.size());
	loc::transform(corresps, vs, pair_element<I>);
	rotate(vs.begin(), loc::min_element(vs), vs.end());
	return loc::is_sorted(vs);
}

// Check order of curves.
void RegionMixer::check_order(const vector_int2& corresps){
	if(!test_order<0>(corresps) or !test_order<1>(corresps))
		throw logic_error{"Inconsistent curve order in region!"};
}

