#ifndef COMB_MORPH_H
#define COMB_MORPH_H

#include "Morph.h"
#include <vector>

/// Combination of morphs.
class CombMorph : public Morph{
	public:
		template<class... T>
		CombMorph(const T&... s);

		/// Apply morph to a single point.
		Point morph(const Point& c) const override;

	private:
		/// List of morphs.
		std::vector<Morph_sp> morphs;
};
		
template<class... T>
inline CombMorph::CombMorph(const T&... s): morphs{s...}{}

#endif
