#include "SVGWriter.h"

#include <iostream>

/// Constructor.
SVGWriter::SVGWriter(
	const std::string& filename,
	double _x0,
	double _y0,
	double _w,
	double _h
):
	out{filename}, x0{_x0}, y0{_y0}, w{_w}, h{_h}
{

	out << "<svg\n"
		<< "xmlns=\"http://www.w3.org/2000/svg\"\n"
		<< "version=\"1.1\"\n"
		<< "width=\"" << w << "\" height=\"" << h << "\"\n"
		<< "viewBox=\"0 0 " << w << ' ' << h << "\"\n"
		<< "baseProfile=\"tiny\"\n"
		<< "xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n"
		<< "<g transform=\"translate(" << -x0 << ", " << -y0
		<< ")\">\n";
	
	closer = {nullptr, [&](void*){ out << "</g>\n</svg>\n"; }};
}

/// Create a group with identifier gid, and flag 'visible'.
void SVGWriter::create_group(int gid, bool visible){
	out << "<g id=\"group" << gid << "\"\n"
	    << "visibility = \"" << (visible? "visible": "hidden")
	    << "\"\n"
	    << ">\n";
}

/// Finishes a group.
void SVGWriter::end_group(){
	out << "</g>\n";
}

/// Define the visibility duration of groups from 0 to maxid.
void SVGWriter::set_visibility(int maxgid, double dur){
	for(int gid = 0; gid <= maxgid; ++gid){
		out << "<animate id=\"anim" << gid << "\"\n";
		out << "xlink:href=\"#group" << gid << "\"\n";
		out << "attributeName=\"visibility\"\n";
		out << "attributeType=\"XML\"\n";
		out << "values=\"visible\"\n";
		if(gid > 0)
			out << "begin=\"anim" << gid-1
			    << ".end\"\n";
		else
			out << "begin=\"0s; anim" << maxgid
			    << ".end \"\n";

		out << "dur=\""<< dur << "s\"\n";
		out << "/>\n";
	}
}

/// Create a path from a curve.
void SVGWriter::create_path(const BezierPath& curve){
	out << "<path\n";

	if(!curve.control_points().empty()){
		out << "d = \"";
		write_path_d(curve);
		out  << "\""<< '\n';
	}

	auto color = curve.get_color();

	out << "fill=\"none\"\n"
		<< "stroke=\"rgb("
		<< int(color[0]) << ','
		<< int(color[1]) << ','
		<< int(color[2]) << ")\"\n"
		<< "stroke-linecap=\"square\"\n"
		<< "stroke-width=\"1\"\n"
		<< "/>\n";
}

/// Write the path info from a curve.
void SVGWriter::write_path_d(const BezierPath& curve, bool start){
	const auto& points = curve.control_points();

	if(points.size() <= 1)
		return;

	out << (start? "M " : "L ") << points[0];

	auto np = points.size()-2;
	for(auto i = 1u; i < np; i+=3)
		out << " C " << points[i] << points[i+1] << points[i+2];
}

/// Create a region.
void SVGWriter::create_region(const Region& region){
	out << "<path\n";

	out << "d = \"";
	bool start = true;
	BezierPath rc;
	for(auto& c: region.get_curves()){
		const auto& curve = *c.curve;
		if(c.fw)
			write_path_d(curve, start);
		else{
			rc.control_points() = curve.control_points();
			loc::reverse(rc.control_points());
			write_path_d(rc, start);
		}
		start = false;
	}
	out << "\"\n";

	auto color = region.get_color();

	out << "fill=\"rgb("
		<< int(color[0]) << ','
		<< int(color[1]) << ','
		<< int(color[2]) << ")\"\n"
		<< "fill-rule=\"evenodd\"\n"
		<< "stroke=\"none\"\n"
		<< "/>\n";
}

/// Create a list of paths from curves.
void SVGWriter::create_paths(const BezierPathList& curves){
	for(auto& c: curves)
		if(c->is_visible())
			create_path(*c);
}

/// Create a list of paths from regions.
void SVGWriter::create_paths(const RegionList& regions){
	for(auto& c: regions)
		if(c->is_visible())
			create_region(*c);
}

/// Create animation for a path.
void SVGWriter::animate_path(
	const std::vector<BezierPath*>& paths,
	int id,
	double begin,
	double end
){
	out << "<animate\n";
	out << "xlink:href=\"#path" << id << "\""<< '\n';
	out << "attributeName=\"d\""<< '\n';
	out << "attributeType=\"XML\""<< '\n';
	out << "values=\""<< '\n';
	auto last = paths.back();
	for(auto path: paths){
		write_path_d(*path);
		if(path != last)
			out << ';' << '\n';
	}
	out << "\"\n";
	out << "begin=\"" << begin << "s\""<< '\n';
	out << "dur=\"" << end - begin << "s\""<< '\n';
	out << "fill=\"freeze\"\n";
	out << "/>\n";
}

