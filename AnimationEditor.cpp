#include "AnimationEditor.h"
#include "LayerWidget.h"
#include "PolygonBlender.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>

using namespace loc::literals;
using std::string;
using std::cout;
using std::function;
using std::ofstream;
using std::map;
using std::exception;

/// Constructor.
AnimationEditor::AnimationEditor() : viewer{animation}, selection{viewer}, speed_widget{*this}{
	setMinimumSize(800, 600);
	setWindowTitle("DiLight");

	setlocale (LC_ALL,"C");

	playing = false;

	editor = new FrameEditor{*this};
	editor->setParent(this);
	editor->set_error(100);

	create_menu();

	auto layout = new QHBoxLayout{};

	layout->addWidget(make_left());
	layout->addWidget(make_right());
	layout->setContentsMargins(0, 0, 0, 0);

	// Set layout in QWidget
	auto window = new QWidget{};
        window->setLayout(layout);

        // Set QWidget as the central layout of the main window
        setCentralWidget(window);

	connect(
		&timer, 
		&QTimer::timeout,
		[&]{
			viewer.next_frame();
			auto f = animation.first_keyframe_index();
			auto l = animation.last_keyframe_index();
			auto i = viewer.current_frame_index();
			if(i > l or (i == l and animation.is_cyclic()))
				viewer.set_current_frame(f);
			update_window();
		}
	);
}

/// Create the left side part of the window.
QWidget* AnimationEditor::make_left(){
	auto layout = new QVBoxLayout{};

	layout->addWidget(make_toolbar());
	layout->addWidget(make_layer_editor());
	layout->addWidget(make_animation_controls());

	auto widget = new QWidget{};
	widget->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
	widget->setFixedWidth(200);
	widget->setLayout(layout);

	return widget;
}

/// Create the right side part of the window.
QWidget* AnimationEditor::make_right(){
	auto layout = new QVBoxLayout{};

	layout->addWidget(make_editor());
	layout->addWidget(make_timeline());

	auto widget = new QWidget{};
	widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	widget->setLayout(layout);
	return widget;
}

/// Create the frame editor.
QWidget* AnimationEditor::make_editor(){
	editor->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	return editor;
}

/// Create the timeline.
QWidget* AnimationEditor::make_timeline(){
	timeline = new TimelineWidget{*this};

	auto widget = new QScrollArea{this};
	widget->setWidget(timeline);

	widget->setBackgroundRole(QPalette::Shadow);
	widget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	widget->setFixedHeight(50);

	return widget;
}

/// Create a tool to the toolbar.
QToolButton* AnimationEditor::make_tool(
		const char* icon,
		const char* tooltip,
		function<void()> f
) const{
	auto button = new QToolButton{};
	button->setIcon(QIcon{icon});
	button->setIconSize({32, 32});
	button->setAutoExclusive(true);
	button->setCheckable(true);
	button->setToolTip(tr(tooltip));
	connect(button, &QPushButton::clicked, f);
	return button;
}

/// Create the pencil tool.
QWidget* AnimationEditor::make_pencil_tool() const{
	auto pencil = make_tool(
		":/images/pencil_small.svg",
		"Pencil",
		[&]{
			editor->set(FrameEditor::drawing);
			editor->set_tool(FrameEditor::pencil);
		}
	);
	pencil->setChecked(true);
	return pencil;
}

/// Create the eraser tool.
QWidget* AnimationEditor::make_eraser_tool(){
	return make_tool(
		":/images/eraser_small.svg", 
		"Eraser",
		[&]{
			selection.select_none();
			editor->set(FrameEditor::drawing);
			editor->set_tool(FrameEditor::eraser);
		}
	);
}

/// Create the sketch pencil tool.
QWidget* AnimationEditor::make_sketch_pencil_tool() const{
	return make_tool(
		":/images/pencil_sketch_small.svg",
		"Sketch pencil",
		[&]{
			editor->set(FrameEditor::sketching);
			editor->set_tool(FrameEditor::pencil);
		}
	);
}

/// Create the sketch eraser tool.
QWidget* AnimationEditor::make_sketch_eraser_tool() const{
	return make_tool(
		":/images/eraser_sketch_small.svg",
		"Sketch eraser",
		[&]{
			editor->set(FrameEditor::sketching);
			editor->set_tool(FrameEditor::eraser);
		}
	);
}

/// Create the curve selector tool.
QWidget* AnimationEditor::make_curve_selector_tool() const{
	return make_tool(
		":/images/hand_point_curve_small.svg",
		"Curve selector",
		[&]{
			editor->set_tool(FrameEditor::curve_selector);
		}
	);
}

/// Create the region selector tool.
QWidget* AnimationEditor::make_region_selector_tool() const{
	return make_tool(
		":/images/hand_point_region_small.svg",
		"Region selector",
		[&]{
			editor->set_tool(FrameEditor::region_selector);
		}
	);
}

/// Create the center point edition tool.
QWidget* AnimationEditor::make_center_tool() const{
	return make_tool(
		":/images/center_icon.svg",
		"Center",
		[&]{
			editor->set_tool(FrameEditor::center_editor);
		}
	);
}

/// Create the control point edition tool.
QWidget* AnimationEditor::make_cp_editor_tool() const{
	return make_tool(
		":/images/cp_editor_icon.svg", 
		"Control point editor",
		[&]{
			editor->set_tool(FrameEditor::cp_editor);
		}
	);
}

/// Create the toolbar.
QWidget* AnimationEditor::make_toolbar(){
	auto layout = new QGridLayout{};

	////////////////////////////////////////////////////////////////////////
	layout->addWidget(make_pencil_tool(), 0, 0);
	layout->addWidget(make_eraser_tool(), 0, 1);
	layout->addWidget(make_sketch_pencil_tool(), 0, 2);
	layout->addWidget(make_sketch_eraser_tool(), 0, 3);

	////////////////////////////////////////////////////////////////////////
	layout->addWidget(make_curve_selector_tool(), 1, 0);
	layout->addWidget(make_region_selector_tool(), 1, 1);
	layout->addWidget(make_center_tool(), 1, 2);
	layout->addWidget(make_color_tool(), 1, 3);

	////////////////////////////////////////////////////////////////////////
	layout->addWidget(make_cp_editor_tool(), 2, 0);

	////////////////////////////////////////////////////////////////////////
	layout->addWidget(make_slider_smoothness(), 3, 0, 1, 4);

	auto widget = new QWidget{this};
	widget->setLayout(layout);
	widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	widget->setFixedHeight(200);
	return widget;
}

/// Create the layer editor.
QWidget* AnimationEditor::make_layer_editor(){
	layer_editor = new LayerWidget{this};
	layer_editor->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	return layer_editor;
}

/// Create the animation controls.
QWidget* AnimationEditor::make_animation_controls(){
	auto layout = new QGridLayout{};

	layout->addWidget(new QLabel{tr("Tolerance")}, 0, 0);
	layout->addWidget(make_slider_tolerance(), 0, 1);
	
	layout->addWidget(make_cyclic_control(), 1, 0, 1, 2);
	
	layout->addWidget(make_button_auto_guides(), 2, 0, 1, 2);
	
	layout->addWidget(make_button_play_animation(), 3, 0, 1, 2);

	layout->addWidget(new QLabel{tr("Frame")}, 4, 0);
	layout->addWidget(make_frame_spinbox(), 4, 1);

	auto widget = new QWidget{this};
	widget->setLayout(layout);
	widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	widget->setFixedHeight(160);
	return widget;
}

/// Create the spinbox to control frames.
QWidget* AnimationEditor::make_frame_spinbox(){
	frame_spinbox = new QSpinBox{};
	frame_spinbox->setMinimum(1);
	frame_spinbox->setMaximum(animation.n_frames());
	void (QSpinBox::* spinbox_signal)(int) = &QSpinBox::valueChanged;
	connect(
		frame_spinbox, 
		spinbox_signal,
		[&](int i){
			viewer.set_current_frame(i-1);
			editor->repaint();
			timeline->repaint();
		});
	return frame_spinbox;
}

/// Create the checkbox to control cyclic animation.
QWidget* AnimationEditor::make_cyclic_control(){
	cyclic_checkbox = new QCheckBox{tr("Cyclic animation")};
	auto signal = &QCheckBox::stateChanged;

	connect(
		cyclic_checkbox, 
		signal,
		[&](int i){
			animation.change_cyclic(i != 0);
			editor->repaint();
			timeline->repaint();
		});

	return cyclic_checkbox;
}

/// Create the slider that controls tolerance.
QWidget* AnimationEditor::make_slider_tolerance(){
	slider_tolerance = new QSlider{Qt::Horizontal};
	slider_tolerance->setGeometry(0, 10, 160, 30);
	slider_tolerance->setMinimum(0);
	slider_tolerance->setMaximum(100);
	slider_tolerance->setValue(25);
	slider_tolerance->setToolTip("Curve distance tolerance");
	connect(
		slider_tolerance, &QSlider::valueChanged,
		[&](int i){ animation.set_tolerance(i/100.0); }
	);
	return slider_tolerance;
}

/// Create the button to generate animation.
QWidget* AnimationEditor::make_button_auto_guides(){
	using namespace std::chrono;

	auto button = new QPushButton{tr("Generate animation")};
	connect(
		button, 
		&QPushButton::clicked,
		[&](){
			randomize_parameters();
			auto now = high_resolution_clock::now;
			selection.select_none();
			auto start = now();
			animation.generate_animation();
			size_t time = duration_cast<milliseconds>(now() - start).count();
			cout << "tolerance = " << animation.get_root_layer().get_tolerance() << '\n';
			cout << "Computation time: " << time << "ms \n";

			update_window();
		}
	);

	return button;
}

/// Create the button to play/stop the animation.
QWidget* AnimationEditor::make_button_play_animation(){
	auto button = new QPushButton{tr("Play/Stop animation")};
	connect(button, &QPushButton::clicked, 	[&]{ play_animation(); });
	return button;
}

/// Create the color tool.
QWidget* AnimationEditor::make_color_tool(){
	color_tool = new ColorWidget{*editor, this};
	color_tool->setFixedSize(30, 30);
	color_tool->setToolTip("Curve/Region color");
	editor->set_color_tool(color_tool);
	return color_tool;
}

/// Create the slider that controls smoothness.
QWidget* AnimationEditor::make_slider_smoothness(){
	auto slider_smoothness = new QSlider{Qt::Horizontal, this};
	slider_smoothness->setGeometry(0, 10, 160, 30);
	slider_smoothness->setMinimum(5);
	slider_smoothness->setMaximum(200);
	slider_smoothness->setValue(100);
	slider_smoothness->setToolTip("Curve smoothness");
	connect(
		slider_smoothness, 
		&QSlider::valueChanged,
		[&](int i){ editor->set_error(i); }
	);
	return slider_smoothness;
}

/// Create the application menu.
void AnimationEditor::create_menu(){
	QMenu* menu = nullptr;

	////////////////////////////////////////////////////////////////////////
	// Add a new item with label str to current menu.
	auto add_item = [&](
			const char* str,
			function<void()> method,
			const char* shortcut){

		auto action = menu->addAction(tr(str));
		connect(action, &QAction::triggered, method);

		if(shortcut != nullptr)
			action->setShortcut(QKeySequence(tr(shortcut)));
		this->addAction(action);

		return action;
	};

	////////////////////////////////////////////////////////////////////////
	// Add a checkable new item with label str to current menu.
	auto add_checkable = [&](
			const char* str,
			function<void()> method,
			const char* shortcut,
			bool checked){

		auto action = add_item(str, method, shortcut);

		action->setCheckable(true);
		action->setChecked(checked);

		return action;
	};

	////////////////////////////////////////////////////////////////////////
	menu = menuBar()->addMenu(tr("&File"));

	////////////////////////////////////////////////////////////////////////
	add_item(
		"&New animation",
		[&]{
			current_filename = "";
			setWindowTitle("DiLight");
			animation = Animation{};
			selection.select_none();
			viewer.goto_root_layer();
			editor->reset();
			playing = false;
			timer.stop();
			layer_editor->update_data();
			cyclic_checkbox->setChecked(false);
			update_window();
		}, nullptr
	);
	add_item("&Open", [&]{ read_frames(); }, nullptr);
	add_item("&Save", [&]{ save_current_file(); }, "CTRL+S");
	add_item("&Save as", [&]{ save_frames(); }, nullptr);
	//add_item("Import from SVG", [&]{ read_svg(); }, nullptr);
	add_item("Export to SVG", [&]{ save_svg(); }, nullptr);
	add_item(
		"Save animation frames",
		[&](){
			animation.save_eps("animation.eps");
		}, nullptr
	);
	//*
	add_item("Save postscript", [&]{ editor->save_eps(); }, nullptr);
	//*/
	menu->addSeparator();
	add_item("E&xit", [&]{ qApp->quit(); }, nullptr);
	////////////////////////////////////////////////////////////////////////

	menu = menuBar()->addMenu(tr("&Edit"));

	////////////////////////////////////////////////////////////////////////
	add_item(
		"Undo",
		[&]{
			editor->undo();
			update_window();
		},
		nullptr
	)->setShortcut(QKeySequence::Undo);

	add_item(
		"Redo",
		[&]{
			editor->redo();
			update_window();
		},
		"CTRL+Y"
	);
	
	add_item(
		"Make region from selected curves",
		[&]{
			make_region_from_selection();
		}, "CTRL+R"
	);

	//add_item("Read background image", [&](){ read_bg_image(); }, nullptr);

	add_item(
		"Clear interpolated frames",
		[&](){
			selection.select_none();
			animation.clear_interpolated_frames();
			update_window();
		}, nullptr
	);

	add_checkable(
		"Show drawing",
		[&]{
			editor->flip(FrameEditor::show_drawing);
		}, 
		"CTRL+D", true
	);
	
	add_checkable(
		"Show guide-lines",
		[&]{
			editor->flip(FrameEditor::show_sketch);
		}, 
		"CTRL+G", true
	);
	
	add_checkable(
		"Onion skin",
		[&]{
			editor->flip(FrameEditor::show_previous_guides);
		}, 
		"CTRL+O", true
	);

	add_checkable(
		"Show auxiliary drawing",
		[&]{
			editor->flip(FrameEditor::show_auxiliary);
		}, 
		"CTRL+A", true
	);

	/*
	add_checkable(
		"Show distance field",
		[&]{
			editor->flip(FrameEditor::show_distance_field);
		}, 
		"CTRL+F", false
	);
	
	add_checkable(
		"Show background image",
		[&]{
			editor->flip(FrameEditor::show_bg_image);
		}, 
		"CTRL+B", true
	);
	*/

	////////////////////////////////////////////////////////////////////////

	menu = menuBar()->addMenu(tr("&Guidelines"));

	////////////////////////////////////////////////////////////////////////
	add_item(
		"Copy guidelines",
		[&]{
			selection.copy_guidelines();
		}, nullptr 
	);
	add_item(
		"Paste guidelines",
		[&]{
			selection.paste_guidelines();
			update_window();
		}, nullptr
	);
	////////////////////////////////////////////////////////////////////////

	menu = menuBar()->addMenu(tr("&Layers"));

	////////////////////////////////////////////////////////////////////////
	add_item("Delete current layer", [&]{ delete_layer(); }, nullptr);
	add_item(
		"Move layer up",
		[&]{
			viewer.layer_up();
			layer_editor->update_data();
			update_window();
		}, nullptr
	);
	add_item(
		"Move layer down",
		[&]{
			viewer.layer_down();
			layer_editor->update_data();
			update_window();
		}, nullptr
	);
	
	menu = menuBar()->addMenu(tr("&Matching"));
	add_item(
		"Mark selected as bad matches",
		[&]{
			add_forbidden_pairs_from_selection();
		}, nullptr
	);

}

/// Show an error message.
void show_error_box(const string& error_message){
	QMessageBox{
		QMessageBox::Critical,
		"Error!",
		error_message.c_str(),
		QMessageBox::Ok
	}.exec();
}

/// Read frames from a file.
void AnimationEditor::read_frames(){
	QString fileName = QFileDialog::getOpenFileName(
		this, tr("Open file"), models_path,
		tr("Json files (*.json)")
	);

	if(fileName == "")
		return;

	try{
		// store path for next time
		models_path = QFileInfo{fileName}.path();
		current_filename = "";

		playing = false;

		Animation new_animation{fileName.toStdString()};

		current_filename = QFileInfo{fileName}.fileName();
		setWindowTitle("DiLight : "+current_filename);

		viewer.set_current_frame(frame_spinbox->value()-1);
		slider_tolerance->setValue(100*new_animation.get_tolerance());

		selection.select_none();
		animation = std::move(new_animation);

		viewer.goto_root_layer();

		cyclic_checkbox->setChecked(animation.is_cyclic());

		editor->reset();

		timeline->update_size();
		layer_editor->update_data();

	}catch(exception& e){
		show_error_box("Could not read file! \nError: "_s + e.what());
	}catch(...){
		show_error_box("Could not read file! \n unkown error.");
	}

	update_window();
}

/// Write frames to a file.
void AnimationEditor::save_current_file(){
	if(current_filename == "")
		save_frames();
	else{
		QFileInfo fileinfo{models_path, current_filename};
		QString filename = fileinfo.absoluteFilePath();

		ofstream out{filename.toStdString()};
		out << animation.to_json().dump();
		out.close();
	}
}

/// Write frames to a file.
void AnimationEditor::save_frames(){
	QString fileName = QFileDialog::getSaveFileName(
		this, tr("Save file"), models_path,
		tr("Json files (*.json)")
	);

	if(fileName == "")
		return;

	QFileInfo fileinfo{fileName};
	models_path = fileinfo.path(); // store path for next time

	ofstream out{fileName.toStdString()};
	out << animation.to_json().dump();
	out.close();

	current_filename = fileinfo.fileName();
	setWindowTitle("DiLight : " + current_filename);
}

/// Read drawing from svg file.
void AnimationEditor::read_svg(){
	QString fileName = QFileDialog::getOpenFileName(
		this, tr("Open file"), models_path,
		tr("SVG files (*.svg)")
	);

	if(fileName == "")
		return;

	models_path = QFileInfo{fileName}.path(); // store path for next time

	viewer.read_svg(fileName.toStdString());
}

/// Save current drawing to svg file.
void AnimationEditor::save_svg(){
	QString fileName = QFileDialog::getSaveFileName(
		this, tr("Open file"), models_path,
		tr("SVG files (*.svg)")
	);

	if(fileName == "")
		return;

	models_path = QFileInfo{fileName}.path(); // store path for next time

	//viewer.save_svg(fileName.toStdString());
	animation.save_animation_svg(fileName.toStdString());
}

/// Create a region from the selected curves.
void AnimationEditor::make_region_from_selection(){
	auto color = toColor(color_tool->get_color());
	auto bg_color = toColor(color_tool->get_bg_color());
	selection.make_region_from_selection(color, bg_color);
	update_window();
}

/// Keypress event.
void AnimationEditor::keyPressEvent(QKeyEvent* k){
	auto modif = k->modifiers();
	bool ctrl = modif == Qt::ControlModifier;
	bool shift = modif == Qt::ShiftModifier;
	bool ctrl_shift = modif == (Qt::ShiftModifier | Qt::ControlModifier);

	map<int, function<void()>> func = {
		{Qt::Key_Backspace, [&]{ selection.remove_selected(); }},
		{Qt::Key_A, [&]{
			if(shift)
				selection.select_all();
			else if(ctrl_shift)
				selection.select_none();
		}},
		{Qt::Key_C, [&]{
			if(ctrl)
				selection.copy_selection_to_clipboard();
		}},
		{Qt::Key_X, [&]{
			if(ctrl)
				selection.selection_to_clipboard();
		}},
		{Qt::Key_V, [&]{
			if(ctrl)
				selection.paste_clipboard();
		}},
		{Qt::Key_Insert, [&]{	new_layer(); }},
		{Qt::Key_Delete, [&]{	delete_layer(); }},
		{Qt::Key_Home, [&]{ viewer.up_depth(); }},
		{Qt::Key_End, [&]{ viewer.down_depth(); }},
		{Qt::Key_Plus, [&]{ selection.uplevel_selected(); }},
		{Qt::Key_Minus, [&]{ selection.downlevel_selected(); }}
	};

	auto f = func.find(k->key());

	if(f == func.end())
		return;
	(f->second)();

	update_window();
}

void AnimationEditor::closeEvent(QCloseEvent* event) {
	speed_widget.close();
}
		
/// Read background image.
void AnimationEditor::read_bg_image(){
	static QString path = "";

	QString filename = QFileDialog::getOpenFileName(
			this, tr("Open file"), path,
			tr("Image files (*.jpg *.png)"));

	if(filename == "")
		return;

	path = QFileInfo{filename}.path(); // store path for next time

	editor->load_image(filename);
}

/// Play the animation.
void AnimationEditor::play_animation(){
	constexpr double fps = 30.0;
	constexpr double time = 1000.0/fps;

	playing = !playing;

	if(playing){
		if(viewer.is_current_keyframe() == false)
			viewer.set_current_frame(animation.first_keyframe_index());


		timer.start(time);
	}else
		timer.stop();
}

/// Create a new layer.
void AnimationEditor::new_layer(){
	static int i = 0;
	auto name = "layer " + loc::to_string(i++);
	viewer.add_layer(name);
	timeline->update_size();
	layer_editor->update_data();
	update_window();
}

/// Remove current layer.
void AnimationEditor::delete_layer(){
	QMessageBox msgBox;
	msgBox.setText("Warning!");
	msgBox.setIcon(QMessageBox::Warning);
	if(viewer.current_layer_index() == 0){
		msgBox.setInformativeText("Cannot delete base layer!");
		msgBox.setStandardButtons(QMessageBox::Ok);
		msgBox.exec();
		return;
	}

	auto txt = "Are you sure you want to delete current layer?"_s;
	txt += "(Everything in this layer will be lost!)";
	msgBox.setInformativeText(txt.c_str());

	auto yes = QMessageBox::Yes;
	auto no = QMessageBox::No;

	msgBox.setStandardButtons(no | yes);
	msgBox.setDefaultButton(no);

	if(msgBox.exec() == no)
		return;

	viewer.remove_layer();
	timeline->update_size();
	layer_editor->update_data();
	update_window();
}

/// Add forbidden pairs of curves.
void AnimationEditor::add_forbidden_pairs_from_selection(){
	auto drawing = viewer.get_drawing(false);
	if(drawing == nullptr)
		return;

	size_t f1 = viewer.current_frame_index();
	
	if(f1 == 0)
		return;

	size_t f0 = f1-1;
	if(!animation.prev_keyframe(f0))
		return;

	auto& layer = viewer.current_layer();
	auto interpolator = layer.get_interpolator(f0, f1);
	if(interpolator == nullptr)
		return;

	auto make_forbidden = [&](const Drawable& d){
		if(d.type() != 1)
			return;

		const BezierPath& curve = toBezierPath(d);
		interpolator->add_forbiden(curve);

	};
	selection.apply_to_selection(make_forbidden);
	selection.select_none();
	editor->repaint();
}

/// Insert new frame at current position.
void AnimationEditor::insert_frame(){
	size_t i = viewer.current_frame_index();
	animation.insert_frame(i);
	editor->insert_frame(i);
	speed_widget.update_curve();
}

/// Insert new frame at current position.
void AnimationEditor::remove_frame(){
	size_t i = viewer.current_frame_index();
	animation.remove_frame(i);
	editor->remove_frame(i);
	speed_widget.update_curve();
}
		
/// Action done when tablet pen enters.
void AnimationEditor::tablet_enter(QTabletEvent* e){
	editor->tablet_enter(e);
}

