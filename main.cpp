#include "AnimationEditor.h"
#include "PostScriptWriter.h"
#include "bipartite_matching.h"
#include <iostream>
#include <fstream>
#include <chrono>
#include <random>
#include <functional>

using std::string;
using std::cout;
using std::cerr;
using std::atof;
using std::exception;
using std::next;

void test_animation(string filename){
	using namespace std::chrono;
	auto now = high_resolution_clock::now;

	Animation animation{filename};

	milliseconds times[5];
	for(auto& time: times){
		auto start = now();
		animation.generate_animation();
		time = duration_cast<milliseconds>(now() - start);
	}

	auto p = loc::minmax_element(times);
	cout << filename
		<< animation.status()
		<< " & "
		<< p.second->count() << "ms"
		<< "\\\\\n";

	loc::replace(filename, '.', '_');
	loc::replace(filename, '/', '_');

	animation.save_animation_svg("animation"+filename+".svg");
}


template<class It>
int arg_test(It b, It e){
	try{
		for(It i = b; i != e; ++i)
			test_animation(*i);
		return 0;
	}catch(exception& e){
		cerr << "Error! " << e.what() << '\n';
	}catch(...){
		cerr << "Error! Unknown exception\n";
	}
	return 1;
}

//! [0]
class TabletApplication : public QApplication{
	public:
		TabletApplication(int &argv, char **args)
			: QApplication(argv, args) {
				editor.show();
			}

		bool event(QEvent *event){
			switch(event->type()){
				case QEvent::TabletEnterProximity:
					editor.tablet_enter(static_cast<QTabletEvent*>(event));
					break;
				default:
					break;
			}
			return QApplication::event(event);
		}

	private:
		AnimationEditor editor;
};

int main(int argv, char** args){
	return (argv > 1)?
		arg_test(args+1, args+argv):
		TabletApplication{argv, args}.exec();
}
