#include "AnimationViewer.h"
#include "SVGReader.h"
#include "SVGWriter.h"

using std::string;
using std::cout;
using std::vector;

/// Update auxiliary curves from the last keyframe to the current keyframe.
/** 
* Or from the current to keyframe to the next.
*/
void AnimationViewer::update_auxiliary_curves(bool from_prev){
	if(!is_current_keyframe())
		return;

	size_t f0, f1;

	if(from_prev){
		// update curves from previous keyframe.
		f1 = _current_frame_index;

		if(f1 == 0)
			return;

		f0 = f1;
		if(animation.prev_keyframe(f0) == false)
			return;
	}else{
		// update curves of next keyframe.
		f0 = _current_frame_index;
		f1 = f0+1;
		if(animation.next_keyframe(f1) == false)
			return;
	}

	if(animation.is_cyclic() and f1 == animation.last_keyframe_index())
		f1 = animation.first_keyframe_index();

	// Update the morph object from fstart to fend at current layer.
	auto& root = animation.get_root_layer();
	calculate_morph(root, f0, f1);
	calculate_auxiliary_curves(root, f0, f1);
}

/// Read drawings from svg file.
void AnimationViewer::read_svg(const string& filename){
	SVGReader reader{filename};

	auto ds = reader.get_drawables();

	if(ds.empty())
		return;
	auto cf = current_frame_index();
	auto cl = current_layer_index();
	auto drawing = get_drawing(cf, cl, true);
	drawing->add_drawables(ds);
}

/// Save the current drawing into svg file.
void AnimationViewer::save_svg(const string& filename){
	auto cf = current_frame_index();
	auto cl = current_layer_index();
	auto drawing = get_drawing(cf, cl);
	if(drawing == nullptr)
		return;

	auto bb = animation.axis_aligned_bb();
	double x0 = bb[0][0];
	double y0 = bb[0][1];
	double x1 = bb[1][0];
	double y1 = bb[1][1];
	SVGWriter out{filename, x0, y0, x1-x0, y1-y0};

	for(auto& c: drawing->get_curves())
		out.create_path(*c);
}

/// Add a new layer over current layer.
void AnimationViewer::add_layer(const string& name){
	size_t l = _current_layer_index;
	auto& layer = base_layer->add_child(l);
	layer.set_name(name);
	layer.reset_drawings(animation.n_frames());

	auto pd = base_layer->get_drawing(_current_frame_index);
	if(pd == nullptr)
		return;

	auto d = layer.get_drawing(_current_frame_index);
	if(d == nullptr)
		return;

	d->set_depth(pd->depth());
}

/// Remove current layer (if it is not the last one).
void AnimationViewer::remove_layer(){
	if(n_layers() == 1)
		return;

	size_t l = _current_layer_index;
	if(l == 0){
		cout << "Can't erase base layer\n";
		return;
	}

	base_layer->remove_child(l-1);

	if(_current_layer_index == n_layers())
		--_current_layer_index;
}

/// Move the current layer up.
void AnimationViewer::layer_up(){
	auto l = _current_layer_index;

	// Protect base layer.
	if(l == 0){
		cout << "can't move base layer\n";
		return;
	}

	// Ignore if layer is the last one.
	if(l == n_layers()-1)
		return;

	// Swap layers.
	base_layer->swap_children(l-1, l);

	++_current_layer_index;
}

/// Move the current layer down.
void AnimationViewer::layer_down(){
	auto l = _current_layer_index;

	// Protect base layer.
	if(l == 0){
		cout << "can't move base layer\n";
		return;
	}

	// Ignore if layer is the first (after the base)
	if(l == 1)
		return;

	// Swap layers.
	base_layer->swap_children(l-1, l-2);

	--_current_layer_index;
}
		
/// Move the current drawing up.
void AnimationViewer::up_depth(){
	current_layer().apply(
		[&](Layer& l){
			auto d = l.get_drawing(_current_frame_index);
			d->up_depth();
		}
	);
}

/// Move the current drawing down.
void AnimationViewer::down_depth(){
	current_layer().apply(
		[&](Layer& l){
			auto d = l.get_drawing(_current_frame_index);
			d->down_depth();
		}
	);
}

/// Set the current layer as the base layer.
void AnimationViewer::set_base_layer(){
	if(base_layer == &current_layer())
		return;

	layer_stack.push(base_layer);
	base_layer = &current_layer();
	_current_layer_index = 0;
}

/// Set the base layer as the parent of the current base layer.
void AnimationViewer::goto_parent_base_layer(){
	if(!layer_stack.empty()){
		base_layer = layer_stack.top();
		layer_stack.pop();
		_current_layer_index = 0;
	}
}

/// Update the center point from current drawing.
void AnimationViewer::update_center_point(){
	if(!is_current_keyframe())
		return;

	auto drawing = get_drawing(false);
	if(drawing != nullptr)
		center_point = drawing->get_center_point();
}

/// Get a list with all visible drawable objects at frame f.
vector<const Drawing*> AnimationViewer::get_visible_drawings(
	const Node<Layer>& layer, 
	size_t f,
	const Node<Layer>* except
){
	if(&layer == except)
		return {};
	
	if(animation.is_cyclic() and f == animation.last_keyframe_index())
		f = animation.first_keyframe_index();

	vector<const Drawing*> res;
	auto d = layer.is_visible()? layer.get_drawing_const(f): nullptr;

	if(d != nullptr)
		res.push_back(d);
	
	if(layer.is_visible() or &layer == &get_base_layer())
		for(const auto& child: layer.get_children())
			loc::insert_back(res, get_visible_drawings(child, f, except));

	loc::stable_sort(
		res, 
		[&](const Drawing* d0, const Drawing* d1){
			return d0->depth() < d1->depth();
		}
	);

	return res;
}


