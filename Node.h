#ifndef NODE_H
#define NODE_H

#include <vector>
#include <memory>

/// Node in a tree diagram.
template<class T>
class Node : public T{
	public:
		/// Get the total number of children (including subchildren).
		size_t total_children() const{
			auto nc = children.size();

			for(const auto& child: children)
				nc += child.total_children();

			return nc;
		}

		/// Get list of children.
		std::vector<Node>& get_children();

		/// Get list of children.
		const std::vector<Node>& get_children() const;

		/// Number of children.
		size_t n_children() const;

		/// Get the i-th child.
		Node& child(size_t i);

		/// Add a new child.
		Node& add_child(int pos = -1);

		/// Remove child.
		void remove_child(int pos = -1);

		/// Swap children i and j.
		void swap_children(size_t i, size_t j);

		/// Apply operation to a Node and its children, including the children of the children.
		void apply(std::function<void(T&)> f);

		/// Apply operation to a Node and its children, including the children of the children (const version).
		void apply(std::function<void(const T&)> f) const;
	private:
		std::vector<Node> children;
};

/// Get list of children.
template<class T>
inline auto Node<T>::get_children() -> std::vector<Node>&{
	return children;
}

template<class T>
inline auto Node<T>::get_children() const -> const std::vector<Node>&{
	return children;
}

/// Number of children.
template<class T>
inline size_t Node<T>::n_children() const{
	return children.size();
}

/// Get the i-th child.
template<class T>
inline Node<T>& Node<T>::child(size_t i){
	return children[i];
}

/// Add a new child.
template<class T>
inline Node<T>& Node<T>::add_child(int i){
	auto pos = (i == -1)? children.end(): children.begin() + i;
	return *children.emplace(pos);
}

/// Remove child.
template<class T>
inline void Node<T>::remove_child(int i){
	auto pos = (i == -1)? children.end(): children.begin() + i;
	children.erase(pos);
}

/// Swap children i and j.
template<class T>
inline void Node<T>::swap_children(size_t i, size_t j){
	std::swap(children[i], children[j]);
}

/// Apply operation to a Node and its children, including the children of the children.
template<class T>
void Node<T>::apply(std::function<void(T&)> f){
	f(*this);
	for(auto& c: children)
		c.apply(f);
}

/// Apply operation to a Node and its children, including the children of the children.
template<class T>
void Node<T>::apply(std::function<void(const T&)> f) const{
	f(*this);
	for(auto& c: children)
		c.apply(f);
}

#endif
