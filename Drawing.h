#ifndef DRAWING_H
#define DRAWING_H

#include "BezierPath.h"
#include "Region.h"
#include "mffd.h"
#include "loc.h"
#include "History.h"
#include "DrawableSet.h"
#include <memory>
#include <array>
#include <functional>

class Drawing;

/// Set of curves, regions, and guidelines.
class Drawing : public History, public DrawableSet{
	public:

	/// Default constructor.
	Drawing() = default;

	/// Constructor from Json object.
	Drawing(const json11::Json& json);

	////////////////////////////////////////////////////////////////////////
	// Methods to get info about the frame.

	/// Check if the frame is empty.
	bool empty() const;

	/// Define the center point.
	void set_center_point(const Point& p);

	/// Get the center point.
	const Point& get_center_point();

	/// Check if center point is calculated automatically.
	bool using_auto_center() const;

	/// Define the flag indicating center point calculation.
	void set_automatic_center(bool v);

	/// List of auxiliary curves.
	const BezierPathList& get_auxiliary_curves() const;

	/// List of guidelines
	BezierPathList& get_guidelines();

	/// List of guidelines (const version).
	const BezierPathList& get_guidelines() const;

	/// Get a list with all the control points.
	PointList all_control_points() const;

	/// Get the index of control point next to position p
	std::pair<size_t, BezierPath_sp> control_point_at(
		const Point& p,
		double dist
	);

	/// Get the minimum axis aligned bounding box of all frames.
	Point2 axis_aligned_bb() const;

	////////////////////////////////////////////////////////////////////////
	// Methods to clear data.

	/// Erase the last curve of the guidelines.
	void clear_guide();

	/// Clear auxiliary curves.
	void clear_auxiliary_curves();

	/// Remove empty curves.
	void clear_empty_curves();

	/// Remove curves that cross segment pq.
	bool remove_curves_crossing(const Point& p, const Point& q);

	/// Remove guidelines that cross segment pq.
	bool remove_guidelines_crossing(const Point& p, const Point& q);

	/// Remove selected drawables.
	void remove_selected();
	
	/// Remove a curve or guideline.
	void remove_curve_or_guideline(const BezierPath_sp& curve);
	
	/// Remove all guidelines that satisfy operation op.
	bool remove_guidelines_if(std::function<bool(const BezierPath&)> op);

	////////////////////////////////////////////////////////////////////////
	// Methods to add/change data.

	/// Add a curve.
	void hist_add(const BezierPath_sp& curve);

	/// Remove a curve.
	void hist_remove(const BezierPath_sp& curve);

	/// Add a region.
	void hist_add(const Region_sp& region);

	/// Remove a region.
	void hist_remove(const Region_sp& region);

	/// Remove all curves that satisfy operation op.
	bool hist_remove_curves_if(
		std::function<bool(const BezierPath&)> op
	);

	/// Remove all drawables that satisfy operation op.
	bool hist_remove_drawables_if(std::function<bool(const Drawable&)> op);

	/// Add a set of drawables.
	void add_drawables(const DrawableList& _drawables);

	/// Add a guideline.
	void add_guideline(
		const BezierPath_sp& curve,
		bool add_guideline = true
	);

	/// Remove a guideline.
	void remove_guideline(
		const BezierPath_sp& curve,
		bool add_to_history = true
	);

	/// Add an auxiliary curve.
	void add_auxiliary_curve(const BezierPath_sp& curve);

	/// Update the index of each drawing according to
	/// its position in the drawing array.
	void update_indices();

	/// Update curve proportions.
	void update_proportions();

	/// Change the orientation of guidelines according to array of booleans.
	void change_orientation(const std::vector<bool>& fw);

	/// Permute the guidelines according to array of indices.
	void permute_guidelines(const std::vector<int>& indices);

	////////////////////////////////////////////////////////////////////////
	// Methods controlling z-ordering.
	/// Increase depth.
	void up_depth();
	
	/// Decrease depth.
	void down_depth();

	/// Get depth.
	int depth() const;
	
	/// Set depth.
	void set_depth(int d);

	////////////////////////////////////////////////////////////////////////
	// Methods controlling curves clustering.

	/// Get the cluster set of curves.
	const std::vector<BezierPathList>& get_clusters() const;

	////////////////////////////////////////////////////////////////////////
	/// Convert to Json object.
	json11::Json to_json() const;

	/// Output stream operator.
	friend std::ostream& operator<<(std::ostream& out, const Drawing& f);

	/// Input stream operator
	friend std::istream& operator>>(std::istream& in, Drawing& d);

	private:
	////////////////////////////////////////////////////////////////////////
	// Parameters.

	/// Guidelines.
	BezierPathList guidelines;

	/// Auxiliary drawing.
	BezierPathList aux_curves;

	/// Curve clustering.
	std::vector<BezierPathList> clusters;

	/// Center point.
	Point center;

	/// Indicate if center point is user defined or automatically calculated.
	bool automatic_center = true;

	/// Z-index of drawing
	int z_index = 0;
};

////////////////////////////////////////////////////////////////////////////////
// Methods to get info about the frame.

/// Define the center point.
inline void Drawing::set_center_point(const Point& p){
	automatic_center = false;
	center = p;
}

/// Check if the frame is empty.
inline bool Drawing::empty() const{
	return drawables.empty() and guidelines.empty();
}
	
/// Define the flag indicating center point calculation.
inline void Drawing::set_automatic_center(bool v){
	automatic_center = v;
}

/// Check if center point is calculated automatically.
inline bool Drawing::using_auto_center() const{
	return automatic_center;
}
/// List of auxiliary curves.
inline const BezierPathList& Drawing::get_auxiliary_curves() const{
	return aux_curves;
}

/// List of guidelines.
inline BezierPathList& Drawing::get_guidelines(){
	return guidelines;
}

/// List of guidelines (const version).
inline const BezierPathList& Drawing::get_guidelines() const{
	return guidelines;
}

////////////////////////////////////////////////////////////////////////////////
// Methods to clear data.

/// Erase the last curve of the guidelines.
inline void Drawing::clear_guide(){
	if(guidelines.empty())
		return;

	guidelines.pop_back();
}

inline void Drawing::clear_auxiliary_curves(){
	aux_curves.clear();
}

////////////////////////////////////////////////////////////////////////////////
// Methods to add/change data.

/// Add an auxiliary curve.
inline void Drawing::add_auxiliary_curve(const BezierPath_sp& curve){
	aux_curves.push_back(curve);
}

/// Get the cluster set of curves.
inline const std::vector<BezierPathList>& Drawing::get_clusters() const{
	return clusters;
}

////////////////////////////////////////////////////////////////////////
// Methods controlling z-ordering.
/// Increase depth.
inline void Drawing::up_depth(){
	++z_index;
}

/// Decrease depth.
inline void Drawing::down_depth(){
	--z_index;
}

/// Get depth.
inline int Drawing::depth() const{
	return z_index;
}
	
/// Set depth.
inline void Drawing::set_depth(int d){
	z_index = d;
}


#endif
