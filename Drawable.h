#ifndef DRAWABLE_H
#define DRAWABLE_H

#include <memory>
#include <array>
#include "Point.h"

class Drawable;

/// Drawable shared pointer.
using Drawable_sp = std::shared_ptr<Drawable>;

/// Important! Don't use std::unique_ptr<Drawable>, for it doesn't have virtual destructor..

using DrawableList = std::vector<Drawable_sp>;

using Color = std::array<int, 3>;

inline Color operator+(const Color& c1, const Color& c2){
	return {c1[0] + c2[0], c1[1] + c2[1], c1[2] + c2[2]};
}

inline Color operator*(double v, const Color& c){
	int r = v*c[0];
	int g = v*c[1];
	int b = v*c[2];
	return {r, g, b};
}

/// An object that can be drawed in Qt.
class Drawable{
	public:
		/// Constructor.
		Drawable(int type_index);

		/// Type of the drawing.
		int type() const;

		/// Index of the drawing.
		int get_index() const;

		/// Index of the drawing.
		int& get_index();

		/// Set the index.
		void set_index(int index);

		/// Increment the index.
		virtual void inc_index();

		/// Decrement the index.
		virtual void dec_index();

		/// Get selection status.
		bool is_selected() const;

		/// Select the object.
		virtual void select();

		/// Deselect the object.
		virtual void deselect();

		/// Change the selection state.
		void change_selection();

		/// Change the color.
		void set_color(const Color& color);

		/// Get the color.
		const Color& get_color() const;

		/// Check if the drawing is close to point p.
		virtual bool is_next_to(const Point& p, double scale = 1.0);

		/// Get the parent drawing.
		Drawable* get_parent();

		/// Get the parent drawing (conts version).
		const Drawable* get_parent() const;

		/// Set the parent drawing.
		void set_parent(Drawable* d);

		/// Get the visibility status.
		bool is_visible() const;

		/// Set visibility.
		void set_visibility(bool v = true);

	protected:
		/// Color of the object.
		Color color = {{0, 0, 0}};

		/// Indicate if object is visible.
		bool visible = true;

		/// Index of the object.
		int index = 0;

		/// Index of the type.
		int type_index;

		/// Indicate if object is selected.
		bool selected = false;

		/// Parent drawable.
		Drawable* parent = nullptr;

};


////////////////////////////////////////////////
// Implementation of inline methods.

/// Constructor.
inline Drawable::Drawable(int _type_index):
	type_index{_type_index}
{}

/// Type of the drawing.
inline int Drawable::type() const{
	return type_index;
}

/// Index of the drawing.
inline int Drawable::get_index() const{
	return index;
}

/// Index of the drawing.
inline int& Drawable::get_index(){
	return index;
}

/// Set the index.
inline void Drawable::set_index(int _index){
	index = _index;
}

/// Increment the index.
inline void Drawable::inc_index(){
	++index;
}

/// Decrement the index.
inline void Drawable::dec_index(){
	--index;
}

/// Get selection status.
inline bool Drawable::is_selected() const{
	return selected;
}

/// Select the object.
inline void Drawable::select(){
	selected = true;
}

/// Deselect the object.
inline void Drawable::deselect(){
	selected = false;
}

/// Change the selection state.
inline void Drawable::change_selection(){
	selected = !selected;
}

/// Change the color.
inline void Drawable::set_color(const Color& c){
	color = c;
}

/// Return the color.
inline const Color& Drawable::get_color() const{
	return color;
}

/// Check if the drawing is close to point p.
inline bool Drawable::is_next_to(const Point& p, double scale){
	return false;
}

/// Get the parent drawing.
inline Drawable* Drawable::get_parent(){
	return parent;
}

/// Get the parent drawing (conts version).
inline const Drawable* Drawable::get_parent() const{
	return parent;
}

/// Set the parent drawing.
inline void Drawable::set_parent(Drawable* d){
	parent = d;
}

/// Get the visibility status.
inline bool Drawable::is_visible() const{
	return visible;
}

/// Set visibility.
inline void Drawable::set_visibility(bool v){
	visible = v;
}

#endif
