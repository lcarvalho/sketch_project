#include "Interpolator.h"
#include "mffd.h"
#include "CombMorph.h"
#include "SimilarityMorph.h"
#include "loc.h"
#include "bipartite_matching.h"
#include "RegionMixer.h"
#include "guideline_operations.h"
#include "PostScriptWriter.h"
#include "FeatureMorph.h"

#include <unordered_map>
#include <fstream>
#include <algorithm>
#include <future>

using std::tuple;
using std::make_tuple;
using std::make_pair;
using std::vector;
using std::make_shared;
using std::min;
using std::max;
using std::round;
using std::pair;
using std::exception;
using std::unordered_map;
using std::copy_if;
using std::get;
using std::cerr;
using std::set;

using vector_int2 = loc::vector_pair<int>;
template<class T1, class T2>
using pair_vector = pair<vector<T1>, vector<T2>>;

/// Constructor.
Interpolator::Interpolator(Drawing& _d0, Drawing& _d1) :
	d0(_d0), d1(_d1)
{}

vector<bool> improve_orientations(const vector<PointList>& curves0, vector<PointList>& curves1){
	vector<bool> res(curves1.size());
	for(size_t i = 0; i < curves0.size(); ++i)
		res[i] = is_forward(curves0[i], curves1[i]);

	return res;
}

loc::vector_pair<PointList> join_vectors(vector<PointList>& c0, vector<PointList>& c1){
	size_t n = std::min(c0.size(), c1.size());
	
	loc::vector_pair<PointList> result;
	result.reserve(n);

	for(size_t i = 0; i < n; ++i)
		result.emplace_back(move(c0[i]), move(c1[i]));

	return result;
}

/// Get all points from a uniform sample of each guideline from drawing d.
vector<Interpolator::SampleInfo> Interpolator::get_sampled_guidelines(){
	// Guidelines from drawings.
	auto& g0 = d0.get_guidelines();
	auto& g1 = d1.get_guidelines();

	// Number of guidelines used.
	int nm = matching_guidelines.size();

	if(nm == 0)
		return {};

	for(auto& g: g0)
		g->update_proportions();

	for(auto& g: g1)
		g->update_proportions();

	// Get sample points from guidelines.
	vector<SampleInfo> res;

	int k = 0;
	for(const auto& gs: matching_guidelines){
		auto s = sample_curves(*g0.at(gs.first), *g1.at(gs.second), 20);
		
		if(!guidelines_orientations[k])
			loc::reverse(s.second);

		res.push_back(
			{
				move(s.first), 
				move(s.second),
				gs.first,
				gs.second
			}
		);
		k += 1;
	}

	return res;
}

void Interpolator::update_sampled_guidelines(
	double w,
	vector<SampleInfo>& samples
){
	// Number of guidelines used.
	int nm = samples.size();

	if(nm == 0)
		return;
	
	// Guidelines from drawings.
	auto& g0 = d0.get_guidelines();
	auto& g1 = d1.get_guidelines();

	int k = 0;
	for(auto& gs: samples){
		auto& ca = *g0.at(gs.index0);
		auto& cb = *g1.at(gs.index1);
		size_t nc = max(ca.n_cubics(), cb.n_cubics());

		double W = max(length(gs.pts0), length(gs.pts1))/(w*nc);

		auto s = sample_curves(ca, cb, W);

		if(!guidelines_orientations[k])
			loc::reverse(s.second);

		gs.pts0 = move(s.first);
		gs.pts1 = move(s.second);
		++k;
	}
}

/// Remove all pairs from ca where the first point is too close to first curve in cb.
void remove_crossings(Interpolator::SampleInfo& ca, Interpolator::SampleInfo& cb){
	double tol = 20.0;
	PointList new_pts0, new_pts1;
	for(size_t ka = 0; ka < ca.pts0.size(); ++ka){
		Point pa = ca.pts0[ka];
		auto too_close = [&](const Point& pb){
			double d = dist(pa, pb);
			return d < tol;
		};
		auto it = loc::find_if(cb.pts0, too_close);
		if(it != cb.pts0.end())
			continue;

		new_pts0.push_back(pa);
		new_pts1.push_back(ca.pts1[ka]);
	}
	ca.pts0 = move(new_pts0);
	ca.pts1 = move(new_pts1);
}
	
auto join_points(const vector<Interpolator::SampleInfo>& pairs){
	PointList pts0, pts1;
	for(auto& p: pairs){
		loc::insert_back(pts0, p.pts0);
		loc::insert_back(pts1, p.pts1);
	}
	return make_pair(pts0, pts1);
}

/// Calculate the morph from d0 to d1.
SimilarityMorph_sp Interpolator::calculate_morph(
	const SimilarityMorph_sp& base_morph
){
	// Reset similarity morph.
	S = {};

	auto pairs = get_sampled_guidelines();
	
	PointList pts0, pts1;
	tie(pts0, pts1) = join_points(pairs);

	if(pts0.empty() or pts1.empty()){
		auto ret = base_morph? base_morph: loc::shared(S);
		morph = ret;
		return ret;
	}
	
	/////////////////////////////////////////////

	if(base_morph != nullptr)
		base_morph->transform(pts0);

	S.get_morph(pts0, pts1);
	
	S.transform(pts0);

	auto S1 = base_morph == nullptr? S: S*(*base_morph);
	
	/////////////////////////////////////////////
	if(mffd == nullptr)
		mffd = make_shared<Mffd>();

	auto domain = increase(1.0, axis_aligned_bounding_box(pts0, pts1));
	double dim = std::min(domain[1][0] - domain[0][0], domain[1][1] - domain[0][1]);
	double w = dim/mffd->max_width();
	update_sampled_guidelines(w, pairs);
	tie(pts0, pts1) = join_points(pairs);
	S1.transform(pts0);

	mffd->get_morph(pts0, pts1);
	
	mffd->transform(pts0);

	/////////////////////////////////////////////

	auto SB = loc::shared(S1);
	morph = make_shared<CombMorph>(SB, mffd);
	return SB;
}

/// Calculate the morphed curves from drawing d0;
void Interpolator::calculate_auxiliary_curves(){
	d1.clear_auxiliary_curves();

	for(auto& d: d0.get_curves()){
		if(!d->is_visible())
			continue;

		d1.add_auxiliary_curve(loc::shared(morphed_curve(*d)));
	}
}

/// Create a new curve applying morph to original control points.
BezierPath Interpolator::morphed_curve(const BezierPath& curve){
	if(morph == nullptr)
		return curve;

	return curve.morph(*morph);
}

void Interpolator::correspondences(
	const BezierPathList& curves1,
	const BezierPathList& curves2,
	double tolerance
){
	auto& result = inbetween->corresp;

	auto n1 = curves1.size();
	auto n2 = curves2.size();

	if(n1 == 0 or n2 == 0)
		return;
	
	auto forbidden_indices = get_forbidden_indices();
	auto is_forbidden = [&](size_t i, size_t j){
		return forbidden_indices.find({i, j}) != forbidden_indices.end();
	};

	auto df = calculate_distances(curves1, curves2);

	auto dist_info = [&](size_t i, size_t j) -> DistanceInfo&{
		BezierPair p{curves1[i].get(), curves2[j].get()};
		return df[p];
	};

	auto get_dist = [&](const pair<BezierPair, DistanceInfo>& elem){
		return elem.second.dist;
	};

	double max_dist = get_dist(*loc::max_val_element(df, get_dist));

	double cut_point = (n1 > 2 and n2 > 2)? tolerance*max_dist: max_dist;

	// Bipartite graph.
	bipartite_matching::BipartiteGraph graph{n1, n2};

	// Create each edge, its weight is calculated as
	// the distance between curves.
	for(auto i = 0u; i < n1; ++i)
		for(auto j = 0u; j < n2; ++j){
			if(is_forbidden(i, j))
				continue;

			auto& d = dist_info(i,j);
			if(d.dist <= cut_point)
				//graph.add_edge(i, j, exp(-pow(d.dist, 2)/1e5));
				//graph.add_edge(i, j, exp(-d.dist/1e5));
				graph.add_edge(i, j, -d.dist);
		}

	// Calculate the maximum weight bipartite matching.
	auto bimatch = graph.maximum_weight_matching();

	// Reserve space to avoid reallocations.
	result.reserve(result.size() + bimatch.size());

	// Add the matching pairs to the result.
	for(auto& p: bimatch){
		int i = p.first;
		int j = p.second;

		auto& d = dist_info(i, j);

		result.push_back({i, j, d.fw});

		inbetween->curve_mixers.push_back(
			make_shared<CurveMixer>(
				inbetween->sc0[i],
				inbetween->sc1[j],
				d.fw
			)
		);
	}
}

/// Set up inbetween information.
bool Interpolator::get_inbetween_info(double tolerance){
	// Make sure morphs and auxiliary curves are updated.
	if(morph == nullptr)
		return false;

	calculate_auxiliary_curves();

	// Update the index of each drawing according to its
	// position in the drawing array.
	d0.update_indices();
	d1.update_indices();

	inbetween = loc::make_unique<InbetweenInfo>();

	// Subcurves from frame 0.
	auto& sc0 = inbetween->sc0 = d0.get_curves();

	// Subcurves from frame 1.
	auto& sc1 = inbetween->sc1 = d1.get_curves();

	// Morphed curves from frame 0.
	Hsc0.clear();
	Hsc0.reserve(sc0.size());

	for(auto& c: sc0)
		Hsc0.push_back(loc::shared(morphed_curve(*c)));

	// Calculate correspondences between curves.
	correspondences(Hsc0, sc1, tolerance);

	// Sort curve mixers according to the index of their curves.
	sort_curve_mixers();

	auto& corresp = inbetween->corresp;

	CurvePairList curve_corresp;
	curve_corresp.reserve(corresp.size());
	for(auto& p: corresp)
		curve_corresp.emplace_back(sc0[p.d0], sc1[p.d1]);

	// Create mixers for guidelines.
	auto& g0 = d0.get_guidelines();
	auto& g1 = d1.get_guidelines();
	size_t k = 0;
	for(auto p: matching_guidelines){
		inbetween->guideline_mixers.push_back(
			std::make_shared<CurveMixer>(
				g0[p.first], 
				g1[p.second], 
				guidelines_orientations[k++]
			)
		);
	}

	// Calculate correspondences between regions.
	get_region_correspondences(curve_corresp);

	return true;
}

/// Mix objects with parameter alpha.
loc::pair_of<SimilarityMorph> Interpolator::mix_objects(
		Drawing& drawing,
		double alpha,
		const SimilarityMorph& Sp,
		const SimilarityMorph& Spia
	){

	// clear drawing.
	drawing = {};

	Point SpC0 = Sp.morph(d0.get_center_point());
	Point SpC1 = d1.get_center_point();

	auto Sa = S.intermediate(alpha, SpC0);
	auto Sia = S.inverse().intermediate(1.0-alpha, SpC1);

	auto SaSp = Sa*Sp;

	// Avoid operation if there is no inbetween data.
	if(inbetween != nullptr){
		for(auto& mixer: inbetween->curve_mixers)
			drawing.hist_add(mixer->curve_at(alpha, SaSp, Sia, Spia));
		
		for(auto& mixer: inbetween->guideline_mixers)
			drawing.add_guideline(mixer->curve_at(alpha, SaSp, Sia, Spia));

		for(auto& r: inbetween->region_mixers)
			drawing.hist_add(r->region_at(alpha, SaSp, Sia, Spia));
	}

	// Sort drawables to keep consistent ordering.
	drawing.sort();

	int depth = round((1.0-alpha)*d0.depth() + alpha*d1.depth());

	drawing.set_depth(depth);

	return {S*Sp, Spia*Sia};
}

/// Create a map of curves from bb2 to bb1, according to corresp.
template<class List>
unordered_map<BezierPath*, BezierPath*> get_map(
	const List& bbs2,
	const CurvePairList& corresp
){
	using Curve = BezierPath*;
	loc::vector_pair<Curve> pairs(bbs2.size());
	loc::pair_of<Curve> empty_pair;
	auto end = corresp.end();
	loc::transform(
		bbs2, pairs, 
		[&](const BezierBool& bb){
			auto c = bb.curve.get();
			auto it = loc::find_if(
				corresp,
				[&](const CurvePair& p){
					return p.second.get() == c;
				}
			);
			return it==end? empty_pair: make_pair(it->first.get(), c);
		}
	);
	return {pairs.begin(), pairs.end()};
}
/// Mix two regions
RegionMixer_up Interpolator::region_mixer(
	const Region& region1,
	const Region& region2,
	const CurvePairList& curve_corresp
){
	try{
		const auto& bbs1 = region1.get_curves();
		const auto& bbs2 = region2.get_curves();

		auto Map = get_map(bbs2, curve_corresp);

		if(Map.empty())
			return nullptr;

		vector_int2 pairs;

		pairs.reserve(bbs1.size());

		for(auto it1 = bbs1.begin(), e1 = bbs1.end(); it1 != e1; ++it1){
			const auto& curve1 = it1->curve.get();
			const auto& curve2 = Map[curve1];

			if(curve2 == nullptr)
				continue;

			auto it2 = loc::find(bbs2, curve2);

			pairs.emplace_back(
					it1 - bbs1.begin(),
					it2 - bbs2.begin()
					);
		}

		if(pairs.size() == 0)
			return nullptr;

		return loc::make_unique<RegionMixer>(
			region1,
			region2,
			pairs,
			*morph,
			inbetween->curve_mixers
		);
	}catch(exception& e){
		cerr << "Error during creation of region_mixer: ";
		cerr << e.what() << '\n';
		return nullptr;
	}catch(...){
		cerr << "Unknown error during creation of region_mixer\n";
		return nullptr;
	}
}

/// Find the correspondences between regions using the curve correspondences.
void Interpolator::get_region_correspondences(
		const CurvePairList& curve_corresp
	){

	auto& r_mixers = inbetween->region_mixers;

	auto ccb = curve_corresp.begin();
	auto cce = curve_corresp.end();

	const auto& r1 = d0.get_regions();
	const auto& r2 = d1.get_regions();

	auto n1 = r1.size();
	auto n2 = r2.size();

	bipartite_matching::BipartiteGraph graph{n1, n2};

	unordered_map<int, RegionMixer_up> mixers;

	////////////////////////////////////////////////////////////
	int i1 = -1;
	for(const auto& region1: r1){
		++i1;

		////////////////////////////////////////////////////////////
		const auto& curves1 = region1->get_curves();
		auto c1end = curves1.end();

		// Function object to locate curves from region 1.
		auto locate_value = [&](const CurvePair& curves){
			return c1end != 
				loc::find_if(
					curves1,
					[&](const BezierBool& bb){
						return bb.curve == curves.first;
					}
				);
		};

		// Correspondences whose first curve is from region r1.
		CurvePairList corresp_r1;
		corresp_r1.reserve(curve_corresp.size());

		copy_if(ccb, cce, back_inserter(corresp_r1), locate_value);

		if(corresp_r1.empty())
			continue;

		int i2 = -1;
		for(const auto& region2: r2){
			++i2;
			auto r = region_mixer(*region1, *region2, corresp_r1);
			if(r == nullptr)
				continue;

			double weight = r->distance(*morph);

			graph.add_edge(i1, i2, -weight);

			mixers[i1 + n1*i2] = move(r);
		}
	}

	// Calculate the maximum weight bipartite matching.
	auto bimatch = graph.maximum_weight_matching();

	r_mixers.reserve(bimatch.size());

	// Add the matching pairs to the result.
	for(auto& p: bimatch){
		int i1 = p.first;
		int i2 = p.second;
		r_mixers.push_back(move(mixers[i1 + n1*i2]));
	}
}

/// Sort curve mixers according to the index of their curves.
void Interpolator::sort_curve_mixers(){
	loc::sort(
		inbetween->curve_mixers,
		[&](const CurveMixer_sp& c0, const CurveMixer_sp& c1){
			return
				c0->get_c0().get_index() <
				c1->get_c0().get_index();
		}
	);
}

/// Rearrange guidelines to make the best matching.
void Interpolator::match_guidelines(){
	// Get guidelines.
	const auto& guidelines0 = d0.get_guidelines();
	const auto& guidelines1 = d1.get_guidelines();

	if(guidelines0.empty() or guidelines1.empty()){
		matching_guidelines.clear();
		return;
	}

	// Get normalized sample points from guidelines.
	auto Tguidelines0 = normalized_points(guidelines0);
	auto Tguidelines1 = normalized_points(guidelines1);
	
	bipartite_matching::Edges min_bimatch;
	CostInfo min_cost_info{loc::infinity<double>(), {}, {}};
	double& min_cost = get<0>(min_cost_info);
	int min_rot = 0;

	// Make the scale of the guidelines similar.
	normalize_scale(Tguidelines0, Tguidelines1);
	
	for(auto& c: Tguidelines0)
		c->update_proportions();
	
	for(auto& c: Tguidelines1)
		c->update_proportions();

	// For each rotation angle.
	for(int rot = 0; rot < 8; ++rot){
		// Get the best match of points.
		auto bimatch = best_match(Tguidelines0, Tguidelines1);

		// Get the cost information about this match.
		auto cost_info = get_cost(bimatch, Tguidelines0, Tguidelines1);

		// Keep if it is the minimum so far.
		if(get<0>(cost_info) < min_cost){
			min_cost_info = move(cost_info);
			min_bimatch = move(bimatch);
			min_rot = rot;
		}

		// Rotate 45 degrees for next iteration.
		rotate_45degrees(Tguidelines0);
		for(auto& c: Tguidelines0)
			c->update_proportions();
	}

	matching_guidelines = move(min_bimatch);
	
	for(int r = 1; r <= min_rot; ++r)
		rotate_45degrees(Tguidelines0);
	for(auto& c: Tguidelines0)
		c->update_proportions();

	vector<PointList> curves0, curves1;

	for(const auto& gs: matching_guidelines){
		auto s = sample_curves(*Tguidelines0.at(gs.first), *Tguidelines1.at(gs.second), 20);
		curves0.push_back(move(s.first));
		curves1.push_back(move(s.second));
	}
	guidelines_orientations = improve_orientations(curves0, curves1);
}

/// Recreate vector of forbidden curves removing curves that don't exist anymore.
void Interpolator::update_forbidden_curves(){
	loc::remove_if_and_erase(
		forbidden_curves, 
		[](const loc::pair_of<BezierPath_wp>& p){
			return p.first.expired() or p.second.expired();
		}
	);
}

/// Get the set of forbidden pairs of curves.
set<loc::pair_of<int>> Interpolator::get_forbidden_indices(){
	update_forbidden_curves();

	const auto& curves0 = d0.get_curves();
	const auto& curves1 = d1.get_curves();

	auto index_of = [](const BezierPathList& curves, const BezierPath_wp& curve){
		auto it = loc::find(curves, curve.lock());
		return (it == curves.end())? -1: it - curves.begin();
	};

	set<loc::pair_of<int>> forbidden_indices;

	for(auto& p: forbidden_curves){
		int i = index_of(curves0, p.first);		
		if(i == -1)
			continue;

		int j = index_of(curves1, p.second);		
		if(j == -1)
			continue;

		forbidden_indices.emplace(i, j);
	};

	return forbidden_indices;
}

/// Add forbidden pair of curves, where second curve is b.
void Interpolator::add_forbiden(const BezierPath& b){
	if(inbetween == nullptr)
		return;

	const auto& curves1 = d1.get_curves();

	auto it1 = loc::find_if(
		curves1, 
		[&](const BezierPath_sp& c){
			return c.get() == &b;
		}
	);
	if(it1 == curves1.end())
		return;

	const BezierPath_sp& curve_b = *it1;

	auto it = loc::find_if(
		inbetween->corresp, 
		[&](const CorrespInfo& c){
			return curves1[c.d1] == curve_b;
		}
	);

	if(it == inbetween->corresp.end())
		return;

	const auto& curves0 = d0.get_curves();

	forbidden_curves.emplace_back(curves0[it->d0], curve_b);
}
