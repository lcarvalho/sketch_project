#include "Drawing.h"
#include "loc.h"
#include "DrawingOperation.h"

#include <iostream>
#include <set>
#include <limits>
#include <unordered_map>

using std::pair;
using std::vector;
using std::static_pointer_cast;
using std::ostream;
using std::istream;
using std::set;
using std::function;
using std::string;
using std::logic_error;
using std::make_shared;

// Get the center point.
const Point& Drawing::get_center_point(){
	if(automatic_center){
		PointList pts;
		for(const auto& c: get_curves())
			loc::insert_back(pts, c->sample(10));
		center = !pts.empty()? loc::average(pts): Point{};
	}
	return center;
}

/// Get a list with all the control points.
PointList Drawing::all_control_points() const{
	PointList pts;

	auto add_pts = [&](const BezierPath_sp& path){
		loc::insert_back(pts, path->control_points());
	};

	for(const auto& d: drawables){
		switch(d->type()){
			case 1:
				add_pts(toBezierPath_s(d));
				break;
			case 2:
				for(const auto& c: toRegion_s(d)->get_curves())
					add_pts(c.curve);
				break;
			default: break;
		}
	}

	for(auto curve_list: {&guidelines, &aux_curves})
		for(const auto& curve: *curve_list)
			add_pts(curve);

	return pts;
}

/// Get the index of control point next to position p
pair<size_t, BezierPath_sp> Drawing::control_point_at(const Point& p, double dist){
	auto is_close = [&](const Point& q){
		return fabs(p[0] - q[0]) < dist and fabs(p[1] - q[1]) < dist;
	};

	auto curves = get_curves();

	for(auto curve_list: {&curves, &guidelines})
		for(auto& curve: *curve_list){
			auto& cpts = curve->control_points();
			auto it = loc::find_if(cpts, is_close);
			if(it != end(cpts))
				return {it-begin(cpts), curve};
		}

	return {0, nullptr};
}

/// Get the minimum axis aligned bounding box of all frames.
Point2 Drawing::axis_aligned_bb() const{
	auto pts = all_control_points();

	if(pts.empty())
		// Create fake bb.
		return {Point{0.0, 0.0}, Point{1.0, 1.0}};

	return axis_aligned_bounding_box(pts);
}

/// Remove empty curves.
void Drawing::clear_empty_curves(){
	auto is_empty = [&](const BezierPath& path){
		return path.empty();
	};

	remove_curves_if(is_empty);
	remove_guidelines_if(is_empty);
}

/// Remove all guidelines that satisfy operation op.
bool Drawing::remove_guidelines_if(function<bool(const BezierPath&)> op){
	return loc::remove_if_and_erase(
		guidelines,
		[&](const BezierPath_sp& d){ 
			return op(*d);
		} 
	);
}

/// Remove curves that cross segment pq.
bool Drawing::remove_curves_crossing(const Point& p, const Point& q){
	for(auto& d: get_curves())
		d->update_proportions();
	// Remove curves crossing pq.
	return hist_remove_curves_if(
		[&](const BezierPath& path){
			return cross_segment(path.sample(10), p, q);
		}
	);
}

/// Remove curves that cross segment pq.
bool Drawing::remove_guidelines_crossing(const Point& p, const Point& q){
	// Create a RemoveCurvesOperation object.
	auto op = loc::make_unique<RemoveGuidelinesOperation>(*this);

	// Remove guidelines crossing pq.
	bool result = loc::remove_if_and_erase(
		guidelines,
		[&](BezierPath_sp& path){
			path->sample(10);
			return path->cross_segment(p, q);
		}
	);
	
	if(result)
		// Insert operation into history.
		add_op(move(op));

	return result;
}

/// Remove selected drawables.
void Drawing::remove_selected(){
	// Remove selected drawables.
	hist_remove_drawables_if(
		[&](const Drawable& d){
			if(!d.is_selected())
				return false;

			return true;
		}
	);
}
	
/// Remove a curve or guideline.
void Drawing::remove_curve_or_guideline(const BezierPath_sp& curve){
	// Remove curve from guidelines list.
	auto it = loc::find(guidelines, curve);
	if(it != guidelines.end()){
		guidelines.erase(it);
		return;
	}

	// Remove other occurrences of curve.
	remove_all(curve);

}

/// Update the index of each drawing according to its position in the drawing array.
void Drawing::update_indices(){
	// Set temporary index -1 to curves.
	for(auto& c: get_curves())
		c->set_index(-1);

	// Set index i for each region
	int i = 0;
	for(auto& d: drawables){
		d->set_index(i++);

		if(d->type() == 2)
			for(auto& c: toRegion(d)->get_curves())
				if(c.curve->get_index() == -1)
					c.curve->set_index(i++);
	}
}

/// Change the orientation of guidelines according to array of booleans.
void Drawing::change_orientation(const vector<bool>& fw){
	int n = fw.size();
	for(int i = 0; i < n; ++i)
		if(!fw[i])
			loc::reverse(guidelines[i]->control_points());
}

/// Permute the guidelines according to array of indices.
void Drawing::permute_guidelines(const vector<int>& indices){
	vector<BezierPath_sp> new_guidelines;
	new_guidelines.reserve(guidelines.size());

	for(const int& i:  indices)
		new_guidelines.emplace_back(move(guidelines[i]));

	for(auto& g: guidelines)
		if(g != nullptr)
			new_guidelines.emplace_back(move(g));

	guidelines = move(new_guidelines);
}

/// Add a curve.
void Drawing::hist_add(const BezierPath_sp& curve){
	add_op(loc::make_unique<AddCurveOperation>(curve, *this));
	add(curve);
}

/// Remove a curve.
void Drawing::hist_remove(const BezierPath_sp& curve){
	add_op(loc::make_unique<RemoveCurvesOperation>(*this));
	remove(curve);
}

/// Add a region.
void Drawing::hist_add(const Region_sp& r){
	add_op(loc::make_unique<AddRegionOperation>(r, *this));
	add(r);
}

/// Remove a region.
void Drawing::hist_remove(const Region_sp& region){
	add_op(loc::make_unique<RemoveRegionsOperation>(*this));
	remove(region);
}

/// Remove all curves that satisfy operation op.
bool Drawing::hist_remove_curves_if(function<bool(const BezierPath&)> op){
	// Add removal operation to history.
	auto rem_op = loc::make_unique<RemoveDrawablesOperation>(*this);

	bool r = remove_curves_if(op);

	if(r)
		add_op(move(rem_op));

	return r;
}

bool Drawing::hist_remove_drawables_if(function<bool(const Drawable&)> op){
	// Add removal operation to history.
	auto rem_op = loc::make_unique<RemoveDrawablesOperation>(*this);

	bool r = remove_drawables_if(op);

	if(r)
		add_op(move(rem_op));

	return r;
}

/// Add a set of drawables.
void Drawing::add_drawables(const DrawableList& _drawables){
	for(const auto& d: _drawables)
		if(d->type() == 1)
			hist_add(static_pointer_cast<BezierPath>(d));
		else
			hist_add(static_pointer_cast<Region>(d));
}

/// Add a guideline .
void Drawing::add_guideline(const BezierPath_sp& curve, bool add_to_history){
	if(add_to_history)
		add_op(loc::make_unique<AddGuidelineOperation>(curve, *this));

	curve->prune(2);
	guidelines.push_back(curve);
}

/// Remove a guideline.
void Drawing::remove_guideline(const BezierPath_sp& curve, bool add_to_history){
	if(add_to_history)
		add_op(loc::make_unique<RemoveGuidelinesOperation>(*this));

	loc::remove_element(guidelines, curve);
}
	
/// Update curve proportions.
void Drawing::update_proportions(){
	auto prune_update = [](BezierPath& b){
		b.prune(0.1);
		b.update_proportions();
	};

	for(auto& d: guidelines)
		prune_update(*d);
	
	for(auto& d: aux_curves)
		prune_update(*d);

	for(auto& d: get_curves())
		prune_update(*d);

	clear_empty_curves();
}


/////////////////////////////////////////////////////////////////////////////
// Friend functions.

/// Output stream operator
ostream& operator<<(ostream& out, const Drawing& f){
	out << "center: " << f.center << '\n';

	out << "n_guidelines: " << f.guidelines.size() << '\n';
	for(auto& c: f.guidelines)
		out << *c;
	
	set<BezierPath*> curve_set;
	set<Region*> region_set;

	for(auto& d: f.drawables){
		switch(d->type()){
			case 1:
				curve_set.insert(toBezierPath(d));
				break;
			case 2:
				{
					auto region = toRegion(d);
					region_set.insert(region);

					for(auto& cd: region->get_curves())
						curve_set.insert(cd.curve.get());
				}
				break;

			default:
				break;
		}
	}

	out << "n_curves: " << curve_set.size() << "\n";
	for(auto& curve: curve_set){
		auto col = curve->get_color();
		out << "color: " << col[0] << ' ' << col[1] << ' ' << col[2] << '\n';
		out << *curve << '\n';
	}

	out << "n_regions: " << region_set.size() << "\n";
	auto cbegin = begin(curve_set);
	for(auto& region: region_set){
		auto col = region->get_color();
		out << "color: " << col[0] << ' ' << col[1] << ' ' << col[2] << '\n';

		const auto& curves = region->get_curves();
		out << "n_curves: " << curves.size() << "\n";
		for(auto& bb: curves){
			BezierPath* region_curve = bb.curve.get();
			auto it = loc::find(curve_set, region_curve);
			out << "curve: " << distance(cbegin, it);
			out << ' ' << bb.fw << '\n';
		}
	}
	
	auto rbegin = begin(region_set);

	out << "\nn_drawables: " << f.drawables.size() << "\n";
	for(const auto& d: f.drawables){
		switch(d->type()){
			case 1:
				out << "curve: " 
					<< distance(
						cbegin, 
						loc::find(curve_set, toBezierPath(d)) 
					)
					<< '\n';
				break;
			case 2:
				out << "region: " 
					<< distance(
						rbegin, 
						loc::find(region_set, toRegion(d)) 
					)
					<< '\n';
				break;
			default:
				break;
		}
	}
	return out;
}

/// Overload operator >> to read constant strings from istreams.
inline istream& operator>>(istream& in, const string& v){
	string a;
	in >> a;
	if(a != v)
		throw logic_error{
			"error:\n expected symbol: \"" + v +
			"\"\nread symbol: \"" + a + '\"'
		};
	return in;
}

/// Input stream operator
istream& operator>>(istream& in, Drawing& d){
	in >> "center:" >> d.center;

	int n_guidelines;
	in >> "n_guidelines:" >> n_guidelines;

	for(int i = 0; i < n_guidelines; ++i){
		auto path = make_shared<BezierPath>();
		in >> *path;
		d.add_guideline(path);
	}

	int n_curves;
	in >> "n_curves:" >> n_curves;
	BezierPathList curves(n_curves);
	for(auto& curve: curves){
		curve = make_shared<BezierPath>();

		Color col;
		in >> "color:" >> col[0] >> col[1] >> col[2];
		curve->set_color(col);

		in >> *curve;
	}

	int n_regions;
	in >> "n_regions:" >> n_regions;
	RegionList regions(n_regions);

	for(auto& region: regions){
		region = make_shared<Region>();

		Color col;
		in >> "color:" >> col[0] >> col[1] >> col[2];
		region->set_color(col);

		in >> "n_curves:" >> n_curves;
		for(int i = 0; i < n_curves; ++i){
			int k;
			bool fw;
			in >> "curve:" >> k >> fw;
			region->add_path(curves.at(k), fw);
		}
	}

	int n_drawables;
	in >> "n_drawables:" >> n_drawables;
	d.drawables.resize(n_drawables);
	for(auto& drawable: d.drawables){
		string type;
		int k;
		in >> type >> k;
		if(type == "curve:")
			drawable = move(curves.at(k));
		else if(type == "region:")
			drawable = move(regions.at(k));
		else
			throw logic_error{
				"Expected \"curve:\" or \"region:\", read \"" + type + "\""
			};
	}

	d.automatic_center = false;
	d.clear_empty_curves();
	
	return in;
}

/// Constructor from Json object.
Drawing::Drawing(const json11::Json& json){
	using namespace json11;
	z_index = json["z_index"].int_value();
	center = json["center"];
	automatic_center = false;
	for(auto& g: json["guidelines"].array_items())
		guidelines.push_back(
			make_shared<BezierPath>(g)
		);

	BezierPathList curves;
	for(auto& curve: json["curves"].array_items()){
		auto& j_col = curve["color"];
		auto curve_sp = make_shared<BezierPath>(curve["control_points"]);
		curve_sp->set_color(
			{
				j_col[0].int_value(),
				j_col[1].int_value(),
				j_col[2].int_value()
			}
		);
		curves.push_back(move(curve_sp));
	}
	
	RegionList regions;
	for(auto& region: json["regions"].array_items()){
		auto& j_col = region["color"];
		auto region_sp = make_shared<Region>();
		region_sp->set_color(
			{
				j_col[0].int_value(),
				j_col[1].int_value(),
				j_col[2].int_value()
			}
		);
		for(auto& curve: region["curves"].array_items()){
			int index = curve["index"].int_value();
			bool fw = curve["fw"].bool_value();
			region_sp->add_path(curves[index], fw);
		}
		regions.push_back(move(region_sp));
	}

	for(auto& drawable: json["drawables"].array_items()){
		auto type = drawable["type"].string_value();
		int index = drawable["index"].int_value();
		if(type == "curve")
			drawables.push_back(curves[index]);
		else if(type == "region")
			drawables.push_back(regions[index]);
	}
}

/// Convert to Json object.
json11::Json Drawing::to_json() const{
	using namespace json11;

	////////////////////////////////////////////////
	Json::array j_guidelines;
	for(auto& c: guidelines)
		j_guidelines.emplace_back(*c);

	////////////////////////////////////////////////
	set<BezierPath*> curve_set;
	set<Region*> region_set;

	for(auto& d: drawables){
		switch(d->type()){
			case 1:
				curve_set.insert(toBezierPath(d));
				break;
			case 2:
				{
					auto region = toRegion(d);
					region_set.insert(region);

					for(auto& cd: region->get_curves())
						curve_set.insert(cd.curve.get());
				}
				break;

			default:
				break;
		}
	}
	
	////////////////////////////////////////////////

	Json::array j_curves;
	for(auto& curve: curve_set){
		Color c = curve->get_color();
		j_curves.push_back(
			Json::object{
				{"color", Json::array{c[0], c[1], c[2]}},
				{"control_points", *curve}
			}
		);
	}
	
	////////////////////////////////////////////////

	auto cbegin = begin(curve_set);
	auto rbegin = begin(region_set);

	Json::array j_regions;
	for(auto& region: region_set){
		Color c = region->get_color();
		Json::array indices;
		for(auto& bb: region->get_curves()){
			int index = distance(
				cbegin, 
				loc::find(curve_set, bb.curve.get())
			);
			indices.push_back(
				Json::object{
					{"index", index},
					{"fw", bb.fw}
				}
			);
		}
		j_regions.push_back(
			Json::object{
				{"color", Json::array{c[0], c[1], c[2]}},
				{"curves", indices}
			}
		);
	}
	
	Json::array j_drawables;
	for(const auto& d: drawables){
		int index;
		switch(d->type()){
			case 1:
				index = distance(
					cbegin, 
					loc::find(curve_set, toBezierPath(d)) 
				);

				j_drawables.push_back(
					Json::object{
						{"type", "curve"},
						{"index", index}
					}
				);
				break;
			case 2:
				index = distance(
					rbegin, 
					loc::find(region_set, toRegion(d)) 
				);

				j_drawables.push_back(
					Json::object{
						{"type", "region"},
						{"index", index}
					}		
				);
				break;
			default:
				break;
		}
	}
	
	return json11::Json::object{
		{"z_index", z_index},
		{"center", center},
		{"guidelines", j_guidelines},
		{"curves", j_curves},
		{"regions", j_regions},
		{"drawables", j_drawables}
	};
}


