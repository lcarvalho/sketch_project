#ifndef CURVE_MIXER_H
#define CURVE_MIXER_H

#include "BezierPath.h"
#include "SimilarityMorph.h"
#include "PolygonBlender.h"

class CurveMixer;
using CurveMixer_sp = std::shared_ptr<CurveMixer>;

using int2 = loc::pair_of<int>;

/// Object used to mix two curves.
class CurveMixer{
	public:
		/// Constructor.
		CurveMixer(
			const BezierPath_sp& c0, 
			const BezierPath_sp& c1,
			bool fw0 = true, 
			bool fw1 = true
		);

		/// Constructor from BezierBool objects.
		CurveMixer(const BezierBool& bb0, const BezierBool& bb1);

		/// Get a curve at parameter alpha in [0.0, 1.0].
		BezierPath_sp curve_at(double alpha);

		/// Get a curve at parameter alpha in [0.0, 1.0].
		/// result = MR((1-alpha)*M0(c0) + alpha*M1(c1))
		BezierPath_sp curve_at(
			double alpha,
			const SimilarityMorph& M0,
			const SimilarityMorph& M1,
			const SimilarityMorph& MR = {}
		);

		/// Get the first curve.
		const BezierPath& get_c0() const;

		/// Get the second curve.
		const BezierPath& get_c1() const;

		/// Get the orientation of first curve.
		bool is_fw0() const;

		/// Get the orientation of second curve.
		bool is_fw1() const;

		/// Get the subcurves of curve i.
		const BezierPath& get_subcurves(int i) const;
		
		/// Get the curve blender object.
		PolygonBlender& get_curve_blender();

	private:
		/// Subdivide curves such that they have
		/// the same number of control points.
		void calculate_subcurves();

		/// Check if each joint should be forced to be G1 continuous.
		void check_joints();

		struct CurveInfo{
			// Curve.
			BezierBool bb;

			/// Subdivision of curve.
			BezierPath sc;
		};

		/// Get data necessary for calculate subcurves.
		BezierPath get_data(CurveInfo& info);

		////////////////////////////////////////////////////////
		// Parameters.

		/// Info about curve 0.
		CurveInfo curve0;
		
		/// Info about curve 1.
		CurveInfo curve1;

		/// Indicate which joints should be G1 continuous.
		std::vector<bool> g1_joints;

		/// Last curve calculated.
		BezierPath_sp last_curve;

		/// Last alpha value used to calculate the curve.
		double last_alpha;

		/// Polygonal curve blender.
		PolygonBlender curve_blender;
};

/// Get the first curve.
inline const BezierPath& CurveMixer::get_c0() const{
	return *curve0.bb.curve;
}

/// Get the second curve.
inline const BezierPath& CurveMixer::get_c1() const{
	return *curve1.bb.curve;
}

/// Get the orientation of first curve.
inline bool CurveMixer::is_fw0() const{
	return curve0.bb.fw;
}

/// Get the orientation of second curve.
inline bool CurveMixer::is_fw1() const{
	return curve1.bb.fw;
}
		
/// Get the subcurves of curve i.
inline const BezierPath& CurveMixer::get_subcurves(int i) const{
	return i==0? curve0.sc: curve1.sc;
}
		
/// Get the curve blender object.
inline PolygonBlender& CurveMixer::get_curve_blender(){
	return curve_blender;
}

#endif

