#ifndef POINT_H
#define POINT_H

#include <iostream>
#include <array>
#include <cmath>
#include <vector>
#include <functional>
#include "json11.hpp"
#include "loc.h"

///  A two-dimensional point.
class Point{
	public:
		/// Constructor.
		constexpr Point(double _x = 0.0, double _y = 0.0);
		
		/// Constructor from Json object.
		Point(const json11::Json& json);

		/// Convert to a Json object.
		json11::Json to_json() const;

		/// Element access operator.
		double& operator[](size_t i);

		/// Element access operator (const version).
		const double& operator[](size_t i) const;

		/// Sum of points.
		Point operator+(const Point& p1) const;

		/// Sum of points.
		Point& operator+=(const Point& p);

		/// Difference between points.
		Point operator-(const Point& p1) const;

		/// Difference between points.
		Point& operator-=(const Point& p);

		/// Multiply point by scalar.
		Point& operator*=(double v);

		/// Multiply point by scalar.
		friend Point operator*(double v, const Point& p);

		/// Euclidean norm.
		double norm() const;

		/// Squared Euclidean norm.
		double norm2() const;

		/// Dot product between points.
		double operator*(const Point& p) const;

		/// Scale point to make its length equal to one.
		void unitize();

		/// Get the perpendicular vector.
		Point perp() const;

		/// Get the x coordinate of point.
		friend const double& get_x(const Point& p);

		/// Get the y coordinate of point.
		friend const double& get_y(const Point& p);

		/// Distance between points
		friend double dist(const Point& p1, const Point& p2);

		/// Squared distance between points.
		friend double dist2(const Point& p1, const Point& p2);

		/// Comparison operator.
		friend bool operator==(const Point& p, const Point& q);
		
		/// Comparison operator.
		friend bool operator!=(const Point& p, const Point& q);

		/// Output stream operator.
		friend std::ostream& operator<<(std::ostream& out, const Point& p);

		/// Input stream operator.
		friend std::istream& operator>>(std::istream& in, Point& p);

		/// Get a transformed point.
		Point transform(std::function<double(double)> f) const;

		/// Inner product between points.
		friend double dot(const Point& p, const Point& q);
		
		/// Cross product between points (z-component)
		friend double cross(const Point& p, const Point& q);

	private:
		/// x-coordinate.
		double x;

		/// y-coordinate.
		double y;
};

/// A pair of points.
using Point2 = std::array<Point, 2>;

/// An array of 3 points.
using Point3 = std::array<Point, 3>;

/// An array of 4 points.
using Point4 = std::array<Point, 4>;

/// A vector of points.
using PointList = std::vector<Point>;

/// Minimum bounding box of a set of points.
Point4 min_bounding_box(const PointList& pts);

/// Convex-hull of a set of points.
PointList convex_hull(const PointList& pts);

/// Get the Hausdorff distance between two point lists.
double hausdorff_distance(const PointList& pts1, const PointList& pts2);

/// Constructor.
inline constexpr Point::Point(double _x, double _y) : x(_x), y(_y){}
		
/// Constructor from Json object.
inline Point::Point(const json11::Json& json){
	x = json[0].number_value();
	y = json[1].number_value();
}
		
/// Convert to a Json object.
inline json11::Json Point::to_json() const{
	return json11::Json::array{x, y};
}

/// Element access operator.
inline double& Point::operator[](size_t i){
	return (i==0)? x: y;
}

/// Element access operator (const version).
inline const double& Point::operator[](size_t i) const{
	return (i==0)? x: y;
}

/// Sum of points.
inline Point Point::operator+(const Point& p1) const{
	return {x+p1.x, y+p1.y};
}

/// Sum of points.
inline Point& Point::operator+=(const Point& p){
	x += p.x;
	y += p.y;
	return *this;
}

/// Difference between points.
inline Point Point::operator-(const Point& p1) const{
	return {x-p1.x, y-p1.y};
}

/// Difference between points.
inline Point& Point::operator-=(const Point& p){
	x -= p.x;
	y -= p.y;
	return *this;
}

/// Multiply point by scalar.
inline Point& Point::operator*=(double v){
	x *= v;
	y *= v;
	return *this;
}

/// Multiply point by scalar.
inline Point operator*(double v, const Point& p){
	return {v*p.x, v*p.y};
}

/// Euclidean norm.
inline double Point::norm() const{
	return std::hypot(x, y);
}

/// Squared Euclidean norm.
inline double Point::norm2() const{
	return x*x + y*y;
}

/// Dot product between points.
inline double Point::operator*(const Point& p) const{
	return x*p.x + y*p.y;
}

/// Scale point to make its length equal to one.
inline void Point::unitize(){
	double n = norm();
	x /= n;
	y /= n;
}

/// Get the perpendicular vector.
inline Point Point::perp() const{
	return {-y, x};
}

/// Get the x coordinate of point.
inline const double& get_x(const Point& p){
	return p.x;
}

/// Get the y coordinate of point.
inline const double& get_y(const Point& p){
	return p.y;
}

/// Squared distance between points.
inline double dist2(const Point& p1, const Point& p2){
	return (p1-p2).norm2();
}

/// Distance between points.
inline double dist(const Point& p1, const Point& p2){
	return sqrt(dist2(p1, p2));
}

/// Output stream operator.
inline std::ostream& operator<<(std::ostream& out, const Point& p){
	return out << p.x << ' ' << p.y << ' ';
}

/// Input stream operator.
inline std::istream& operator>>(std::istream& in, Point& p){
	return in >> p.x >> p.y;
}

/// Calculate the length of a polygonal curve.
double length(const PointList& p);

/// Angle between two vectors.
inline double angle(const Point& v1, const Point& v2){
	double cosa = (v1*v2)/(v1.norm()*v2.norm());
	return acos(std::max(-1.0, std::min(1.0, cosa)));
}

/// Rotate a vector at an angle.
inline Point rotate(const Point& v, double angle){
	double cosa = cos(angle);
	double sina = sin(angle);

	double x = v[0];
	double y = v[1];

	return {cosa*x + sina*y, -sina*x + cosa*y};
}

/// Rotate a vector 90 degrees.
inline Point rotate90(const Point& v){
	Point rv;
	rv[0] = -v[1];
	rv[1] = v[0];
	return rv;
}

/// Bracket function.
inline double bracket(const Point& p0, const Point& p1, const Point& p2){
	auto u = p1 - p0;
	auto v = p2 - p0;

	return u[0]*v[1] - u[1]*v[0];
}

/// Check if three points are collinear.
inline bool is_collinear(const Point& p0, const Point& p1, const Point& p2){
	auto u = p1 - p0;
	auto v = p2 - p0;

        //return fabs(u[0]*v[1] - u[1]*v[0]) < 1e-3;
        return fabs(bracket(p0, p1, p2)) <= 1e-5*std::max(u.norm(), v.norm());
}

/// Check if point p is inside triangle T.
inline bool inside_triangle(const Point& p, const Point3& T){
	if(is_collinear(T[0], T[1], T[2]))
		return false;

	bool b0 = bracket(T[0], T[1], p) > 0;
	bool b1 = bracket(T[1], T[2], p) > 0;
	bool b2 = bracket(T[2], T[0], p) > 0;

	return b0==b1 and b1==b2;
}

/// Checks if segments p0-p1 and q0-q1 are crossing each other.
inline bool crossing_segments(
	const Point& p0, const Point& p1,
	const Point& q0, const Point& q1
){
	return (bracket(p0, p1, q0)*bracket(p0, p1, q1) < 0) and
		(bracket(q0, q1, p0)*bracket(q0, q1, p1) < 0);
}

/// Checks if segments p and q are crossing each other.
inline bool crossing_segments(const Point2& p, const Point2& q){
	return crossing_segments(p[0], p[1], q[0], q[1]);
}

/// Check if the curve crosses segment (xp,yp)-(xq, yq).
inline bool cross_segment(const PointList& c, const Point& p, const Point& q){
	for(auto i = 1u; i < c.size(); ++i)
		if(crossing_segments(p, q, c[i-1], c[i]))
			return true;

	return false;
}

/// Find parameter t_min of the point in the
/// line (1-t)p0 + tp1 closer to point p.
inline double project(const Point& p0, const Point& p1, const Point& p){
        Point v1 = p1 - p0;
        Point v = p - p0;
        return (v*v1)/(v1*v1);
}

/// Axis aligned bounding box of a set of points.
template<class PointContainer>
inline Point2 axis_aligned_bounding_box(const PointContainer& pts){
        auto bbx = loc::minmax_val_element(pts, get_x);
        auto bby = loc::minmax_val_element(pts, get_y);

	return {
		Point{(*bbx.first)[0], (*bby.first)[1]},
		Point{(*bbx.second)[0], (*bby.second)[1]}
	};
}

template<class PointContainer0, class PointContainer1, class... T>
inline Point2 axis_aligned_bounding_box(
	const PointContainer0& pts0, 
	const PointContainer1& pts1, 
	T&&... tail
){
	Point2 bb0 = axis_aligned_bounding_box(pts0);
	Point2 bb1 = axis_aligned_bounding_box(pts1);

	return axis_aligned_bounding_box(
		Point4{bb0[0], bb0[1], bb1[0], bb1[1]}, 
		tail...
	);
}

/// Union of two bounding boxes.
inline Point2 bounding_box_union(const Point2& bb0, const Point2& bb1){
	double minx = std::min(bb0[0][0], bb1[0][0]);
	double miny = std::min(bb0[0][1], bb1[0][1]);

	double maxx = std::max(bb0[1][0], bb1[1][0]);
	double maxy = std::max(bb0[1][1], bb1[1][1]);

	return {Point{minx, miny}, Point{maxx, maxy}};
}

/// Scale a bounding box.
inline Point2 increase(double percent, const Point2& bb){
	double x0 = bb[0][0];
	double x1 = bb[1][0];
	double dx = percent*(x1 - x0);

	double y0 = bb[0][1];
	double y1 = bb[1][1];
	double dy = percent*(y1 - y0);

	return {Point{x0 - dx, y0 - dy}, Point{x1 + dx, y1 + dy}};
}

/// Comparison operator.
inline bool operator==(const Point& p, const Point& q){
	return p[0] == q[0] and p[1] == q[1];
}

/// Comparison operator.
inline bool operator!=(const Point& p, const Point& q){
	return p[0] != q[0] or p[1] != q[1];
}
		
/// Get a transformed point.
inline Point Point::transform(std::function<double(double)> f) const{
	return {f(x), f(y)};
}
		
/// Inner product between points.
inline double dot(const Point& p, const Point& q){
	return p.x*q.x + p.y*q.y;
}
		
/// Cross product between points (z-component)
inline double cross(const Point& p, const Point& q){
	return p.x*q.y - p.y*q.x;
}

#endif
