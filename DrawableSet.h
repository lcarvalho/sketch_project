#ifndef DRAWABLE_SET_H
#define DRAWABLE_SET_H

#include "BezierPath.h"
#include "Region.h"

/// Set of Drawable objects.
class DrawableSet{
	public:
	/// Total number of drawables.
	size_t n_drawables() const;

	/// List of drawble objects
	DrawableList& get_drawables();
	
	/// List of drawble objects
	const DrawableList& get_drawables() const;

	/// List of curves.
	BezierPathList get_curves();

	/// List of regions.
	RegionList get_regions();
		
	/// Add a curve.
	void add(const BezierPath_sp& curve);

	/// Remove a curve.
	void remove(const BezierPath_sp& curve);
	
	/// Remove a curve anywhere it appears.
	void remove_all(const BezierPath_sp& curve);

	/// Remove all curves that satisfy operation op.
	bool remove_curves_if(std::function<bool(const BezierPath&)> op);
	
	/// Remove all regions that satisfy operation op.
	bool remove_regions_if(std::function<bool(const Region&)> op);

	/// Remove all drawables that satisfy operation op.
	bool remove_drawables_if(std::function<bool(const Drawable&)> op);

	/// Add a region.
	void add(const Region_sp& region);

	/// Remove a region.
	void remove(const Region_sp& region);

	/// Sort the drawable list by the index.
	void sort();

	protected:
	/// Drawable objects.
	DrawableList drawables;
};

/// Number of drawables.
inline size_t DrawableSet::n_drawables() const{
	return drawables.size();
}

/// List of drawble objects
inline DrawableList& DrawableSet::get_drawables(){
	return drawables;
}

/// List of drawble objects
inline const DrawableList& DrawableSet::get_drawables() const{
	return drawables;
}

inline BezierPathList DrawableSet::get_curves(){
	BezierPathList curves;
	curves.reserve(drawables.size());
	for(auto& d: drawables)
		if(d->type() == 1)
			curves.push_back(toBezierPath_s(d));
	return curves;
}

inline RegionList DrawableSet::get_regions(){
	RegionList regions;
	regions.reserve(drawables.size());
	for(auto& d: drawables)
		if(d->type() == 2)
			regions.push_back(toRegion_s(d));
	return regions;
}

/// Add a curve.
inline void DrawableSet::add(const BezierPath_sp& curve){
	drawables.push_back(curve);
}

/// Remove a curve.
inline void DrawableSet::remove(const BezierPath_sp& curve){
	loc::remove_element(drawables, curve);
}

/// Remove a region.
inline void DrawableSet::remove(const Region_sp& region){
	loc::remove_element(drawables, region);
}
	
#endif
